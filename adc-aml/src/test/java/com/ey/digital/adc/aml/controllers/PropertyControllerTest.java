package com.ey.digital.adc.aml.controllers;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.ey.digital.adc.aml.model.Property;
import com.ey.digital.adc.aml.sql.server.repo.PropertyRepository;
import com.ey.digital.adc.aml.ui.request.PropertyUIRequest;
import com.ey.digital.adc.core.controllers.AbstractControllerTest;
import com.ey.digital.adc.core.sql.server.utils.SqlServerUtils;

@PrepareForTest(SqlServerUtils.class)
public class PropertyControllerTest extends AbstractControllerTest{
	private final Logger _logger = LoggerFactory.getLogger(this.getClass());
	private final String URI = "/property";
	
	@Mock
	private PropertyRepository repo;
	
	@InjectMocks
	private PropertyController controller;
	
	@Override
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
		setUp(controller);
		PowerMockito.mockStatic(SqlServerUtils.class); 
	}
	
	@Test
	public void test_GetPropertiesInvalidToken() {
		try {
			String uri = URI;
//			PowerMockito.when(SqlServerUtils.validateToken(any(String.class), any(EntityManager.class))).thenReturn(-1);
			mvc.perform(MockMvcRequestBuilders.get(uri)
					.contentType(MediaType.APPLICATION_JSON)
					.accept(MediaType.APPLICATION_JSON)
					.param("token", "invalidToken")
					.param("id","1"))
					.andExpect(status().is(400));
			
		}catch(Exception e){
			_logger.error("{}", e.getMessage());
		}
	}
	
	@Test
	public void test_Create_Property() {
		try {
//			PowerMockito.when(SqlServerUtils.validateToken(any(String.class), any(EntityManager.class))).thenReturn(1);
			PropertyUIRequest request = this.getValidRequestForCreate();
			
			String uri = URI;
			String inputJson = super.mapToJson(request);
			MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
					.contentType(MediaType.APPLICATION_JSON)
					.accept(MediaType.APPLICATION_JSON)
					.content(inputJson))
					.andReturn();
			String content = result.getResponse().getContentAsString();
			int status = result.getResponse().getStatus();
			_logger.info("Status: [{}], Content: [{}]", status, content);
			assertEquals(status, 201);
		}catch(Exception e){
			_logger.error("{}", e.getMessage());
		}
   }

	@Test
	public void test_Update_Property() {
		try {
//			PowerMockito.when(SqlServerUtils.validateToken(any(String.class), any(EntityManager.class))).thenReturn(1);
			PropertyUIRequest request = this.getValidRequestForCreate();
			PowerMockito.when(repo.findPropertyWithId(any(Integer.class))).thenReturn(getProperty());
			
			String uri = URI;
			String inputJson = super.mapToJson(request);
			MvcResult result = mvc.perform(MockMvcRequestBuilders.put(uri)
					.contentType(MediaType.APPLICATION_JSON)
					.accept(MediaType.APPLICATION_JSON)
					.content(inputJson))
					.andReturn();
			String content = result.getResponse().getContentAsString();
			int status = result.getResponse().getStatus();
			_logger.info("Status: [{}], Content: [{}]", status, content);
			assertEquals(status, 200);
		}catch(Exception e){
			_logger.error("{}", e.getMessage());
		}
   }
	
	private Property getProperty() {
		Property property = new Property();
		property.setId(1234);
		property.setPageName("Landing Page");
		property.setValue("Trace");
		property.setLabel("title");
		property.setCreatedBy(1234);
		property.setLastUpdatedBy(1234);
		
		return property;
	}
	
	private PropertyUIRequest getValidRequestForCreate() {
		PropertyUIRequest propRequest = new PropertyUIRequest();
		propRequest.setToken("ValidToken");
		propRequest.setMessage(getProperty());
	
		return propRequest;
	}
		
}

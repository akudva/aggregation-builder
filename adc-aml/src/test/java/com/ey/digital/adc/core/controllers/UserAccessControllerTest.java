/**
 * 
 */
package com.ey.digital.adc.core.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.ey.digital.adc.core.util.DateTimeUtils;
import com.ey.digital.adc.core.ui.request.LoginRequest;
import com.ey.digital.adc.aml.controllers.UserAccessController;
import com.ey.digital.adc.aml.model.ActiveUser;
import com.ey.digital.adc.aml.model.Users;
import com.ey.digital.adc.aml.sql.server.repo.ActiveUserRepository;
import com.ey.digital.adc.aml.sql.server.repo.UsersRepository;



/**
 * This is the main test class for testing the <code>UserAccessController</code>.
 * <p>It should test all the code paths in the controller, including the edge cases 
 * that have been coded.<br>
 * Currently the controller only supports <code>POST</code> incoming HTTP requests. 
 * <ul>
 *	<li>null Token provided.
 * </ul>
 * </p>
 * @author mulaysh
 * Date Created: 2017-01-22
 */
public final class UserAccessControllerTest extends AbstractControllerTest implements BaseController {

	private final Logger _logger = LoggerFactory.getLogger(this.getClass());
	@Mock
	private UsersRepository usersRepo;
	
	@Mock
	private ActiveUserRepository activeUserRepo;
	
	@Mock
	ApplicationContext appContext;
	
	@Mock
	BCryptPasswordEncoder encoder;
	
	@InjectMocks
	private UserAccessController controller;
	
	@Override
	@Before
	public void setup(){
		super.setup();
		MockitoAnnotations.initMocks(this);
		setUp(controller);
		_logger.info("test setup complete");
	}
	
	@Test
	public void testRegister_Failure_NoEmail(){
		try {
			Users user = getValidUserForRegister();
			user.setEmail(null);
			String uri = "/register";
			String inputJson = super.mapToJson(user);
			MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
					.contentType(MediaType.APPLICATION_JSON)
					.accept(MediaType.APPLICATION_JSON)
					.content(inputJson))
					.andReturn();
			String content = result.getResponse().getContentAsString();
			int status = result.getResponse().getStatus();
			_logger.info("Content is [{}], status is [{}]", content, status);

			verify(usersRepo, times(0)).findByEmail(any(String.class));
			
			assertThat(status).as("failure - expected status 400").isEqualTo(400);
			assertThat(content.trim().length())
				.as("failure-expected HTTP response body to have NO value")
				.isEqualTo(0);
		}catch(Exception e){
			_logger.error("test case didn't run ", e);
		}
	}
	
	@Test
	public void testRegister_Failure_NoFirstName(){
		try {
			Users user = getValidUserForRegister();
			user.setFirstName(null);
			String uri = "/register";
			String inputJson = super.mapToJson(user);
			MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
					.contentType(MediaType.APPLICATION_JSON)
					.accept(MediaType.APPLICATION_JSON)
					.content(inputJson))
					.andReturn();
			String content = result.getResponse().getContentAsString();
			int status = result.getResponse().getStatus();
			_logger.info("Content is [{}], status is [{}]", content, status);

			verify(usersRepo, times(0)).findByEmail(any(String.class));
			
			assertThat(status).as("failure - expected status 400").isEqualTo(400);
			assertThat(content.trim().length())
				.as("failure-expected HTTP response body to have NO value")
				.isEqualTo(0);
		}catch(Exception e){
			_logger.error("test case didn't run ", e);
		}
	}
	
	@Test
	public void testRegister_Failure_NoLastName(){
		try {
			Users user = getValidUserForRegister();
			user.setLastName(null);
			String uri = "/register";
			String inputJson = super.mapToJson(user);
			MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
					.contentType(MediaType.APPLICATION_JSON)
					.accept(MediaType.APPLICATION_JSON)
					.content(inputJson))
					.andReturn();
			String content = result.getResponse().getContentAsString();
			int status = result.getResponse().getStatus();
			_logger.info("Content is [{}], status is [{}]", content, status);

			verify(usersRepo, times(0)).findByEmail(any(String.class));
			
			assertThat(status).as("failure - expected status 400").isEqualTo(400);
			assertThat(content.trim().length())
				.as("failure-expected HTTP response body to have NO value")
				.isEqualTo(0);
		}catch(Exception e){
			_logger.error("test case didn't run ", e);
		}
	}
	
	@Test
	public void testRegister_Failure_NoPassword(){
		try {
			Users user = getValidUserForRegister();
			user.setPassword(null);
			String uri = "/register";
			String inputJson = super.mapToJson(user);
			MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
					.contentType(MediaType.APPLICATION_JSON)
					.accept(MediaType.APPLICATION_JSON)
					.content(inputJson))
					.andReturn();
			String content = result.getResponse().getContentAsString();
			int status = result.getResponse().getStatus();
			_logger.info("Content is [{}], status is [{}]", content, status);

			verify(usersRepo, times(0)).findByEmail(any(String.class));
			
			assertThat(status).as("failure - expected status 400").isEqualTo(400);
			assertThat(content.trim().length())
				.as("failure-expected HTTP response body to have NO value")
				.isEqualTo(0);
		}catch(Exception e){
			_logger.error("test case didn't run ", e);
		}
	}

	@Test
	public void testRegister_Failure_UserIdPopulated(){
		try {
			Users user = getValidUserForRegister();
			user.setPtUserId(1000);;
			String uri = "/register";
			String inputJson = super.mapToJson(user);
			MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
					.contentType(MediaType.APPLICATION_JSON)
					.accept(MediaType.APPLICATION_JSON)
					.content(inputJson))
					.andReturn();
			String content = result.getResponse().getContentAsString();
			int status = result.getResponse().getStatus();
			_logger.info("Content is [{}], status is [{}]", content, status);
			
			assertThat(status).as("failure - expected status 400").isEqualTo(400);
			assertThat(content.trim().length())
				.as("failure-expected HTTP response body to have NO value")
				.isEqualTo(0);
		}catch(Exception e){
			_logger.error("test case didn't run ", e);
		}
	}
	
	@Test
	@Ignore
	public void testRegister_Failure_UserAlreadyExists(){
		try {
			Users user = getValidUserForRegister();
			Users response = getValidUserForRegister();
			response.setPtUserId(1000);
			when(usersRepo.findByEmail(any(String.class))).thenReturn(response);
			String uri = "/register";
			String inputJson = super.mapToJson(user);
			MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
					.contentType(MediaType.APPLICATION_JSON)
					.accept(MediaType.APPLICATION_JSON)
					.content(inputJson))
					.andReturn();
			String content = result.getResponse().getContentAsString();
			int status = result.getResponse().getStatus();
			_logger.info("Content is [{}], status is [{}]", content, status);

			verify(usersRepo, times(1)).findByEmail(any(String.class));
			
			assertThat(status).as("failure - expected status 409").isEqualTo(409);
			assertThat(content.trim().length())
				.as("failure-expected HTTP response body to have NO value")
				.isEqualTo(0);
		}catch(Exception e){
			_logger.error("test case didn't run ", e);
		}
	}
	
	@Test
	@Ignore
	public void testRegister_Success(){
		try {
			Users user = getValidUserForRegister();
			Users response = getValidUserForRegister();
			response.setPtUserId(1000);
			when(appContext.getBean(any(String.class))).thenReturn(encoder);
			when(encoder.encode(any(String.class))).thenReturn(response.getPassword());
			when(usersRepo.findByEmail(any(String.class))).thenReturn(new Users());
			when(usersRepo.save(any(Users.class))).thenReturn(response);
			String uri = "/register";
			String inputJson = super.mapToJson(user);
			MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
					.contentType(MediaType.APPLICATION_JSON)
					.accept(MediaType.APPLICATION_JSON)
					.content(inputJson))
					.andReturn();
			String content = result.getResponse().getContentAsString();
			int status = result.getResponse().getStatus();
			_logger.info("Content is [{}], status is [{}]", content, status);

			verify(appContext, times(1)).getBean(any(String.class));
			verify(encoder, times(1)).encode(any(String.class));
			verify(usersRepo, times(1)).findByEmail(any(String.class));
			verify(usersRepo, times(1)).save(any(Users.class));
			
			assertThat(status).as("failure - expected status 201").isEqualTo(201);
			assertThat(content.trim().length())
				.as("failure-expected HTTP response body to have Some value")
				.isGreaterThan(0);
		}catch(Exception e){
			_logger.error("test case didn't run ", e);
		}
	}
		
	@Test
	public void testLogin_Failure_NoEmail(){
		try {
			LoginRequest request = getValidUserForLogin();
			request.setEmail(null);
			String uri = "/login";
			String inputJson = super.mapToJson(request);
			MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
					.contentType(MediaType.APPLICATION_JSON)
					.accept(MediaType.APPLICATION_JSON)
					.content(inputJson))
					.andReturn();
			String content = result.getResponse().getContentAsString();
			int status = result.getResponse().getStatus();
			_logger.info("Content is [{}], status is [{}]", content, status);

			assertThat(status).as("failure - expected status 400").isEqualTo(400);
			assertThat(content.trim().length())
				.as("failure-expected HTTP response body to have NO value")
				.isEqualTo(0);
		}catch(Exception e){
			_logger.error("test case didn't run ", e);
		}
	}
	
	@Test
	public void testLogin_Failure_NoPassword(){
		try {
			LoginRequest request = getValidUserForLogin();
			request.setPassword(null);
			String uri = "/login";
			String inputJson = super.mapToJson(request);
			MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
					.contentType(MediaType.APPLICATION_JSON)
					.accept(MediaType.APPLICATION_JSON)
					.content(inputJson))
					.andReturn();
			String content = result.getResponse().getContentAsString();
			int status = result.getResponse().getStatus();
			_logger.info("Content is [{}], status is [{}]", content, status);

			assertThat(status).as("failure - expected status 400").isEqualTo(400);
			assertThat(content.trim().length())
				.as("failure-expected HTTP response body to have NO value")
				.isEqualTo(0);
		}catch(Exception e){
			_logger.error("test case didn't run ", e);
		}
	}
	
	@Test
	public void testLogin_Failure_InvalidUserId(){
		try {
			LoginRequest request = getValidUserForLogin();
			request.setEmail("user.not.valid@dumy.com");
			when(appContext.getBean(any(String.class))).thenReturn(encoder);
			when(usersRepo.findByEmail(any(String.class))).thenReturn(new Users());
			String uri = "/login";
			String inputJson = super.mapToJson(request);
			MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
					.contentType(MediaType.APPLICATION_JSON)
					.accept(MediaType.APPLICATION_JSON)
					.content(inputJson))
					.andReturn();
			String content = result.getResponse().getContentAsString();
			int status = result.getResponse().getStatus();
			_logger.info("Content is [{}], status is [{}]", content, status);

			verify(appContext, times(1)).getBean(any(String.class));
			verify(usersRepo, times(1)).findByEmail(any(String.class));
			assertThat(status).as("failure - expected status 404").isEqualTo(404);
			assertThat(content.trim().length())
				.as("failure-expected HTTP response body to have NO value")
				.isEqualTo(0);
		}catch(Exception e){
			_logger.error("test case didn't run ", e);
		}
	}
	
	@Test
	public void testLogin_Failure_InvalidPassword(){
		try {
			LoginRequest request = getValidUserForLogin();
			Users response = getValidUserForRegister();
			// change the password for the response
			response.setPassword("abc");
			when(appContext.getBean(any(String.class))).thenReturn(encoder);
			when(encoder.matches(any(String.class), any(String.class))).thenReturn(false);
			when(usersRepo.findByEmail(any(String.class))).thenReturn(response);
			String uri = "/login";
			String inputJson = super.mapToJson(request);
			MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
					.contentType(MediaType.APPLICATION_JSON)
					.accept(MediaType.APPLICATION_JSON)
					.content(inputJson))
					.andReturn();
			String content = result.getResponse().getContentAsString();
			int status = result.getResponse().getStatus();
			_logger.info("Content is [{}], status is [{}]", content, status);

			verify(usersRepo, times(1)).findByEmail(any(String.class));
			verify(encoder, times(1)).matches(any(String.class), any(String.class));
			verify(appContext, times(1)).getBean(any(String.class));
			assertThat(status).as("failure - expected status 404").isEqualTo(404);
			assertThat(content.trim().length())
				.as("failure-expected HTTP response body to have NO value")
				.isEqualTo(0);
		}catch(Exception e){
			_logger.error("test case didn't run ", e);
		}
	}
	
	@Test
	public void testLogin_Failure_UserAlreadyLoggedIn(){
		try {
			LoginRequest request = getValidUserForLogin();
			Users response = getValidUserForRegister();
			// change the password for the response
			when(appContext.getBean(any(String.class))).thenReturn(encoder);
			when(encoder.matches(any(String.class), any(String.class))).thenReturn(true);
			when(usersRepo.findByEmail(any(String.class))).thenReturn(response);
			when(activeUserRepo.findByLoginId(any(String.class))).thenReturn(getActiveUserList());
			String uri = "/login";
			String inputJson = super.mapToJson(request);
			MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
					.contentType(MediaType.APPLICATION_JSON)
					.accept(MediaType.APPLICATION_JSON)
					.content(inputJson))
					.andReturn();
			String content = result.getResponse().getContentAsString();
			int status = result.getResponse().getStatus();
			_logger.info("Content is [{}], status is [{}]", content, status);

			verify(usersRepo, times(1)).findByEmail(any(String.class));
			verify(activeUserRepo, times(1)).findByLoginId(any(String.class));
			verify(encoder, times(1)).matches(any(String.class), any(String.class));
			verify(appContext, times(1)).getBean(any(String.class));
			
			assertThat(status).as("failure - expected status 200").isEqualTo(200);
			assertThat(content.trim().length())
				.as("failure - expected HTTP response body to have SOME value")
				.isGreaterThan(0);
		}catch(Exception e){
			_logger.error("test case didn't run ", e);
		}
	}
	
	@Test
	public void testLogout_Failure_NullToken(){
		try {
			// change the password for the response
			//when(activeUserRepo.findByToken(any(String.class))).thenReturn(null);
			String uri = "/logout?token=";
			byte[] temp = new byte[0];
			MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
					.contentType(MediaType.APPLICATION_JSON)
					.accept(MediaType.APPLICATION_JSON)
					.content(temp))
					.andReturn();
			String content = result.getResponse().getContentAsString();
			int status = result.getResponse().getStatus();
			_logger.info("Content is [{}], status is [{}]", content, status);

			//verify(activeUserRepo, times(1)).findByToken(any(String.class));
			
			assertThat(status).as("failure - expected status 400").isEqualTo(400);
			assertThat(content.trim().length())
				.as("failure - expected HTTP response body to have NO value")
				.isEqualTo(0);
		}catch(Exception e){
			_logger.error("test case didn't run ", e);
		}
	}
	
	@Test
	public void testLogout_Failure_InvalidToken(){
		try {
			// change the password for the response
			when(activeUserRepo.findByToken(any(String.class))).thenReturn(null);
			String uri = "/logout?token=ABC";
			byte[] temp = new byte[0];
			MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
					.contentType(MediaType.APPLICATION_JSON)
					.accept(MediaType.APPLICATION_JSON)
					.content(temp))
					.andReturn();
			String content = result.getResponse().getContentAsString();
			int status = result.getResponse().getStatus();
			_logger.info("Content is [{}], status is [{}]", content, status);

			verify(activeUserRepo, times(1)).findByToken(any(String.class));
			
			assertThat(status).as("failure - expected status 400").isEqualTo(400);
			assertThat(content.trim().length())
				.as("failure - expected HTTP response body to have NO value")
				.isEqualTo(0);
		}catch(Exception e){
			_logger.error("test case didn't run ", e);
		}
	}
	
	@Test
	public void testLogout_Success(){
		try {
			// change the password for the response
			ActiveUser user = new ActiveUser();
			ActiveUser response = new ActiveUser();
			response.setLogoutDtTime(DateTimeUtils.getCurrentDateTime());
			when(activeUserRepo.findByToken(any(String.class))).thenReturn(user);
			when(activeUserRepo.save(any(ActiveUser.class))).thenReturn(response);
			String uri = "/logout?token=ABC";
			byte[] temp = new byte[0];
			MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
					.contentType(MediaType.APPLICATION_JSON)
					.accept(MediaType.APPLICATION_JSON)
					.content(temp))
					.andReturn();
			String content = result.getResponse().getContentAsString();
			int status = result.getResponse().getStatus();
			_logger.info("Content is [{}], status is [{}]", content, status);

			verify(activeUserRepo, times(1)).findByToken(any(String.class));
			verify(activeUserRepo, times(1)).save(any(ActiveUser.class));
			assertThat(status).as("failure - expected status 200").isEqualTo(200);
			assertThat(content.trim().length())
				.as("failure - expected HTTP response body to have SOME value")
				.isGreaterThan(0);
		}catch(Exception e){
			_logger.error("test case didn't run ", e);
		}
	}
	
	private Users getValidUserForRegister(){
		Users user = new Users();
		user.setEmail("test@abc.com");
		user.setFirstName("First");
		user.setLastName("Last");
		user.setPassword("password");
		return user;
	}

	private LoginRequest getValidUserForLogin(){
		LoginRequest loginRequest = new LoginRequest();
		loginRequest.setEmail("test@abc.com");
		loginRequest.setPassword("password");
		return loginRequest;
	}

	private ActiveUser getActiveUser(){
		ActiveUser activeUser = new ActiveUser();
		activeUser.setLoginDtTime(DateTimeUtils.getCurrentDateTime());
		activeUser.setLoginId(getValidUserForLogin().getEmail());
		activeUser.setLogoutDtTime(null);
		activeUser.setSessionValidity(null);
		activeUser.setToken("ABC-CDE-FGH");
		return activeUser;
	}
	
	private List<ActiveUser> getActiveUserList(){
		List<ActiveUser> list = new ArrayList<>();
		list.add(getActiveUser());
		return list;
	}
}

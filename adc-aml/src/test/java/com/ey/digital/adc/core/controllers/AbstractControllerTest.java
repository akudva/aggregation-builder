/**
 * 
 */
package com.ey.digital.adc.core.controllers;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author mulaysh
 * Date Created: 2017-01-05
 */
@WebAppConfiguration
public abstract class AbstractControllerTest extends AbstractTest {

	protected MockMvc mvc;
	
	@Autowired
	protected WebApplicationContext webAppContext;
	
	protected void setUp(){
		mvc = MockMvcBuilders.webAppContextSetup(webAppContext).build();
	}
	
	protected void setUp(BaseController controller){
		mvc = MockMvcBuilders.standaloneSetup(controller).build();
	}
	
	protected String mapToJson(Object obj) throws JsonProcessingException{
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(obj);
	}
	
	protected <T> T mapFromJson(String json, Class<T> clazz) throws JsonParseException, JsonMappingException, IOException{
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(json, clazz);
	}
}
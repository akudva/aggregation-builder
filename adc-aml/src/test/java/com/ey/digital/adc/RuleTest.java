package com.ey.digital.adc;

import com.ey.digital.adc.aml.model.RuleExecutionJob;
import com.ey.digital.adc.aml.model.RuleExecutionResult;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.Test;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;

public class RuleTest {
    @Test
    public void ruleJobTest() throws Exception {
        RuleExecutionJob rj = new RuleExecutionJob();
        rj.setRtcid(111);
        rj.setRtcid(12);
        rj.setCustomRunDates(new HashSet<>());

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date parsedDate = dateFormat.parse("2017-09-04");
        Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
        rj.setEndRangeRunDate(timestamp);
        rj.getCustomRunDates().add(timestamp);
        rj.getCustomRunDates().add(timestamp);
        rj.setStartRangeRunDate(timestamp);
        rj.setId(11);
        rj.setRtcid(12);
        log(rj);
        RuleExecutionResult rr = new RuleExecutionResult();
        rr.setRunDate(timestamp);
        rr.setStartExecutionTime(timestamp);
        rr.setExecutionId(111);
        rr.setJobId(12);
        rr.setStatus("COMPLETE");
        log(rr);

    }

    @Test
    public void testRegex() throws Exception {
       assert "insert into ${exec_id}".replaceAll("\\$\\{exec_id\\}","123").equals("insert into 123");
    }

    @Test
    public void testDate() throws Exception {
        Calendar calendar=Calendar.getInstance();
        calendar.add(Calendar.YEAR, -1);
        System.out.println(new Timestamp(calendar.getTime().getTime()));
    }

    private void log(Object rj) throws JsonProcessingException {
        ObjectMapper om = new ObjectMapper();
        om.enable(SerializationFeature.INDENT_OUTPUT);
        System.out.println(om.writeValueAsString(rj));
    }
}

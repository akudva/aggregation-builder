package com.ey.digital.adc.aml.controllers;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.mockito.Mockito.when;
import com.ey.digital.adc.aml.controllers.RuleController;
import com.ey.digital.adc.aml.controllers.UserAccessController;
import com.ey.digital.adc.aml.model.Condition;
import com.ey.digital.adc.aml.model.ConditionMetaData;
import com.ey.digital.adc.aml.model.Rule;
import com.ey.digital.adc.aml.model.RuleBuilder;
import com.ey.digital.adc.aml.model.SegmentRuleRiskMultiplier;
import com.ey.digital.adc.aml.sql.server.repo.ActiveUserRepository;

import com.ey.digital.adc.aml.sql.server.repo.RuleRepository;
import com.ey.digital.adc.aml.sql.server.repo.SegmentRuleRiskMultRepo;
import com.ey.digital.adc.aml.sql.server.repo.UsersRepository;
import com.ey.digital.adc.aml.ui.request.RuleUIRequest;
import com.ey.digital.adc.core.controllers.AbstractControllerTest;
import com.ey.digital.adc.core.controllers.BaseController;
import com.ey.digital.adc.core.sql.server.utils.SqlServerUtils;
import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * This is the main test class for testing the <code>UserAccessController</code>.
 * <p>It should test all the code paths in the controller, including the edge cases 
 * that have been coded.<br>
 * Currently the controller only supports <code>POST</code> incoming HTTP requests. 
 * <ul>
 *	<li>null Token provided.
 * </ul>
 * </p>
 * @author kumarpr15
 * Date Created: 2017-01-22
 */
@Ignore
@PrepareForTest(SqlServerUtils.class)
public class RuleControllerTest extends AbstractControllerTest implements BaseController {
	private final Logger _logger = LoggerFactory.getLogger(this.getClass());
	
	@Mock
	private RuleRepository ruleRepo;
	
	
	@Mock
	private SegmentRuleRiskMultRepo segmentRuleRiskMultRepo;
	
	@Mock
	ApplicationContext appContext;
	
	@Mock
	BCryptPasswordEncoder encoder;

	
	@InjectMocks
	private RuleController ruleController;

  	@Override
  	@Before
  	public void setup(){
  		setUp(ruleController);
  		super.setup();
  		MockitoAnnotations.initMocks(this);
  		PowerMockito.mockStatic(SqlServerUtils.class);
  		_logger.info("test setup complete");
  		
  	}
  	
  	private int ruleId = 123;
  	
  	private String conditionCounterId = "condition-1";
  	
  	private String conditionText = "condition_text";
  	
  	private String lookBack = "look_back";
  	
  	private int userId = 123;
  	
  	private String createQuery = "create";
  	
  	private String insertQuery = "insert";
  
  	private String transactionQuery = "transaction";
  	
  	private String ruleText = "count|tran_amt|all_inc|300|>=|1~sum|tran_amt|ce_inc|365|>=|10^AND^max|tran_amt|ce_all|560|>=|10^OR^sum|tran_amt|all|500|>=|10";
  	
  	private String aggregationKey = "1";
  	
  	private String token = "ABC";
	
  	@Test
  	public void test_create_internal() throws Exception {
//  		PowerMockito.when(SqlServerUtils.validateToken(any(String.class), any(EntityManager.class))).thenReturn(1);
  		RuleUIRequest request = this.getValidRequestForCreate();
  
  		when(ruleRepo.save(any(Rule.class))).thenReturn(getRule());
  
  		//when(parser.ruleParser(any(String.class), any(Boolean.class), any(Map.class))).thenReturn(new StringBuffer());
  
  		//when(parser.buildHiveQuery(any(Rule.class), any(Map.class), 
  		//		any(StringBuffer.class),any(JdbcTemplate.class))).thenReturn(getRuleBuilder());
  		
  	//	when(ruleConditionRepo.save(any(RuleCondition.class))).thenReturn(getRuleCondition());
  		when(segmentRuleRiskMultRepo.save(any(SegmentRuleRiskMultiplier.class))).thenReturn(getSegmentRuleRiskMultiplier());
  
  		String uri = "/rule";
  		String inputJson = super.mapToJson(request);
  	
  		_logger.info(inputJson);
  		MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
  				.contentType(MediaType.APPLICATION_JSON)
  				.accept(MediaType.APPLICATION_JSON)
  				.content(inputJson))
  				.andReturn();
  		String content = result.getResponse().getContentAsString();
  		int status = result.getResponse().getStatus();
  		_logger.info("Status: [{}], Content: [{}]", status, content);
  		
  		//assertThat(content, containsString(ruleText));
  		//assertEquals(status, 201);
  	}		
  	
  
  	private SegmentRuleRiskMultiplier getSegmentRuleRiskMultiplier() {
  		SegmentRuleRiskMultiplier segmentRuleRiskMultiplier = new SegmentRuleRiskMultiplier();
  		segmentRuleRiskMultiplier.setRuleid(ruleId);
  		segmentRuleRiskMultiplier.setRuleConditionId(conditionCounterId);
  		return segmentRuleRiskMultiplier; 
  	}
  	/**
  	private RuleCondition getRuleCondition() {
  		RuleCondition ruleCondition = new RuleCondition();
  		ruleCondition.setRuleId(ruleId);
  		ruleCondition.setConditionId(conditionCounterId);
  		ruleCondition.setConditionText(conditionText);
  		ruleCondition.setLookback(lookBack);
  		
  		return ruleCondition;
  	}
  	*/
  	private Map<String, ConditionMetaData> getFieldAggregations() {
  		Map<String, ConditionMetaData> mapFieldAggregations = new HashMap<String, ConditionMetaData>();
  		List<Condition> conditionList = new ArrayList<Condition>();
  		ConditionMetaData metaData = new ConditionMetaData();
  		
  		Condition condition = new Condition();
  		//condition.setConditionText(conditionText);
  		//condition.setLookback(lookBack);
  		conditionList.add(condition);
  		
  		metaData.setConditionsList(conditionList);
  		mapFieldAggregations.put(aggregationKey, metaData);
  		
  		return mapFieldAggregations; 
  	}
  	
  	private RuleBuilder getRuleBuilder() {
  		RuleBuilder ruleBuilder = new RuleBuilder();
  		ruleBuilder.setAlertDetailCreateQuery(createQuery);
  		ruleBuilder.setAlertDetailInsertQuery(insertQuery);
  		ruleBuilder.setAlertTransactionQuery(transactionQuery);
  		return ruleBuilder;
  	}
  	
  	private Rule getRule() {
  		List<SegmentRuleRiskMultiplier> segRuleRiskMultList = new ArrayList<>();
  		Rule rule = new Rule();
  		//rule.setRuleText(ruleText);
  		//rule.setSegRuleRiskMultList(segRuleRiskMultList);
  		return rule;
  	}
  
  	private RuleUIRequest getValidRequestForCreate() {
  		RuleUIRequest ruleRequest = new RuleUIRequest();
  		ruleRequest.setToken(token);
  		ruleRequest.setMessage(getRule());
  		return ruleRequest;
  	}
}

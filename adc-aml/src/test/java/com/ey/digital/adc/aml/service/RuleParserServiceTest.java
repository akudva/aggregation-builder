package com.ey.digital.adc.aml.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.JdbcTemplate;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ey.digital.adc.aml.model.Condition;
import com.ey.digital.adc.aml.model.ConditionMetaData;
import com.ey.digital.adc.aml.model.Rule;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.refEq;
import static org.mockito.Mockito.when;
import com.ey.digital.adc.aml.model.RuleBuilder;
import com.ey.digital.adc.aml.model.SegmentRuleRiskMultiplier;
import com.ey.digital.adc.core.controllers.AbstractControllerTest;
import com.ey.digital.adc.core.sql.server.utils.SqlServerUtils;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;

@Ignore
@PrepareForTest(SqlServerUtils.class)
public class RuleParserServiceTest extends AbstractControllerTest {

	private final Logger _logger = LoggerFactory.getLogger(this.getClass());

	private int ruleId = 123;

	private String conditionCounterId = "condition-1";

	private String conditionText = "condition_text";

	private String lookBack = "look_back";

	private String createQuery = "create";

	private String insertQuery = "insert";

	private String transactionQuery = "transaction";

	private String ruleText = "count|tran_amt|all_inc|300|>=|1~sum|tran_amt|ce_inc|365|>=|10^AND^max|tran_amt|ce_all|560|>=|10^OR^sum|tran_amt|all|500|>=|10";

	private String aggregationKey = "conditionId-1";

	@Mock
	JdbcTemplate jdbcTemplate;

	@BeforeClass
	public static void setUpClass() {

	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before()
	public void setUp() {
		super.setup();
		MockitoAnnotations.initMocks(this);
		PowerMockito.mockStatic(SqlServerUtils.class);
		_logger.info("test setup complete");
	}

	@After
	public void tearDown() {
	}

	@Test
	public void test_build_hive_query() {
		// when(jdbcTemplate.queryForObject(eq("input string"), refEq(new Object[] {
		// 1001 }), eq(String.class)))
		// .thenReturn("hiveSchema");
		// StringBuffer ruleClause = new StringBuffer();
		// ruleClause.append("ruleClause");
		//
		// RuleBuilder ruleBuilder = ruleParserService.buildHiveQuery(getRule(),
		// getFieldAggregations(), ruleClause,
		// jdbcTemplate);
		//
		// assertThat(ruleBuilder, instanceOf(RuleBuilder.class));
	}

	private Map<String, ConditionMetaData> getFieldAggregations() {
		Map<String, ConditionMetaData> mapFieldAggregations = new HashMap<String, ConditionMetaData>();
		// List<Condition> conditionList = new ArrayList<Condition>();
		// ConditionMetaData metaData = new ConditionMetaData();
		//
		// Condition condition = new Condition();
		// condition.setConditionText(conditionText);
		// condition.setLookback("123");
		// condition.setAggregation("count");
		// condition.setAggregation("aggregation");
		// condition.setColumnName("columnName");
		// condition.setTransactionGroup("all");
		// condition.setTransactionGroup("transactionGroup");
		// conditionList.add(condition);
		//
		// metaData.setConditionsList(conditionList);
		// mapFieldAggregations.put(aggregationKey, metaData);

		return mapFieldAggregations;
	}

	private Rule getRule() {
		List<SegmentRuleRiskMultiplier> segRuleRiskMultList = new ArrayList<>();
		Rule rule = new Rule();
		// rule.setRuleText(ruleText);
		// rule.setSegRuleRiskMultList(segRuleRiskMultList);
		// rule.setFocalEntity("focalEntity");
		// rule.setFocalEntity("account");
		// rule.setRunDate("runDate");
		// rule.setRuleId(123);
		// rule.setRuleName("ruleName");
		// rule.setHiveSchema("hiveSchema");

		return rule;
	}
}

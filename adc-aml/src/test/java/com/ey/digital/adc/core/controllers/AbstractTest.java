/**
 * 
 */
package com.ey.digital.adc.core.controllers;

import org.joda.time.LocalDate;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author mulaysh
 * Date Created: 2017-01-05
 */
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(SpringRunner.class)
@PowerMockIgnore("javax.management.*")
//@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
//@SpringApplicationConfiguration(classes = SpringTestConfig.class)
public abstract class AbstractTest {
	protected Logger _logger = LoggerFactory.getLogger(this.getClass());
	@Rule public TestName name = new TestName();
	
	@Before
	public void setup(){
		_logger.info("\n*** Starting Test {} ***", name.getMethodName());
	}
	
	@After
	public void teardown(){
		_logger.info("*** Finished Test {} ***\n", name.getMethodName());
	}
    /*@BeforeClass
    public static void setLogger() throws MalformedURLException
    {
        System.setProperty("log4j.configurationFile","./src/test/resources/log4j2-test.xml");
    	//api
    	System.out.println(org.apache.logging.log4j.Logger.class.getResource("/org/apâ€Œâ€‹ache/logging/log4j/Logger.class"));
    	//core
    	System.out.println(org.apache.logging.log4j.Logger.class.getResource("/org/apâ€Œâ€‹ache/logging/log4j/core/Appender.class"));
    	//config
    	System.out.println(org.apache.logging.log4j.Logger.class.getResource("/log4j2-test.xml"));
    	System.out.println(org.apache.logging.log4j.Logger.class.getResource("/log4j2.xml"));
    	System.out.println(System.getProperty("log4j.configurationFile"));
		System.out.println("user directory is [" + System.getProperty("user.dir") + "]");
    }*/
    
    
	protected String getStartDate(){
		LocalDate now = LocalDate.now();
		return now.minusDays(2).toString("MM/dd/yyyy");
	}
	
	protected String getEndDate(){
		LocalDate now = LocalDate.now();
		return now.plusMonths(1).toString("MM/dd/yyyy");
	}
	
}
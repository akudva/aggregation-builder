package com.ey.digital.adc.aml.util;

import java.util.regex.Pattern;

public final class FocalEntityUtil {
	
	public static String getFocalEntityColumn(String focalEntity,String focalEntityColumn, char alias) {
		String[] focalEntityColumnArr;
		StringBuffer focalEntityColumnWithAlias = new StringBuffer();
		if(!focalEntity.equalsIgnoreCase("Transaction")) {
		if(focalEntityColumn.contains(",")){
			focalEntityColumnArr = focalEntityColumn.trim().split(Pattern.quote(","));
			for(int i=0; i<focalEntityColumnArr.length; i++){
				if(i == focalEntityColumnArr.length -1){
					focalEntityColumnWithAlias.append(alias+"."+focalEntityColumnArr[i]);
				}else{
					focalEntityColumnWithAlias.append(alias+"."+focalEntityColumnArr[i]+", ");
				}
				
			}
		}
		else{
			focalEntityColumnWithAlias.append(alias+"."+focalEntityColumn);
		}
		
		return focalEntityColumnWithAlias.toString();
		}
		else
		{
			focalEntityColumnWithAlias.append(alias+"."+focalEntityColumn + " as transaction_key");
			return focalEntityColumnWithAlias.toString();
		}
	}
	
	
	
	public static String getFocalEntityColumn(String focalEntityColumn, char alias) {
		String[] focalEntityColumnArr;
		StringBuffer focalEntityColumnWithAlias = new StringBuffer();
		if(focalEntityColumn.contains(",")){
			focalEntityColumnArr = focalEntityColumn.trim().split(Pattern.quote(","));
			for(int i=0; i<focalEntityColumnArr.length; i++){
				if(i == focalEntityColumnArr.length -1){
					focalEntityColumnWithAlias.append(alias+"."+focalEntityColumnArr[i]);
				}else{
					focalEntityColumnWithAlias.append(alias+"."+focalEntityColumnArr[i]+", ");
				}
				
			}
		}
		else{
			focalEntityColumnWithAlias.append(alias+"."+focalEntityColumn);
		}
		
		return focalEntityColumnWithAlias.toString();
		}
		
	
	
	public static String getFocalEntityColumnWithColumnNames(String focalEntityColumn, char alias) {
		String[] focalEntityColumnArr;
		StringBuffer focalEntityColumnWithAlias = new StringBuffer();
		
		if(focalEntityColumn.contains(",")){
			focalEntityColumnArr = focalEntityColumn.trim().split(Pattern.quote(","));
			for(int i=0; i<focalEntityColumnArr.length; i++){
				if(i == focalEntityColumnArr.length -1){
					focalEntityColumnWithAlias.append(alias+"."+focalEntityColumnArr[i]+" as "+focalEntityColumnArr[i]);
				}else{
					focalEntityColumnWithAlias.append(alias+"."+focalEntityColumnArr[i]+" as "+focalEntityColumnArr[i]+", ");
				}
				
			}
		}
		else{
			focalEntityColumnWithAlias.append(alias+"."+focalEntityColumn+" as "+focalEntityColumn);
		}
		
		return focalEntityColumnWithAlias.toString();
	}
	
    public static String getFocalEntityConcatClause(String focalEntityColumn, char alias) {
    		String[] focalEntityColumns = FocalEntityUtil.getFocalEntityColumn(focalEntityColumn, alias).split(",");
		String focalEntityConcatClause = "";

		for (String s : focalEntityColumns) {
			String feName = s.substring(s.indexOf(".") + 1);
			if (focalEntityConcatClause.length() > 0)
				focalEntityConcatClause = "concat("+s+"," + focalEntityConcatClause+ ")";
			else
				focalEntityConcatClause =  s ;
		}
		return focalEntityConcatClause;
    }
	
	public static String getAlertFocalEntityColumn(String focalEntityColumn, char alias) {
		String[] focalEntityColumnArr;
		StringBuffer focalEntityColumnWithAlias = new StringBuffer();
		
		if(focalEntityColumn.contains(",")){
			focalEntityColumnArr = focalEntityColumn.split(Pattern.quote(","));
			for(int i=0; i<focalEntityColumnArr.length; i++){
				if(i == focalEntityColumnArr.length -1){
					focalEntityColumnWithAlias.append(alias+"."+focalEntityColumnArr[i] + " as focus_key_2");
				}else{
					focalEntityColumnWithAlias.append(alias+"."+focalEntityColumnArr[i]+" as focus_key_1, ");
				}
				
			}
		}
		else{
			focalEntityColumnWithAlias.append(alias+"."+focalEntityColumn+ " as focus_key_1");
			focalEntityColumnWithAlias.append(", NULL as focus_key_2 ");
		}
		
		return focalEntityColumnWithAlias.toString();
	}
	
	public static String getJoinConditionFocalEntityColumn(String focalEntityColumn, char alias1, char alias2) {
		String[] focalEntityColumnArr;
		StringBuffer focalEntityColumnWithAlias = new StringBuffer();
		
		if(focalEntityColumn.contains(",")){
			focalEntityColumnArr = focalEntityColumn.split(Pattern.quote(","));
			for(int i=0; i<focalEntityColumnArr.length; i++){
				if(i == focalEntityColumnArr.length -1){
					focalEntityColumnWithAlias.append(alias1 + "." + focalEntityColumnArr[i] + "=" +  alias2 + "." + focalEntityColumnArr[i]);
				}else{
					focalEntityColumnWithAlias.append(alias1 + "." + focalEntityColumnArr[i] + "=" +  alias2 + "." + focalEntityColumnArr[i]+" AND ");
				}
				
			}
		}
		else{
			focalEntityColumnWithAlias.append(alias1 + "." + focalEntityColumn + "=" +  alias2 + "." + focalEntityColumn);
		}
		
		return focalEntityColumnWithAlias.toString();
	}
}

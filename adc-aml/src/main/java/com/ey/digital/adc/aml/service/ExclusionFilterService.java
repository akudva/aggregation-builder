package com.ey.digital.adc.aml.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ey.digital.adc.aml.model.Condition;
import com.ey.digital.adc.aml.model.ExclusionFilter;
import com.ey.digital.adc.aml.model.Rule;

@Service
public class ExclusionFilterService {

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	private Set<String> operatorsThatCompareMultipleValues = new HashSet<String>(Arrays.asList("in", "not in"));

	@Autowired
	public ExclusionFilterService() {
	}

	public StringBuffer createExclusionFilterSQL(ExclusionFilter filter, Rule rule, Condition condition,
			ArrayList<String> tableArr, ArrayList<Character> alias, StringBuffer joinCondition, StringBuffer fromClause,
			ArrayList<String> factTableArr) {

		StringBuffer filterSQL = new StringBuffer();
		String resultColumn;
		String resultTable = "";

		String operator; // Always equal to 'operator'?
		String type = filter.getType();

		if (type != null)
			switch (type) {
			case "attribute":
				LOGGER.info("Exclusion Filter - attribute=" + type);
				resultColumn = filter.getValue().getAttribute();
				resultTable = filter.getValue().getTable();

				filter.setResultcolumn(resultColumn);
				filter.setResultTable(resultTable);

				if (tableArr.contains(resultTable)) {
					int index = 0;
					for (String t : tableArr) {
						if (tableArr.get(index).equalsIgnoreCase(resultTable)) {
							break;
						} else {
							index++;
						}
					}
					filterSQL.append(" " + alias.get(index) + "." + resultColumn + " ");
				}
				/*
				 * Only apply Exclusion Filter if the condition table is same as the inclusion
				 * filter table
				 */

				LOGGER.info("Exclusion Filter - Attribute SQL=" + filterSQL);
				break;
			case "data":
				LOGGER.info("Exclusion Filter - Data=" + type);
				String data = filter.getValue().getData();

				try {
					Double.valueOf(data);
					filterSQL.append(" CAST('" + data + "' as DOUBLE) ");
					LOGGER.info("Exclusion Filter - Data SQL=" + filterSQL);
				} catch (NumberFormatException e) {
					if (!data.contains(",")) {
						filterSQL.append(" '" + data + "' ");
					} else if (data.contains(",")) {
						Set<String> dataArray = new HashSet<String>(
								Arrays.asList(filter.getValue().getData().split(Pattern.quote(","))));
						// filterSQL.append("(");
						int index = 0;
						for (String a : dataArray) {
							index++;
							filterSQL.append("'" + a + "'");
							if (index != dataArray.size()) // TODO: test object
															// equality. Need to
															// validate
								filterSQL.append(",");
						}
						// filterSQL.append(")");

						LOGGER.info("Inclusion Filter - Data SQL=" + filterSQL);
					}
				}

				break;

			// Assuming Exclusion Filters will not have thresholds
			// case "threshold_label":
			// break;
			case "operator":
				LOGGER.info("Exclusion Filter - Operator=" + type);
				operator = filter.getValue().getOperator().toLowerCase();

				filterSQL.append(createExclusionFilterSQL(filter.getChildren().get(0), rule, condition, tableArr, alias,
						joinCondition, fromClause, factTableArr) + " " + operator);

				if (operatorsThatCompareMultipleValues.contains(operator)) {
					filterSQL.append(" (" + createExclusionFilterSQL(filter.getChildren().get(1), rule, condition,
							tableArr, alias, joinCondition, fromClause, factTableArr) + ")");
				} else {
					filterSQL.append(" " + createExclusionFilterSQL(filter.getChildren().get(1), rule, condition,
							tableArr, alias, joinCondition, fromClause, factTableArr));
				}

				LOGGER.info("Exclusion Filter - Operator SQL=" + filterSQL);
				break;

			}

		LOGGER.info("Created Exclusion Filter" + filterSQL);
		return filterSQL;

	}
}
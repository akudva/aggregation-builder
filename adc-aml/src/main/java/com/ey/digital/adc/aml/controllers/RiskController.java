package com.ey.digital.adc.aml.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


import com.ey.digital.adc.aml.model.Risk;
import com.ey.digital.adc.aml.sql.server.repo.RiskLevelsRepository;
import com.ey.digital.adc.aml.ui.request.RuleUIRequest;
import com.ey.digital.adc.aml.ui.response.ResponseJSON;
import com.ey.digital.adc.aml.ui.response.RiskLevelsResponse;
//import com.ey.digital.adc.aml.ui.response.RiskResponse;
import com.ey.digital.adc.aml.ui.request.RiskUIRequest;
import com.ey.digital.adc.core.controllers.AbstractController;
import com.ey.digital.adc.core.exceptions.InvalidMessageException;
import com.ey.digital.adc.core.exceptions.InvalidTokenException;
import com.ey.digital.adc.core.model.ApiResponse;
import com.ey.digital.adc.core.ui.resp.utils.HttpResponseUtils;
import com.ey.digital.adc.core.util.DateTimeUtils;


@RestController
@RequestMapping("/risk")
public class RiskController extends AbstractController<RiskUIRequest, Risk> {

	
	@Autowired
	private RiskLevelsRepository riskRepo;
	
	private final Logger _logger = LoggerFactory.getLogger(this.getClass());
	
	public RiskController(RiskLevelsRepository repo) {
		super(repo);
		this.riskRepo=repo;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ey.digital.adc.producttrace.controllers.AbstractController#
	 * createInternal(int,
	 * com.ey.digital.adc.producttrace.client.messages.AbstractClientRequest)
	 */
	@Override
	public Risk createInternal(int userId, RiskUIRequest request) throws Exception {
		throw new UnsupportedOperationException("This operation is not supported");

	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ey.digital.adc.producttrace.controllers.AbstractController#
	 * updateInternal(int,
	 * com.ey.digital.adc.producttrace.client.messages.AbstractClientRequest)
	 */
	@Override
	public Risk updateInternal(int userId, RiskUIRequest request) throws Exception {
		throw new UnsupportedOperationException("This operation is not supported");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ey.digital.adc.producttrace.controllers.AbstractController#
	 * getRiskLevels
	 */
	
	@RequestMapping(value = "/riskLevels", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON, params = {"token"})
	public ResponseEntity<List<String>> getRiskLevels(@RequestParam("token") String token)
			     throws Exception {
		
		int userId = 0;
		_logger.info("processing get risklevels  ", token);
		try {
			userId = validateToken(token);
		} catch (InvalidTokenException e) {
			_logger.warn("Returning error response [{}]", e.getMessage());
			return HttpResponseUtils.getInvalidTokenErrorResponse();
		}
		
		try {
		     List<Risk> risks = riskRepo.findAll();
		     List<String> riskLevels = new ArrayList<String> ();	     
		     risks.forEach(r -> {
		    	 riskLevels.add(r.getRiskName());
		     });
			return HttpResponseUtils.getResponse(riskLevels);
	   
		}catch (Exception e) {
			_logger.error("", e);
			return HttpResponseUtils.getErrorResponse(HttpStatus.NOT_FOUND, e.getMessage());

		}
		
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ey.digital.adc.producttrace.controllers.AbstractController#
	 * getListInternal(int)
	 */
	@Override
	public List<Risk> getListInternal(int listForId) {
		throw new UnsupportedOperationException("This operation is not supported");
	}

}

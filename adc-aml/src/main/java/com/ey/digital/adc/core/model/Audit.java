package com.ey.digital.adc.core.model;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import com.ey.digital.adc.aml.model.Users;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@MappedSuperclass
public abstract class Audit implements IModel{

	private static final long serialVersionUID = 1L;
	
	@Column(name = "createdby")
	protected Integer createdBy;

	@Transient
	protected String createdByName;

	@Column(name = "createddtm")
	protected String createdDateTime;

	@Column(name = "lastupdtuser")
	protected Integer lastUpdatedBy;

	@Transient
	protected String lastUpdatedByName;

	@Column(name = "lastupdtdtm")
	protected String lastUpdatedDateTime;
	
	
	@OneToOne
	@JoinColumn(name = "createdby", insertable=false, updatable = false)
	@JsonIgnore
	protected Users createdByUser;

	@OneToOne
	@JoinColumn(name = "lastupdtuser", insertable=false, updatable = false)
	@JsonIgnore
	protected Users lastUpdtByUser;
	
	/**
	 * @return the createdBy
	 */
	public Integer getCreatedBy() {
		return createdBy;
		//return (createdBy !=null ) ? createdBy : 0;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdByName
	 */
	public String getCreatedByName() {
		if (createdByUser != null){
			createdByName = createdByUser.getFirstName() + " " + createdByUser.getLastName();
		}
		return createdByName;
	}

	/**
	 * @param createdByName the createdByName to set
	 */
	public void setCreatedByName(String createdByName) {
		this.createdByName = createdByName;
	}

	/**
	 * @return the createdDateTime
	 */
	public String getCreatedDateTime() {
		return createdDateTime;
	}

	/**
	 * @param createdDateTime the createdDateTime to set
	 */
	public void setCreatedDateTime(String createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

	/**
	 * @return the lastUpdatedBy
	 */
	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	/**
	 * @param lastUpdatedBy the lastUpdatedBy to set
	 */
	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	/**
	 * @return the lastUpdatedByName
	 */
	public String getLastUpdatedByName() {
		if (lastUpdtByUser != null){
			this.lastUpdatedByName = lastUpdtByUser.getFirstName() + " " + lastUpdtByUser.getLastName();
		}
		return lastUpdatedByName;
	}

	/**
	 * @param lastUpdatedByName the lastUpdatedByName to set
	 */
	public void setLastUpdatedByName(String lastUpdatedByName) {
		this.lastUpdatedByName = lastUpdatedByName;
	}

	/**
	 * @return the lastUpdatedDateTime
	 */
	public String getLastUpdatedDateTime() {
		return lastUpdatedDateTime;
	}

	/**
	 * @param lastUpdatedDateTime the lastUpdatedDateTime to set
	 */
	public void setLastUpdatedDateTime(String lastUpdatedDateTime) {
		this.lastUpdatedDateTime = lastUpdatedDateTime;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AbstractProductTraceMessage [createdBy=" + createdBy + ", createdByName=" + createdByName
				+ ", createdDateTime=" + createdDateTime + ", lastUpdatedBy=" + lastUpdatedBy + ", lastUpdatedByName="
				+ lastUpdatedByName + ", lastUpdatedDateTime=" + lastUpdatedDateTime + "]";
	}	

}

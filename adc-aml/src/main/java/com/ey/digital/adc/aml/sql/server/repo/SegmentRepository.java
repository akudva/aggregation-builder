package com.ey.digital.adc.aml.sql.server.repo;


import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ey.digital.adc.aml.model.Segment;
import com.ey.digital.adc.core.sql.server.repo.ISQLRepository;

@Repository
public interface SegmentRepository extends ISQLRepository<Segment, Integer>{
	
//	@Query(value = "SELECT * from Segment where focalentity=:focalEntity",nativeQuery=true)
	public List<Segment> findByFocalEntity(@Param("focalEntity") String focalEntity);
	
	@Query(value = "SELECT DISTINCT segmodelid FROM [segment] WHERE name = :name AND focalentity = :focalentity", nativeQuery = true)
	public Integer getSegmentModelIdBySegmentNameAndFocalEntity(@Param("name") String name, @Param("focalentity") String focalEntity);
	
}


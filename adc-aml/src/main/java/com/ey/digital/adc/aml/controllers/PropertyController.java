package com.ey.digital.adc.aml.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ey.digital.adc.aml.model.Property;
import com.ey.digital.adc.aml.sql.server.repo.PropertyRepository;
import com.ey.digital.adc.aml.ui.request.PropertyUIRequest;
import com.ey.digital.adc.core.controllers.AbstractController;
import com.ey.digital.adc.core.util.DateTimeUtils;

@RestController
@RequestMapping("/property")
public class PropertyController  extends AbstractController<PropertyUIRequest, Property> {

	private final Logger _logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private PropertyRepository propertyRepo;


	/**
	 * 
	 */
	public PropertyController(PropertyRepository repo) {
		super(repo);
		this.propertyRepo = repo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ey.digital.adc.producttrace.controllers.AbstractController#
	 * createInternal(int,
	 * com.ey.digital.adc.producttrace.client.messages.AbstractClientRequest)
	 */
	@Override
	public Property createInternal(int userId, PropertyUIRequest request) throws Exception {
			Property propertyReq=	request.getMessage();
			
			Property property = new Property();
			String currentDateTime = DateTimeUtils.getCurrentDateTime();
			property.setCreatedBy(userId);
			property.setCreatedDateTime(currentDateTime);
			property.setLastUpdatedBy(userId);
			property.setLastUpdatedDateTime(currentDateTime);
			property.setPageName(propertyReq.getPageName());
			property.setLabel(propertyReq.getLabel());
			property.setValue(propertyReq.getValue());
			propertyRepo.save(property);	
				
			return property;

	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ey.digital.adc.producttrace.controllers.AbstractController#
	 * updateInternal(int,
	 * com.ey.digital.adc.producttrace.client.messages.AbstractClientRequest)
	 */
//	@Override
//	public Property updateInternal(int userId, PropertyUIRequest request) throws Exception {
//		throw new UnsupportedOperationException("This operation is not supported");
//	}
	
	@Override
	public Property updateInternal(int userId, PropertyUIRequest request) throws Exception {
	
		Property property = request.getMessage();
		String token = request.getToken();
		Property updated;
	    
		_logger.info("processing update property request for token [{}]", token);
		_logger.info("processing update property request for property [{}]", property);
		try {
			
			userId = validateToken(token);
			
				String currentDateTime = DateTimeUtils.getCurrentDateTime();
				Property pr = propertyRepo.findPropertyWithId(property.getId());
				pr.setLastUpdatedBy(userId);
				pr.setLastUpdatedDateTime(currentDateTime);
				pr.setLabel(property.getLabel());
				pr.setLabel(property.getLabel());
				pr.setPageName(property.getLabel());
				pr.setPageName(property.getPageName());
				pr.setValue(property.getValue());
				updated = propertyRepo.save(pr);
				
				_logger.info("Updated row"+updated);
			
		} catch (Exception e) {
			throw e;
		}
		return updated;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ey.digital.adc.producttrace.controllers.AbstractController#
	 * getListInternal(int)
	 */
	@Override
	public List<Property> getListInternal(int listForId) {
		throw new UnsupportedOperationException("This operation is not supported");
	}
}
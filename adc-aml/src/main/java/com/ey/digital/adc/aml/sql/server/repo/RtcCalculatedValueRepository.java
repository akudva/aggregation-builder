package com.ey.digital.adc.aml.sql.server.repo;



import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ey.digital.adc.aml.model.RtcCalculatedValue;
import com.ey.digital.adc.core.sql.server.repo.ISQLRepository;

public interface RtcCalculatedValueRepository extends ISQLRepository<RtcCalculatedValue, Integer> {
	
	@Query(value = "SELECT * FROM RtcCalculatedValue where rtcid=:rtcid and calculatedValue is null ", nativeQuery=true)
	public List<RtcCalculatedValue> getThresholdsBySegment(@Param("rtcid") Integer rtcid);
	
	@Query(value = "SELECT * FROM RtcCalculatedValue where rtcid=:rtcid and calculatedValue is null ", nativeQuery=true)
	public List<RtcCalculatedValue> findEmptyCalculatedValuesByRtcid(@Param("rtcid") Integer rtcid);
	
	
}
/**
 * 
 */
package com.ey.digital.adc.core.sql.server.repo;

import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import com.ey.digital.adc.core.model.IModel;


/**
 * @author mulaysh
 * Date Created: 2016-12-08
 */
@NoRepositoryBean
public interface ISQLRepository<T extends IModel, ID extends Serializable> 
		extends JpaRepository<T, ID>{

}

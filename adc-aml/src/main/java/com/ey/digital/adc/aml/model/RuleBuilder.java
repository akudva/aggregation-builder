package com.ey.digital.adc.aml.model;

import lombok.Data;

@Data
public class RuleBuilder {

	private String alertDetailCreateQuery;
	private String alertDetailInsertQuery;
	private String alertTransactionQuery;
	
	@Override
	public String toString() {
		return "RuleBuilder [alertDetailCreateQuery=" + alertDetailCreateQuery + ", alertDetailInsertQuery=" + alertDetailInsertQuery
				+ ", alertTransactionQuery=" + alertTransactionQuery + "]";
	}
}

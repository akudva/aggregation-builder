package com.ey.digital.adc.aml.service;

import java.sql.SQLDataException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ey.digital.adc.aml.model.Condition;
import com.ey.digital.adc.aml.model.Rule;
import com.ey.digital.adc.aml.sql.server.repo.ConditionRepository;
import com.ey.digital.adc.core.util.DateTimeUtils;

@Service
public class ConditionService {

	private final Logger _logger = LoggerFactory.getLogger(this.getClass());

	private final ConditionRepository conditionRepository;
	private final AggregationWithTwoOperandsService aggregationWithTwoOperandsService;

	private Set<String> aggWithTwoOperandsSet = new HashSet<String>(Arrays.asList("difference", "divide", "add"));
	private Set<String> combineDatasetSet = new HashSet<String>(Arrays.asList("union", "and", "or"));
	private Set<String> operatorSet = new HashSet<String>(Arrays.asList("top", "bottom", ">", ">=", "<", "<=", "="));
	private Set<String> aggWithOneOperandSet = new HashSet<String>(
			Arrays.asList("sum", "max", "min", "count", "value"));
	private Map<String, String> hiveFunctions = new HashMap<String, String>();
	private int sequenceId;

	private final CombineDataSetService combineDataSetService;
	private final MasterOperatorService masterOperatorService;
	private final AggregationWithOneOperandService aggregationWithOneOperandService;

	private Map<String, String> operatorNames = new HashMap<String, String>();
	Query query = null;

	@Autowired
	public ConditionService(ConditionRepository conditionRepository,
			AggregationWithTwoOperandsService aggregationWithTwoOperandsService,
			CombineDataSetService combineDataSetService, MasterOperatorService masterOperatorService,
			AggregationWithOneOperandService aggregationWithOneOperandService) {
		this.conditionRepository = conditionRepository;
		this.aggregationWithTwoOperandsService = aggregationWithTwoOperandsService;
		this.combineDataSetService = combineDataSetService;
		this.masterOperatorService = masterOperatorService;
		this.aggregationWithOneOperandService = aggregationWithOneOperandService;

		this.initializeHiveFunctionMap();
		this.initializeOperatorNamesMap();
	}

	public void createCondition(Condition condition, Rule rule, String ruleUUID) {
		/**
		 * ================================================================================
		 * POST-ORDER TREE TRAVERSAL 
		 * ------------------------- 
		 * This sets all the leaf
		 * node attributes first before the parent node, hence allowing the parent node
		 * to access each child node's attributes such as: 1. resultTable 2.
		 * resultColumn These child node attributes allow the parent node to generate
		 * it's SQL statement and set the corresponding filters
		 * 
		 * ================================================================================
		 */

		// 1. Visit children nodes first
		List<Condition> children = condition.getChildren();

		if (children != null && !children.isEmpty()) {
			children.forEach(childCondition -> {
				createCondition(childCondition, rule, ruleUUID);
			});
		}

		// checking if the condition is valid
		if (condition.getType().equalsIgnoreCase("operator") || condition.getType().equalsIgnoreCase("logical")
				|| condition.getType().equalsIgnoreCase("aggregation")) {
			if (condition.getChildren() == null || condition.getChildren().size() == 0) {
				_logger.warn(String.format("Node %s should have children", condition.getType()));
				String operator = "";
				if (condition.getValue().getAggregation() != null)
					operator = condition.getValue().getAggregation();
				else if (condition.getValue().getLogical() != null)
					operator = condition.getValue().getLogical();
				else
					operator = condition.getValue().getOperator();
				throw new UnsupportedOperationException(
						String.format("The operator %s should have operands", operator));
			}
		}

		if (condition.getType().equalsIgnoreCase("data") || condition.getType().equalsIgnoreCase("thresholdlabel")
				|| condition.getType().equalsIgnoreCase("attribute")
				|| condition.getType().equalsIgnoreCase("commonaggregation")) {
			if (condition.getChildren() != null && condition.getChildren().size() > 0) {
				_logger.warn(String.format("Node %s should not have children", condition.getType()));
				throw new UnsupportedOperationException(
						String.format("The node %s should not have operands", condition.getType()));
			}
		}

		// 3. Set each child node's attributes
		String currentDateTime = DateTimeUtils.getCurrentDateTime();
		condition.setCreatedBy(rule.getLastUpdatedBy());
		condition.setCreatedDateTime(currentDateTime);
		condition.setLastUpdatedBy(rule.getLastUpdatedBy());
		condition.setLastUpdatedDateTime(currentDateTime);

		// Set sequence ID
		sequenceId = conditionRepository.getNextConditionSequenceValue();
		condition.setSequenceid(sequenceId); // TODO use sequence generator
		condition.setRuleuuid(ruleUUID);
	}

	@Transactional
	public boolean deleteCondition(String ruleId) throws SQLDataException {

		Integer delStatus = 0;
		try {
			delStatus = conditionRepository.deleteByRuleId(Integer.parseInt(ruleId));
		} catch (Exception e) {
			throw new SQLDataException("Deleting existing conditions failed");
		}
		if (delStatus > 0) {
			return true;
		}
		return false;
	}

	private void initializeHiveFunctionMap() {
		hiveFunctions.put("top", ">=");
		hiveFunctions.put("bottom", "<=");
		hiveFunctions.put("union", "union");
		hiveFunctions.put("sum", "SUM");
		hiveFunctions.put("divide", "/");
		hiveFunctions.put("difference", "-");
		hiveFunctions.put("add", "+");
		hiveFunctions.put("and", "and");
		hiveFunctions.put("value", "");
		hiveFunctions.put("count", "COUNT");
	}

	private void initializeOperatorNamesMap() {
		operatorNames.put(">=", "GTEQ");
		operatorNames.put("<=", "LTEQ");
		operatorNames.put(">", "GT");
		operatorNames.put("<", "LT");
		operatorNames.put("=", "EQ");
		operatorNames.put("top", "top");
		operatorNames.put("bottom", "bottom");
	}

	public void setConditionRuleIDs(Rule rule, String ruleuuid) {

		Integer ruleId = rule.getRuleId();

		List<Condition> conditions = conditionRepository.findByRuleuuid(ruleuuid);

		conditions.forEach(c -> {
			c.setRuleId(ruleId);
			conditionRepository.save(c);
		});

	}

	public void createQueries(Rule rule) {

		Integer ruleId = rule.getRuleId();
		List<Condition> conditions = conditionRepository.findByRuleId(rule.getRuleId());
		Integer rootConditionId = conditionRepository.getRootConditionIdByRuleId(ruleId);

		conditions.sort((Condition c1, Condition c2) -> c1.getSequenceid() - c2.getSequenceid());

		for (Condition c : conditions) {
			Integer id = c.getPrimaryconditionid();
			if (id % rootConditionId == 0)
				c.setIsRootCondition(true);
			c = createConditionQueries(c, rule);
			conditionRepository.save(c);
		}

		// Set threshold resultTable and resultColumn
		conditions.forEach(c -> {
			if (c.getType().equalsIgnoreCase("thresholdlabel") || c.getType().equalsIgnoreCase("commonaggregation")) {
				Integer parent = conditionRepository.findParentByPrimaryconditionid(c.getPrimaryconditionid());
				List<Condition> children = conditionRepository.findByParentId(parent);
				children.forEach(child -> {
					if (!child.getType().equalsIgnoreCase("thresholdlabel")
							|| c.getType().equalsIgnoreCase("commonaggregation")) {
						c.setResultTable(child.getResultTable());
						c.setResultColumn(child.getResultColumn());
					}
				});
				conditionRepository.save(c);
			}
		});
	}

	public Map<String, String> setAttributesByFocalEntity(String focalEntity) {

		Map<String, String> attributes = new HashMap<>();

		switch (focalEntity) {
		case "Account_Security":
			attributes.put("focalEntityColumn", "acct_key,security_key");
			attributes.put("segment", "acct_segment");
			attributes.put("risk", "acct_risk");
			attributes.put("segmentMappingTable", "acct_segment_mapping");
			break;

		case "Account":
			attributes.put("focalEntityColumn", "acct_key");
			attributes.put("segment", "acct_segment");
			attributes.put("risk", "acct_risk");
			attributes.put("segmentMappingTable", "acct_segment_mapping");
			break;

		case "Customer":
			attributes.put("focalEntityColumn", "party_key");
			attributes.put("segment", "party_segment");
			attributes.put("risk", "party_risk");
			attributes.put("segmentMappingTable", "party_segment_mapping");
			break;

		case "Transaction":
			attributes.put("focalEntityColumn", "tran_key");
			attributes.put("segment", "tran_segment");
			attributes.put("risk", "tran_risk");
			attributes.put("segmentMappingTable", "tran_segment_mapping");
			break;

		default:
			attributes.put("focalEntityColumn", "party_key");
			attributes.put("segment", "party_segment");
			attributes.put("risk", "party_risk");
			attributes.put("segmentMappingTable", "party_segment_mapping");
			break;
		}

		return attributes;

	}

	public Condition createConditionQueries(Condition condition, Rule rule) {

		String focalEntityColumn, segment, risk, segmentMappingTable;
		String focalEntity = rule.getFocalEntity();
		String hiveSchema = rule.getHiveSchema();
		String construct_type = rule.getConstructType();

		String comment = rule.getComment();
		Integer ruleId = rule.getRuleId();
		Integer sequenceId = condition.getSequenceid();
		String resultColumn = "";
		String resultTable = "";
		String hiveFunction = "";
		StringBuffer hiveSqlInsert = new StringBuffer();

		ArrayList<String> factTableArr = new ArrayList<String>(
				Arrays.asList("Settlement", "Trade", "Transaction", "settlement_view"));

		Map<String, String> attributes = setAttributesByFocalEntity(focalEntity);

		focalEntityColumn = attributes.get("focalEntityColumn");
		segment = attributes.get("segment");
		risk = attributes.get("risk");
		segmentMappingTable = attributes.get("segmentMappingTable");

		if (comment.equalsIgnoreCase("DA")) {
			segment = "default_segment";
			risk = "default_risk";
		}

		String type = condition.getType();

		switch (type) {
		case "attribute":
			resultColumn = condition.getValue().getAttribute().trim();
			resultTable = condition.getValue().getTable().trim();

			condition.setResultColumn(resultColumn);
			condition.setResultTable(resultTable);
			condition.setPlatformTable(resultTable);
			condition.setAlertResultSelectColumn(focalEntityColumn);

			// TODO: hardcoded joinkeys for now. Will need to change after discussion.
			// Probably a table in SQL server
			if (resultTable.equalsIgnoreCase("Market_Data")) {
				condition.setResultjoinkey("lcdb_dt,cusip_id"); // Use enums, arrays?
			} else if (resultTable.equalsIgnoreCase("Settlement")) {
				condition.setResultjoinkey("trade_dt,cusip_id");
			}
			break;

		case "data":
			condition.setAlertResultSelectColumn(focalEntityColumn);
			break;

		case "commonaggregation":
			condition.setResultData(condition.getValue().getData());
			condition.setAlertResultSelectColumn(focalEntityColumn);
			break;

		case "thresholdLabel":
			String threshold_label = condition.getValue().getThresholdLabel();
			
			condition.setAlertResultSelectColumn(focalEntityColumn);
			condition.setResultData(condition.getValue().getData());
			condition.setThreshold(true);
			condition.setThresholdLabel(threshold_label);
			break;

		case "aggregation":
			String aggregation = condition.getValue().getAggregation().trim().toLowerCase();

			hiveFunction = hiveFunctions.get(aggregation);

			resultColumn = aggregation + "_" + sequenceId;
			resultTable = "alert_" + ruleId + "_${exec_id}_" + aggregation + "_condition_" + sequenceId;

			condition.setResultColumn(resultColumn);
			condition.setResultTable(resultTable);

			if (aggWithTwoOperandsSet.contains(aggregation)) {
				hiveSqlInsert.append(aggregationWithTwoOperandsService.getHiveSqlForAggregationWithTwoOperands(
						condition, aggregation, hiveFunction, rule, focalEntity, focalEntityColumn, hiveSchema,
						segmentMappingTable, factTableArr));

			} else if (aggWithOneOperandSet.contains(aggregation)) {
				hiveSqlInsert.append(aggregationWithOneOperandService.getHiveSqlForAggregationWithOneOperand(condition,
						aggregation, hiveFunction, rule, focalEntity, focalEntityColumn, segment, risk, hiveSchema,
						segmentMappingTable));
			}
			break;

		case "logical":
			String logicalOperation = condition.getValue().getLogical().trim().toLowerCase();
			hiveFunction = hiveFunctions.get(logicalOperation);

			resultColumn = logicalOperation + "_" + sequenceId;
			resultTable = "alert_" + ruleId + "_${exec_id}_" + logicalOperation + "_condition_" + sequenceId;

			condition.setResultColumn(resultColumn);
			condition.setResultTable(resultTable);

			if (combineDatasetSet.contains(logicalOperation.toLowerCase())) {
				hiveSqlInsert.append(combineDataSetService.getHiveSqltoCombineDataSet(condition, logicalOperation,
						hiveFunction, rule, focalEntity, focalEntityColumn, hiveSchema));
			}
			break;

		case "operator":
			String operator = condition.getValue().getOperator().trim().toLowerCase();
			hiveFunction = hiveFunctions.get(operator);
			String operator_name = operatorNames.get(operator);

			resultColumn = operator_name + "_" + sequenceId;
			resultTable = "alert_" + ruleId + "_${exec_id}_" + operator_name + "_condition_" + sequenceId;

			condition.setResultColumn(resultColumn);
			condition.setResultTable(resultTable);

			if (operatorSet.contains(operator.toLowerCase())) {
				hiveSqlInsert.append(masterOperatorService.getHiveSqlToEvaluateMasterOperator(condition, operator,
						hiveFunction, rule, focalEntity, focalEntityColumn, hiveSchema));
			}
			break;

		default:
			throw new UnsupportedOperationException(String.format("The operator %s is not supported", type));
		}

		return condition;
	}
}
package com.ey.digital.adc.aml.sql.server.repo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import com.ey.digital.adc.aml.model.Page;

public class PaginationRepository<E> {

	@Autowired
	JdbcTemplate jdbcTemplate;
	
    @SuppressWarnings({ "unchecked", "rawtypes" })
	public Page<E> fetchPage(
            final JdbcTemplate jdbcTemplate,
            final String sqlCountRows,
            final String sqlFetchRows,
            // final Object args[],
            final Integer pageNo,
            final Integer pageSize,
            final RowMapper<E> rowMapper) {

        // determine how many rows are available
        final int rowCount = jdbcTemplate.queryForObject(sqlCountRows, new Object[]{}, Integer.class);

        // calculate the number of pages
        int pageCount = rowCount / pageSize;
        if (rowCount > pageSize * pageCount) {
            pageCount++;
        }

        // create the page object
        final Page<E> page = new Page<E>();
        page.setPageNumber(pageNo);
        page.setPagesAvailable(pageCount);

        // fetch a single page of results
        final int startRow = (pageNo - 1) * pageSize;
        jdbcTemplate.query(
                sqlFetchRows,
                new ResultSetExtractor() {
                    public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
                        final List pageItems = page.getPageItems();
                        int currentRow = 0;
                        while (rs.next() && currentRow <startRow + pageSize) {
                            if (currentRow >= startRow) {
                                pageItems.add(rowMapper.mapRow(rs, currentRow));
                            }
                            currentRow++;
                        }
                        return page;
                    }
                });
        return page;
    }

}

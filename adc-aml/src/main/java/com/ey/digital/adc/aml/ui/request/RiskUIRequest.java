package com.ey.digital.adc.aml.ui.request;

import com.ey.digital.adc.aml.model.Risk;
import com.ey.digital.adc.core.ui.request.AbstractUIRequest;


public class RiskUIRequest extends AbstractUIRequest<Risk> {
	private static final long serialVersionUID = 1L;
	
	
	@Override
	public String toString() {
		return "RiskUIRequest [message=" + getMessage() + ", toString()=" + super.toString() + "]";
	}

}
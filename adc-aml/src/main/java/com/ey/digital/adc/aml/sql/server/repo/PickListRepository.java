package com.ey.digital.adc.aml.sql.server.repo;


import com.ey.digital.adc.aml.model.PickList;
import com.ey.digital.adc.core.sql.server.repo.ISQLRepository;

public interface PickListRepository extends ISQLRepository<PickList, Integer>{

}


package com.ey.digital.adc.aml.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ey.digital.adc.core.model.IModel;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "tablejoin")
public class TableJoin implements IModel{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "table_lhs")
	private String table_lhs;
	
	@Column(name = "table_rhs")
	private String table_rhs;

	@Column(name = "joinkey_lhs")
	private String joinkey_lhs;
	
	@Column(name = "joinkey_rhs")
	private String joinkey_rhs;
	
	@Column(name = "toggle_table_position")
	private String toggle_table_position;
	

}


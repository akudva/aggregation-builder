package com.ey.digital.adc.aml.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OozieJobStatus {

	@Override
	public String toString() {
		return "OozieJobStatus [status=" + status + ", appName=" + appName + ", externalId=" + externalId + ", conf="
				+ conf + "]";
	}
	private String status;
	private String appName;
	private String externalId;
	private String conf;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getAppName() {
		return appName;
	}
	public void setAppName(String appName) {
		this.appName = appName;
	}
	public String getExternalId() {
		return externalId;
	}
	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}
	public String getConf() {
		return conf;
	}
	public void setConf(String conf) {
		this.conf = conf;
	}
}
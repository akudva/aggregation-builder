package com.ey.digital.adc.aml.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ey.digital.adc.core.model.IModel;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = false)
@Data
@Entity
@Table(name = "rtcthresholdsegmentrisk")
public class RtcThresholdSegmentRisk implements IModel {

	private static final long serialVersionUID = 1369129635458082496L;

	@Id
	@Column(name = "tsrid")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer tsrid;
	
	@Column(name = "rtcid")
	private Integer rtcid;

	@Column(name = "rtccalculatedvalueid")
	private Integer rtcCalculatedValueId;

	@Column(name = "segment")
	private String segmentName;

	@Column(name = "label")
	private String thresholdLabel;

	@Column(name = "calculatedtype")
	private String calculatedType;

	@Column(name = "calculatedfuncname")
	private String calculatedFuncName;

	@Column(name = "calculatedvalue")
	private Float calculatedValue;
	
	@Column(name = "scalar")
	private Double scalar;
	
	@Column(name = "finalvalue")
	private Double finalvalue;
	
	@Column(name = "risk")
	private String risk;
	
	@Column(name = "modelid")
	private Integer modelId;
	

	@Override
	public String toString() {
		return "RtcThresholdSegmentRisk [segmentname=" + segmentName + ",thresholdLabel=" + thresholdLabel
				+ ",calculatedType=" + calculatedType + ",calculatedFuncName=" + calculatedFuncName
				+ ",calculatedValue=" + calculatedValue + "]";
	}

}

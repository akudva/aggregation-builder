package com.ey.digital.adc.aml.sql.server.repo;

import com.ey.digital.adc.aml.model.FilterValue;
import com.ey.digital.adc.core.sql.server.repo.ISQLRepository;

public interface FilterValueRepository extends ISQLRepository<FilterValue, Integer>{

}

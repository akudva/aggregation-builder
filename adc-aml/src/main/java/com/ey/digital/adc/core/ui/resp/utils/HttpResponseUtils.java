/**
 * 
 */
package com.ey.digital.adc.core.ui.resp.utils;

import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;

/**
 * @author mulaysh
 * Date Created: 2017-01-06
 */
public class HttpResponseUtils {
	
	public static MultiValueMap<String, String> getMap(String message){
		MultiValueMap<String, String> map = new HttpHeaders();
		map.add("message", message);
		map.add( "Access-Control-Allow-Origin", "*");
		map.add("Access-Control-Expose-Headers","message");
		return map;
	}
	
	public static <T> ResponseEntity<List<T>> getResponse(List<T> list) {
		if (list == null) {
			return new ResponseEntity<List<T>>(HttpResponseUtils.getMap("Invalid token provided"),
					HttpStatus.BAD_REQUEST);
		} else if (list.size() == 0) {
			return new ResponseEntity<List<T>>(
					HttpResponseUtils.getMap("No data found"), HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<List<T>>(list,
				HttpResponseUtils.getMap(String.format("Found [%d] rows", list.size())),
				HttpStatus.OK);
	}
	
	public static <T> ResponseEntity<T> getResponse(HttpStatus status, String message, T object){
		return new ResponseEntity<T>(object, getMap(message), status);
	}
	
	public static <T> ResponseEntity<T> getResponse(T object){
		return new ResponseEntity<T>(HttpStatus.OK);
	}
	
	public static <T> ResponseEntity<T> getErrorResponse(HttpStatus status, String message){
		return new ResponseEntity<T>(getMap(message), status);
	}

	public static <T> ResponseEntity<T> getInvalidTokenErrorResponse(){
		return getErrorResponse(HttpStatus.BAD_REQUEST, "Invalid token provided");
	}
}

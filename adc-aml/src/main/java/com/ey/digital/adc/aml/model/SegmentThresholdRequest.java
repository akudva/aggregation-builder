package com.ey.digital.adc.aml.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"ruleId",
"segmentIds",
"isRiskTune",
"riskLevels",
"runDates"
})
public class SegmentThresholdRequest {

@JsonProperty("ruleId")
private String ruleId;
@JsonProperty("segmentIds")
private List<String> segmentIds = null;
@JsonProperty("isRiskTune")
private Boolean isRiskTune;
@JsonProperty("riskLevels")
private List<RiskLevel> riskLevels = null;
@JsonProperty("runDates")
private List<String> runDates = null;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

/**
* No args constructor for use in serialization
* 
*/
public SegmentThresholdRequest() {
}

/**
* 
* @param isRiskTune
* @param riskLevels
* @param segmentIds
* @param ruleId
* @param runDates
*/
public SegmentThresholdRequest(String ruleId, List<String> segmentIds, Boolean isRiskTune, List<RiskLevel> riskLevels, List<String> runDates) {
super();
this.ruleId = ruleId;
this.segmentIds = segmentIds;
this.isRiskTune = isRiskTune;
this.riskLevels = riskLevels;
this.runDates = runDates;
}

@JsonProperty("ruleId")
public String getRuleId() {
return ruleId;
}

@JsonProperty("ruleId")
public void setRuleId(String ruleId) {
this.ruleId = ruleId;
}

@JsonProperty("segmentIds")
public List<String> getSegmentIds() {
return segmentIds;
}

@JsonProperty("segmentIds")
public void setSegmentIds(List<String> segmentIds) {
this.segmentIds = segmentIds;
}

@JsonProperty("isRiskTune")
public Boolean getIsRiskTune() {
return isRiskTune;
}

@JsonProperty("isRiskTune")
public void setIsRiskTune(Boolean isRiskTune) {
this.isRiskTune = isRiskTune;
}

@JsonProperty("riskLevels")
public List<RiskLevel> getRiskLevels() {
return riskLevels;
}

@JsonProperty("riskLevels")
public void setRiskLevels(List<RiskLevel> riskLevels) {
this.riskLevels = riskLevels;
}

@JsonProperty("runDates")
public List<String> getRunDates() {
return runDates;
}

@JsonProperty("runDates")
public void setRunDates(List<String> runDates) {
this.runDates = runDates;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}
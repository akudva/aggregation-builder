package com.ey.digital.adc.aml.model;

import java.util.List;

public class ConditionMetaData {

	private List<Condition> conditionsList;
	
	public List<Condition> getConditionsList() {
		return conditionsList;
	}
	public void setConditionsList(List<Condition> conditionsList) {
		this.conditionsList = conditionsList;
	}
	@Override
	public String toString() {
		return "ConditionMetaData [ conditionsList=" + conditionsList + "]";
	}

}

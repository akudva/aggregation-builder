package com.ey.digital.adc.aml.model;

import com.ey.digital.adc.core.model.IModel;
import lombok.Data;

import javax.persistence.*;

@Table(name = "cmtexportresult")
@Entity
@Data
public class CmtExportResult implements IModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(name = "ruleexecutionresultid")
    private Integer executionId;
    @Enumerated(EnumType.STRING)
    private Status status;
    public enum Status {STARTED, COMPLETE}
}

package com.ey.digital.adc.aml.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import com.ey.digital.adc.core.model.Audit;
import com.ey.digital.adc.core.model.IModel;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "[rule]")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Rule extends Audit implements IModel{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ruleid")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer ruleId;

	@Column(name = "constructtype")
	private String constructType;
	
	@Column(name = "rulename")
	private String rulename;

	@Column(name = "description")
	private String description;

    @Column(name = "focalentity")
    private String focalEntity;

	@Column(name = "frequency")
	private String frequency;
	
//	segmodel will be set in Threshold Tuning phase
//    @OneToOne(targetEntity = Time.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//    @JoinColumn(name = "lookbackid")
    private Time lookback;

    @Column(name = "comment")
    private String comment;


    // segmodel will be set in Threshold Tuning phase
//	@Column(name = "activertcid")
//	private String activeRtcId;

//	? @ElementCollection
//	? @Column(name = "inclusionlist", columnDefinition = "blob")
//	Bidirectional relationship with inclusionfilter
	
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "ruleid")
	private List<InclusionFilter> inclusionlist = new ArrayList<>();
	
//	? @ElementCollection
//	? @Column(name = "exclusionlist", columnDefinition = "blob")
//	Bidirectional relationship with inclusionfilter
	
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "ruleid")
	private List<ExclusionFilter> exclusionlist = new ArrayList<>();

//	? @Column(columnDefinition = "blob")
//	Unidirectional relationship with Condition
	@OneToOne(targetEntity = Condition.class, cascade = CascadeType.ALL, fetch=FetchType.LAZY)
	@JoinColumn(name = "condition_id")
	private Condition condition; // Rule keeps track of main condition.

	@Column(name = "status")
	private String status;

	@Column(name = "clientid")
	private String clientId;
	
	@Column(name = "approvedby")
	@JsonIgnore
	private Integer approvedBy;

	@Column(name = "approveddtm")
	@JsonIgnore
	private String approvedDtm;

	@Column(name = "rejectedby")
	@JsonIgnore
	private Integer rejectedBy;

	@Column(name = "rejecteddtm")
	@JsonIgnore
	private String rejectedDtm;

	@Column(name = "approvalcomment")
	@JsonIgnore
	private String approvalComment;

	@Column(name = "rejectioncomment")
	@JsonIgnore
	private String rejectionComment;

	@Column(name = "alertdetailcreatequery")
	@JsonIgnore
	private String alertDetailCreateQuery;

	@Column(name = "alertdetailinsertquery")
	@JsonIgnore
	private String alertDetailInsertQuery;

	@Column(name = "alerttransactionquery")
	@JsonIgnore
	private String alertTransactionQuery;

	@Transient
	protected String clientName;

	@Transient
	protected String hiveSchema;

	@ManyToOne
	@JoinColumn(name = "clientid", insertable = false, updatable = false)
	@JsonIgnore
	private Client client;
	
	//@OneToMany(targetEntity = Segment.class,mappedBy="segmentid")
	@Transient
	private List<Segment> ruleSegments;
	
	@Transient
	private List<Risk> ruleRisks;
	
	@Transient
	private List<String> ruleThresholdLabels;

//	@OneToMany(cascade = CascadeType.ALL)
//	@JoinTable(name = "segment", joinColumns = @JoinColumn(name = "focalentity"),
//			inverseJoinColumns = @JoinColumn(name = "focalentity"))
//	private List<Segment> ruleSegments;
	
	@OneToMany(targetEntity = RuleThresholdConfig.class,mappedBy="ruleId")
	private List<RuleThresholdConfig> ruleThresholdConfig;

	 @Transient
	 private List<Integer> ruleIds;

	@Transient
	private String isApprove;

	public String getClientName() {
		if (client != null) {
			clientName = client.getName();
		}
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public void setHiveSchema(String hiveSchema) {
		this.hiveSchema = hiveSchema;
	}

	/**
	 * @return the clientName
	 */
	public String getHiveSchema() {
		if (client != null) {
			hiveSchema = client.getHiveSchema();
		}
		return hiveSchema;
	}
	
	
	// @OneToMany(targetEntity = SegmentRuleRiskMultiplier.class
	// , mappedBy = "rule")
	// private List<SegmentRuleRiskMultiplier> segRuleRiskMultList=new
	// ArrayList<>();

	@Override
	public String toString() {
		return "Rule [ruleId=" + ruleId + ", constructType=" + constructType + ", rulename=" + rulename
				+ ", description=" + description + ", focalEntity=" + focalEntity + ", lookback=" + getLookback()
				+ ", comment=" + comment + ", frequency=" + frequency + ", condition=" + getCondition() + ", status=" + status + ", clientId="
				+ clientId + ", approvedBy=" + approvedBy + ", approvedDtm=" + approvedDtm + ", rejectedBy="
				+ rejectedBy + ", rejectedDtm=" + rejectedDtm + ", clientName=" + clientName + ", client=" + client
				+ ", ruleSegments=" + ruleSegments + ", ruleRisks=" + ruleRisks + ", ruleThresholdLabels="
				+ ruleThresholdLabels + ", ruleThresholdConfig=" + getRuleThresholdConfig() + ", isApprove=" + isApprove
				+ "]";
	}

	public List<RuleThresholdConfig> getRuleThresholdConfig() {
		return ruleThresholdConfig;
	}

	public void setRuleThresholdConfig(List<RuleThresholdConfig> ruleThresholdConfig) {
		this.ruleThresholdConfig = ruleThresholdConfig;
	}
}

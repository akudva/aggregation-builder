package com.ey.digital.adc.aml.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.ey.digital.adc.core.model.Audit;
import com.ey.digital.adc.core.model.IModel;

import lombok.Data;

@Entity
@Table(name = "property")
@Data
public class Property extends Audit implements IModel {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@NotNull
	@Column(name = "pagename")
	private String pageName;
	
	@NotNull
	@Column(name = "label")
	private String label;
	
	@NotNull
	@Column(name = "value")
	private String value;
	
}

package com.ey.digital.adc.aml.controllers;

import com.ey.digital.adc.aml.model.RuleExecutionJob;
import com.ey.digital.adc.aml.service.RuleExecutionJobService;
import com.ey.digital.adc.aml.sql.server.repo.RuleExecutionJobRepository;
import com.ey.digital.adc.aml.ui.request.DeactivateJobUIRequest;
import com.ey.digital.adc.aml.ui.request.RuleExecutionJobUIRequest;
import com.ey.digital.adc.core.controllers.AbstractController;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.logging.Logger;

@RestController
@RequestMapping("/ruleExecution")
public class RuleExecutionJobController extends AbstractController<RuleExecutionJobUIRequest, RuleExecutionJob> {

    private final static Logger LOGGER = Logger.getLogger(RuleExecutionJobController.class.getName());
    @Autowired
    RuleExecutionJobService ruleExecutionJobService;
    @Autowired
    RuleExecutionJobRepository ruleExecutionJobRepository;

    public RuleExecutionJobController(RuleExecutionJobRepository repo) {
        super(repo);
        ruleExecutionJobRepository = repo;
    }

    @PostMapping(path = "/deactivate")
    public void deactivateRules(@RequestBody DeactivateJobUIRequest deactivateJobUIRequest) {
        List<RuleExecutionJob> jobs = ruleExecutionJobRepository.findAll(deactivateJobUIRequest.getMessage());
        for (RuleExecutionJob job : jobs) {
            job.setStatus(RuleExecutionJob.JobStatus.inactive);
        }
        String token = deactivateJobUIRequest.getToken();
        ruleExecutionJobRepository.save(jobs);
        ruleExecutionJobRepository.flush();
    }

    @GetMapping(params = {"token", "ruleId"})
    public List<RuleExecutionJob> getJobsByRuleId(@Valid @NotEmpty(message = "token may not be Empty/Null") @RequestParam(value = "token") String token, @RequestParam("ruleId") Integer ruleId) {
        return ruleExecutionJobService.getJobsByRuleId(ruleId);
    }

    @Override
    public RuleExecutionJob createInternal(int userId, RuleExecutionJobUIRequest request) throws Exception {
        return ruleExecutionJobService.createOrUpdate(request.getMessage());
    }

    @Override
    public RuleExecutionJob updateInternal(int userId, RuleExecutionJobUIRequest request) throws Exception {
        return null;
    }

    @Override
    public List<RuleExecutionJob> getListInternal(int listForId) {
        return null;
    }

}


package com.ey.digital.adc.aml.sql.server.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ey.digital.adc.aml.model.Segment;
import com.ey.digital.adc.aml.model.ThresholdMatrixSegment;
import com.ey.digital.adc.core.sql.server.repo.ISQLRepository;

@Repository
public interface SegmentThresholdRepository extends ISQLRepository<ThresholdMatrixSegment, Integer>{
	
}
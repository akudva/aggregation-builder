package com.ey.digital.adc.aml.service;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ey.digital.adc.aml.model.Condition;
import com.ey.digital.adc.aml.model.ExclusionFilter;
import com.ey.digital.adc.aml.model.Filter;
import com.ey.digital.adc.aml.model.InclusionFilter;
import com.ey.digital.adc.aml.model.LookBackRef;
import com.ey.digital.adc.aml.model.Rule;
import com.ey.digital.adc.aml.sql.server.repo.LookBackRefRepository;
import com.ey.digital.adc.aml.sql.server.repo.TableJoinRepository;
import com.ey.digital.adc.aml.util.FocalEntityUtil;

@Service
public class AggregationWithTwoOperandsService {

	private final Logger _logger = LoggerFactory.getLogger(this.getClass());

	private final TableJoinRepository tableJoinRepo;

	private final TableJoinService tableJoinService;

	private final FilterService filterService;

	private final InclusionFilterService inclusionFilterService;

	private final ExclusionFilterService exclusionFilterService;

	@Autowired
	private LookBackRefRepository lookBackRepo;

	@Autowired
	public AggregationWithTwoOperandsService(TableJoinRepository tableJoinRepo, TableJoinService tableJoinService,
			FilterService filterService, InclusionFilterService inclusionFilterService,
			ExclusionFilterService exclusionFilterService) {

		this.tableJoinRepo = tableJoinRepo;
		this.tableJoinService = tableJoinService;
		this.filterService = filterService;
		this.inclusionFilterService = inclusionFilterService;
		this.exclusionFilterService = exclusionFilterService;
	}

	public String getHiveSqlForAggregationWithTwoOperands(Condition condition, String aggregation, String hiveFunction,
			Rule rule, String focalEntity, String focalEntityColumn, String hiveSchema, String segmentMappingTable,
			ArrayList<String> factTableArr) {

		Integer ruleId = rule.getRuleId();
		Integer sequenceId = condition.getSequenceid();

		StringBuffer hiveAlertInsertSQL = new StringBuffer();
		StringBuffer hiveAlertCreateSQL = new StringBuffer();
		StringBuffer hiveAlertTxnInsertSQL = new StringBuffer();

		StringBuffer filterclause = new StringBuffer();
		StringBuffer inclusionClause = new StringBuffer();
		StringBuffer exclusionClause = new StringBuffer();
		StringBuffer masterLookbackWhereClause = new StringBuffer();
		StringBuffer txnCreateOrInsertClause = new StringBuffer();

		ArrayList<Character> alias = new ArrayList<Character>();
		alias.add('a');

		ArrayList<String> tableArr = new ArrayList<String>();

		// Get source table and column params for 1st child
		Condition child1 = condition.getChildren().get(0);
		String sourceColumnParam1 = child1.getResultColumn();
		String sourceTableParam1 = child1.getResultTable();
		String sourceDetailTable = child1.getTxnDetailTable();

		if (sourceDetailTable == null) {
			sourceDetailTable = hiveSchema + "." + sourceTableParam1; // TODO throw an error
		}

		// Get source table and column params for 2nd child
		Condition child2 = condition.getChildren().get(1);
		String sourceColumnParam2 = child2.getResultColumn();
		String sourceTableParam2 = child2.getResultTable();

		StringBuffer alertResultSelectColumn = new StringBuffer();
		StringBuffer fromClause = new StringBuffer();
		StringBuffer joinCondition = new StringBuffer();

		String alertTable = hiveSchema + ".alert_" + ruleId + "_${exec_id}_" + aggregation + "_condition_" + sequenceId;
		String alertTxnTable = alertTable + "_transaction";
		Character tableAlias = alias.get(0);
		
		String focalEntityConcatClause = FocalEntityUtil.getFocalEntityConcatClause(focalEntityColumn, tableAlias);
		String focalEntityClause = FocalEntityUtil.getFocalEntityColumn(focalEntity,focalEntityColumn, tableAlias);

		// =======================================================================
		if (condition.isRootCondition())
			txnCreateOrInsertClause.append("INSERT INTO " + hiveSchema + ".final_alert_table ").append(
					"(exec_date,alert_date,alert_id,ruleid,tran_key,focal_entity,focus_key_1,focus_key_2,executionid,party_key) "
					+ "SELECT to_date(from_unixtime(unix_timestamp())) as exec_date, '${run_date}' as alert_date, "
							+ "b.alert_id, " + ruleId + " as ruleid, " + tableAlias + ".tran_key as tran_key, " + "'"
							+ focalEntity + "' as focal_Entity, "
							+ FocalEntityUtil.getAlertFocalEntityColumn(focalEntityColumn, tableAlias) +","
							+ "CAST(SUBSTRING('${exec_id}',0,length('${exec_id}')-1) AS INT) as executionid "
							); // Adds
																											// focus_key
																											// aliases
		else
			txnCreateOrInsertClause.append(" CREATE TABLE IF NOT EXISTS " + alertTxnTable + " AS ").append(
					"SELECT to_date(from_unixtime(unix_timestamp())) as exec_date, '${run_date}' as alert_date, "
							+ "b.alert_id, " + ruleId + " as ruleid, " + tableAlias + ".tran_key as tran_key, " + "'"
							+ focalEntity + "' as focal_Entity, "
							+ "CAST(SUBSTRING('${exec_id}',0,length('${exec_id}')-1) AS INT) as executionid, "
							+ focalEntityClause);

		hiveAlertTxnInsertSQL.append(txnCreateOrInsertClause);

		// =======================================================================

		// If sourceTableParam2 is null, it means child2 condition is a "data" type
		// condition
		// Set sourceTableParam2=sourceTableParam1 so that aggregation can be done on
		// the first argument's table
		if (sourceTableParam2 == null) {
			sourceTableParam2 = sourceTableParam1;
		}

		
		if (sourceTableParam1.equalsIgnoreCase(sourceTableParam2)) { // No need to join

			String joinkeyLHS = child1.getResultjoinkey();
			condition.setResultjoinkey(joinkeyLHS);
			condition.setPlatformTable(sourceTableParam1);

			hiveAlertInsertSQL.append(" CREATE TABLE if not exists " + alertTable
					+ " AS SELECT to_date(from_unixtime(unix_timestamp())) as exec_date, '${run_date}' as alert_date, "
					+ " CAST(concat(" + tableAlias + ".party_key, " + 
							"concat(" + focalEntityConcatClause+ 
							", SUBSTRING('${exec_id}',0,length('${exec_id}')-1) )) AS BIGINT) as alert_id, " + "'alert_" + ruleId + "_${exec_id}' as alert_exec_id, ("
							+ alias.get(0) + "." + sourceColumnParam1 + hiveFunction);

			if (sourceColumnParam2 == null) {
				hiveAlertInsertSQL.append(Double.valueOf(child2.getValue().getData()) + ") as " + aggregation + "_"
						+ sequenceId + "," + alias.get(0) + "." + sourceColumnParam1 + ",'"
						+ child2.getValue().getData() + "' as data_" + child2.getSequenceid());

			} else {
				hiveAlertInsertSQL
						.append(alias.get(0) + "." + sourceColumnParam2 + ") as " + aggregation + "_" + sequenceId);
			}

			hiveAlertInsertSQL.append(", '" + focalEntity + "' as focal_Entity, "
					+  FocalEntityUtil.getFocalEntityColumn(focalEntityColumn, tableAlias));

			alertResultSelectColumn.append("alert_exec_id,focal_Entity," + focalEntityColumn);

			if (!focalEntity.equalsIgnoreCase("customer")) {
				hiveAlertInsertSQL.append(", " + tableAlias + "." + "party_key");
				hiveAlertTxnInsertSQL.append(", " + tableAlias + "." + "party_key");
				alertResultSelectColumn.append(", party_key");
			} else if (condition.isRootCondition())
				hiveAlertTxnInsertSQL.append(", " + tableAlias + ".party_key ");

			fromClause.append(hiveSchema + "." + sourceTableParam1 + " " + alias.get(0) + " ");

			tableArr.add(sourceTableParam1);

			hiveAlertTxnInsertSQL.append(" FROM " + sourceDetailTable + " " + tableAlias + " join " + alertTable + " b"
					+ " ON " + FocalEntityUtil.getJoinConditionFocalEntityColumn(focalEntityColumn, 'a', 'b'));

			List<Filter> filters = child1.getValue().getFilters();

			if (filters != null && !filters.isEmpty())
				filters.forEach(f -> {
					filterclause.append(filterService.createFilterSQL(f, rule, condition, tableArr, alias,
							joinCondition, fromClause, factTableArr, hiveSchema)).append(" and ");
				});
			if (filterclause != null && filterclause.length() > 0)
				filterclause.setLength(filterclause.length() - 5);

			// Call inclusion filter service
			List<InclusionFilter> inclusionFilters = rule.getInclusionlist();
			_logger.info("inclusionfilters size =" + inclusionFilters.size());
			if (inclusionFilters != null && !inclusionFilters.isEmpty())
				inclusionFilters.forEach(f -> {
					_logger.info("inclusionfilters - check if type is an  attribute ="
							+ f.getChildren().get(0).getType().equalsIgnoreCase("attribute"));
					if (f.getChildren().get(0).getType().equalsIgnoreCase("attribute")
							&& tableArr.contains(f.getChildren().get(0).getValue().getTable())) {
						inclusionClause.append(inclusionFilterService.createInclusionFilterSQL(f, rule, condition,
								tableArr, alias, joinCondition, fromClause, factTableArr)).append(" and ");
					}
				});
			if (inclusionClause != null && inclusionClause.length() > 0)
				inclusionClause.setLength(inclusionClause.length() - 5);

			// Call exclusion filter service
			List<ExclusionFilter> exclusionFilters = rule.getExclusionlist();

			if (exclusionFilters != null && !exclusionFilters.isEmpty())
				exclusionFilters.forEach(f -> {
					_logger.info("exclusionfilters - check if type is an  attribute ="
							+ f.getChildren().get(0).getType().equalsIgnoreCase("attribute"));
					if (f.getChildren().get(0).getType().equalsIgnoreCase("attribute")
							&& tableArr.contains(f.getChildren().get(0).getValue().getTable())) {
						exclusionClause.append(exclusionFilterService.createExclusionFilterSQL(f, rule, condition,
								tableArr, alias, joinCondition, fromClause, factTableArr)).append(" and ");
					}
				});
			if (exclusionClause != null && exclusionClause.length() > 0)
				exclusionClause.setLength(exclusionClause.length() - 5);

			hiveAlertInsertSQL.append(" FROM " + fromClause);

		} else if (!sourceTableParam1.equalsIgnoreCase(sourceTableParam2)) {

			// *******************************************************************************************************
			// Change FocalEntityUtil.getFocalEntityColumn to
			// tableJoinService.getSelectColumnsWithoutDuplicates ????

			/**
			 * TODO: 1. conditionally add party_key based on focal entity 2. Add txnDetail
			 * SQLs
			 */
			tableArr.add(sourceTableParam1);
			tableArr.add(sourceTableParam2);
			alias.add(new Character((char) (alias.get(alias.size() - 1).charValue() + 1)));
			String isToggle = tableJoinRepo.isToggleTablePosition(sourceTableParam1, sourceTableParam2);

			/*
			 * For join conditions, only Tables such as Trade, Settlement or Transactions
			 * should be on the LHS of the join if one of the above is on the RHS, then
			 * there is a need to toggle table positions
			 * 
			 * Check if isToggle is Y or N if isToggle=N then toggle lhs and rhs table
			 * positions if isToggle=Y then do not toggle table positions if isToggle=null
			 * then one(or both) tables do not exist in the database as they are not the
			 * primary tables
			 */
			if (isToggle == null) {
				// Enter here if sourceTableParam1 or sourceTableParam2 does not
				// exist in the
				// tableJoin mapping table

				hiveAlertInsertSQL.append(" CREATE TABLE if not exists " + alertTable
						+ " AS SELECT to_date(from_unixtime(unix_timestamp())) as exec_date, '${run_date}' as alert_date, "
						+ " CAST(concat("+tableAlias+ ".party_key, concat("+ focalEntityConcatClause + ", SUBSTRING('${exec_id}',0,length('${exec_id}')-1))) AS BIGINT) as alert_id, " +
						"'alert_" + ruleId
						+ "_${exec_id}' as alert_exec_id, (" + alias.get(0) + "." + sourceColumnParam1 + hiveFunction
						+ alias.get(1) + "." + sourceColumnParam2 + ") as " + aggregation + "_" + sequenceId + ","
						+ alias.get(0) + "." + sourceColumnParam1 + "," + alias.get(1) + "." + sourceColumnParam2 + ",'"
						+ focalEntity + "' as focal_Entity, "
						+  FocalEntityUtil.getFocalEntityColumn(focalEntityColumn, tableAlias));

				alertResultSelectColumn.append("alert_exec_id,focal_Entity," + focalEntityColumn);

				if (!focalEntity.equalsIgnoreCase("customer")) {
					hiveAlertInsertSQL.append(", " + tableAlias + "." + "party_key");
					alertResultSelectColumn.append(", party_key");
					hiveAlertTxnInsertSQL.append(", " + tableAlias + "." + "party_key");
				} else if (condition.isRootCondition())
					hiveAlertTxnInsertSQL.append(", " + tableAlias + ".party_key ");

				String joinkeyLHS = child1.getResultjoinkey();
				String joinkeyRHS = child2.getResultjoinkey();

				/*
				 * Since one of the two source tables is not a primary table, use the
				 * resultJoinKey of the left and right child conditions as join keys
				 */
				joinCondition.append(tableJoinService.getJoinConditionForIntermediateTables(joinkeyLHS, joinkeyRHS,
						alias.get(0), alias.get(1), focalEntityColumn)); // alias.get(0) , alias.get(1)

				fromClause.append(hiveSchema + "." + sourceTableParam1 + " " + alias.get(0) + " join " + hiveSchema
						+ "." + sourceTableParam2 + " " + alias.get(1));
				condition.setResultjoinkey(joinkeyLHS);
				condition.setPlatformTable(sourceTableParam1);

				hiveAlertTxnInsertSQL
						.append(" FROM " + sourceDetailTable + " " + tableAlias + " join " + alertTable + " b" + " ON "
								+ FocalEntityUtil.getJoinConditionFocalEntityColumn(focalEntityColumn, 'a', 'b'));

			} else {

				if (isToggle.equalsIgnoreCase("Y")) {
					// Swap resultTableParam1 and resultTableParam2
					sourceTableParam1 = child2.getResultTable().trim();

					sourceTableParam2 = child1.getResultTable().trim();

					/*
					 * Since the tow tables are primary tables, use the TableJoin mapping table to
					 * identify left and right join keys
					 */
					joinCondition.append(tableJoinService.getJoinConditionForDataModelTables(sourceTableParam1,
							sourceTableParam2, new Character('a'), new Character('b')));

					condition.setResultjoinkey(tableJoinService.getResultJoinKey(sourceTableParam1, sourceTableParam2));
					condition.setPlatformTable(sourceTableParam1);

					hiveAlertInsertSQL.append(" CREATE TABLE if not exists " + alertTable
							+ " AS SELECT to_date(from_unixtime(unix_timestamp())) as exec_date, '${run_date}' as alert_date, "
							+ " CAST(concat("+tableAlias+ ".party_key, concat("+ focalEntityConcatClause + ", SUBSTRING('${exec_id}',0,length('${exec_id}')-1))) AS BIGINT) as alert_id, " +
							"'alert_" + ruleId
							+ "_${exec_id}' as alert_exec_id, (" + alias.get(1) + "." + sourceColumnParam1
							+ hiveFunction + alias.get(0) + "." + sourceColumnParam2 + ") as " + aggregation + "_"
							+ sequenceId + "," + alias.get(1) + "." + sourceColumnParam1 + "," + alias.get(0) + "."
							+ sourceColumnParam2 + ",'" + focalEntity + "' as focal_Entity, "
							+  FocalEntityUtil.getFocalEntityColumn(focalEntityColumn, tableAlias) + ","
							+ alias.get(0));

					alertResultSelectColumn.append("alert_exec_id,focal_Entity," + focalEntityColumn);

					if (!focalEntity.equalsIgnoreCase("customer")) {
						hiveAlertInsertSQL.append(", " + tableAlias + "." + "party_key");
						alertResultSelectColumn.append(", party_key");
						hiveAlertTxnInsertSQL.append(", " + tableAlias + "." + "party_key");
					} else if (condition.isRootCondition())
						hiveAlertTxnInsertSQL.append(", " + tableAlias + ".party_key ");

					fromClause.append(hiveSchema + "." + sourceTableParam1 + " " + alias.get(0) + " join " + hiveSchema
							+ "." + sourceTableParam2 + " " + alias.get(1));

					hiveAlertTxnInsertSQL.append(
							" FROM " + sourceDetailTable + " " + tableAlias + " join " + alertTable + " b" + " ON "
									+ FocalEntityUtil.getJoinConditionFocalEntityColumn(focalEntityColumn, 'a', 'b'));
				} else {
					hiveAlertInsertSQL.append(" CREATE TABLE if not exists " + alertTable
							+ " AS SELECT to_date(from_unixtime(unix_timestamp())) as exec_date, '${run_date}' as alert_date, "
							+ " CAST(concat("+tableAlias+ ".party_key, concat("+ focalEntityConcatClause + ", SUBSTRING('${exec_id}',0,length('${exec_id}')-1))) AS BIGINT) as alert_id, " +
							"'alert_" + ruleId
							+ "_${exec_id}' as alert_exec_id, (" + alias.get(0) + "." + sourceColumnParam1
							+ hiveFunction + alias.get(1) + "." + sourceColumnParam2 + ") as " + aggregation + "_"
							+ sequenceId + ", '" + focalEntity + "' as focal_Entity, "
							+ FocalEntityUtil.getFocalEntityColumn(focalEntityColumn, tableAlias)  + ","
							+ alias.get(0));

					alertResultSelectColumn.append("alert_exec_id,focal_Entity," + focalEntityColumn);

					if (!focalEntity.equalsIgnoreCase("customer")) {
						hiveAlertInsertSQL.append(", " + tableAlias + "." + "party_key");
						alertResultSelectColumn.append(", party_key");
						hiveAlertTxnInsertSQL.append(", " + tableAlias + "." + "party_key");
					} else if (condition.isRootCondition())
						hiveAlertTxnInsertSQL.append(", " + tableAlias + ".party_key ");

					joinCondition.append(tableJoinService.getJoinConditionForDataModelTables(sourceTableParam1,
							sourceTableParam2, new Character('a'), new Character('b')));

					condition.setResultjoinkey(tableJoinService.getResultJoinKey(sourceTableParam1, sourceTableParam2));
					condition.setPlatformTable(sourceTableParam1);

					fromClause.append(hiveSchema + "." + sourceTableParam1 + " " + alias.get(0) + " join " + hiveSchema
							+ "." + sourceTableParam2 + " " + alias.get(1));

					hiveAlertTxnInsertSQL.append(
							" FROM " + sourceDetailTable + " " + tableAlias + " join " + alertTable + " b" + " ON "
									+ FocalEntityUtil.getJoinConditionFocalEntityColumn(focalEntityColumn, 'a', 'b'));
				}

			}

			// At the aggregation condition, check the filters of child1 and child2
			// conditions
			List<Filter> filters1 = child1.getValue().getFilters();

			if (filters1 != null && !filters1.isEmpty())
				filters1.forEach(f -> {
					filterclause.append(filterService.createFilterSQL(f, rule, condition, tableArr, alias,
							joinCondition, fromClause, factTableArr, hiveSchema)).append(" and ");
				});

			List<Filter> filters2 = child2.getValue().getFilters();

			if (filters2 != null && !filters2.isEmpty())
				filters2.forEach(f -> {
					filterclause.append(filterService.createFilterSQL(f, rule, condition, tableArr, alias,
							joinCondition, fromClause, factTableArr, hiveSchema)).append(" and ");
				});

			if (filterclause != null && filterclause.length() > 0)
				filterclause.setLength(filterclause.length() - 5);

			List<InclusionFilter> inclusionFilters = rule.getInclusionlist();
			if (inclusionFilters != null && !inclusionFilters.isEmpty())
				inclusionFilters.forEach(f -> {
					_logger.info("inclusionfilters - check if type is an  attribute ="
							+ f.getChildren().get(0).getType().equalsIgnoreCase("attribute"));
					if (f.getChildren().get(0).getType().equalsIgnoreCase("attribute")
							&& tableArr.contains(f.getChildren().get(0).getValue().getTable())) {
						inclusionClause.append(inclusionFilterService.createInclusionFilterSQL(f, rule, condition,
								tableArr, alias, joinCondition, fromClause, factTableArr)).append(" and ");
					}
				});
			if (inclusionClause != null && inclusionClause.length() > 0)
				inclusionClause.setLength(inclusionClause.length() - 5);

			// Call exclusion filter service
			List<ExclusionFilter> exclusionFilters = rule.getExclusionlist();

			if (exclusionFilters != null && !exclusionFilters.isEmpty())
				exclusionFilters.forEach(f -> {
					_logger.info("exclusionfilters - check if type is an  attribute ="
							+ f.getChildren().get(0).getType().equalsIgnoreCase("attribute"));
					if (f.getChildren().get(0).getType().equalsIgnoreCase("attribute")
							&& tableArr.contains(f.getChildren().get(0).getValue().getTable())) {
						exclusionClause.append(exclusionFilterService.createExclusionFilterSQL(f, rule, condition,
								tableArr, alias, joinCondition, fromClause, factTableArr)).append(" and ");
					}
				});
			if (exclusionClause != null && exclusionClause.length() > 0)
				exclusionClause.setLength(exclusionClause.length() - 5);

			hiveAlertInsertSQL
					.append(" FROM " + fromClause + " ON " + joinCondition);

		}

		if (filterclause.length() > 0 || inclusionClause.length() > 0 || exclusionClause.length() > 0) {
			hiveAlertInsertSQL.append(" WHERE ");
			hiveAlertTxnInsertSQL.append(" WHERE ");
		}

		if (factTableArr.contains(sourceTableParam1) || factTableArr.contains(sourceTableParam2)) {

			LookBackRef lookBackRef = lookBackRepo.findBytablename(sourceTableParam1);
			if (lookBackRef != null) {
				LookBackRef.LookBackUnit lookBackUnit = lookBackRef.getLookbackunit();

				switch (lookBackUnit.toString()) {
				case "Daily":
					masterLookbackWhereClause.append(
							" " + alias.get(0) + "." + lookBackRef.getDatecolumn() + " > " + "'${lookback_date}' AND "
									+ alias.get(0) + "." + lookBackRef.getDatecolumn() + " <= " + "'${run_date}'");
					break;
				case "Monthly":
					masterLookbackWhereClause.append(" " + alias.get(0) + "." + lookBackRef.getDatecolumn() + " > "
							+ "substr(" + "'${lookback_date}'" + ",1,7) " + "AND " + alias.get(0) + "."
							+ lookBackRef.getDatecolumn() + " <= " + "substr(" + "'${run_date}' " + ",1,7)");
					break;
				case "Yearly":
					masterLookbackWhereClause.append(" " + alias.get(0) + "." + lookBackRef.getDatecolumn() + " > "
							+ "YEAR(" + "'${lookback_date}'" + ") AND " + alias.get(0) + "."
							+ lookBackRef.getDatecolumn() + " <= " + "YEAR(" + "'${run_date}'" + ") ");
					break;
				}
			}
		}

		hiveAlertInsertSQL.append(filterclause);
		hiveAlertTxnInsertSQL.append(filterclause);

		if (filterclause.length() > 0 && masterLookbackWhereClause.length() > 0) {
			hiveAlertInsertSQL.append(" AND " + masterLookbackWhereClause);
			hiveAlertTxnInsertSQL.append(" AND " + masterLookbackWhereClause);
		} else if (filterclause.length() <= 0 && masterLookbackWhereClause.length() > 0) {
			hiveAlertInsertSQL.append(masterLookbackWhereClause);
			hiveAlertTxnInsertSQL.append(masterLookbackWhereClause);
		}

		// append inclusion filter clause if it is not empty
		if (inclusionClause.length() > 0) {
			hiveAlertInsertSQL.append(" AND " + inclusionClause);
			hiveAlertTxnInsertSQL.append(" AND " + inclusionClause);
		}

		// append exclusion filter clause if it is not empty
		if (exclusionClause.length() > 0) {
			hiveAlertInsertSQL.append(" AND " + exclusionClause);
			hiveAlertTxnInsertSQL.append(" AND " + exclusionClause);
		}

		// Generate Create table statement
		String[] alertResultSelectColumnArray = alertResultSelectColumn.toString().split(Pattern.quote(","));
		hiveAlertCreateSQL.append("CREATE TABLE if not exists " + alertTable + " ( ");
		for (int i = 0; i < alertResultSelectColumnArray.length; i++) {
			if (i == alertResultSelectColumnArray.length - 1) {
				hiveAlertCreateSQL.append(alertResultSelectColumnArray[i] + " String)");
			} else {
				hiveAlertCreateSQL.append(alertResultSelectColumnArray[i] + " String,");
			}
		}

		condition.setResultColumn(aggregation + "_" + sequenceId);
		condition.setAlertResultSelectColumn(alertResultSelectColumn.toString());
		condition.setAlertInsertSql(hiveAlertInsertSQL.toString());
		condition.setAlertTranInsertSql(hiveAlertTxnInsertSQL.toString());
		condition.setTxnDetailTable(alertTxnTable);

		_logger.info(hiveAlertInsertSQL.toString());
		_logger.info(hiveAlertTxnInsertSQL.toString());

		return hiveAlertInsertSQL.toString();
	}
}

package com.ey.digital.adc.aml.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.ey.digital.adc.core.model.Audit;
import com.ey.digital.adc.core.model.IModel;

import lombok.Data;

@Data
public class ThresholdMetadata {
	
	private String label;
	private String type;
	//private Value value;
	//private ThresholdValue thresholdValue;
	private ThresholdValue value;
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public ThresholdValue getvalue() {
		return value;
	}
	public void setThresholdValue(ThresholdValue value) {
		this.value = value;
	}

}
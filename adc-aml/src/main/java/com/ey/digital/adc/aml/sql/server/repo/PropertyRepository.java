package com.ey.digital.adc.aml.sql.server.repo;


import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ey.digital.adc.aml.model.Property;
import com.ey.digital.adc.core.sql.server.repo.ISQLRepository;

public interface PropertyRepository extends ISQLRepository<Property, Integer>{
	
	@Query(value = "SELECT * from Property where id= :id" , nativeQuery = true)
	Property findPropertyWithId( @Param("id") Integer id);
	
	@Modifying
	@Transactional
	@Query(value = "UPDATE Property SET pagename = :pagename, label = :label, value = :value where id= :id" , nativeQuery = true)
	void updateProperty( @Param("pagename") String pagename,@Param("label") String label, @Param("value") String value, @Param("id") Integer id);

}


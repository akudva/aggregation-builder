package com.ey.digital.adc.aml.sql.server.repo;



import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ey.digital.adc.aml.model.Rule;
import com.ey.digital.adc.aml.model.RuleThresholdConfig;
import com.ey.digital.adc.aml.model.RuleThresholdQuery;
import com.ey.digital.adc.core.sql.server.repo.ISQLRepository;




public interface RuleThresholdQueryRepo extends ISQLRepository<RuleThresholdQuery, Integer> {
	
	@Query(value = "SELECT * FROM RuleThresholdQuery where ruleid=:ruleid and rtcid=:rtcid and  executionid=:texecid order by sequenceid, querytype", nativeQuery=true)
	public List<RuleThresholdQuery> getThresholdQueries(@Param("ruleid") Integer ruleid,@Param("rtcid") Integer rtcid,@Param("texecid") String texecid);
	
	//@Query(value = "SELECT * FROM RuleThresholdQuery where ruleid=:ruleid and rtcid=:rtcid and execid=:texecid order by sequenceid", nativeQuery=true)
	//public List<RuleThresholdQuery> getThresholdQueries(@Param("ruleid") Integer ruleid,@Param("rtcid") Integer rtcid);

	
}


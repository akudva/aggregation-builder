package com.ey.digital.adc.aml.controllers;

import com.ey.digital.adc.aml.model.RuleExecutionResult;
import com.ey.digital.adc.aml.service.RuleExecutionJobService;
import com.ey.digital.adc.aml.sql.server.repo.RuleExecutionResultRepository;
import com.ey.digital.adc.aml.ui.request.RuleExecutionResultUIRequest;
import com.ey.digital.adc.core.controllers.AbstractController;
import com.ey.digital.adc.core.exceptions.InvalidTokenException;
import com.ey.digital.adc.core.ui.resp.utils.HttpResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@RestController
@RequestMapping("/ruleExecutionResult")

public class RuleExecutionResultController extends AbstractController<RuleExecutionResultUIRequest, RuleExecutionResult> {

    private final static Logger LOGGER = Logger.getLogger(RuleExecutionResultController.class.getName());
    @Autowired
    RuleExecutionJobService ruleExecutionJobService;
    @Autowired
    RuleExecutionResultRepository ruleExecutionResultRepository;

    public RuleExecutionResultController(RuleExecutionResultRepository repo) {
        super(repo);
        ruleExecutionResultRepository = repo;
    }

    @Override
    public RuleExecutionResult createInternal(int userId, RuleExecutionResultUIRequest request) throws Exception {
        throw new NotImplementedException();
    }

    @GetMapping(params = {"jobId", "token"})
    public ResponseEntity<List<RuleExecutionResult>> getResultsByJobId(@RequestParam("jobId") Integer jobId, @RequestParam("token") String token) {

        try {
            Integer userId = validateToken(token);
        } catch (InvalidTokenException e) {
            LOGGER.log(Level.WARNING, e.getMessage());
            return HttpResponseUtils.getInvalidTokenErrorResponse();
        }
        try {
            return HttpResponseUtils.getResponse(ruleExecutionResultRepository.findAllByJobId(jobId));
        } catch (Exception e) {
            System.out.println("exception");
            LOGGER.log(Level.WARNING, e.getMessage());
            return HttpResponseUtils.getErrorResponse(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @Override
    public RuleExecutionResult updateInternal(int userId, RuleExecutionResultUIRequest request) throws Exception {
        return null;
    }

    @Override
    public List<RuleExecutionResult> getListInternal(int listForId) {
        return null;
    }
}


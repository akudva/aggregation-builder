
package com.ey.digital.adc.aml.sql.server.repo;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ey.digital.adc.aml.model.TableJoin;
import com.ey.digital.adc.core.sql.server.repo.ISQLRepository;

public interface TableJoinRepository extends ISQLRepository<TableJoin, Integer> {

	@Modifying
	@Transactional
	@Query(value = "select id, table_lhs, table_rhs, joinkey_lhs, joinkey_rhs, toggle_table_position from tablejoin where table_lhs= :table_lhs and  table_rhs= :table_rhs" , nativeQuery = true)
	List<TableJoin> getJoinKeys( @Param("table_lhs") String table_lhs, @Param("table_rhs") String table_rhs);
	
	@Query(value = "select toggle_table_position from tablejoin where table_lhs= :table_lhs and  table_rhs= :table_rhs" , nativeQuery = true)
	String isToggleTablePosition(@Param("table_lhs") String table_lhs, @Param("table_rhs") String table_rhs);
	
}



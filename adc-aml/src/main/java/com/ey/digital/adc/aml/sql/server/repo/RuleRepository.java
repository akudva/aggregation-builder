package com.ey.digital.adc.aml.sql.server.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ey.digital.adc.aml.model.Rule;
//import com.ey.digital.adc.aml.ui.response.RuleDetails;
import com.ey.digital.adc.core.sql.server.repo.ISQLRepository;

@Repository
public interface RuleRepository  extends ISQLRepository<Rule, Integer>{
	
	@Query(value = "SELECT * FROM \"Rule\" where status= :status", nativeQuery=true)
	public List<Rule> findByRuleStatus(@Param("status") String status);
	public List<Rule> findDistinctByRuleThresholdConfigStatus(String status);
	public List<Rule> findDistinctByRuleThresholdConfigStatusAndStatus(String rtcStatus, String status);
	public Integer findClientIdByRuleId(@Param("ruleId") Integer ruleId);

}

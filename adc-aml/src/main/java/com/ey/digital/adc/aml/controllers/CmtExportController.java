package com.ey.digital.adc.aml.controllers;

import com.ey.digital.adc.aml.model.CmtExportResult;
import com.ey.digital.adc.aml.service.CmtExportService;
import com.ey.digital.adc.aml.sql.server.repo.RuleExecutionResultRepository;
import com.ey.digital.adc.aml.ui.request.CmtExportIdsUIRequest;
import com.ey.digital.adc.aml.ui.request.CmtExportJobUIRequest;
import com.ey.digital.adc.core.controllers.AbstractController;
import com.ey.digital.adc.core.exceptions.InvalidTokenException;
import com.ey.digital.adc.core.ui.resp.utils.HttpResponseUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//import com.ey.digital.adc.aml.ui.response.SegmentsMetaData;


@RestController
@RequestMapping("/cmtExport")
public class CmtExportController extends AbstractController<CmtExportJobUIRequest, CmtExportResult> {


    @Autowired
    private RuleExecutionResultRepository ruleExecutionResultRepository;

    private final Logger _logger = LoggerFactory.getLogger(this.getClass());


    public CmtExportController() {
        super(null);
    }


    @Override
    public CmtExportResult createInternal(int userId, CmtExportJobUIRequest request) throws Exception {
        return null;
    }

    @Override
    public CmtExportResult updateInternal(int userId, CmtExportJobUIRequest request) throws Exception {
        return null;
    }

    @Override
    public List<CmtExportResult> getListInternal(int listForId) {
        return null;
    }

    @Autowired
    CmtExportService cmtExportService;

    @PostMapping(value = "/export")
    public ResponseEntity<List<Integer>> postResults(@RequestBody CmtExportIdsUIRequest cmtExportIdsUIRequest)
            throws Exception {
        int userId = 0;
        try {
            userId = validateToken(cmtExportIdsUIRequest.getToken());
        } catch (InvalidTokenException e) {
            _logger.warn("Returning error response [{}]", e.getMessage());
            return HttpResponseUtils.getInvalidTokenErrorResponse();
        }
        try {
            List<Integer> message = cmtExportIdsUIRequest.getMessage();
            cmtExportService.export(message);
            return HttpResponseUtils.getResponse(message);
        } catch (Exception e) {
            _logger.error("", e);
            return HttpResponseUtils.getErrorResponse(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
package com.ey.digital.adc.aml.ui.response;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.ey.digital.adc.aml.model.SegThresholdMetadata;
import com.ey.digital.adc.aml.model.ThresholdDefault;
import com.ey.digital.adc.core.model.Audit;
import com.ey.digital.adc.core.model.IModel;

import lombok.Data;


public class ThresholdGnerateDataResponse {
	
	private Integer rule_id;
	private List<String> run_dates;
	private String rule_name;
	private Integer version;
	private List<ThresholdDefault> threshold_defaults;
	private List<SegThresholdMetadata> segments;
	public Integer getRule_id() {
		return rule_id;
	}
	public void setRule_id(Integer rule_id) {
		this.rule_id = rule_id;
	}
	public List<String> getRun_dates() {
		return run_dates;
	}
	public void setRun_dates(List<String> run_dates) {
		this.run_dates = run_dates;
	}
	public String getRule_name() {
		return rule_name;
	}
	public void setRule_name(String rule_name) {
		this.rule_name = rule_name;
	}
	public Integer getVersion() {
		return version;
	}
	public void setVersion(Integer version) {
		this.version = version;
	}
	public List<ThresholdDefault> getThreshold_defaults() {
		return threshold_defaults;
	}
	public void setThreshold_defaults(List<ThresholdDefault> threshold_defaults) {
		this.threshold_defaults = threshold_defaults;
	}
	public List<SegThresholdMetadata> getSegments() {
		return segments;
	}
	public void setSegments(List<SegThresholdMetadata> segments) {
		this.segments = segments;
	}
	
	
		

}
package com.ey.digital.adc.aml.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ey.digital.adc.aml.model.Condition;
import com.ey.digital.adc.aml.model.Rule;
import com.ey.digital.adc.aml.util.FocalEntityUtil;

@Service
public class MasterOperatorService {

	private final Logger _logger = LoggerFactory.getLogger(this.getClass());

	private final TableJoinService tableJoinService;

	private Map<String, String> operatorNames = new HashMap<String, String>();

	private Map<String, String> focalEntityModelSegmentTable = new HashMap<String, String>();

	@Autowired
	public MasterOperatorService(TableJoinService tableJoinService) {
		this.tableJoinService = tableJoinService;
		this.initializeOperatorNamesMap();
		this.initializeFocalEntityModelSegmentTablesMap();
	}

	public String getHiveSqlToEvaluateMasterOperator(Condition condition, String operator, String hiveFunction,
			Rule rule, String focalEntity, String focalEntityColumn, String hiveSchema) {

		// This evaluates the top-level condition
		// 1. Identifies the threshold label and value
		// 2. Gets the LHS data set
		Integer ruleId = rule.getRuleId();
		Integer sequenceId = condition.getSequenceid();

		StringBuffer hiveAlertInsertSQL1 = new StringBuffer();
		StringBuffer hiveAlertInsertSQL2 = new StringBuffer();

		StringBuffer fromClause = new StringBuffer();
		StringBuffer filterClause = new StringBuffer();
		StringBuffer joinCondition = new StringBuffer();
		StringBuffer alertResultSelectColumn = new StringBuffer();
		StringBuffer hiveAlertTxnInsertSQL = new StringBuffer();
		StringBuffer txnCreateOrInsertClause = new StringBuffer();
		StringBuffer hiveAlertMetricsSQL = new StringBuffer();

		Condition child1 = condition.getChildren().get(0);
		String sourceColumnParam1 = child1.getResultColumn();
		String sourceTableParam1 = child1.getResultTable();
		String sourceDetailTable = child1.getTxnDetailTable();

		if (sourceDetailTable == null) {
			sourceDetailTable = hiveSchema + "." + sourceTableParam1;
		}

		Condition child2 = condition.getChildren().get(1);
		String sourceColumnParam2 = child2.getResultColumn();
		String sourceTableParam2 = child2.getResultTable();

		ArrayList<Character> alias = new ArrayList<Character>();
		alias.add('a');

		ArrayList<String> tableArr = new ArrayList<String>();

		Character alias1 = 'a', alias2 = 'b';
		String aggregation = "";
		String percentile_double = "";
		String operator_value = "";
		String operator_name = "";
		String[] percentile_double_array;
		String percentile = "";

		String alertTable = "";
		String alertTxnTable = "";
		Character tableAlias = alias.get(0);
		String focalEntityConcatClause = FocalEntityUtil.getFocalEntityConcatClause(focalEntityColumn, tableAlias);

		if (child2.getType().equalsIgnoreCase("commonaggregation")) {
			if (child2.getValue().getAggregation() != null)
				aggregation = child2.getValue().getAggregation().toLowerCase();
		}

		Double sourceDataParam2;
		String resultTable1 = "alert_" + ruleId + "_${exec_id}_" + operator + "_DISTINCT";
		String resultTable2 = "alert_" + ruleId + "_${exec_id}_" + operator + "_condition_" + sequenceId;
		String focalEntityClause = FocalEntityUtil.getFocalEntityColumn(focalEntity,focalEntityColumn, tableAlias);
		switch (operator) {

		case "top":
			operator_value = ">=";
			sourceDataParam2 = Double.valueOf(child2.getResultData());
			alertTable = hiveSchema + "." + resultTable2;
			alertTxnTable = alertTable + "_transaction";

			if (aggregation.equalsIgnoreCase("percentile")) {
				percentile_double = String.valueOf((Double.valueOf(100.00) - sourceDataParam2) / 100.00);
				percentile_double_array = String.valueOf((Double.valueOf(100.00) - sourceDataParam2))
						.split(Pattern.quote("."));
				percentile = "p" + percentile_double_array[0];
			}

			// 1. Calculate the percentile value - hiveAlertInsertSQL1
			hiveAlertInsertSQL1.append(" CREATE TABLE IF NOT EXISTS " + hiveSchema + "." + resultTable1
					+ " AS SELECT to_date(from_unixtime(unix_timestamp())) as exec_date, '${run_date}' as alert_date, "
					+ " percentile_approx(" + alias1 + "." + sourceColumnParam1 + "," + percentile_double + ") as "
					+ sourceColumnParam1 + "_" + percentile + " FROM ( SELECT DISTINCT " + sourceColumnParam1 + " FROM "
					+ hiveSchema + "." + sourceTableParam1 + ") " + alias1);

			// 2. Cross-join source data table with result table from 1 above.
			hiveAlertInsertSQL2.append(" CREATE TABLE IF NOT EXISTS " + hiveSchema + "." + resultTable2
					+ " AS SELECT to_date(from_unixtime(unix_timestamp())) as exec_date, '${run_date}' as alert_date, "
					+ " CAST(concat(" + alias1 + ".party_key, concat(" + focalEntityConcatClause
					+ ",SUBSTRING('${exec_id}',0,length('${exec_id}')-1) )) AS BIGINT) as alert_id, " + alias1 + "."
					+ sourceColumnParam1 + " AS " + operator + "_" + sequenceId);
			StringBuffer joinString = tableJoinService.getSelectColumnsWithoutDuplicates(condition, alias1, alias2);
			joinString.trimToSize();
			if (joinString.length() > 0)
				hiveAlertInsertSQL2.append(", " + joinString);
			hiveAlertInsertSQL2.append(" FROM " + hiveSchema + "." + sourceTableParam1 + " " + alias1
					+ " CROSS JOIN ( SELECT DISTINCT " + sourceColumnParam1 + "_" + percentile + " FROM " + hiveSchema
					+ "." + resultTable1 + " ) " + alias2 + " WHERE " + alias1 + "." + sourceColumnParam1 + " "
					+ operator_value + " " + alias2 + "." + sourceColumnParam1 + "_" + percentile);

			// 3. Join alert detail table with result from 2 above.
			// =======================================================================
			if (condition.isRootCondition())
				txnCreateOrInsertClause.append("INSERT INTO " + hiveSchema + ".final_alert_table ").append(
						"(exec_date,alert_date,alert_id,ruleid,tran_key,focal_entity,focus_key_1,focus_key_2,executionid,party_key) SELECT to_date(from_unixtime(unix_timestamp())) as exec_date, '${run_date}' as alert_date, "
								+ "b.alert_id, " + ruleId + " as ruleid, " + tableAlias + ".tran_key as tran_key, "
								+ "'" + focalEntity + "' as focal_Entity, "
								+ FocalEntityUtil.getAlertFocalEntityColumn(focalEntityColumn, tableAlias)
								+ ", CAST(SUBSTRING('${exec_id}',0,length('${exec_id}')-1) AS INT) as executionid ");
			else
				txnCreateOrInsertClause.append(" CREATE TABLE IF NOT EXISTS " + alertTxnTable + " AS ").append(
						"SELECT to_date(from_unixtime(unix_timestamp())) as exec_date, '${run_date}' as alert_date, "
								+ "b.alert_id, " + ruleId + " as ruleid, " + tableAlias + ".tran_key as tran_key, "
								+ "'" + focalEntity + "' as focal_Entity, "
								+ "CAST(SUBSTRING('${exec_id}',0,length('${exec_id}')-1) AS INT) as executionid, "
								+ focalEntityClause);

			hiveAlertTxnInsertSQL.append(txnCreateOrInsertClause);

			if (!focalEntity.equalsIgnoreCase("customer")) {
				hiveAlertTxnInsertSQL.append(", " + tableAlias + ".party_key");
				alertResultSelectColumn.append(", party_key");
			} else if (condition.isRootCondition())
				hiveAlertTxnInsertSQL.append(", " + tableAlias + ".party_key ");

			hiveAlertTxnInsertSQL.append(" FROM " + sourceDetailTable + " " + alias1 + " join " + alertTable + " b"
					+ " ON " + FocalEntityUtil.getJoinConditionFocalEntityColumn(focalEntityColumn, 'a', 'b'));

			// set result table
			condition.setResultTable(resultTable1 + "|" + resultTable2);
			condition.setAlertInsertSql(hiveAlertInsertSQL1 + "|" + hiveAlertInsertSQL2);
			condition.setAlertTranInsertSql(hiveAlertTxnInsertSQL.toString());
			condition.setTxnDetailTable(alertTxnTable);

			break;
		case "bottom":
			operator_value = "<=";
			sourceDataParam2 = Double.valueOf(child2.getResultData());
			alertTable = hiveSchema + "." + resultTable2;
			alertTxnTable = alertTable + "_transaction";

			if (aggregation.equalsIgnoreCase("percentile")) {
				percentile_double = String.valueOf((sourceDataParam2) / 100.00);
				percentile_double_array = String.valueOf(sourceDataParam2).split(Pattern.quote("."));
				percentile = "p" + percentile_double_array[0];
			}

			// 1. Calculate the percentile value - hiveAlertInsertSQL1
			hiveAlertInsertSQL1.append(" CREATE TABLE IF NOT EXISTS " + hiveSchema + "." + resultTable1
					+ " AS SELECT to_date(from_unixtime(unix_timestamp())) as exec_date, '${run_date}' as alert_date, "
					+ " percentile_approx(" + alias1 + "." + sourceColumnParam1 + "," + percentile_double + ") as "
					+ sourceColumnParam1 + "_" + percentile + " FROM ( SELECT DISTINCT " + sourceColumnParam1 + " FROM "
					+ hiveSchema + "." + sourceTableParam1 + ")" + alias1);

			// 2. Cross-join source data table with result table from 1 above.
			hiveAlertInsertSQL2.append(" CREATE TABLE IF NOT EXISTS " + hiveSchema + "." + resultTable2
					+ " AS SELECT to_date(from_unixtime(unix_timestamp())) as exec_date, '${run_date}' as alert_date, "
					+ " CAST(concat(" + tableAlias + ".party_key, concat(" + focalEntityConcatClause
					+ ", SUBSTRING('${exec_id}',0,length('${exec_id}')-1) )) AS BIGINT) as alert_id, " + alias1 + "."
					+ sourceColumnParam1 + " AS " + operator + "_" + sequenceId);

			StringBuffer joinStringBottom = tableJoinService.getSelectColumnsWithoutDuplicates(condition, alias1,
					alias2);
			joinStringBottom.trimToSize();
			if (joinStringBottom.length() > 0)
				hiveAlertInsertSQL2.append(", " + joinStringBottom);
			hiveAlertInsertSQL2.append(" FROM " + hiveSchema + "." + sourceTableParam1 + " " + alias1
					+ " CROSS JOIN ( SELECT DISTINCT " + sourceColumnParam1 + "_" + percentile + " FROM " + hiveSchema
					+ "." + resultTable1 + " ) " + alias2 + " WHERE " + alias1 + "." + sourceColumnParam1 + " "
					+ operator_value + " " + alias2 + "." + sourceColumnParam1 + "_" + percentile);

			// 3. Join alert detail table with result from 2 above.
			// =======================================================================
			if (condition.isRootCondition())
				txnCreateOrInsertClause.append("INSERT INTO " + hiveSchema + ".final_alert_table ").append(
						"(exec_date,alert_date,alert_id,ruleid,tran_key,focal_entity,focus_key_1,focus_key_2,executionid,party_key) "
								+ "SELECT to_date(from_unixtime(unix_timestamp())) as exec_date, '${run_date}' as alert_date, "
								+ "b.alert_id, " + ruleId + " as ruleid, " + tableAlias + ".tran_key as tran_key, "
								+ "'" + focalEntity + "' as focal_Entity, "
								+ FocalEntityUtil.getAlertFocalEntityColumn(focalEntityColumn, tableAlias)
								+ ", CAST(SUBSTRING('${exec_id}',0,length('${exec_id}')-1) AS INT) as executionid ");
			else
				txnCreateOrInsertClause.append(" CREATE TABLE IF NOT EXISTS " + alertTxnTable + " AS ").append(
						"SELECT to_date(from_unixtime(unix_timestamp())) as exec_date, '${run_date}' as alert_date, "
								+ "b.alert_id, " + ruleId + " as ruleid, " + tableAlias + ".tran_key as tran_key, "
								+ "'" + focalEntity + "' as focal_Entity, "
								+ "CAST(SUBSTRING('${exec_id}',0,length('${exec_id}')-1) AS INT) as executionid, "
								+ focalEntityClause);

			hiveAlertTxnInsertSQL.append(txnCreateOrInsertClause);

			if (!focalEntity.equalsIgnoreCase("customer")) {
				hiveAlertTxnInsertSQL.append(", " + tableAlias + ".party_key");
				alertResultSelectColumn.append(", party_key");
			} else if (condition.isRootCondition())
				hiveAlertTxnInsertSQL.append(", " + tableAlias + ".party_key ");

			hiveAlertTxnInsertSQL.append(" FROM " + sourceDetailTable + " " + tableAlias + " join " + alertTable + " b"
					+ " ON " + FocalEntityUtil.getJoinConditionFocalEntityColumn(focalEntityColumn, 'a', 'b'));
			// .append(" WHERE " + alias1 + "." + sourceColumnParam1 + " " + operator_value
			// + " " + alias2 + "."
			// + sourceColumnParam1 + "_" + percentile);

			condition.setResultTable(resultTable1 + "|" + resultTable2);
			hiveAlertInsertSQL1.append("|" + hiveAlertInsertSQL2);
			condition.setAlertInsertSql(hiveAlertInsertSQL1.toString());
			condition.setAlertTranInsertSql(hiveAlertTxnInsertSQL.toString());
			condition.setTxnDetailTable(alertTxnTable);

			break;
		default:
			operator_value = operator;
			operator_name = operatorNames.get(operator_value);
			alertTable = hiveSchema + ".alert_" + ruleId + "_${exec_id}_" + operator_name + "_condition_" + sequenceId;
			alertTxnTable = alertTable + "_transaction";

			tableArr.add(sourceTableParam1);
			if (sourceTableParam2 != null) {
				tableArr.add(sourceTableParam2);
				alias.add(new Character((char) (alias.get(alias.size() - 1).charValue() + 1)));
			}

			// =======================================================================
			if (condition.isRootCondition())
				txnCreateOrInsertClause.append("INSERT INTO " + hiveSchema + ".final_alert_table ").append(
						"(exec_date,alert_date,alert_id,ruleid,tran_key,focal_entity,focus_key_1,focus_key_2,executionid,party_key) "
								+ "SELECT to_date(from_unixtime(unix_timestamp())) as exec_date, '${run_date}' as alert_date, "
								+ "b.alert_id, " + ruleId + " as ruleid, " + tableAlias + ".tran_key as tran_key, "
								+ "'" + focalEntity + "' as focal_Entity, "
								+ FocalEntityUtil.getAlertFocalEntityColumn(focalEntityColumn, tableAlias)
								+ ", CAST(SUBSTRING('${exec_id}',0,length('${exec_id}')-1) AS INT) as executionid ");
			else
				txnCreateOrInsertClause.append(" CREATE TABLE IF NOT EXISTS " + alertTxnTable + " AS ").append(
						"SELECT to_date(from_unixtime(unix_timestamp())) as exec_date, '${run_date}' as alert_date, "
								+ "b.alert_id, " + ruleId + " as ruleid, " + tableAlias + ".tran_key as tran_key, "
								+ "'" + focalEntity + "' as focal_Entity, "
								+ "CAST(SUBSTRING('${exec_id}',0,length('${exec_id}')-1) AS INT) as executionid, "
								+ focalEntityClause);

			hiveAlertTxnInsertSQL.append(txnCreateOrInsertClause);

			// =======================================================================

			_logger.info("condition " + condition.getConditionId() + " child2.gettype=" + child2.getType());

			if (child2.getType().equalsIgnoreCase("data")) {
				sourceDataParam2 = Double.valueOf(child2.getValue().getData());

				hiveAlertInsertSQL1.append(" CREATE TABLE IF NOT EXISTS " + alertTable
						+ " AS SELECT to_date(from_unixtime(unix_timestamp())) as exec_date, '${run_date}' as alert_date, "
						+ " CAST(concat(" + tableAlias + ".party_key,concat(" + focalEntityConcatClause
						+ ",SUBSTRING('${exec_id}',0,length('${exec_id}')-1) )) AS BIGINT) as alert_id, " + alias1 + "."
						+ sourceColumnParam1 + " as " + operator_name + "_" + sequenceId);
				joinString = tableJoinService.getSelectColumnsWithoutDuplicates(condition, alias1, alias2);
				joinString.trimToSize();
				if (joinString.length() > 0)
					hiveAlertInsertSQL1.append(", " + joinString);

				alertResultSelectColumn.append(condition.getChildren().get(0).getAlertResultSelectColumn());

				if (!focalEntity.equalsIgnoreCase("customer")) {
					hiveAlertTxnInsertSQL.append(", " + tableAlias + ".party_key");
					alertResultSelectColumn.append(", party_key");
				} else if (condition.isRootCondition())
					hiveAlertTxnInsertSQL.append(", " + tableAlias + ".party_key ");

				fromClause.append(" FROM " + hiveSchema + "." + sourceTableParam1 + " " + alias1);
				hiveAlertInsertSQL1.append(fromClause).append(
						" WHERE " + alias1 + "." + sourceColumnParam1 + " " + operator_value + " " + sourceDataParam2);

				hiveAlertTxnInsertSQL
						.append(" FROM " + sourceDetailTable + " " + tableAlias + " join " + alertTable + " b" + " ON "
								+ FocalEntityUtil.getJoinConditionFocalEntityColumn(focalEntityColumn, 'a', 'b'));
				// hiveAlertTxnInsertSQL.append(
				// " WHERE " + alias1 + "." + sourceColumnParam1 + " " + operator_value + " " +
				// sourceDataParam2);
				condition.setResultColumn(operator_name + "_" + sequenceId);
				condition.setAlertResultSelectColumn(alertResultSelectColumn.toString());

			} else if (child2.getType().equalsIgnoreCase("thresholdlabel")) {
				final String thresholdTable = "rtcthresholdsegmentrisk" + "_" + ruleId + "_${rtcid}_${exec_id}";
				tableArr.add(thresholdTable);
				alias.add(new Character((char) (alias.get(alias.size() - 1).charValue() + 1)));
				alias.add(new Character((char) (alias.get(alias.size() - 1).charValue() + 1)));

				hiveAlertInsertSQL1.append(" CREATE TABLE IF NOT EXISTS " + alertTable
						+ " AS SELECT to_date(from_unixtime(unix_timestamp())) as exec_date, '${run_date}' as alert_date, "
						+ " CAST(concat(" + tableAlias + ".party_key, concat(" + focalEntityConcatClause
						+ ", SUBSTRING('${exec_id}',0,length('${exec_id}')-1) )) AS BIGINT) as alert_id, "
						+ alias.get(0) + "." + sourceColumnParam1 + " as " + operator_name + "_" + sequenceId + ", "
						+ alias.get(2) + ".calculatedvalue");
				joinString = tableJoinService.getSelectColumnsWithoutDuplicates(condition, alias1, alias2);
				joinString.trimToSize();
				if (joinString.length() > 0)
					hiveAlertInsertSQL1.append(", " + joinString);

				alertResultSelectColumn.append(condition.getChildren().get(0).getAlertResultSelectColumn());

				if (!focalEntity.equalsIgnoreCase("customer")) {
					hiveAlertTxnInsertSQL.append(", " + tableAlias + ".party_key");
					alertResultSelectColumn.append(", party_key");
				} else if (condition.isRootCondition())
					hiveAlertTxnInsertSQL.append(", " + tableAlias + ".party_key ");

				// ******************************************************************************************************
				// ******************************************************************************************************
				fromClause
						.append(" FROM " + hiveSchema + "." + sourceTableParam1 + " " + alias.get(0) + " JOIN "
								+ hiveSchema + ".modelsegmentpartyref " + alias.get(1))
						.append(" ON " + alias.get(0) + ".party_key = " + alias.get(1) + ".party_key ")
						.append("JOIN " + hiveSchema + "." + thresholdTable + " " + alias.get(2));

				// Add join for party_segment_ref based on enum
				joinCondition.append(alias.get(1) + ".segment_name = " + alias.get(2) + ".segment ");
				// .append(" AND " + alias.get(1) + ".model_id = " + alias.get(2) + ".modelid
				// ");
				// ******************************************************************************************************
				// ******************************************************************************************************

				filterClause.append(alias.get(2) + ".label='" + child2.getValue().getThresholdLabel() + "'" + " AND "
						+ alias.get(2) + ".rtcId=${rtcid} " + " AND " + alias.get(0) + "." + sourceColumnParam1 + " "
						+ operator_value + " " + alias.get(2) + ".calculatedvalue");

				condition.setResultColumn(operator_name + "_" + sequenceId);
				condition.setAlertResultSelectColumn(alertResultSelectColumn.toString());

				/*
				 * Add ruleid and configid filter to filterClause Also filter by approved and
				 * isRunning(Production config)
				 */

				hiveAlertInsertSQL1.append(fromClause + " ON " + joinCondition + " WHERE " + filterClause);

				hiveAlertTxnInsertSQL
						.append(" FROM " + sourceDetailTable + " " + tableAlias + " join " + alertTable + " b" + " ON "
								+ FocalEntityUtil.getJoinConditionFocalEntityColumn(focalEntityColumn, 'a', 'b'));

			} else if (child2.getType().equalsIgnoreCase("aggregation")) {
				if (tableArr.get(0).equals(tableArr.get(1))) {

					hiveAlertInsertSQL1.append(" CREATE TABLE IF NOT EXISTS " + alertTable
							+ " AS SELECT to_date(from_unixtime(unix_timestamp())) as exec_date, '${run_date}' as alert_date, "
							+ " CAST(concat(" + tableAlias + ".party_key, concat(" + focalEntityConcatClause
							+ ",SUBSTRING('${exec_id}',0,length('${exec_id}')-1))) AS BIGINT) as alert_id, "
							+ alias.get(0) + "." + sourceColumnParam1 + " as " + operator_name + "_" + sequenceId);
					joinString = tableJoinService.getSelectColumnsWithoutDuplicates(condition, alias1, alias2);
					joinString.trimToSize();
					if (joinString.length() > 0)
						hiveAlertInsertSQL1.append(", " + joinString);

					alertResultSelectColumn.append(condition.getChildren().get(0).getAlertResultSelectColumn());

					if (!focalEntity.equalsIgnoreCase("customer")) {
						hiveAlertTxnInsertSQL.append(", " + tableAlias + ".party_key");
						alertResultSelectColumn.append(", party_key");
					} else if (condition.isRootCondition())
						hiveAlertTxnInsertSQL.append(", " + tableAlias + ".party_key ");

					fromClause.append(" FROM " + hiveSchema + "." + sourceTableParam1 + " " + alias.get(0));
					filterClause.append(alias.get(0) + "." + sourceColumnParam1 + " " + operator_value + " "
							+ alias.get(0) + "." + sourceColumnParam2);
					hiveAlertInsertSQL1.append(fromClause + " WHERE " + filterClause);

					hiveAlertTxnInsertSQL.append(
							" FROM " + sourceDetailTable + " " + tableAlias + " join " + alertTable + " b" + " ON "
									+ FocalEntityUtil.getJoinConditionFocalEntityColumn(focalEntityColumn, 'a', 'b'));

					// hiveAlertTxnInsertSQL.append(" WHERE " + filterClause);

				} else {
					hiveAlertInsertSQL1.append(" CREATE TABLE IF NOT EXISTS " + alertTable
							+ " AS SELECT to_date(from_unixtime(unix_timestamp())) as exec_date, '${run_date}' as alert_date, "
							+ " CAST(concat( " + tableAlias + ".party_key, concat(" + focalEntityConcatClause
							+ ", SUBSTRING('${exec_id}',0,length('${exec_id}')-1) )) AS BIGINT) as alert_id, "
							+ alias.get(0) + "." + sourceColumnParam1 + " as " + operator_name + "_" + sequenceId);
					joinString = tableJoinService.getSelectColumnsWithoutDuplicates(condition, alias1, alias2);
					joinString.trimToSize();
					if (joinString.length() > 0)
						hiveAlertInsertSQL1.append(", " + joinString);

					alertResultSelectColumn.append(condition.getChildren().get(0).getAlertResultSelectColumn());

					if (!focalEntity.equalsIgnoreCase("customer")) {
						hiveAlertTxnInsertSQL.append(", " + tableAlias + ".party_key");
						alertResultSelectColumn.append(", party_key");
					} else if (condition.isRootCondition())
						hiveAlertTxnInsertSQL.append(", " + tableAlias + ".party_key ");

					fromClause.append(" FROM " + hiveSchema + "." + sourceTableParam1 + " " + alias.get(0) + " JOIN "
							+ hiveSchema + "." + sourceTableParam2 + " " + alias.get(1));
					joinCondition.append(
							FocalEntityUtil.getJoinConditionFocalEntityColumn(focalEntityColumn, alias1, alias2));

					filterClause.append(alias.get(0) + "." + sourceColumnParam1 + " " + operator_value + " "
							+ alias.get(1) + "." + sourceColumnParam2);
					hiveAlertInsertSQL1.append(fromClause + " ON " + joinCondition + " WHERE " + filterClause);

					hiveAlertTxnInsertSQL.append(
							" FROM " + sourceDetailTable + " " + tableAlias + " join " + alertTable + " b" + " ON "
									+ FocalEntityUtil.getJoinConditionFocalEntityColumn(focalEntityColumn, 'a', 'b'));

				}

				condition.setResultColumn(operator_name + "_" + sequenceId);

				// For below, need to get dups without alias
				condition.setAlertResultSelectColumn(alertResultSelectColumn.toString());
				_logger.info("condition " + condition.getConditionId() + " alertResultSelectColumn="
						+ alertResultSelectColumn);
				_logger.info(
						"condition " + condition.getConditionId() + " Type= " + condition.getValue().getOperator());
			}

			condition.setAlertInsertSql(hiveAlertInsertSQL1.toString());
			condition.setAlertTranInsertSql(hiveAlertTxnInsertSQL.toString());
			condition.setTxnDetailTable(alertTxnTable);
		}

		// hiveAlertTxnInsertSQL.append(" WHERE " + filterClause);
		/*
		 * Alert metrics table
		 */
		StringBuffer joinStringAlertMetrics = new StringBuffer();
		StringBuffer whereConditionAlertMetrics = new StringBuffer();
		joinStringAlertMetrics.append(alias1 + ".party_key" + "=" + alias2 + ".party_key");
		joinStringAlertMetrics.append(" AND " + alias1 + ".alert_id" + "=" + alias2 + ".alert_id");
		// joinStringAlertMetrics.append(" and "+alias.get(0)+
		// ".model_id"+"="+alias.get(1)+".model_id");
		whereConditionAlertMetrics.append(alias2 + ".ruleid=" + ruleId);
		hiveAlertMetricsSQL.append(" INSERT INTO " + hiveSchema + ".final_alert_metrics_table ")
				.append("SELECT CAST(SUBSTRING('${exec_id}',0,length('${exec_id}')-1) AS INT) as executionid, "
						+ condition.getPrimaryconditionid() + " as alert_metrics_key, " + ruleId + " as ruleid, "
						+ alias1 + "." + sourceColumnParam1 + " as metrics_column, " + "'"
						+ child2.getValue().getThresholdLabel() + "'" + " as thresholdlabel, " + alias1
						+ ".party_key as party_key, " + "'" + rule.getRulename() + "'" + " as alert_reason, "
						+ "${rtcid} as rtcid, " + child2.getPrimaryconditionid() + " as rtcthresholdlabelid" + ", "
						+ FocalEntityUtil.getAlertFocalEntityColumn(focalEntityColumn, tableAlias) + ", " + alias1
						+ ".alert_id as alert_id")
				.append(" FROM " + hiveSchema + "." + sourceTableParam1 + " " + alias1)
				.append(" JOIN " + hiveSchema + ".final_alert_table " + alias2 + " ON " + joinStringAlertMetrics)
				.append(" WHERE " + whereConditionAlertMetrics);

		condition.setAlertMetricSql(hiveAlertMetricsSQL.toString());

		_logger.info(hiveAlertInsertSQL1.toString());
		_logger.info(hiveAlertInsertSQL2.toString());
		_logger.info(hiveAlertTxnInsertSQL.toString());

		return hiveAlertInsertSQL1.toString();
	}

	private void initializeOperatorNamesMap() {
		operatorNames.put(">=", "GTEQ");
		operatorNames.put("<=", "LTEQ");
		operatorNames.put(">", "GT");
		operatorNames.put("<", "LT");
		operatorNames.put("=", "EQ");
	}

	private void initializeFocalEntityModelSegmentTablesMap() {
		focalEntityModelSegmentTable.put(focalEntities.PARTY.name(), "modelsegmentpartyref");
		focalEntityModelSegmentTable.put(focalEntities.CUSTOMER.name(), "modelsegmentcustomerref");
	}

	private enum focalEntities {
		PARTY, ACCOUNT, CUSTOMER, HOUSEHOLD, SECURITY, CUSTOMER_ACCOUNT
	}
}
package com.ey.digital.adc.aml.controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;
import javax.ws.rs.core.MediaType;

import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URISyntaxException;
import java.text.ParseException;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ey.digital.adc.aml.model.RtcRiskLevel;
import com.ey.digital.adc.aml.model.RtcRunDate;
import com.ey.digital.adc.aml.model.RtcSegment;
import com.ey.digital.adc.aml.model.RtcThresholdLabel;
import com.ey.digital.adc.aml.model.RtcThresholdSegmentRisk;
import com.ey.digital.adc.aml.model.Rule;
import com.ey.digital.adc.aml.model.RuleThresholdConfig;
import com.ey.digital.adc.aml.model.RuleThresholdQuery;
import com.ey.digital.adc.aml.model.SequenceFetcher;
import com.ey.digital.adc.aml.oozie.rest.client.OozieClient;
import com.ey.digital.adc.aml.service.ConditionService;
import com.ey.digital.adc.aml.service.RuleService;
import com.ey.digital.adc.aml.service.RuleThresholdConfigService;
import com.ey.digital.adc.aml.sql.server.repo.ClientRepository;
import com.ey.digital.adc.aml.sql.server.repo.ConditionRepository;
import com.ey.digital.adc.aml.sql.server.repo.RtcCalculatedValueRepository;
import com.ey.digital.adc.aml.sql.server.repo.RtcRiskLevelRepository;
import com.ey.digital.adc.aml.sql.server.repo.RtcRunDateRepository;
import com.ey.digital.adc.aml.sql.server.repo.RtcSegmentRepository;
import com.ey.digital.adc.aml.sql.server.repo.RtcThresholdLabelRepository;
import com.ey.digital.adc.aml.sql.server.repo.RtcThresholdSegmentRiskRepo;
import com.ey.digital.adc.aml.sql.server.repo.RuleRepository;
import com.ey.digital.adc.aml.sql.server.repo.RuleThresholdConfigRepository;
import com.ey.digital.adc.aml.ui.request.ExecutionPayloadUIRequest;
import com.ey.digital.adc.aml.ui.request.RuleThresholdConfigUIRequest;
import com.ey.digital.adc.aml.ui.request.ThresholdTuningUIRequest;
import com.ey.digital.adc.aml.ui.response.ThresholdGnerateDataResponse;
import com.ey.digital.adc.core.controllers.AbstractController;
//import com.ey.digital.adc.core.exceptions.InvalidCreateIdException;
import com.ey.digital.adc.core.exceptions.InvalidMessageException;
import com.ey.digital.adc.core.exceptions.InvalidTokenException;
import com.ey.digital.adc.core.model.ApiResponse;
import com.ey.digital.adc.core.ui.resp.utils.HttpResponseUtils;
import com.ey.digital.adc.core.util.DateTimeUtils;
import com.ey.digital.adc.aml.model.Condition;
import com.ey.digital.adc.aml.model.ExecutionPayload;
import com.ey.digital.adc.aml.model.RestResponse;
import com.ey.digital.adc.aml.model.RtcCalculatedValue;

@RestController
@RequestMapping("/threshold")
public class RuleThresholdConfigController
        extends AbstractController<RuleThresholdConfigUIRequest, RuleThresholdConfig> {

    private final static Logger _logger = Logger.getLogger(RuleController.class.getName());
    @Autowired
    OozieClient oozieClient;
    @Autowired
    private RuleThresholdConfigRepository ruleThresholdConfigRepository;

    @Autowired
    private RtcCalculatedValueRepository calculatedvalueRepository;

    @Autowired
    private RtcSegmentRepository rtcSegmentRepository;

    @Autowired
    private RtcRiskLevelRepository rtcRiskLevelRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private RtcThresholdLabelRepository rtcThresholdLabelRepository;

    @Autowired
    private RtcRunDateRepository rtcRunDateRepository;

    @Autowired
    private RuleRepository ruleRepo;

    @Autowired
    private ConditionRepository conditionRepo;

    @Autowired
    private RtcThresholdSegmentRiskRepo rtcThresholdSegRiskRepo;

    @PersistenceContext
    protected EntityManager em;

    @Autowired
    private RuleThresholdConfigService ruleThresholdConfigService;

    public RuleThresholdConfigController(RuleThresholdConfigRepository ruleThresholdConfigRepository) {
        super(ruleThresholdConfigRepository);
        this.ruleThresholdConfigRepository = ruleThresholdConfigRepository;

    }

    @Override
    public RuleThresholdConfig createInternal(int userId, RuleThresholdConfigUIRequest request) throws Exception {
        // TODO Auto-generated method stub

        return null;
    }

    @Override
    public RuleThresholdConfig updateInternal(int userId, RuleThresholdConfigUIRequest request) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<RuleThresholdConfig> getListInternal(int listForId) {
        return null;

    }

    /**
     * Generate Data for Rule Threshold Config
     *
     * @param request RuleThresholdConfigUIRequest
     * @return RuleThresholdConfig
     */
    @PostMapping(value = "/generateData", consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    // @Transactional
    public RuleThresholdConfig generateData(@Valid @RequestBody RuleThresholdConfigUIRequest request) throws Exception {

        int userId = 0;
        userId = validateToken(request.getToken());

        RuleThresholdConfig message = request.getMessage();

        RuleThresholdConfig returnThresholdConfig = null;

        if (message == null)
            throw new InvalidMessageException("Null/empty message received for create");

        if (message.getRtcId() == null) {
            /* insert record into rulethresholdconfig table */
            returnThresholdConfig = ruleThresholdConfigService.createThresholdConfig(message, userId);
            /*
             * read condition table and get sql queries and insert into rulethresholdquery
			 * table
			 */

            if (returnThresholdConfig.getRtcId() != null && returnThresholdConfig.getStatus().equalsIgnoreCase("Generating Data")) {
                String texecId = ruleThresholdConfigService.createThresholdConfigQuery(returnThresholdConfig, userId);
                // ruleThresholdConfigService.executeRuleThresholdQueries(returnThresholdConfig,texecId,userId);
                // //uncomment to put some test data

                // List<ExecutionPayload> testData = ruleThresholdConfigService.createTestData(
                // returnThresholdConfig.getRuleId(), returnThresholdConfig.getRtcId(), texecId,
                // userId);
                // ruleThresholdConfigService.processQueryResults(testData, userId); // needs to
                // be deleted when go live.

                // String texecId =
                // ruleThresholdConfigService.createThresholdConfigQuery(message, userId);
                // before calling oozie, update the rtcid status as 'Generating Data'

                try {
                    // Integer clientId =
                   // Rule rule =  ruleRepo.findClientIdByRuleId(returnThresholdConfig.getRuleId());
                    // String hiveSchema = clientRepository.findHiveSchemaByClientId(clientId);
                		Integer ruleid = returnThresholdConfig.getRuleId();
                     Rule rule = ruleRepo.findOne(ruleid);
                    
                		Integer clientId = Integer.valueOf(rule.getClientId());
                    String hiveSchema = rule.getClient().getHiveSchema();
                    System.out.println("Hive schema>>>>>>>>>" + hiveSchema); 
                    String payload = oozieClient.createPayloadForGenerateData(returnThresholdConfig.getRuleId(),
                            returnThresholdConfig.getRtcId(), texecId, hiveSchema);
                    /// user/adc/fincrime/oozie/Java-Threshold-Tuning-workflow-v01/
                    // "/user/hue/oozie/deployments/_dev_-oozie-4280-1507215387.43/

                    System.out.println("Threshold payload: " + payload);
                    RestResponse response = oozieClient.oozieRestClientPost(payload);

                    // getETLJobStatusInOozie(response);

                    // startOozieJob(response);

                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                // } catch (InterruptedException e) {
                // // TODO Auto-generated catch block
                // e.printStackTrace();
                // }

            }
            return returnThresholdConfig;

        }
        throw new InvalidMessageException("RTC id received for generate data..");
    }

    @PostMapping(value = "/save")
    public ResponseEntity<String> save(@Valid @RequestBody RuleThresholdConfigUIRequest request) {
        int userId = 0;
        try {
            userId = validateToken(request.getToken());
        } catch (InvalidTokenException e) {
            _logger.warning("Returning error response [{}]" + e.getMessage());
            return HttpResponseUtils.getInvalidTokenErrorResponse();
        }

        RuleThresholdConfig rtc = null;

        try {
            rtc = ruleThresholdConfigRepository.save(request.getMessage());

            // loop through risks and multiply by scalar
            for (RtcCalculatedValue cv : rtc.getRtcCalculatedValue()) {
                for (RtcRiskLevel rl : rtc.getRtcRiskLevels()) {

                    RtcThresholdSegmentRisk _rtc = rtcThresholdSegRiskRepo.findByRtcCalculatedValueIdAndScalar(
                            cv.getRtcCalculatedValueId(), Double.valueOf(rl.getRiskScalarValue()));

                    if (_rtc == null)
                        _rtc = new RtcThresholdSegmentRisk();

                    _rtc.setCalculatedFuncName(cv.getCalculatedFuncName());
                    _rtc.setCalculatedType(cv.getCalculatedType());
                    _rtc.setCalculatedValue(cv.getCalculatedValue());
                    _rtc.setRtcCalculatedValueId(cv.getRtcCalculatedValueId());
                    _rtc.setSegmentName(cv.getSegmentName());
                    _rtc.setThresholdLabel(cv.getThresholdLabel());
                    _rtc.setRisk(rl.getRiskLevelName());
                    _rtc.setRtcid(rtc.getRtcId());

                    Double scalar = null;
                    if (rl.getRiskScalarValue() != null) {
                        scalar = Double.valueOf(rl.getRiskScalarValue());
                        _rtc.setScalar(scalar);
                    }

                    Double val = null;
                    if (_rtc.getCalculatedValue() != null) {
                        val = Double.valueOf(_rtc.getCalculatedValue());
                        _rtc.setFinalvalue(scalar * val);
                    }

                    rtcThresholdSegRiskRepo.save(_rtc);
                }
            }

        } catch (Exception e) {
            _logger.log(Level.SEVERE, e.getMessage());
            return HttpResponseUtils.getErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        String message = "Saved Threshold data generated successfully";
        return HttpResponseUtils.getResponse(HttpStatus.CREATED, message, null);
    }

    @PostMapping(value = "/commit")
    public ResponseEntity<RuleThresholdConfig> commit(@Valid @RequestBody RuleThresholdConfigUIRequest request) {
        int userId = 0;
        try {
            userId = validateToken(request.getToken());
        } catch (InvalidTokenException e) {
            _logger.warning(String.format("Returning error response [{}]", e.getMessage()));
            return HttpResponseUtils.getInvalidTokenErrorResponse();
        }

        RuleThresholdConfig rtc = null;

        try {
            rtc = ruleThresholdConfigRepository.save(request.getMessage());

            Integer ruleid = rtc.getRuleId();
            Integer rtcid = rtc.getRtcId();
            String texecid = rtc.getRtcCalculatedValue().get(0).getPrecalculatedValues().get(0).getTexecid();
            Rule rule = ruleRepo.findOne(ruleid);
            Integer clientId = Integer.valueOf(rule.getClientId());
            String hiveSchema = rule.getClient().getHiveSchema();
            String action = "CM";

            List<RtcCalculatedValue> rtcCalcVals = calculatedvalueRepository.findEmptyCalculatedValuesByRtcid(rtcid);

            String segment = "";
            String focalEntity = rule.getFocalEntity().toLowerCase();
            System.out.println("Fiocal entity: " + focalEntity);
            switch (focalEntity) {
                case "account_security":
                case "account":
                    segment = "acct_segment";
                    break;
                case "customer":
                    segment = "party_segment";
                    break;
                case "transaction":
                    segment = "tran_segment";
                    break;
                default:
                    segment = "default_segment";
                    break;
            }

            if (rule.getComment().equalsIgnoreCase("DA")) {
                segment = "default_segment";
            }

            for (RtcCalculatedValue rcv : rtcCalcVals) {
                String thresholdlabel = rcv.getThresholdLabel();
                Condition c = conditionRepo.findByRuleIdAndThresholdLabel(ruleid, thresholdlabel);

                String _cs = "SELECT percentile_approx(" + c.getResultColumn() + ", "
                        + Double.valueOf(rcv.getCalculatedFuncName().substring(rcv.getCalculatedFuncName().toLowerCase().indexOf("p") + 1)) / 100
                        + ", 10000) from " + hiveSchema + ".UNION_" + ruleid + "_" + rtcid + "_"
                        + thresholdlabel.replaceAll("[^\\w\\s]", "").replaceAll("\\s+", "") + " where " + segment
                        + " = '" + rcv.getSegmentName() + "' ";

                rcv.setCommitsql(_cs);
                calculatedvalueRepository.save(rcv);
            }

            try {
                String payload = oozieClient.createCommitPayload(ruleid, rtcid, texecid, hiveSchema);
                RestResponse response = oozieClient.oozieRestClientPost(payload);
                // getETLJobStatusInOozie(response); // response has jobID
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            // catch (InterruptedException e) {
            // // TODO Auto-generated catch block
            // e.printStackTrace();
            // }

            // loop through risks and multiply by scalar
            for (RtcCalculatedValue cv : rtc.getRtcCalculatedValue()) {
                for (RtcRiskLevel rl : rtc.getRtcRiskLevels()) {
                    RtcThresholdSegmentRisk _rtc = rtcThresholdSegRiskRepo.findByRtcCalculatedValueIdAndScalar(
                            cv.getRtcCalculatedValueId(), Double.valueOf(rl.getRiskScalarValue()));

                    if (_rtc == null)
                        _rtc = new RtcThresholdSegmentRisk();

                    _rtc.setCalculatedFuncName(cv.getCalculatedFuncName());
                    _rtc.setCalculatedType(cv.getCalculatedType());
                    _rtc.setCalculatedValue(cv.getCalculatedValue());
                    _rtc.setRtcCalculatedValueId(cv.getRtcCalculatedValueId());
                    _rtc.setSegmentName(cv.getSegmentName());
                    _rtc.setThresholdLabel(cv.getThresholdLabel());
                    _rtc.setRisk(rl.getRiskLevelName());
                    _rtc.setRtcid(rtc.getRtcId());

                    Double scalar = null;
                    if (rl.getRiskScalarValue() != null) {
                        scalar = Double.valueOf(rl.getRiskScalarValue());
                        _rtc.setScalar(scalar);
                    }

                    Double val = null;
                    if (_rtc.getCalculatedValue() != null) {
                        val = Double.valueOf(_rtc.getCalculatedValue());
                        _rtc.setFinalvalue(scalar * val);
                    } else
                        _rtc.setFinalvalue(null);

                    rtcThresholdSegRiskRepo.save(_rtc);
                }
            }

        } catch (Exception e) {
            _logger.log(Level.SEVERE, e.getMessage());
            return HttpResponseUtils.getErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        String message = "Saved Threshold data generated successfully";
        return HttpResponseUtils.getResponse(HttpStatus.CREATED, message, rtc);
    }


    /**
     * Approves or Rejects Thresholds based upon rtcIds
     *
     * @param request RuleThresholdConfigUIRequest
     * @return ResponseEntity for thresholds
     */
    @PostMapping(value = "/approve-reject-thresholds", consumes = MediaType.APPLICATION_JSON)
    @ResponseBody
    public ResponseEntity<ApiResponse> approveorRejectThresholds(
            @Valid @RequestBody RuleThresholdConfigUIRequest request) throws Exception {
        RuleThresholdConfig message = request.getMessage();
        if (message == null)
            throw new InvalidMessageException("Null/empty message received for approve/reject thresholds");
        if (message.getRtcIds() == null || message.getRtcIds().isEmpty())
            throw new InvalidMessageException("Null/Empty rtc ids list received for approve/reject thresholds");
        List<String> errors = null;
        ApiResponse apiResponse = null;
        int userId = 0;
        _logger.info("processing approve/reject thresholds for token [{}] message [{}]" + request.getToken()
                + request.getMessage());
        try {
            userId = validateToken(request.getToken());
        } catch (InvalidTokenException e) {
            _logger.warning("Returning error response [{}]" + e.getMessage());
            return HttpResponseUtils.getInvalidTokenErrorResponse();
        }
        try {
            for (Integer ruleId : message.getRtcIds()) {
                String currentDateTime = DateTimeUtils.getCurrentDateTime();
                RuleThresholdConfig current = ruleThresholdConfigRepository.findOne(ruleId);
                // if (!current.getStatus().equalsIgnoreCase("pending approval")) {
                // throw new InvalidMessageException(
                // "RTC Ids" + current.getRtcId() + " is not in pending approval status");
                // }
                current.setLastUpdatedBy(userId);
                current.setLastUpdatedDateTime(currentDateTime);
                if (Boolean.parseBoolean(message.getIsApprove())) {
                    current.setApprovedBy(userId);
                    current.setApprovedDtm(currentDateTime);
                    current.setApprovalComment(message.getApprovalComment());
                    current.setStatus("Approved");

                } else {
                    current.setRejectedBy(userId);
                    current.setRejectedDtm(currentDateTime);
                    current.setStatus("Rejected");
                    current.setRejectionComment(message.getRejectionComment());
                }
                ruleThresholdConfigRepository.save(current);
            }
        } catch (Exception e) {
            _logger.log(Level.SEVERE, e.getMessage());
            return HttpResponseUtils.getErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        apiResponse = new ApiResponse(HttpStatus.CREATED, "Success", errors);
        return HttpResponseUtils.getResponse(HttpStatus.CREATED, "Success", apiResponse);
    }

    /**
     * Set rtc to pending approval state when submitting for approval
     *
     * @param request RuleThresholdConfigUIRequest
     * @return ResponseEntity for thresholds i.e. API response success or bad
     * request
     */
    @PostMapping(value = "/submit-for-approval", consumes = MediaType.APPLICATION_JSON)
    @ResponseBody
    public ResponseEntity<ApiResponse> submitForApproval(@Valid @RequestBody RuleThresholdConfigUIRequest request)
            throws Exception {
        RuleThresholdConfig message = request.getMessage();
        if (message == null)
            throw new InvalidMessageException("Null/empty message received when submitting for approval");
        if (message.getRtcIds() == null || message.getRtcIds().isEmpty())
            throw new InvalidMessageException("Null/Empty rtc ids list received when submitting for approval");

        List<String> errors = null;
        ApiResponse apiResponse = null;

        int userId = 0;
        _logger.info(
                "processing submit-for-approval token [{}] message [{}]" + request.getToken() + request.getMessage());
        try {
            userId = validateToken(request.getToken());
        } catch (InvalidTokenException e) {
            _logger.warning("Returning error response [{}]" + e.getMessage());
            return HttpResponseUtils.getInvalidTokenErrorResponse();
        }
        try {
            for (Integer ruleId : message.getRtcIds()) {
                String currentDateTime = DateTimeUtils.getCurrentDateTime();
                RuleThresholdConfig current = ruleThresholdConfigRepository.findOne(ruleId);
                current.setLastUpdatedBy(userId);
                current.setLastUpdatedDateTime(currentDateTime);
                current.setApprovedBy(userId);
                current.setApprovedDtm(currentDateTime);
                current.setApprovalComment(message.getApprovalComment());
                current.setStatus("Pending Approval");
                ruleThresholdConfigRepository.save(current);
            }
        } catch (Exception e) {
            _logger.log(Level.SEVERE, e.getMessage());
            return HttpResponseUtils.getErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        apiResponse = new ApiResponse(HttpStatus.OK, "Success", errors);
        return HttpResponseUtils.getResponse(HttpStatus.OK, "Success", apiResponse);
    }
}

package com.ey.digital.adc.aml.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.ey.digital.adc.core.model.IModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Entity
@EqualsAndHashCode(callSuper = false)
@Table(name = "[filtervalue]")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class FilterValue implements IModel{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "valueid")
	private Integer filterValueId;
	
//	@OneToOne
//	@JoinColumn(name = "parentval")
//	private Filter parentval;

	@Column(name = "threshold_label")
	private String threshold_label;
	
	@Column(name = "value_data")
	private String data;
	
	@Column(name = "value_operator")
	private String operator;
	
	@Column(name = "value_logical")
	private String logical;
	
	@Column(name = "value_attribute")
	private String attribute;
	
	@Column(name = "value_table")
	private String table;

}

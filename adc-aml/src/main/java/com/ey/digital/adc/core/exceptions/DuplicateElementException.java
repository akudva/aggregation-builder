/**
 * 
 */
package com.ey.digital.adc.core.exceptions;

/**
 * @author mulaysh
 * Date Created: 2017-02-28
 */
public class DuplicateElementException extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * @param message
	 */
	public DuplicateElementException(String message) {
		super(message);
	}

}

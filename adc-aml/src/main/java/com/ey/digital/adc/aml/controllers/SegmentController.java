package com.ey.digital.adc.aml.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.ws.rs.core.MediaType;

import org.hibernate.validator.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ey.digital.adc.aml.model.Rule;
import com.ey.digital.adc.aml.model.Segment;
import com.ey.digital.adc.aml.sql.server.repo.RuleRepository;
import com.ey.digital.adc.aml.sql.server.repo.SegmentRepository;
import com.ey.digital.adc.aml.ui.request.RuleUIRequest;
import com.ey.digital.adc.aml.ui.request.SegmentUIRequest;
import com.ey.digital.adc.aml.ui.response.SegmentDetails;
import com.ey.digital.adc.aml.ui.response.SegmentResponse;
//import com.ey.digital.adc.aml.ui.response.SegmentsMetaData;
import com.ey.digital.adc.core.controllers.AbstractController;
import com.ey.digital.adc.core.exceptions.InvalidMessageException;
import com.ey.digital.adc.core.exceptions.InvalidTokenException;
import com.ey.digital.adc.core.model.ApiResponse;
import com.ey.digital.adc.core.ui.resp.utils.HttpResponseUtils;
import com.ey.digital.adc.core.util.DateTimeUtils;


@RestController
@RequestMapping("/segment")
public class SegmentController extends AbstractController<SegmentUIRequest, Segment> {

	
	@Autowired
	private SegmentRepository segmentRepo;

	@Autowired
	private RuleRepository ruleRepo;
	
	private final Logger _logger = LoggerFactory.getLogger(this.getClass());

	
	public SegmentController(SegmentRepository segmentRepo, RuleRepository ruleRepo) {
		super(segmentRepo);
		this.segmentRepo =segmentRepo;
		this.ruleRepo = ruleRepo;
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ey.digital.adc.producttrace.controllers.AbstractController#
	 * createInternal(int,
	 * com.ey.digital.adc.producttrace.client.messages.AbstractClientRequest)
	 */
	@Override
	public Segment createInternal(int userId, SegmentUIRequest request) throws Exception {
		throw new UnsupportedOperationException("This operation is not supported");

	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ey.digital.adc.producttrace.controllers.AbstractController#
	 * updateInternal(int,
	 * com.ey.digital.adc.producttrace.client.messages.AbstractClientRequest)
	 */
	@Override
	public Segment updateInternal(int userId, SegmentUIRequest request) throws Exception {
		throw new UnsupportedOperationException("This operation is not supported");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ey.digital.adc.producttrace.controllers.AbstractController#
	 * getListInternal(int)
	 */
	@Override
	public List<Segment> getListInternal(int listForId) {
		throw new UnsupportedOperationException("This operation is not supported");
	}
	
	@RequestMapping(value = "/get-segments")
	public ResponseEntity<List<SegmentDetails>> getSegments(
			@Valid @NotEmpty(message = "token may not be Empty/Null") @RequestParam(value = "token") String token, 
			@Valid @NotEmpty(message = "ruleId may not be Empty/Null") @RequestParam(value = "id") Integer ruleId)
					throws Exception {
		
		int userId = 0;
		_logger.info("Processing get segments testssss: "+token+ " : "+ ruleId);
		try {
			userId = validateToken(token);
		} catch (InvalidTokenException e) {
			_logger.warn("Returning error response [{}]", e.getMessage());
			return HttpResponseUtils.getInvalidTokenErrorResponse();
		}
		
		try {
		     	_logger.info("Get segments: getting rule by id ", token, ruleId);
			    System.out.println("coming to get rule id:");
			    _logger.info("Get segments: before get the rule id ");
				Rule rule = ruleRepo.findOne(ruleId);
				_logger.info("Get segments: got the rule record", rule.getFocalEntity());
				List<SegmentDetails> segDtlsList = new ArrayList<SegmentDetails>();
				
				String focalEntity = rule.getFocalEntity();
				List<Segment> segmentsList = segmentRepo.findByFocalEntity(focalEntity);
				segDtlsList = segmentsList.stream()
				            .map(segmnt -> new SegmentDetails(segmnt.getName(),segmnt.getDescription(),segmnt.getFocalEntity()))
				            	.collect(Collectors.toList());		
				return HttpResponseUtils.getResponse(segDtlsList);
				
	   }catch (Exception e) {
		 _logger.error("", e);
		 return HttpResponseUtils.getErrorResponse(HttpStatus.NOT_FOUND, e.getMessage());

	   }
    }
}
package com.ey.digital.adc.aml.sql.server.repo;


import org.springframework.data.repository.query.Param;

import com.ey.digital.adc.aml.model.Client;
import com.ey.digital.adc.core.sql.server.repo.ISQLRepository;

public interface ClientRepository extends ISQLRepository<Client, Integer>{
	
	public String findHiveSchemaByClientId(@Param("clientid") Integer clientid);

}

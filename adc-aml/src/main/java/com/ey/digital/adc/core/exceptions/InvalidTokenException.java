/**
 * 
 */
package com.ey.digital.adc.core.exceptions;

/**
 * @author mulaysh
 * Date Created: 2017-01-13
 */
public class InvalidTokenException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	public InvalidTokenException() {
		this("Invalid Token received");
	}

	/**
	 * @param message
	 */
	public InvalidTokenException(String message) {
		super(message);
	}

}

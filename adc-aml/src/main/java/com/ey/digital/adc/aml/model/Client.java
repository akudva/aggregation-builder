package com.ey.digital.adc.aml.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.ey.digital.adc.core.model.Audit;
import com.ey.digital.adc.core.model.IModel;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.EqualsAndHashCode;


@Entity
@Data
@Table(name = "client")
public class Client extends Audit implements IModel {

	@Id
	@Column(name = "clientid")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer clientId;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "hiveschema")
	private String hiveSchema;

	@OneToMany(targetEntity = Rule.class
	, mappedBy = "client")
	@JsonIgnore
	private  List<Rule> ruleList = new ArrayList<>();

	@Override
	public String toString() {
		return "Client [clientId=" + clientId + ", name=" + name + "]";
	}
}

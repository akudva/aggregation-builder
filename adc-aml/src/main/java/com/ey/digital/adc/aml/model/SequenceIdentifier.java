package com.ey.digital.adc.aml.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.hibernate.annotations.GenericGenerator;
import lombok.Data;

@Data
@Entity
public class SequenceIdentifier {
	

@Id
@SequenceGenerator(name = "MySequence", sequenceName = "sequence", allocationSize=1)
@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="MySequence")
private Long seqid;

public Long getSeqid() {
	return this.seqid;
}

public void setSeqid(Long seqid) {
	this.seqid = seqid;
}



}
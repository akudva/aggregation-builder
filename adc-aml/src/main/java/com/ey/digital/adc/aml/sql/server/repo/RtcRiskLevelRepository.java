package com.ey.digital.adc.aml.sql.server.repo;



import com.ey.digital.adc.aml.model.RtcRiskLevel;
import com.ey.digital.adc.core.sql.server.repo.ISQLRepository;




public interface RtcRiskLevelRepository extends ISQLRepository<RtcRiskLevel, Integer> {
	

	
}

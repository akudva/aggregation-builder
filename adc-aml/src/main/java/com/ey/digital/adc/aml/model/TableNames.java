package com.ey.digital.adc.aml.model;

import com.ey.digital.adc.core.model.AbstractTableAttributeValues;

import lombok.Data;

@Data
public class TableNames extends AbstractTableAttributeValues {

}

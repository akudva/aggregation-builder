package com.ey.digital.adc.aml.model;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class Page<E> {

    private int pageNumber;
    private int pagesAvailable;
    private List<E> pageItems = new ArrayList<E>();

}

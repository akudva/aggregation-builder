package com.ey.digital.adc.aml.model;


import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.ey.digital.adc.core.model.Audit;
import com.ey.digital.adc.core.model.IModel;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Data;
import lombok.EqualsAndHashCode;

//@EqualsAndHashCode(callSuper = false,exclude={"rtcSegment","rtcThresholdLabel","rtcRiskLevel","rtcRunDate"})
@Data
@Entity
@Table(name = "rulethresholdquery")
public class RuleThresholdQuery extends Audit implements IModel {
	private static final long serialVersionUID = 1L;
    
	
	@Id
	@Column(name = "rulethresholdsqlid")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer ruleThresholdSQLId;
	
	@Column(name = "ruleid")
	private Integer ruleId;
	
	@Column(name = "rtcid")
	private Integer rtcId;

	@Column(name = "rundate")
	private String runDate;
	
	@Column(name = "thresholdlabel")
	private String thresholdLabel;
	
	@Column(name = "querytype")
	private String queryType;

//	@Column(name = "funcname")
//	private String funcName;
//	
	
	@Column(name = "sequenceid")
	private Integer sequenceId;
	
	@Column(name = "dropquery")
	private String dropQuery;
	
	@Column(name = "createquery")
	private String createQuery;
	
	@Column(name = "executequery")
	private String executeQuery;
	
	@Column(name = "thresholdquery")
	private String thresholdQuery;
	
	@Column(name = "executionid")
	private String executionId;
	
	@Transient
	@Column(name = "queryoutput")
	private RtcQueryOutput queryOutput;
	
	
	/**
	 * No-arg constructor 
	 */
	public RuleThresholdQuery() {

	}

	/**
	 * Copy constructor.
	 */
	public RuleThresholdQuery(RuleThresholdQuery ruleThresholdQuery) {
		this.thresholdQuery = ruleThresholdQuery.thresholdQuery;
		this.executionId = ruleThresholdQuery.executionId;
		this.executeQuery = ruleThresholdQuery.executeQuery;
		this.createQuery = ruleThresholdQuery.createQuery;
		this.dropQuery = ruleThresholdQuery.dropQuery;
		this.sequenceId = ruleThresholdQuery.sequenceId;
		this.queryType = ruleThresholdQuery.queryType;
		this.thresholdLabel=ruleThresholdQuery.thresholdLabel;
		this.runDate=ruleThresholdQuery.runDate;
		this.rtcId=ruleThresholdQuery.rtcId;
		this.ruleId=ruleThresholdQuery.ruleId;

	}	  
	  
	@Override
	public String toString() {
		return "Rule Threshold Config SQL  [rtcId=" + rtcId + ", ruleId=" +ruleId + ",runDate=" + runDate +", configthresholdlabel=" + thresholdLabel + ", SequenceId=" + sequenceId + ",func name="  +",dropquery=" + dropQuery +",createQuery=" + createQuery + ", executeQuery=" + executeQuery + ", thresholdquery= " + thresholdQuery+ "]";
	}
		
}

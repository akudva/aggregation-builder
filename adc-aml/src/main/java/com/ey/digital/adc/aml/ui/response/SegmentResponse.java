package com.ey.digital.adc.aml.ui.response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ey.digital.adc.aml.model.Segment;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"segments"
})
public class SegmentResponse {

@JsonProperty("segments")
private List<Segment> segments = null;


@JsonProperty("segments")
public List<Segment> getSegments() {
return segments;
}

@JsonProperty("segments")
public void setSegments(List<Segment> segments) {
this.segments = segments;
}


}
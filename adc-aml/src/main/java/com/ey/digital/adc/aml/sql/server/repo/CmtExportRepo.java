package com.ey.digital.adc.aml.sql.server.repo;


import com.ey.digital.adc.aml.model.CmtExportResult;
import com.ey.digital.adc.aml.model.RuleThresholdQuery;
import com.ey.digital.adc.core.sql.server.repo.ISQLRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface CmtExportRepo extends ISQLRepository<CmtExportResult, Integer> {
	
}


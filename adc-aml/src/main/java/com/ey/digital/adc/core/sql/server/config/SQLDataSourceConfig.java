/**
 * 
 */
package com.ey.digital.adc.core.sql.server.config;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.apache.commons.dbcp2.cpdsadapter.DriverAdapterCPDS;
import org.apache.commons.dbcp2.datasources.SharedPoolDataSource;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

/**
 * @author mulaysh
 * Date Created: 2017-02-09
 */
@Configuration
@EnableConfigurationProperties
public class SQLDataSourceConfig {

	@Value("${aml.datasource.dbcp2.url}")
	private String url;
	
	@Value("${aml.datasource.dbcp2.username}")
	private String userName;
	
	@Value("${aml.datasource.dbcp2.password}")
	private String password;
	
	@Value("${aml.datasource.dbcp2.driver-class-name}")
	private String driverClassName;
	
	@Value("${aml.datasource.dbcp2.test-while-idle}")
	private String testWhileIdle;
	
	@Value("${aml.datasource.dbcp2.test-on-borrow}")
	private String testOnBorrow;
	
	@Value("${aml.datasource.dbcp2.time-between-eviction-runs-millis}")
	private String timeBetweenEvictionRunsMillis;
	
	@Value("${aml.datasource.dbcp2.validation-query}")
	private String validationQuery;
	
	@Value("${aml.datasource.dbcp2.min-idle}")
	private String minIdle;
	
	@Value("${aml.datasource.dbcp2.max-total}")
	private String maxTotal;
	
	@Value("${aml.datasource.dbcp2.max-idle}")
	private String maxIdle;
	
	@Bean
	@Primary
	public DataSource dataSource() throws Exception{
		DriverAdapterCPDS cpds = new DriverAdapterCPDS();
		cpds.setUrl(url);
		cpds.setUser(userName);
		cpds.setPassword(password);
		cpds.setDriver(driverClassName);
		
		SharedPoolDataSource ds = null;
		try {
			ds = new SharedPoolDataSource();
			ds.setConnectionPoolDataSource(cpds);
			ds.setDefaultTestWhileIdle(Boolean.getBoolean(testWhileIdle));
			ds.setDefaultTestOnBorrow(Boolean.getBoolean(testOnBorrow));
			ds.setDefaultTimeBetweenEvictionRunsMillis(Long.parseLong(timeBetweenEvictionRunsMillis));
			ds.setValidationQuery(validationQuery);
			ds.setDefaultMinIdle(Integer.parseInt(minIdle));
			ds.setMaxTotal(Integer.parseInt(maxTotal));
			ds.setDefaultMaxIdle(Integer.parseInt(maxIdle));
		} finally {
			if (ds != null)
				ds.close();
		}
		return ds;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TraceDataSourceConfig [url=" + url + ", userName=" + userName + ", password=" + password
				+ ", driverClassName=" + driverClassName + ", testWhileIdle=" + testWhileIdle + ", testOnBorrow="
				+ testOnBorrow + ", timeBetweenEvictionRunsMillis=" + timeBetweenEvictionRunsMillis
				+ ", validationQuery=" + validationQuery + ", minIdle=" + minIdle + ", maxTotal=" + maxTotal
				+ ", maxIdle=" + maxIdle + "]";
	}
	
	
}

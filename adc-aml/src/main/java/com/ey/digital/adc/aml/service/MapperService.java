package com.ey.digital.adc.aml.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class MapperService {
	
	private ObjectMapper obj;
	
	@Autowired
	public MapperService(ObjectMapper obj) {
		this.obj = obj;
	}

	public ObjectMapper getObjectMapper() {
		return obj;
	}

}

/**
 *
 */
package com.ey.digital.adc.core.controllers;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import com.ey.digital.adc.core.exceptions.InvalidTokenException;
import com.ey.digital.adc.core.exceptions.NoDataFoundException;
import com.ey.digital.adc.core.model.IModel;
import com.ey.digital.adc.core.sql.server.repo.ISQLRepository;
import com.ey.digital.adc.core.sql.server.utils.SqlServerUtils;
import com.ey.digital.adc.core.ui.request.AbstractUIRequest;
import com.ey.digital.adc.core.ui.resp.utils.HttpResponseUtils;


/**
 * @author mulaysh
 * Date Created: 2017-01-18
 */
public abstract class AbstractController<T extends AbstractUIRequest<V>, V extends IModel>
        implements BaseController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final String controllerName = this.getClass().getSimpleName().replaceAll("Controller", "");
    @SuppressWarnings("rawtypes")
    private ISQLRepository repo;


    // abstract methods
    public abstract V createInternal(int userId, T request) throws Exception;

    public abstract V updateInternal(int userId, T request) throws Exception;

    public abstract List<V> getListInternal(int listForId);

    @SuppressWarnings("rawtypes")
    public AbstractController(ISQLRepository repo) {
        this.repo = repo;
    }

    @PostMapping()
    public ResponseEntity<V> create(@Valid @RequestBody T request) {
        logger.info("processing create request for: [{}]", request.toString());
        final String token = request.getToken();
        int userId = 0;
        try {
            userId = validateToken(token);
        } catch (InvalidTokenException e) {
            logger.warn("Returning error response [{}]", e.getMessage());
            return HttpResponseUtils.getInvalidTokenErrorResponse();
        }

        V inserted = null;
        try {
            inserted = createInternal(userId, request);
        } catch (Exception e) {
            logger.error("Exception ", e);
            return HttpResponseUtils.getErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage());
        }

        String message = String.format("%s created successfully", controllerName);
        return HttpResponseUtils.getResponse(HttpStatus.CREATED, message, inserted);
    }

    @PutMapping
    public ResponseEntity<V> update(@Valid @RequestBody T request) {
        logger.info("processing update request for: [{}]", request.toString());
        final String token = request.getToken();
        int userId = 0;
        try {
            userId = validateToken(token);
        } catch (InvalidTokenException e) {
            logger.warn("Returning error response [{}]", e.getMessage());
            return HttpResponseUtils.getInvalidTokenErrorResponse();
        }
        V updated = null;
        try {
            updated = updateInternal(userId, request);
        } catch (Exception e) {
            logger.error("Exception ", e);
            return HttpResponseUtils.getErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        String message = String.format("%s updated successfully", controllerName);
        return HttpResponseUtils.getResponse(HttpStatus.OK, message, updated);
    }

    @SuppressWarnings("unchecked")
    @GetMapping(params = {"token", "id"})
    public ResponseEntity<V> get(@RequestParam("token") String token
            , @RequestParam("id") Integer id) {
        logger.info("processing get request for params token:[{}], id [{}]", token, id);
        try {
            validateToken(token);
        } catch (InvalidTokenException e) {
            logger.warn("Returning error response [{}]", e.getMessage());
            return HttpResponseUtils.getInvalidTokenErrorResponse();
        }
        logger.info("processing get entity request for [{}]", id);
        V entity = null;
        try {
            entity = (V) repo.findOne(id);
        } catch (Exception e) {
            logger.error("", e);
            return HttpResponseUtils.getErrorResponse(HttpStatus.NOT_FOUND, e.getMessage());
        }
        return HttpResponseUtils.getResponse(HttpStatus.OK
                , "Data retrieved successfully"
                , entity);
    }

    @GetMapping(params = {"token", "listForId"})
    public ResponseEntity<List<V>> getList(@RequestParam("token") String token
            , @RequestParam("listForId") Integer listForId) {
        logger.info("processing get request for params token:[{}], listForId [{}]", token, listForId);
        try {
            validateToken(token);
        } catch (InvalidTokenException e) {
            logger.warn("Invalid token provided, returning error response");
            return HttpResponseUtils.getInvalidTokenErrorResponse();
        }

        logger.info("processing request get user journey steps for user journey id [{}]", listForId);

        String message = null;
        HttpStatus httpStatus = HttpStatus.OK;
        List<V> list = null;
        try {
            list = getListInternal(listForId);
            if (list == null || list.size() == 0) {
                message = String.format("No data found for [%s] ID [%d]", controllerName, listForId);
                httpStatus = HttpStatus.NOT_FOUND;
                throw new NoDataFoundException(message);
            }
        } catch (Exception e) {
            logger.error("", e);
            if (httpStatus.equals(HttpStatus.OK))
                httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            return HttpResponseUtils.getErrorResponse(httpStatus, message);
        }
        message = String.format("Retrieved [%d] [%s] rows", list.size(), controllerName);
        return HttpResponseUtils.getResponse(httpStatus, message, list);
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    @GetMapping(params = {"token"})
    public ResponseEntity<List<V>> getAll(@RequestParam("token") String token) {
        logger.info("processing get all request for token [{}]", token);
        List<V> list = null;
        try {
            validateToken(token);
            long start = System.currentTimeMillis();
            list = repo.findAll();
            logger.trace("time to retrieve records [{}]", (System.currentTimeMillis() - start));
            if (list != null && list.size() > 1) { // need more than 2 to compare
                logger.debug("size of the list [{}]", list.size());
                Class<?> clazz = null;
                for (V var : list) {
                    logger.trace("{}", var.toString());
                    clazz = var.getClass();
                    break;
                }
                // just checking if the field name exists.
                if (clazz != null)
                    logger.trace("Class Name [{}]", clazz.getName());
                /*
				if (repo instanceof SpecializedProductTraceRepository) {
					list = ((SpecializedProductTraceRepository)repo).findAllByOrderByLastUpdatedDateTimeDesc();
				}
				*/
                logger.trace("time taken [{}]", (System.currentTimeMillis() - start));
            }
        } catch (Exception e) {
            logger.error("", e);
            return HttpResponseUtils.getErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
        String message =
                String.format("Retrieved [%d] rows successfully", (list == null ? 0 : list.size()));
        return HttpResponseUtils.getResponse(HttpStatus.OK, message, list);
    }

    @DeleteMapping(params = {"token", "id"})
    public ResponseEntity<V> delete(@RequestParam("token") String token
            , @RequestParam("id") Integer id) {
        return null;
    }

    @Autowired
    SqlServerUtils sqlServerUtils;

    protected int validateToken(String token) throws InvalidTokenException {
        if (token == null || "".equals(token.trim())) {
            throw new InvalidTokenException();
        }
        final int userId = sqlServerUtils.validateToken(token);
        if (userId <= 0) {
            throw new InvalidTokenException();
        }
        return userId;
    }

    protected String getControllerName() {
        return controllerName;
    }

}
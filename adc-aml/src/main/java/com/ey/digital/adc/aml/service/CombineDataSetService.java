package com.ey.digital.adc.aml.service;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.ey.digital.adc.aml.model.Condition;
import com.ey.digital.adc.aml.model.Rule;
import com.ey.digital.adc.aml.util.FocalEntityUtil;

@Service
public class CombineDataSetService {
	private final Logger _logger = LoggerFactory.getLogger(this.getClass());

	private final TableJoinService tableJoinService;

	private final Integer NUM_ALIASES = 5;

	public CombineDataSetService(TableJoinService tableJoinService) {
		this.tableJoinService = tableJoinService;
	}

	public String getHiveSqltoCombineDataSet(Condition condition, String logicalOperation, String hiveFunction,
			Rule rule, String focalEntity, String focalEntityColumn, String hiveSchema) {

		Integer ruleId = rule.getRuleId();
		Integer sequenceId = condition.getSequenceid();

		StringBuffer hiveAlertInsertSQL1 = new StringBuffer();
		StringBuffer hiveAlertInsertSQL2 = new StringBuffer();
		StringBuffer hiveAlertTxnInsertSQL_1 = new StringBuffer();
		StringBuffer hiveAlertTxnInsertSQL_2 = new StringBuffer();
		StringBuffer txnCreateOrInsertClause_1 = new StringBuffer();
		StringBuffer txnCreateOrInsertClause_2 = new StringBuffer();
		StringBuffer alertResultSelectColumn = new StringBuffer();

		Condition child1 = condition.getChildren().get(0);
		String sourceColumnParam1 = child1.getResultColumn();
		String sourceTableParam1 = child1.getResultTable();
		String sourceDetailTable_1 = child1.getTxnDetailTable();

		if (sourceDetailTable_1 == null) {
			sourceDetailTable_1 = hiveSchema + "." + sourceTableParam1;
		}

		Condition child2 = condition.getChildren().get(1);
		String sourceColumnParam2 = child2.getResultColumn();
		String sourceTableParam2 = child2.getResultTable();
		String sourceDetailTable_2 = child2.getTxnDetailTable();

		if (sourceDetailTable_2 == null) {
			sourceDetailTable_2 = hiveSchema + "." + sourceTableParam2;
		}

		ArrayList<String> tableArr = new ArrayList<String>();
		tableArr.add(sourceTableParam1);
		tableArr.add(sourceTableParam2);
		ArrayList<Character> alias = new ArrayList<Character>();

		while (alias.size() < NUM_ALIASES) {
			if (alias.isEmpty())
				alias.add('a');
			alias.add(new Character((char) (alias.get(alias.size() - 1).charValue() + 1)));
		}

		String alertTable = hiveSchema + ".alert_" + ruleId + "_${exec_id}_" + logicalOperation + "_condition_"
				+ sequenceId;
		String alertTxnTable = alertTable + "_transaction";
		Character tableAlias_1 = alias.get(0);
		Character tableAlias_2 = alias.get(1);
		Character tableAlias_3 = alias.get(2);
		Character tableAlias_4 = alias.get(3);
        
		String focalEntityConcatClauseX = FocalEntityUtil.getFocalEntityConcatClause(focalEntityColumn, 'x');
		String focalEntityConcatClause = FocalEntityUtil.getFocalEntityConcatClause(focalEntityColumn, tableAlias_1);
		
		switch (logicalOperation) {

		case "union":

			// =======================================================================
			if (condition.isRootCondition()) {
				txnCreateOrInsertClause_1.append("INSERT INTO " + hiveSchema + ".final_alert_table ").append(
						"(exec_date,alert_date,alert_id,ruleid,tran_key,focal_entity,focus_key_1,focus_key_2,executionid,party_key)  "
						+ "SELECT DISTINCT y.* FROM ( SELECT to_date(from_unixtime(unix_timestamp())) as exec_date, '${run_date}' as alert_date, "
								+ tableAlias_2 + ".alert_id, " + ruleId + " as ruleid, " + tableAlias_1
								+ ".tran_key as tran_key, " + "'" + focalEntity + "' as focal_Entity, "
								+ FocalEntityUtil.getAlertFocalEntityColumn(focalEntityColumn, tableAlias_1) +","
								+ "CAST(SUBSTRING('${exec_id}',0,length('${exec_id}')-1) AS INT) as executionid "
								);
			} else {
				txnCreateOrInsertClause_1.append(" CREATE TABLE IF NOT EXISTS " + alertTxnTable + " AS ").append(
						"SELECT DISTINCT y.* FROM ( SELECT to_date(from_unixtime(unix_timestamp())) as exec_date, '${run_date}' as alert_date, "
								+ tableAlias_2 + ".alert_id, " + ruleId + " as ruleid, " + tableAlias_1
								+ ".tran_key as tran_key, " + "'" + focalEntity + "' as focal_Entity, "
								+ "CAST(SUBSTRING('${exec_id}',0,length('${exec_id}')-1) AS INT) as executionid, "
								+ FocalEntityUtil.getAlertFocalEntityColumn(focalEntityColumn, tableAlias_1));
			}

			txnCreateOrInsertClause_2.append(
					" SELECT to_date(from_unixtime(unix_timestamp())) as exec_date, '${run_date}' as alert_date, "
							+ tableAlias_4 + ".alert_id, " + ruleId + " as ruleid, " + tableAlias_3
							+ ".tran_key as tran_key, " + "'" + focalEntity + "' as focal_Entity, "
							+ "CAST(SUBSTRING('${exec_id}',0,length('${exec_id}')-1) AS INT) as executionid, "
							+ FocalEntityUtil.getAlertFocalEntityColumn(focalEntityColumn, tableAlias_3));

			alertResultSelectColumn.append(condition.getChildren().get(0).getAlertResultSelectColumn());

			if (!focalEntity.equalsIgnoreCase("customer")) {
				txnCreateOrInsertClause_1.append(", " + tableAlias_1 + "." + "party_key");
				txnCreateOrInsertClause_2.append(", " + tableAlias_3 + "." + "party_key");
				alertResultSelectColumn.append(", party_key");
			} else if (condition.isRootCondition()) {
				txnCreateOrInsertClause_1.append(", " + tableAlias_1 + "." + "party_key");
				txnCreateOrInsertClause_2.append(", " + tableAlias_3 + "." + "party_key");
			}

			hiveAlertTxnInsertSQL_1.append(txnCreateOrInsertClause_1);
			hiveAlertTxnInsertSQL_2.append(txnCreateOrInsertClause_2);

			// =======================================================================

			_logger.info("condition " + condition.getConditionId() + " LogicalType= " + logicalOperation);

			hiveAlertInsertSQL1.append(" CREATE TABLE if not exists " + alertTable
					+ " AS SELECT DISTINCT x.* FROM ( SELECT " + tableAlias_1 + "." + sourceColumnParam1 + " as "
					+ logicalOperation + "_" + sequenceId + ", " + tableAlias_1 + ".*" + " FROM " + hiveSchema + "."
					+ sourceTableParam1 + " " + tableAlias_1);

			hiveAlertInsertSQL2.append(" SELECT " + alias.get(1) + "." + sourceColumnParam2 + " as " + logicalOperation
					+ "_" + sequenceId + ", " + tableAlias_2 + ".*" + " FROM " + hiveSchema + "." + sourceTableParam2
					+ " " + tableAlias_2 + " ) x");

			hiveAlertTxnInsertSQL_1.append(" FROM " + sourceDetailTable_1 + " " + tableAlias_1 + " join " + alertTable
					+ " b" + " ON "
					+ FocalEntityUtil.getJoinConditionFocalEntityColumn(focalEntityColumn, tableAlias_1, tableAlias_2));

			hiveAlertTxnInsertSQL_2.append(" FROM " + sourceDetailTable_2 + " " + tableAlias_3 + " join " + alertTable
					+ " " + tableAlias_4 + " ON "
					+ FocalEntityUtil.getJoinConditionFocalEntityColumn(focalEntityColumn, tableAlias_3, tableAlias_4)
					+ " ) y");

			/*
			 * 
			 * Since we doing a union of two datasets, both tables should have the same join
			 * keys Setting joinkeyKHS as the resultJoinkey
			 * 
			 */

			String joinkeyLHS = child1.getResultjoinkey();
			condition.setResultjoinkey(joinkeyLHS);

			hiveAlertInsertSQL1.append(" UNION ALL " + hiveAlertInsertSQL2);
			hiveAlertTxnInsertSQL_1.append(" UNION ALL " + hiveAlertTxnInsertSQL_2);

			break;

		case "and":

			// =======================================================================
			if (condition.isRootCondition())
				txnCreateOrInsertClause_1.append("INSERT INTO " + hiveSchema + ".final_alert_table ").append(
						"(exec_date,alert_date,alert_id,ruleid,tran_key,focal_entity,focus_key_1,focus_key_2,executionid,party_key) "
						+ "SELECT to_date(from_unixtime(unix_timestamp())) as exec_date, '${run_date}' as alert_date, "
								+ "b.alert_id, " + ruleId + " as ruleid, " + tableAlias_1 + ".tran_key as tran_key, "
								+ "'" + focalEntity + "' as focal_Entity, "
								+ FocalEntityUtil.getAlertFocalEntityColumn(focalEntityColumn, tableAlias_1)
								+ ", CAST(SUBSTRING('${exec_id}',0,length('${exec_id}')-1) AS INT) as executionid"
								); // Adds
																												// focus_key
																												// aliases
			else
				txnCreateOrInsertClause_1.append(" CREATE TABLE IF NOT EXISTS " + alertTxnTable + " AS ").append(
						"SELECT to_date(from_unixtime(unix_timestamp())) as exec_date, '${run_date}' as alert_date, "
								+ "b.alert_id, " + ruleId + " as ruleid, " + tableAlias_1 + ".tran_key as tran_key, "
								+ "'" + focalEntity + "' as focal_Entity, "
								+ "CAST(SUBSTRING('${exec_id}',0,length('${exec_id}')-1) AS INT) as executionid, "
								+ FocalEntityUtil.getAlertFocalEntityColumn(focalEntityColumn, tableAlias_1));

			hiveAlertTxnInsertSQL_1.append(txnCreateOrInsertClause_1);

			// =======================================================================

			if (!focalEntity.equalsIgnoreCase("customer")) {
				hiveAlertTxnInsertSQL_1.append(", " + tableAlias_1 + ".party_key");
				alertResultSelectColumn.append(", party_key");
			} else if (condition.isRootCondition())
				hiveAlertTxnInsertSQL_1.append(", " + tableAlias_1 + ".party_key ");

			hiveAlertInsertSQL1.append(" CREATE TABLE if not exists " + alertTable
					+ " AS SELECT to_date(from_unixtime(unix_timestamp())) as exec_date, '${run_date}' as alert_date, ")
					.append(" CAST(concat("+tableAlias_1 + ".party_key, concat("
							+ focalEntityConcatClause + ", SUBSTRING('${exec_id}',0,length('${exec_id}')-1))) AS BIGINT) as alert_id, ")
					.append(tableAlias_1 + "." + sourceColumnParam1 + " as " + logicalOperation + "_" + sequenceId);
			StringBuffer joinString = tableJoinService.getSelectColumnsWithoutDuplicates(condition, tableAlias_1,
					tableAlias_2);
			joinString.trimToSize();
			if (joinString.length() > 0)
				hiveAlertInsertSQL1.append(", " + joinString);
			hiveAlertInsertSQL1.append(" FROM " + hiveSchema + "." + sourceTableParam1 + " " + tableAlias_1 + " JOIN "
					+ hiveSchema + "." + sourceTableParam2 + " " + tableAlias_2 + " ON "
					+ FocalEntityUtil.getJoinConditionFocalEntityColumn(focalEntityColumn, tableAlias_1, tableAlias_2));

			hiveAlertTxnInsertSQL_1.append(" FROM " + sourceDetailTable_1 + " " + tableAlias_1 + " join " + alertTable
					+ " " + tableAlias_2 + " ON "
					+ FocalEntityUtil.getJoinConditionFocalEntityColumn(focalEntityColumn, tableAlias_1, tableAlias_2));
			
			break;

		case "or":

			// =======================================================================
			if (condition.isRootCondition()) {
				txnCreateOrInsertClause_1.append("INSERT INTO " + hiveSchema + ".final_alert_table ").append(
						"(exec_date,alert_date,alert_id,ruleid,tran_key,focal_entity,focus_key_1,focus_key_2,executionid,party_key) "
						+ "SELECT DISTINCT y.* FROM ( SELECT to_date(from_unixtime(unix_timestamp())) as exec_date, '${run_date}' as alert_date, "
								+ tableAlias_2 + ".alert_id, " + ruleId + " as ruleid, " + tableAlias_1
								+ ".tran_key as tran_key, " + "'" + focalEntity + "' as focal_Entity, "
								+ FocalEntityUtil.getAlertFocalEntityColumn(focalEntityColumn, tableAlias_1)
								+ ", CAST(SUBSTRING('${exec_id}',0,length('${exec_id}')-1) AS INT) as executionid "
								);
			} else {
				txnCreateOrInsertClause_1.append(" CREATE TABLE IF NOT EXISTS " + alertTxnTable + " AS ").append(
						"SELECT DISTINCT y.* FROM ( SELECT to_date(from_unixtime(unix_timestamp())) as exec_date, '${run_date}' as alert_date, "
								+ tableAlias_2 + ".alert_id, " + ruleId + " as ruleid, " + tableAlias_1
								+ ".tran_key as tran_key, " + "'" + focalEntity + "' as focal_Entity, "
								+ "CAST(SUBSTRING('${exec_id}',0,length('${exec_id}')-1) AS INT) as executionid, "
								+ FocalEntityUtil.getAlertFocalEntityColumn(focalEntityColumn, tableAlias_1));
			}

			txnCreateOrInsertClause_2.append(
					" SELECT to_date(from_unixtime(unix_timestamp())) as exec_date, '${run_date}' as alert_date, "
							+ tableAlias_4 + ".alert_id, " + ruleId + " as ruleid, " + tableAlias_3
							+ ".tran_key as tran_key, " + "'" + focalEntity + "' as focal_Entity, "
							+ "CAST(SUBSTRING('${exec_id}',0,length('${exec_id}')-1) AS INT) as executionid, "
							+ FocalEntityUtil.getAlertFocalEntityColumn(focalEntityColumn, tableAlias_3));

			alertResultSelectColumn.append(condition.getChildren().get(0).getAlertResultSelectColumn());

			if (!focalEntity.equalsIgnoreCase("customer")) {
				txnCreateOrInsertClause_1.append(", " + tableAlias_1 + "." + "party_key");
				txnCreateOrInsertClause_2.append(", " + tableAlias_3 + "." + "party_key");
				alertResultSelectColumn.append(", party_key");
			} else if (condition.isRootCondition()) {
				txnCreateOrInsertClause_1.append(", " + tableAlias_1 + "." + "party_key");
				txnCreateOrInsertClause_2.append(", " + tableAlias_3 + "." + "party_key");
			}

			hiveAlertTxnInsertSQL_1.append(txnCreateOrInsertClause_1);
			hiveAlertTxnInsertSQL_2.append(txnCreateOrInsertClause_2);

			// =======================================================================

			_logger.info("condition " + condition.getConditionId() + " LogicalType= " + logicalOperation);
			
			

			hiveAlertInsertSQL1.append(" CREATE TABLE if not exists " + alertTable
					+ " AS SELECT to_date(from_unixtime(unix_timestamp())) as exec_date,'${run_date}' as alert_date,"
					+"CAST(concat("+'x' + ".party_key, concat("+ focalEntityConcatClauseX + ", SUBSTRING('${exec_id}',0,length('${exec_id}')-1))) AS BIGINT) as alert_id, x.party_key as party_key, "
					+FocalEntityUtil.getFocalEntityColumnWithColumnNames(focalEntityColumn, 'x') 
					+ " FROM ( SELECT "+ tableAlias_1 + ".party_key as party_key,  "
					+ FocalEntityUtil.getFocalEntityColumnWithColumnNames(focalEntityColumn, tableAlias_1) + " FROM " + hiveSchema
					+ "." + sourceTableParam1 + " " + tableAlias_1);

			hiveAlertInsertSQL2.append("SELECT " + tableAlias_2 + ".party_key as party_key, "
					+ FocalEntityUtil.getFocalEntityColumnWithColumnNames(focalEntityColumn, tableAlias_2) + " FROM " + hiveSchema
					+ "." + sourceTableParam2 + " " + tableAlias_2 + " ) x group by x.party_key, "
					+ FocalEntityUtil.getFocalEntityColumn(focalEntityColumn, 'x'));

			hiveAlertTxnInsertSQL_1.append(" FROM " + sourceDetailTable_1 + " " + tableAlias_1 + " join " + alertTable
					+ " b" + " ON " + FocalEntityUtil.getJoinConditionFocalEntityColumn(focalEntityColumn, 'a', 'b'));

			hiveAlertTxnInsertSQL_2.append(" FROM " + sourceDetailTable_2 + " " + " c" + " join " + alertTable + " d"
					+ " ON " + FocalEntityUtil.getJoinConditionFocalEntityColumn(focalEntityColumn, 'c', 'd') + " ) y");

			/*
			 * 
			 * Since we doing a union of two datasets, both tables should have the same join
			 * keys Setting joinkeyKHS as the resultJoinkey
			 * 
			 */

			joinkeyLHS = child1.getResultjoinkey();
			condition.setResultjoinkey(joinkeyLHS);

			hiveAlertInsertSQL1.append(" UNION ALL " + hiveAlertInsertSQL2);
			hiveAlertTxnInsertSQL_1.append(" UNION ALL " + hiveAlertTxnInsertSQL_2);

			break;

		}

		condition.setAlertInsertSql(hiveAlertInsertSQL1.toString());
		condition.setAlertTranInsertSql(hiveAlertTxnInsertSQL_1.toString());
		condition.setTxnDetailTable(alertTxnTable);
		condition.setResultColumn(logicalOperation + "_" + sequenceId);

		_logger.info(hiveAlertInsertSQL1.toString());
		_logger.info(hiveAlertTxnInsertSQL_1.toString());

		return hiveAlertInsertSQL1.toString();
	}
}

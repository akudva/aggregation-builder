/**
 * 
 */
package com.ey.digital.adc.aml.controllers;

import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ey.digital.adc.aml.model.ActiveUser;
import com.ey.digital.adc.aml.model.Client;
import com.ey.digital.adc.aml.model.Users;
import com.ey.digital.adc.aml.sql.server.repo.ActiveUserRepository;
import com.ey.digital.adc.aml.sql.server.repo.ClientRepository;
import com.ey.digital.adc.aml.sql.server.repo.UsersRepository;
import com.ey.digital.adc.core.controllers.BaseController;
import com.ey.digital.adc.core.exceptions.InvalidRequiredFieldException;
import com.ey.digital.adc.core.ui.resp.utils.HttpResponseUtils;
import com.ey.digital.adc.core.util.DateTimeUtils;
import com.ey.digital.adc.core.ui.request.LoginRequest;
import com.ey.digital.adc.core.util.FieldValidationUtil;


/**
 * @author mulaysh
 * Date Created: 2016-12-08
 */
@RestController
public class UserAccessController implements BaseController {
	
	private final Logger _logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	ApplicationContext appContext;
	@Autowired
	private UsersRepository usersRepo;
	@Autowired
	private ActiveUserRepository activeUserRepo;
	@Autowired
	private ClientRepository clientRepo;
	
	
	
	@RequestMapping(path="/login", method=RequestMethod.POST)
	public ResponseEntity<ActiveUser> login(@Valid @RequestBody LoginRequest request){
		_logger.debug("received login request [{}]", request.toString());
		try {
			FieldValidationUtil.validateRequiredField("Email", request.getEmail());
			FieldValidationUtil.validateRequiredField("Password", request.getPassword());
		}catch (Exception e){
			_logger.error("", e);
			return HttpResponseUtils.getErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage());
		}
		
		// check if the user is valid
		Users current = null;
		Client client=null;
		try {
			current = usersRepo.findByEmail(request.getEmail());
			client= clientRepo.findOne(current.getClientId());
			BCryptPasswordEncoder encoder = (BCryptPasswordEncoder) appContext.getBean("passwordEncoder");
			if (!encoder.matches(request.getPassword(), current.getPassword())){
				throw new InvalidRequiredFieldException("Invalid User ID/Password");
			}
		}catch(Exception e){
			_logger.error("", e);
			return HttpResponseUtils.getErrorResponse(HttpStatus.NOT_FOUND, e.getMessage());
		}
		// check if the user is already logged in
		ActiveUser activeUser = null;
		try {
			List<ActiveUser> activeUsers = activeUserRepo.findByLoginId(request.getEmail());
			_logger.debug("active user list size [{}]", activeUsers.size());
			for(ActiveUser au : activeUsers){
				_logger.debug("checking active user [{}]", au.toString());
				if (au.getLogoutDtTime() == null || "".equals(au.getLogoutDtTime().trim())){
					activeUser = au;
					activeUser.setFirstName(current.getFirstName());
					activeUser.setLastName(current.getLastName());
					activeUser.setUserId(current.getPtUserId());
					activeUser.setClientName(client.getName());
					activeUser.setClientId(client.getClientId());
					throw new InvalidRequiredFieldException("User already logged in");
				}
			}
		}catch(Exception e){
			_logger.error("", e);
			if (activeUser != null){
				return HttpResponseUtils.getResponse(HttpStatus.OK, e.getMessage(), activeUser);
			}
		}
		
		// if all checks pass then log the user in
		ActiveUser inserted = null;
		try {
			activeUser = new ActiveUser();
			activeUser.setLoginId(request.getEmail());
			activeUser.setLoginDtTime(DateTimeUtils.getCurrentDateTime());
			activeUser.setToken(UUID.randomUUID().toString());
			inserted = activeUserRepo.save(activeUser);
			inserted.setFirstName(current.getFirstName());
			inserted.setLastName(current.getLastName());
			inserted.setUserId(current.getPtUserId());
		}catch(Exception e){
			_logger.error("", e);
			return HttpResponseUtils.getErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
		}
		return HttpResponseUtils.getResponse(HttpStatus.OK, "User logged in successfully", inserted);
	}
	
	@RequestMapping(value="/logout", params={"token"}, method=RequestMethod.POST)
	public ResponseEntity<ActiveUser> logout(@RequestParam("token") String token){
		_logger.debug("received logout request for token [{}]", token);
		ActiveUser updated = null;
		HttpStatus status = HttpStatus.OK;
		try {
			try {
				FieldValidationUtil.validateRequiredField("Token", token);
			}catch(Exception e){
				status = HttpStatus.BAD_REQUEST;
				throw e;
			}
			ActiveUser current = activeUserRepo.findByToken(token);
			if (current == null || current.getLogoutDtTime() != null){
				status = HttpStatus.BAD_REQUEST;
				throw new InvalidRequiredFieldException("User not logged in");
			}
			current.setLogoutDtTime(DateTimeUtils.getCurrentDateTime());
			updated = activeUserRepo.save(current);
		}catch(Exception e){
			_logger.error("", e);
			if (status.equals(HttpStatus.BAD_REQUEST)){
				return HttpResponseUtils.getErrorResponse(status, e.getMessage());
			}
			return HttpResponseUtils.getErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
		}
		return HttpResponseUtils.getResponse(status, "User logged off successfully", updated);
	}
	
	@RequestMapping(value="/register", method=RequestMethod.POST)
	public ResponseEntity<Users> register(@Valid @RequestBody Users user){
		_logger.debug("received register user request [{}]", user.toString());
		String encryptedPassword = null;
		// validate required fields
		try {
			FieldValidationUtil.validateRequiredField("Email", user.getEmail());
			FieldValidationUtil.validateRequiredField("First Name", user.getFirstName());
			FieldValidationUtil.validateRequiredField("Last Name", user.getLastName());
			FieldValidationUtil.validateRequiredField("Password", user.getPassword());
			if (user.getPtUserId() != null){
				String message = "User ID is system generated and can't be populated in Register request";
				throw new InvalidRequiredFieldException(message);
			}
		}catch (Exception e){
			_logger.error("", e);
			return HttpResponseUtils.getErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage());
		}
		
		if (appContext != null){
			BCryptPasswordEncoder encoder = (BCryptPasswordEncoder) appContext.getBean("passwordEncoder");
			if (encoder != null){
				encryptedPassword = encoder.encode(user.getPassword());
				user.setPassword(encryptedPassword);
				_logger.info("password encoded");
			} else {
				_logger.warn("encoder object is null");
			}
		} else {
			_logger.warn("app context is null");
		}
		// check if the user already exists
		HttpStatus status = HttpStatus.CREATED;
		Users inserted = null;
		try {
			Users current = usersRepo.findByEmail(user.getEmail());
			if (current.getPtUserId() != null && current.getPtUserId() > 0){
				String message = String.format("The user [%s] already exists, "
						+ "Please use a different email id to register", user.getEmail());
				status = HttpStatus.CONFLICT;
				throw new InvalidRequiredFieldException(message);
			}
		}catch (Exception e){
			_logger.error("", e);
			if (status.equals(HttpStatus.CONFLICT)){
				return HttpResponseUtils.getErrorResponse(status, e.getMessage());
			}
		}

		try {
			// the user does not exist, register the user
			inserted = usersRepo.save(user);
		}catch(Exception e){
			_logger.error("", e);
			return HttpResponseUtils.getErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
		}
		return HttpResponseUtils.getResponse(status, "User registered successfully", inserted);
		
	}
	
}

package com.ey.digital.adc.aml.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ey.digital.adc.core.model.Audit;
import com.ey.digital.adc.core.model.IModel;

import lombok.Data;

@Data
@Entity
@Table(name = "picklist")
public class PickList  implements IModel {

	@Id
	@Column(name = "picklistid")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer pickListId;
	
	@Column(name = "picklisttype")
	private String pickListType;
	
	@Column(name = "keydesc")
	private String keyDesc;
	
	@Column(name = "value")
	private String value;
	
	@Column(name = "sequence")
	private String sequence;
	
}
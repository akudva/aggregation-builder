package com.ey.digital.adc.aml.controllers;



import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ey.digital.adc.aml.model.PickList;
import com.ey.digital.adc.aml.sql.server.repo.PickListRepository;
import com.ey.digital.adc.aml.ui.request.PickListUIRequest;
import com.ey.digital.adc.core.controllers.AbstractController;

@RestController
@RequestMapping("/picklist")
public class PickListController extends AbstractController<PickListUIRequest, PickList> {

	private final Logger _logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private PickListRepository pickListRepo;


	/**
	 * 
	 */
	public PickListController(PickListRepository repo) {
		super(repo);
		this.pickListRepo = repo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ey.digital.adc.producttrace.controllers.AbstractController#
	 * createInternal(int,
	 * com.ey.digital.adc.producttrace.client.messages.AbstractClientRequest)
	 */
	@Override
	public PickList createInternal(int userId, PickListUIRequest request) throws Exception {
		throw new UnsupportedOperationException("This operation is not supported");

	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ey.digital.adc.producttrace.controllers.AbstractController#
	 * updateInternal(int,
	 * com.ey.digital.adc.producttrace.client.messages.AbstractClientRequest)
	 */
	@Override
	public PickList updateInternal(int userId, PickListUIRequest request) throws Exception {
		throw new UnsupportedOperationException("This operation is not supported");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ey.digital.adc.producttrace.controllers.AbstractController#
	 * getListInternal(int)
	 */
	@Override
	public List<PickList> getListInternal(int listForId) {
		throw new UnsupportedOperationException("This operation is not supported");
	}
}
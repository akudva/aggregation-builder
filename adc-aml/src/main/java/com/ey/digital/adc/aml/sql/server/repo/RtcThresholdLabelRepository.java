package com.ey.digital.adc.aml.sql.server.repo;

import com.ey.digital.adc.aml.model.RtcThresholdLabel;
import com.ey.digital.adc.core.sql.server.repo.ISQLRepository;




public interface RtcThresholdLabelRepository extends ISQLRepository<RtcThresholdLabel, Integer> {
	

	
}


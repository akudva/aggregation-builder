/**
 * 
 */
package com.ey.digital.adc.core.exceptions;

/**
 * @author mulaysh
 * Date Created: 2017-02-01
 */
public class InvalidMessageException extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * @param message
	 */
	public InvalidMessageException(String message) {
		super(message);
	}

}

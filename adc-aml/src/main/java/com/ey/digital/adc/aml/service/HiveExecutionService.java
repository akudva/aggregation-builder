package com.ey.digital.adc.aml.service;

import com.ey.digital.adc.aml.model.*;
import com.ey.digital.adc.aml.oozie.rest.client.OozieClient;
import com.ey.digital.adc.aml.sql.server.repo.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class HiveExecutionService {

	private final Logger _logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	RuleExecutionResultRepository ruleExecutionResultRepository;
	@Autowired
	RuleRepository ruleRepository;
	@Autowired
	RuleExecutionJobRepository ruleExecutionJobRepository;
	@Autowired
	ConditionRepository conditionRepository;
	@Autowired
	RuleThresholdConfigRepository ruleThresholdConfigRepository;
	@Autowired
	OozieClient oozieClient;

	public ExecutionPayload generateExecutionPayload(Integer executionId) {
		RuleExecutionResult ruleExecutionResult = ruleExecutionResultRepository.findOne(executionId);
		Integer jobId = ruleExecutionResult.getJobId();
		RuleExecutionJob ruleExecutionJob = ruleExecutionJobRepository.findOne(jobId);
		Integer rtcid = ruleExecutionJob.getRtcid();
		Integer ruleId = ruleThresholdConfigRepository.findByRtcId(rtcid).getRuleId();
		if (ruleExecutionJob == null)
			throw new RuntimeException("ruleId " + ruleId + " doesn't have active configurations");
		ExecutionPayload executionPayload = new ExecutionPayload();
		executionPayload.setSequenceId(executionId);
		executionPayload.setRuleId(ruleId);
		executionPayload.setActionType("RE");
		Rule rule = ruleRepository.findOne(ruleId);
		List<Condition> conditions = conditionRepository.findByRuleId(ruleId).stream()
				.sorted(Comparator.comparing(Condition::getSequenceid)).collect(Collectors.toList());

		System.out.println("lookbackDate = " + ruleExecutionResult.getLookbackDate());
		List<Condition> conditionList = conditions.stream().sorted(Comparator.comparing(Condition::getSequenceid))
				.collect(Collectors.toList());

		List<String> sqlQueries = conditionList.stream()
				.map(condition -> Arrays.asList(condition.getAlertInsertSql(), condition.getAlertTranInsertSql(),
						condition.getAlertMetricSql())) // ,
				// condition.getAlertInsertSql(),condition.getAlertTranCreateSql(),
				// condition.getAlertTranInsertSql()

				.flatMap(s -> s.stream()).filter(s -> s != null).map(s -> Arrays.asList(s.split(";|\\|")))
				.flatMap(s -> s.stream()).filter(s -> s != null && !s.isEmpty())
				.map(s -> replaceVars(s, ruleExecutionResult.getExecutionString(), ruleId, ruleExecutionJob.getRtcid(),
						ruleExecutionResult.getRunDate(), ruleExecutionResult.getLookbackDate()))
				.collect(Collectors.toList());
				executionPayload.getSqlQueries().addAll(sqlQueries);

		Condition lastCondition = conditionList.get(conditionList.size() - 1);
		if (lastCondition != null) {
			String resultTable = lastCondition.getResultTable();
			if (resultTable.contains("|"))
				resultTable = resultTable.split("\\|")[1].trim();
			executionPayload.getSqlQueries()
					.add(replaceVars("select count(*) from " + rule.getHiveSchema() + "." + resultTable,
							ruleExecutionResult.getExecutionString(), ruleId, ruleExecutionJob.getRtcid(),
							ruleExecutionResult.getRunDate(), ruleExecutionResult.getLookbackDate()));
		}
		return executionPayload;
	}

	public String generateResultTable(Integer executionId) {
		RuleExecutionResult ruleExecutionResult = ruleExecutionResultRepository.findOne(executionId);
		Integer jobId = ruleExecutionResult.getJobId();
		RuleExecutionJob ruleExecutionJob = ruleExecutionJobRepository.findOne(jobId);
		Integer rtcid = ruleExecutionJob.getRtcid();
		Integer ruleId = ruleThresholdConfigRepository.findByRtcId(rtcid).getRuleId();
		if (ruleExecutionJob == null)
			throw new RuntimeException("ruleId " + ruleId + " doesn't have active configurations");
		Rule rule = ruleRepository.findOne(ruleId);
		List<Condition> conditions = conditionRepository.findByRuleId(ruleId).stream()
				.sorted(Comparator.comparing(Condition::getSequenceid)).collect(Collectors.toList());
		List<Condition> conditionList = conditions.stream().sorted(Comparator.comparing(Condition::getSequenceid))
				.collect(Collectors.toList());

		Condition lastCondition = conditionList.get(conditionList.size() - 1);
		if (lastCondition != null) {
			String resultTable = lastCondition.getResultTable();
			if (resultTable.contains("|"))
				resultTable = resultTable.split("\\|")[1].trim();
			return replaceVars(rule.getHiveSchema() + "." + resultTable, ruleExecutionResult.getExecutionString(),
					ruleId, ruleExecutionJob.getRtcid());
		}
		throw new RuntimeException("No result table");
	}

	public void updateExecution(Integer executionId, String alertNumber) {
		RuleExecutionResult ruleExecutionResult = ruleExecutionResultRepository.findOne(executionId);
		ruleExecutionResult.setStatus("COMPLETE");
		ruleExecutionResult.setAlertNumber(Integer.parseInt(alertNumber));
		ruleExecutionResult.setEndExecutionTime(new Timestamp(new Date().getTime()));
		ruleExecutionResultRepository.saveAndFlush(ruleExecutionResult);
	}

	public String replaceVars(String sql, String execId, Integer ruleId, Integer rtcid, Timestamp runDate,
			Timestamp lookbackDate) {
		String res = sql.trim().replaceAll("\\$\\{exec_id\\}", execId)
				.replaceAll("\\$\\{lookback_date\\}", lookbackDate.toString())
				.replaceAll("\\$\\{run_date\\}", runDate.toString()).replaceAll("\\$\\{rule_id\\}", ruleId.toString())
				.replaceAll("\\$\\{rtcid\\}", rtcid.toString());
		System.out.println("res = " + res);
		return res;
	}

	public String replaceVars(String sql, String execId, Integer ruleId, Integer rtcid) {
		String res = sql.trim().replaceAll("\\$\\{exec_id\\}", execId).replaceAll("\\$\\{rule_id\\}", ruleId.toString())
				.replaceAll("\\$\\{rtcid\\}", rtcid.toString());
		System.out.println("res = " + res);
		return res;
	}

	public void startOozieJob(RuleExecutionJob job) throws IOException, InterruptedException {
		Integer ruleId = ruleThresholdConfigRepository.findByRtcId(job.getRtcid()).getRuleId();
		Rule rule = ruleThresholdConfigRepository.findByRtcId(job.getRtcid()).getRule();
		Set<Timestamp> customRunDates = job.getCustomRunDates();
		if (customRunDates != null) {
			for (Timestamp customRunDate : customRunDates) {
				Timestamp current = new Timestamp(new Date().getTime());
				if (job.getCustomRunDates().stream().anyMatch(current::after)) {
					Time lookback = rule.getLookback();
					Calendar calendar = Calendar.getInstance();
					calendar.setTime(new Date(customRunDate.getTime()));
					Timestamp lookbackDate = null;
					if (lookback.getType().equalsIgnoreCase("YEAR(S)")) {
						calendar.add(Calendar.YEAR, -1 * Integer.parseInt(lookback.getValue()));
						lookbackDate = new Timestamp(calendar.getTime().getTime());
					}
					if (lookback.getType().equalsIgnoreCase("MONTH(S)")) {
						calendar.add(Calendar.MONTH, -1 * Integer.parseInt(lookback.getValue()));
						lookbackDate = new Timestamp(calendar.getTime().getTime());
					}
					if (lookback.getType().equalsIgnoreCase("DAY(S)")) {
						calendar.add(Calendar.DAY_OF_WEEK, -1 * Integer.parseInt(lookback.getValue()));
						lookbackDate = new Timestamp(calendar.getTime().getTime());
					}
					RuleExecutionResult ruleExecutionResult = new RuleExecutionResult();
					ruleExecutionResult.setJobId(job.getId());
					ruleExecutionResultRepository.saveAndFlush(ruleExecutionResult);
					ruleExecutionResult.setLookbackDate(lookbackDate);
					ruleExecutionResult.setRunDate(customRunDate);
					String payload = null;
					RestResponse responseFrRuleExecJob = null;
					if (!conditionRepository.findByRuleIdAndIsThresholdTrue(ruleId).isEmpty()) {
						payload = oozieClient.createPayloadForThresholdTuning(ruleId, job.getRtcid(),
								ruleExecutionResult.getExecutionString(), "cs_mock");
						responseFrRuleExecJob = oozieClient.oozieRestClientPost(payload);
						System.out.println("payload = " + payload);
					} else {
						payload = oozieClient.createPayloadForNoLabel(ruleId, job.getRtcid(),
								ruleExecutionResult.getExecutionString(), "cs_mock");
						responseFrRuleExecJob = oozieClient.oozieRestClientPost(payload);
					}
					System.out.println("restResponse = " + responseFrRuleExecJob.getRespBody());
					ruleExecutionResult.setOozieId(responseFrRuleExecJob.getRespBody());
					ruleExecutionResult.setStartExecutionTime(new Timestamp(new Date().getTime()));
					ruleExecutionResult.setStatus("started");
					ruleExecutionResultRepository.saveAndFlush(ruleExecutionResult);
				}
			}
		}
	}
}
/**
 * 
 */
package com.ey.digital.adc.core.exceptions;

/**
 * @author mulaysh
 * Date Created: 2017-01-20
 */
public class InvalidUpdateIdException extends Exception {

	private static final long serialVersionUID = 1L;
	/**
	 * @param message
	 */
	public InvalidUpdateIdException(String type) {
		super(String.format(" %s ID can't be null/zero for a PUT operation", type));
	}

}

package com.ey.digital.adc.aml.ui.request;

import com.ey.digital.adc.aml.model.RuleThresholdConfig;
import com.ey.digital.adc.core.ui.request.AbstractUIRequest;

public class RuleThresholdConfigUIRequest extends AbstractUIRequest<RuleThresholdConfig> {
	private static final long serialVersionUID = 1L;
	
	
	@Override
	public String toString() {
		return "RuleThresholdConfigUIRequest [message=" + getMessage() + ", toString()=" + super.toString() + "]";
	}

}
package com.ey.digital.adc.aml.model;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.ey.digital.adc.core.model.Audit;
import com.ey.digital.adc.core.model.IModel;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "modelsegmentpartyref")
@Data
@EqualsAndHashCode(callSuper = false)
public class ModelSegmentParty extends Audit implements IModel{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long segmentmodelpartyid;
	
	@Column(name = "focal_entity")
	private String focalEntity;
	
	@Column(name = "party_key")
	private Integer partyKey;
	
	@Column(name = "modelid")
	private Integer modelId; 
	
	@Column(name = "segment")
	private String segment;
	
	@Column(name = "clientid")
	private String clientId;
	
	@Column(name = "description")
	private String description;

	@Transient
	private Integer population;
	
}

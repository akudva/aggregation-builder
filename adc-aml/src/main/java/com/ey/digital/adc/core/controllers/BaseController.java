/**
 * 
 */
package com.ey.digital.adc.core.controllers;

/**
 * Marker Interface, mainly used for unit test cases
 * @author mulaysh
 * Date Created: 2017-01-05
 */
public interface BaseController {
}
package com.ey.digital.adc.aml.service;

import com.ey.digital.adc.aml.model.CmtExportResult;
import com.ey.digital.adc.aml.model.Rule;
import com.ey.digital.adc.aml.model.RuleExecutionResult;
import com.ey.digital.adc.aml.oozie.rest.client.OozieClient;
import com.ey.digital.adc.aml.sql.server.repo.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class CmtExportService {

	private final Logger _logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	RuleExecutionResultRepository ruleExecutionResultRepository;
	@Autowired
	RuleExecutionJobRepository ruleExecutionJobRepository;
	@Autowired
	RuleRepository ruleRepository;
	@Autowired
	RuleThresholdConfigRepository ruleThresholdConfigRepository;
	@Autowired

	OozieClient oozieClient;
	@Autowired
	CmtExportRepo cmtExportRepo;
	@Autowired
	HiveExecutionService hiveExecutionService;

	public List<Integer> export(List<Integer> message) throws IOException {
		List<RuleExecutionResult> executionResults = ruleExecutionResultRepository.findAll(message);
		for (RuleExecutionResult executionResult : executionResults) {
			CmtExportResult cmtExportResult = new CmtExportResult();
			cmtExportResult.setExecutionId(executionResult.getExecutionId());
			cmtExportResult.setStatus(CmtExportResult.Status.STARTED);
			cmtExportRepo.saveAndFlush(cmtExportResult);
			Rule rule = executionResult.getRuleExecutionJob().getRuleThresholdConfig().getRule();
			oozieClient.startCmtExport(cmtExportResult.getId().toString(), executionResult.getJobId().toString(),
					executionResult.getExecutionId().toString(),
					executionResult.getRuleExecutionJob().getRtcid().toString(), rule.getRuleId().toString(),
					rule.getHiveSchema(), rule.getRulename());
		}
		return message;
	}
}

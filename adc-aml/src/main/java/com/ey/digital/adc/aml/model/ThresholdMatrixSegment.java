package com.ey.digital.adc.aml.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.ey.digital.adc.core.model.Audit;
import com.ey.digital.adc.core.model.IModel;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Entity
@Table(name = "threshold_matrix_segment")
@Data
public class ThresholdMatrixSegment  extends Audit implements IModel{

	@Id
	@Column(name = "thresholdMatrixSegmentId")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer thresholdMatrixSegmentId;
	
	@Column(name = "ruleid")
	private Integer ruleId;
		
	@Transient
	private List<ThresholdSegmentsRequest> segmentIds;
	
	@Column(name ="isRiskTune")
	private String isRiskTune;
	

//	==================================================================
//	POJO ATTRIBUTE NAMES SHOULD MATCH JSON FIELD NAMES :)
//	Otherwise you need to map the two manually somehow
//	==================================================================
	@Transient
	private List<ThresholdRiskRequest> riskLevels; // This now matches the 'riskLevels' field in the JSON
	
	@Column(name ="runDates")
	@ElementCollection(targetClass=String.class)
	private List<String> runDates;
	
}


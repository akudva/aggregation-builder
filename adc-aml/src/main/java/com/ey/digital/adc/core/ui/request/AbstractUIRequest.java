package com.ey.digital.adc.core.ui.request;

import java.util.List;

import lombok.Data;

@Data
public class AbstractUIRequest<V> implements IUIRequest {
	private static final long serialVersionUID = 1L;
	private String token;
	private V message;
	private List<V> messages;


	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public V getMessage() {
		return message;
	}

	public void setMessage(V message) {
		this.message = message;
	}
	
	public List<V> getMessages() {
		return messages;
	}
	
	public void setMessages(List<V> messages) {
		this.messages= messages;
	}
	
	
}

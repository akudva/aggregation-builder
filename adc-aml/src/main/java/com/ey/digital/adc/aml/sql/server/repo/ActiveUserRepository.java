/**
 * 
 */
package com.ey.digital.adc.aml.sql.server.repo;

import java.util.List;

import com.ey.digital.adc.aml.model.ActiveUser;
import com.ey.digital.adc.core.sql.server.repo.ISQLRepository;



/**
 * @author mulaysh
 * Date Created: 2017-01-22
 */
public interface ActiveUserRepository extends ISQLRepository<ActiveUser, Integer> {
	ActiveUser findByToken(String token);
	List<ActiveUser> findByLoginId(String loginId);
}

package com.ey.digital.adc.aml.sql.server.repo;


import com.ey.digital.adc.aml.model.Risk;
import com.ey.digital.adc.core.sql.server.repo.ISQLRepository;

public interface RiskLevelsRepository extends ISQLRepository<Risk, Integer>{

}
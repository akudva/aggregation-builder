package com.ey.digital.adc.aml.service;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ey.digital.adc.aml.model.Condition;
import com.ey.digital.adc.aml.model.ExecutionPayload;
import com.ey.digital.adc.aml.model.RtcCalculatedValue;
import com.ey.digital.adc.aml.model.RtcPrecalculatedValue;
import com.ey.digital.adc.aml.model.RtcQueryOutput;
import com.ey.digital.adc.aml.model.RtcRunDate;
import com.ey.digital.adc.aml.model.RtcSegment;
import com.ey.digital.adc.aml.model.RtcThresholdLabel;
import com.ey.digital.adc.aml.model.Rule;
import com.ey.digital.adc.aml.model.RuleThresholdConfig;
import com.ey.digital.adc.aml.model.RuleThresholdQuery;
import com.ey.digital.adc.aml.sql.server.repo.ConditionRepository;
import com.ey.digital.adc.aml.sql.server.repo.RuleRepository;
import com.ey.digital.adc.aml.sql.server.repo.RuleThresholdConfigRepository;
import com.ey.digital.adc.aml.sql.server.repo.RuleThresholdQueryRepo;
import com.ey.digital.adc.aml.sql.server.repo.SegmentRepository;
import com.ey.digital.adc.core.util.DateTimeUtils;

@Service
public class RuleThresholdConfigService {

	private final Logger _logger = LoggerFactory.getLogger(this.getClass());

	private static int percent = 0;
	private static int std = -5;

	@Autowired
	private RuleThresholdConfigRepository ruleThresholdConfigRepository;

	@Autowired
	private ConditionRepository conditionRepo;

	@PersistenceContext
	protected EntityManager em;

	@Autowired
	private SegmentRepository segmentRepo;

	@Autowired
	private RuleThresholdQueryRepo ruleThresholdQueryRepo;

	@Autowired
	private RuleRepository ruleRepository;

	@Transactional
	public RuleThresholdConfig createThresholdConfig(RuleThresholdConfig thresholdConfig, Integer userId) // pass in
																											// focal
																											// entity
			throws Exception {

		String currentDateTime = DateTimeUtils.getCurrentDateTime();
		Query query = em.createNativeQuery("SELECT NEXT VALUE FOR SEQUENCEFETCHER");
		String seqname = String.valueOf((BigInteger) query.getSingleResult());
		String focalEntityName = ruleRepository.findOne(thresholdConfig.getRuleId()).getFocalEntity();
		List<RtcSegment> segments = thresholdConfig.getRtcSegments();
		Integer modelId = segmentRepo.getSegmentModelIdBySegmentNameAndFocalEntity(
				segments.get(0).getSegName(), focalEntityName);
		_logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>sequence value:::" + seqname);
		thresholdConfig.setConfigName("RTC_" + thresholdConfig.getRuleId().toString() + "_" + seqname);
		thresholdConfig.setCreatedBy(userId);
		thresholdConfig.setCreatedDateTime(currentDateTime);
		if (thresholdConfig.getRtcThresholdLabels().size() > 0) {
			thresholdConfig.setStatus("Generating Data");
		} else {
			thresholdConfig.setStatus("Pending Approval");
		}

		thresholdConfig.setModelId(modelId); // TODO get model id from FE
		thresholdConfig.getRtcRunDates().forEach(rd -> {
			rd.setCreatedBy(userId);
			rd.setCreatedDateTime(currentDateTime);
		});
		thresholdConfig.getRtcThresholdLabels().forEach(tl -> {
			tl.setCreatedBy(userId);
			tl.setCreatedDateTime(currentDateTime);
		});
		thresholdConfig.getRtcRiskLevels().forEach(rl -> {
			rl.setCreatedBy(userId);
			rl.setCreatedDateTime(currentDateTime);
		});
		thresholdConfig.getRtcSegments().forEach(rs -> {
			rs.setCreatedBy(userId);
			rs.setCreatedDateTime(currentDateTime);
		});
		RuleThresholdConfig newThConfig = ruleThresholdConfigRepository.save(thresholdConfig);
		em.persist(thresholdConfig);
		_logger.info("Inserted rule threshold config  " + thresholdConfig.getRuleId() + " to repository");
		return newThConfig;
	}

	@Transactional
	public RuleThresholdConfig updateThresholdConfig(RuleThresholdConfig thresholdConfig, Integer userId) {
		String currentDateTime = DateTimeUtils.getCurrentDateTime();
		ruleThresholdConfigRepository.findByRtcId(thresholdConfig.getRtcId()).setStatus("Draft");
		ruleThresholdConfigRepository.findByRtcId(thresholdConfig.getRtcId()).setLastUpdatedBy(userId);
		ruleThresholdConfigRepository.findByRtcId(thresholdConfig.getRtcId()).setLastUpdatedDateTime(currentDateTime);
		RuleThresholdConfig newThConfig = ruleThresholdConfigRepository.save(thresholdConfig);
		_logger.info("Updqted rule threshold config  " + thresholdConfig.getRuleId() + " to repository");
		return newThConfig;
	}

	@Transactional
	public String createThresholdConfigQuery(RuleThresholdConfig thresholdConfig, Integer userId) {
		Rule rule = ruleRepository.findOne(thresholdConfig.getRuleId());
		_logger.info("Rule lookback time:" + rule.getLookback());
		String lookbacktype = rule.getLookback().getType();
		String hiveSchema = rule.getHiveSchema();
		String lookbackvalue = rule.getLookback().getValue();
		String currentDateTime = DateTimeUtils.getCurrentDateTime();
		List<RtcRunDate> rtcRunDates = thresholdConfig.getRtcRunDates();
		List<RtcThresholdLabel> rtcThresholdLabels = thresholdConfig.getRtcThresholdLabels();

		List<Condition> conditions = conditionRepo.findByRuleIdOrderBySequenceidDesc(thresholdConfig.getRuleId());
		String tempDate = null;
		String resultColumn = null;
		StringBuffer unionQuery = null;
		String crUnionQry = null;
		String unionQueryTable = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String exec_id = "\\$\\{exec_id\\}";
		String lookback_date = "\\$\\{lookback_date\\}";
		String run_date = "\\$\\{run_date\\}";
		String active_rtcid = "\\$\\{activeRtcId\\}";
		String texecid = "\\$\\{texec_id\\}";
		String rtcid = "\\$\\{rtcid\\}";
		String segRiskTable = "rtcthresholdsegmentrisk_" + thresholdConfig.getRuleId()
				+ "_\\$\\{rtcid\\}_\\$\\{exec_id\\}";
		String[] queries = null;
		String[] resultTables = null;
		String segment, risk;
		Query query = em.createNativeQuery("SELECT NEXT VALUE FOR SEQUENCEFETCHER");
		String execSequence = "TEXEC_" + String.valueOf((BigInteger) query.getSingleResult());

		_logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>sequence value:::" + execSequence);

		StringBuffer percentiles = new StringBuffer();
		for (double p = 10; p < 1000; p += 10) {
			if (p / 1000 == 0.01) {
				percentiles.append(Double.toString(p / 1000));
			} else {
				percentiles.append("," + Double.toString(p / 1000));
			}
		}
		_logger.info("100 percentiles:" + percentiles);
		for (Condition c : conditions) {
			if (c.getAlertInsertSql() != null || c.getType().equalsIgnoreCase("thresholdLabel")) {
				for (RtcRunDate dt : rtcRunDates) {
					try {
						String inputDate = dt.getRunDate();
						String runDatestr = inputDate.substring(0, 4) + "-" + inputDate.substring(5, 7) + "-"
								+ inputDate.substring(8, 10);

						System.out.print("+++++++++++++++++++++++RUNNING for RUN DATE " + runDatestr);

						Date runDate = sdf.parse(runDatestr);
						Calendar cal = Calendar.getInstance();
						cal.setTime(runDate);
						Date lookbackdate = null;

						_logger.info("lookback type:" + lookbacktype);
						_logger.info("lookback type:" + lookbackvalue);

						if (lookbacktype.equalsIgnoreCase("Year(s)")) {
							lookbackdate = DateUtils.addYears(cal.getTime(), -Integer.parseInt(lookbackvalue));
						} else if (lookbacktype.equalsIgnoreCase("Month(s)")) {
							lookbackdate = DateUtils.addMonths(cal.getTime(), -Integer.parseInt(lookbackvalue));

						} else if (lookbacktype.equalsIgnoreCase("Day(s)")) {
							lookbackdate = DateUtils.addDays(cal.getTime(), -Integer.parseInt(lookbackvalue));
						} else if (lookbacktype.equalsIgnoreCase("Week(s)")) {
							lookbackdate = DateUtils.addWeeks(cal.getTime(), -Integer.parseInt(lookbackvalue));
						} else {
							lookbackdate = DateUtils.addDays(cal.getTime(), -0);
						}

						String lookbackdatestr = sdf.format(lookbackdate);

						_logger.info("setting execute query=========" + thresholdConfig.getRuleId());
						_logger.info("look bck date =========" + lookbackdatestr);
						_logger.info("Run date =========" + sdf.format(runDate));
						String replace_exec_id = thresholdConfig.getRtcId()
								+ sdf.format(runDate).substring(0, 4) + sdf.format(runDate).substring(5, 7)
								+ sdf.format(runDate).substring(8, 10);

						if (c.getAlertInsertSql() != null) {
							queries = c.getAlertInsertSql().split("\\|");
							resultTables = c.getResultTable().split("\\|");
							for (int i = 0; i < queries.length; i++) {
								RuleThresholdQuery ruleThresholdQuery = new RuleThresholdQuery();
								ruleThresholdQuery.setRuleId(thresholdConfig.getRuleId());
								ruleThresholdQuery.setRtcId(thresholdConfig.getRtcId());
								ruleThresholdQuery.setRunDate(runDatestr);
								ruleThresholdQuery.setSequenceId(c.getSequenceid());
								ruleThresholdQuery.setExecutionId(execSequence);
								ruleThresholdQuery.setCreatedBy(userId);
								ruleThresholdQuery.setCreatedDateTime(currentDateTime);

								String dropTableQuery = "DROP TABLE IF EXISTS " + hiveSchema + "."
										+ resultTables[i].replaceAll(exec_id, replace_exec_id);
								_logger.info("Drop query:" + dropTableQuery);
								ruleThresholdQuery.setExecuteQuery(dropTableQuery);
								RuleThresholdQuery createQuery = new RuleThresholdQuery(ruleThresholdQuery); // copy
																												// before
																												// save
								ruleThresholdQueryRepo.save(ruleThresholdQuery);

								_logger.info("Drop RuleThresholdQuery record added ....");
								
								createQuery.setExecuteQuery(queries[i]
										.replaceAll(segRiskTable,
												"rtcthresholdsegmentrisk_" + thresholdConfig.getRuleId() + "_"
														+ thresholdConfig.getRtcId().toString() + "_" + execSequence)
										.replaceAll(exec_id, replace_exec_id).replaceAll(lookback_date, lookbackdatestr)
										.replaceAll(active_rtcid, thresholdConfig.getRtcId().toString())
										.replaceAll(rtcid, thresholdConfig.getRtcId().toString())
										.replaceAll(run_date, sdf.format(runDate)));

								createQuery.setQueryType(Integer.toString(i));

								if (c.getAlertCreateSql() != null) {
									ruleThresholdQuery
											.setCreateQuery(c.getAlertCreateSql().replaceAll(exec_id, replace_exec_id));
								}
								createQuery.setCreatedBy(userId);
								createQuery.setCreatedDateTime(currentDateTime);
								ruleThresholdQueryRepo.save(createQuery);

								_logger.info("Creat RuleThresholdQuery record added ....");
							}
						}
						if (c.getType().equalsIgnoreCase("thresholdlabel")) {
							resultColumn = c.getResultColumn();
							_logger.info("Threshold lable " + c.getThresholdLabel());
							if (tempDate != null) {
								unionQuery.append(" UNION ALL ");
							} else {

								unionQuery = new StringBuffer();
								unionQueryTable = "UNION_" + thresholdConfig.getRuleId() + "_"
										+ thresholdConfig.getRtcId() + "_"
										+ c.getThresholdLabel().replaceAll("[^\\w\\s]", "").replaceAll("\\s+", "");

								unionQuery.append("DROP table if exists " + hiveSchema + "." + unionQueryTable);
								RuleThresholdQuery unionThresholdQuery = new RuleThresholdQuery();

								unionThresholdQuery.setExecuteQuery(unionQuery.toString());
								unionThresholdQuery.setRunDate(null);
								unionThresholdQuery.setCreateQuery(null);
								unionThresholdQuery.setSequenceId(c.getSequenceid());
								unionThresholdQuery.setQueryType("0");
								unionThresholdQuery.setExecutionId(execSequence);
								unionThresholdQuery.setThresholdLabel(c.getThresholdLabel());
								unionThresholdQuery.setRuleId(thresholdConfig.getRuleId());
								unionThresholdQuery.setRtcId(thresholdConfig.getRtcId());
								unionThresholdQuery.setCreatedBy(userId);
								unionThresholdQuery.setCreatedDateTime(currentDateTime);
								_logger.info("writing union query... ");
								ruleThresholdQueryRepo.save(unionThresholdQuery);

								unionQuery = new StringBuffer();
								crUnionQry = "CREATE TABLE IF NOT EXISTS " + hiveSchema + "." + unionQueryTable
										+ " AS ";
								tempDate = new String(sdf.format(runDate));
							}
							_logger.info("unionQuery 1 " + unionQuery);
							unionQuery.append("select * from " + hiveSchema + "."
									+ c.getResultTable().replaceAll(exec_id, replace_exec_id));
						}

					} catch (ParseException e) {
						_logger.info("Error in lookback date:");
						_logger.error(e.toString());
					}
				}

				_logger.info("union query before " + unionQuery);

				if (unionQuery != null) {
					RuleThresholdQuery unionThresholdQuery = new RuleThresholdQuery();
					_logger.info("inside loop: " + unionQuery);
					_logger.info(crUnionQry + " Select DISTINCT x.* from (" + unionQuery.toString() + " ) x");
					unionThresholdQuery.setExecuteQuery(crUnionQry
							+ "SELECT  msp.segment_name as segment_name, untb.* FROM ( SELECT DISTINCT x.* FROM ("
							+ unionQuery.toString() + " ) x) untb JOIN " + hiveSchema
							+ ".modelsegmentpartyref msp ON msp.party_key = untb.party_key AND msp.model_id ="
							+ thresholdConfig.getModelId());
					unionThresholdQuery.setRunDate(null);
					unionThresholdQuery.setCreateQuery(null);
					unionThresholdQuery.setSequenceId(c.getSequenceid());
					unionThresholdQuery.setQueryType("1");
					unionThresholdQuery.setExecutionId(execSequence);
					unionThresholdQuery.setThresholdLabel(c.getThresholdLabel());
					unionThresholdQuery.setRuleId(thresholdConfig.getRuleId());
					unionThresholdQuery.setRtcId(thresholdConfig.getRtcId());
					unionThresholdQuery.setCreatedBy(userId);
					unionThresholdQuery.setCreatedDateTime(currentDateTime);
					_logger.info("writing union query... ");

					ruleThresholdQueryRepo.save(unionThresholdQuery);

					// preparing for percentile query
					String focalEntity = rule.getFocalEntity();
					_logger.info("Fiocal entity: " + focalEntity);

					switch (focalEntity) {
					case "Account":
						segment = "acct_segment";
						risk = "acct_risk";
						break;

					case "Customer":

						segment = "party_segment";
						risk = "party_risk";
						break;

					case "Transaction":

						segment = "tran_segment";
						risk = "tran_risk";
						break;

					default:

						segment = "party_segment";
						risk = "party_risk";
						break;
					}

					RuleThresholdQuery percentileThresholdQuery = new RuleThresholdQuery(unionThresholdQuery);
					percentileThresholdQuery.setQueryType("2");
					percentileThresholdQuery.setExecuteQuery("SELECT segment_name , percentile_approx(" + resultColumn
							+ ", array(" + "0.01,0.02,0.03,0.04,0.05,0.06,0.07,0.08,0.09,0.10,\n"
							+ "0.11,0.12,0.13,0.14,0.15,0.16,0.17,0.18,0.19,0.20,\n"
							+ "0.21,0.22,0.23,0.24,0.25,0.26,0.27,0.28,0.29,0.30,\n"
							+ "0.31,0.32,0.33,0.34,0.35,0.36,0.37,0.38,0.39,0.40,\n"
							+ "0.41,0.42,0.43,0.44,0.45,0.46,0.47,0.48,0.49,0.50,\n"
							+ "0.51,0.52,0.53,0.54,0.55,0.56,0.57,0.58,0.59,0.60,\n"
							+ "0.61,0.62,0.63,0.64,0.65,0.66,0.67,0.68,0.69,0.70,\n"
							+ "0.71,0.72,0.73,0.74,0.75,0.76,0.77,0.78,0.79,0.80,\n"
							+ "0.81,0.82,0.83,0.84,0.85,0.86,0.87,0.88,0.89,0.90,\n"
							+ "0.91,0.92,0.93,0.94,0.95,0.96,0.97,0.98,0.99),1000) from " + hiveSchema + "."
							+ unionQueryTable + " group by segment_name");

					percentileThresholdQuery.setCreatedBy(userId);
					percentileThresholdQuery.setCreatedDateTime(currentDateTime);
					_logger.info("writing percentile query... ");
					ruleThresholdQueryRepo.save(percentileThresholdQuery);

					// writing population query
					RuleThresholdQuery populationThresholdQuery = new RuleThresholdQuery(percentileThresholdQuery);
					populationThresholdQuery.setQueryType("3");
					populationThresholdQuery.setExecuteQuery("SELECT segment_name, count(*) as population from "
							+ hiveSchema + "." + unionQueryTable + " group by segment_name");
					populationThresholdQuery.setCreatedBy(userId);
					populationThresholdQuery.setCreatedDateTime(currentDateTime);
					_logger.info("writing population query... ");
					ruleThresholdQueryRepo.save(populationThresholdQuery);

					// writing standard deviation queries
					RuleThresholdQuery stdThresholdQuery = new RuleThresholdQuery(percentileThresholdQuery);
					stdThresholdQuery.setQueryType("4");
					stdThresholdQuery.setExecuteQuery(
							"SELECT x.seg, -4*x.std,-3*x.std,-2*x.std,-1*x.std,0*x.std,1*x.std,2*x.std,3*x.std,4*x.std from ( SELECT "
									+ " segment_name as seg, stddev(" + resultColumn + ") as std from " + hiveSchema
									+ "." + unionQueryTable + " group by segment_name)x");
					stdThresholdQuery.setCreatedBy(userId);
					stdThresholdQuery.setCreatedDateTime(currentDateTime);
					_logger.info("writing standard deviation query... ");
					ruleThresholdQueryRepo.save(stdThresholdQuery);

					tempDate = null;
					unionQuery = null;

				}

			}
		}
		_logger.info("100 percentiles:" + percentiles);
		return execSequence;
	}

	public List<ExecutionPayload> getThresholdSqls(Integer ruleid, Integer rtcid, String texecid) {

		_logger.info("in getThresholdSqls>>>>>>>>>>>>");
		_logger.info("Ruleid:" + ruleid + ",rtcid:" + rtcid + ",texecid:" + texecid);

		List<RuleThresholdQuery> ruleThresholdQueries = ruleThresholdQueryRepo.getThresholdQueries(ruleid, rtcid,
				texecid);
		List<ExecutionPayload> executionPayloads = new ArrayList<ExecutionPayload>();
		_logger.info("ruleThresholdQueries:" + ruleThresholdQueries);
		for (RuleThresholdQuery ruleThresholdQuery : ruleThresholdQueries) {
			ExecutionPayload executionPayload = new ExecutionPayload();
			List<String> sqlQueries = new ArrayList<String>();
			executionPayload.setActionType("GD");
			executionPayload.setRuleId(ruleThresholdQuery.getRuleId());
			executionPayload.setId(ruleThresholdQuery.getRuleThresholdSQLId());
			executionPayload.setSequenceId(ruleThresholdQuery.getSequenceId());
			if (ruleThresholdQuery.getQueryType() != null) {
				if (ruleThresholdQuery.getQueryType().equalsIgnoreCase("2")
						|| ruleThresholdQuery.getQueryType().equalsIgnoreCase("3")
						|| ruleThresholdQuery.getQueryType().equalsIgnoreCase("4")) {
					executionPayload.setGetResult(true);
				}
			}
			sqlQueries.add(ruleThresholdQuery.getCreateQuery());
			sqlQueries.add(ruleThresholdQuery.getExecuteQuery());
			executionPayload.setSqlQueries(sqlQueries);
			executionPayloads.add(executionPayload);
		}
		return executionPayloads;
	}

	public void updateRTCStatus(int id, String status, int userId) {
		String currentDateTime = DateTimeUtils.getCurrentDateTime();
		//RuleThresholdQuery ruleThresholdQuery = ruleThresholdQueryRepo.findOne(executionPayloads.get(0).getId());
		//System.out.println("Failed RTC Id:" + ruleThresholdQueryRepo.findOne(executionPayloads.get(0).getId()).getRtcId());
		System.out.println("Failed RTC Id:" + id + " " + status);
		RuleThresholdConfig ruleThresholdConfig = ruleThresholdConfigRepository.findByRtcId(id);
		ruleThresholdConfig.setStatus(status);
		ruleThresholdConfig.setLastUpdatedBy(userId);
		ruleThresholdConfig.setLastUpdatedDateTime(currentDateTime);
		ruleThresholdConfigRepository.save(ruleThresholdConfig);
	}
	
	public void processQueryResults(List<ExecutionPayload> executionPayloads, int userId) {
		int rtcid = 0;
		String currentDateTime = DateTimeUtils.getCurrentDateTime();
		String currThLabel = null;
		String prevThLabel = null;
		Map<String, List<String>> results = null;
		RuleThresholdConfig ruleThresholdConfig = null;
		RuleThresholdQuery ruleThresholdQuery = null;
		for (ExecutionPayload executionPayload : executionPayloads) {
			_logger.info("processQueryResults>>>>>>>>>>>>>>>>" + executionPayload.isGetResult());
			if (executionPayload.isGetResult()) {
				results = executionPayload.getResultData();
				ruleThresholdQuery = ruleThresholdQueryRepo.findOne(executionPayload.getId());
				currThLabel = ruleThresholdQuery.getThresholdLabel();
				if (prevThLabel != null && currThLabel.equalsIgnoreCase(prevThLabel)) {
					if (ruleThresholdQuery.getQueryType().equalsIgnoreCase("3")) {
						updateCVForPopulation(results, ruleThresholdQuery, ruleThresholdConfig);
					} else if (ruleThresholdQuery.getQueryType().equalsIgnoreCase("4")) {
						updateCVandPreCVForSD(results, ruleThresholdQuery, ruleThresholdConfig);
					}
				} else {
					ruleThresholdConfig = ruleThresholdConfigRepository.findByRtcId(ruleThresholdQuery.getRtcId());
					if (ruleThresholdQuery.getQueryType().equalsIgnoreCase("2")) {
						updateCVandPreCVForPercentile(results, ruleThresholdQuery, ruleThresholdConfig);
					}
				}
				prevThLabel = currThLabel;
			}
		}
		ruleThresholdConfig.setStatus("Generated Data");
		ruleThresholdConfig.setLastUpdatedBy(userId);
		ruleThresholdConfig.setLastUpdatedDateTime(currentDateTime);
		ruleThresholdConfigRepository.save(ruleThresholdConfig);
	}

	@Transactional
	public void updateCVandPreCVForPercentile(Map<String, List<String>> results, RuleThresholdQuery ruleThresholdQuery,
			RuleThresholdConfig ruleThresholdConfig) {

		if (ruleThresholdQuery.getQueryType() != null) {
			String thresholdLabel = ruleThresholdQuery.getThresholdLabel();
			List<RtcCalculatedValue> rtcCalculatedValues = new ArrayList<RtcCalculatedValue>();

			results.forEach((segment, values) -> {
				RtcCalculatedValue rtcCalculatedValue = new RtcCalculatedValue();
				rtcCalculatedValue.setSegmentName(segment);
				rtcCalculatedValue.setThresholdLabel(thresholdLabel);
				List<RtcPrecalculatedValue> rtcPrecalculatedValues = new ArrayList<RtcPrecalculatedValue>();
				percent = 0;
				values.forEach(p -> {
					RtcPrecalculatedValue rtcPrecalculatedValue = new RtcPrecalculatedValue();
					percent = percent + 1;
					rtcPrecalculatedValue.setFuncName("P" + String.valueOf(percent));
					rtcPrecalculatedValue.setValue(Double.parseDouble(p));
					rtcPrecalculatedValue.setRuleid(ruleThresholdQuery.getRuleId());
					rtcPrecalculatedValue.setRtcid(ruleThresholdQuery.getRtcId());
					rtcPrecalculatedValue.setTexecid(ruleThresholdQuery.getExecutionId());
					rtcPrecalculatedValue.setType("percentile");
					rtcPrecalculatedValue.setSegmentname(segment);
					rtcPrecalculatedValue.setThresholdlabel(thresholdLabel);
					// rtcPrecalculatedValue.setFuncName("P5");
					rtcPrecalculatedValues.add(rtcPrecalculatedValue);
				});
				rtcCalculatedValue.setPrecalculatedValues(rtcPrecalculatedValues);
				rtcCalculatedValues.add(rtcCalculatedValue);
			});
			// ruleThresholdConfig.setRtcCalculatedValue(rtcCalculatedValues); //SK
			ruleThresholdConfig.getRtcCalculatedValue().addAll(rtcCalculatedValues);
			// ruleThresholdConfigRepository.save(ruleThresholdConfig);
		}
		_logger.info("pre calculated ends=========");
	}

	@Transactional
	public void updateCVandPreCVForSD(Map<String, List<String>> results, RuleThresholdQuery ruleThresholdQuery,
			RuleThresholdConfig ruleThresholdConfig) {

		if (ruleThresholdQuery.getQueryType() != null) {
			String thresholdLabel = ruleThresholdQuery.getThresholdLabel();
			List<RtcCalculatedValue> rtcCalculatedValues = ruleThresholdConfig.getRtcCalculatedValue();
			results.forEach((segment, values) -> {
				RtcCalculatedValue rtcCalculatedValue = getRtcCalculatedValue(rtcCalculatedValues, segment,
						thresholdLabel);
				List<RtcPrecalculatedValue> rtcPrecalculatedValues = new ArrayList<RtcPrecalculatedValue>();
				std = -5;
				values.forEach(p -> {
					RtcPrecalculatedValue rtcPrecalculatedValue = new RtcPrecalculatedValue();
					std = std + 1;
					rtcPrecalculatedValue.setFuncName(String.valueOf(std) + "SD");
					rtcPrecalculatedValue.setValue(Double.parseDouble(p));
					rtcPrecalculatedValue.setRuleid(ruleThresholdQuery.getRuleId());
					rtcPrecalculatedValue.setRtcid(ruleThresholdQuery.getRtcId());
					rtcPrecalculatedValue.setTexecid(ruleThresholdQuery.getExecutionId());
					rtcPrecalculatedValue.setType("standard deviation");
					rtcPrecalculatedValue.setSegmentname(segment);
					rtcPrecalculatedValue.setThresholdlabel(thresholdLabel);
					rtcCalculatedValue.getPrecalculatedValues().add(rtcPrecalculatedValue);
				});
			});
		}
		_logger.info("SD calculated ends=========");
	}

	@Transactional
	public void updateCVForPopulation(Map<String, List<String>> results, RuleThresholdQuery ruleThresholdQuery,
			RuleThresholdConfig ruleThresholdConfig) {

		if (ruleThresholdQuery.getQueryType() != null) {
			RtcPrecalculatedValue rtcPrecalculatedValue = null;
			List<RtcPrecalculatedValue> rtcPrecalculatedValues = null;
			String thresholdLabel = ruleThresholdQuery.getThresholdLabel();
			List<RtcCalculatedValue> rtcCalculatedValues = ruleThresholdConfig.getRtcCalculatedValue();
			results.forEach((segment, values) -> {
				RtcCalculatedValue rtcCalculatedValue = getRtcCalculatedValue(rtcCalculatedValues, segment,
						thresholdLabel);
				values.forEach(p -> {
					rtcCalculatedValue.setPopulation(Integer.valueOf(p));
				});
			});
		}
		_logger.info("Update of population ends=========");
	}

	private RtcCalculatedValue getRtcCalculatedValue(List<RtcCalculatedValue> rtcCalculatedValues, String segmentName,
			String thresholdLbl) {
		for (RtcCalculatedValue rtcCalculatedValue : rtcCalculatedValues) {
			if (segmentName.equalsIgnoreCase(rtcCalculatedValue.getSegmentName())
					&& thresholdLbl.equalsIgnoreCase(rtcCalculatedValue.getThresholdLabel())) {
				return rtcCalculatedValue;
			}
		}
		return null;
	}

	public List<ExecutionPayload> createTestData(Integer ruleid, Integer rtcid, String texecid, Integer userId) {
		_logger.info("in createTestData>>>>>>>>>>>>");
		_logger.info("Ruleid:" + ruleid + ",rtcid:" + rtcid + ",texecid:" + texecid);

		List<RuleThresholdQuery> ruleThresholdQueries = ruleThresholdQueryRepo.getThresholdQueries(ruleid, rtcid,
				texecid);
		List<ExecutionPayload> executionPayloads = new ArrayList<ExecutionPayload>();
		_logger.info("ruleThresholdQueries:" + ruleThresholdQueries);
		for (RuleThresholdQuery ruleThresholdQuery : ruleThresholdQueries) {
			if (ruleThresholdQuery.getQueryType() != null) {
				if (ruleThresholdQuery.getQueryType().equalsIgnoreCase("2")
						|| ruleThresholdQuery.getQueryType().equalsIgnoreCase("3")
						|| ruleThresholdQuery.getQueryType().equalsIgnoreCase("4")) {
					ExecutionPayload executionPayload = new ExecutionPayload();
					// List<String> sqlQueries = new ArrayList<String>();
					executionPayload.setGetResult(true);
					executionPayload.setActionType("GD");
					executionPayload.setRuleId(ruleThresholdQuery.getRuleId());
					executionPayload.setId(ruleThresholdQuery.getRuleThresholdSQLId());
					executionPayload.setSequenceId(ruleThresholdQuery.getSequenceId());
					executionPayloads.add(executionPayload);
					if (ruleThresholdQuery.getQueryType().equalsIgnoreCase("2")) {
						int percent = 0;
						List<String> percentiles = new ArrayList<String>();
						Double p = 94.45;
						for (int i = 1; i < 21; i++) {
							Double y = p - (3 * i);
							double roundoff = (double) Math.round(y * 100) / 100;
							percentiles.add(Double.toString(roundoff));
						}
						Map<String, List<String>> results = new HashMap<String, List<String>>();
						results.put("all", percentiles);
						executionPayload.setResultData(results);
					}
					if (ruleThresholdQuery.getQueryType().equalsIgnoreCase("3")) {
						int percent = 0;
						List<String> percentiles = new ArrayList<String>();
						percentiles.add("1000");
						Map<String, List<String>> results = new HashMap<String, List<String>>();
						results.put("all", percentiles);
						executionPayload.setResultData(results);
					}
					if (ruleThresholdQuery.getQueryType().equalsIgnoreCase("4")) {
						List<String> percentiles = new ArrayList<String>();
						// int stdd = -5;
						List<String> stds = new ArrayList<String>();
						Double p = 94.45;
						for (int i = -4; i < 5; i++) {
							Double x = p * i;
							double roundoff = (double) Math.round(x * 100) / 100;
							stds.add(Double.toString(roundoff));
						}

						Map<String, List<String>> results = new HashMap<String, List<String>>();
						results.put("all", stds);
						executionPayload.setResultData(results);
					}

				}
			}
		}
		_logger.info("Test Data:" + executionPayloads);
		return executionPayloads;

	}

	@Transactional
	public void executeRuleThresholdQueries(RuleThresholdConfig ruleThresholdConfig, String texecid, int userId) {

		List<RuleThresholdQuery> ruleThresholdquerylist = ruleThresholdQueryRepo
				.getThresholdQueries(ruleThresholdConfig.getRuleId(), ruleThresholdConfig.getRtcId(), texecid);

		List<RtcCalculatedValue> rtcCalculatedValues = new ArrayList<RtcCalculatedValue>();
		for (RuleThresholdQuery ruleThresholdQuery : ruleThresholdquerylist) {
			// assuming received the list of percentiles
			_logger.info(">>>>>>>>>>>>>query type:" + ruleThresholdQuery.getQueryType());

			if (ruleThresholdQuery.getQueryType() != null) {
				RtcQueryOutput rtcQueryOutput = new RtcQueryOutput();
				rtcQueryOutput.setSegName("all");
				rtcQueryOutput.setPopulation(1000);

				if (ruleThresholdQuery.getQueryType().equalsIgnoreCase("2")) {
					int percent = 0;
					List<Double> percentiles = new ArrayList<Double>();
					Double p = 94.45;
					for (int i = 1; i < 21; i++) {
						Double y = p - (3 * i);
						double roundoff = (double) Math.round(y * 100) / 100;
						percentiles.add(roundoff);
					}
					rtcQueryOutput.setValues(percentiles);
				}
				RtcCalculatedValue rtcCalculatedValue = new RtcCalculatedValue();
				List<RtcPrecalculatedValue> rtcPrecalculatedValues = new ArrayList<RtcPrecalculatedValue>();
				if (ruleThresholdQuery.getQueryType().equalsIgnoreCase("2")) {
					ruleThresholdQuery.setQueryOutput(rtcQueryOutput);
					rtcCalculatedValue.setSegmentName(ruleThresholdQuery.getQueryOutput().getSegName());
					rtcCalculatedValue.setThresholdLabel(ruleThresholdQuery.getThresholdLabel());
					rtcCalculatedValue.setCalculatedFuncName(rtcQueryOutput.getFuncName());
					_logger.info("before rtc calculated----" + ruleThresholdQuery.getRtcId());
					rtcCalculatedValues.add(rtcCalculatedValue);
					ruleThresholdConfig.setRtcCalculatedValue(rtcCalculatedValues);
					_logger.info("record inserted into rtccalculatedvalue table=========");
					List<Double> values = ruleThresholdQuery.getQueryOutput().getValues();
					_logger.info("pre calculated started =========" + ruleThresholdQuery.getQueryType());
					int percentx = 0;
					for (Double value : values) {
						RtcPrecalculatedValue rtcPrecalculatedValue = new RtcPrecalculatedValue();
						if (values.size() == 20) {
							percentx = percentx + 5;
							rtcPrecalculatedValue.setFuncName("P" + String.valueOf(percentx));
						}
						rtcPrecalculatedValue.setValue(value);
						rtcPrecalculatedValue.setRuleid(ruleThresholdQuery.getRuleId());
						rtcPrecalculatedValue.setRtcid(ruleThresholdQuery.getRtcId());
						rtcPrecalculatedValue.setPopulation(rtcQueryOutput.getPopulation().toString());
						rtcPrecalculatedValue.setTexecid(ruleThresholdQuery.getExecutionId());
						rtcPrecalculatedValue.setType("percentile");
						rtcPrecalculatedValue.setSegmentname(rtcQueryOutput.getSegName());
						rtcPrecalculatedValue.setThresholdlabel(ruleThresholdQuery.getThresholdLabel());
						rtcPrecalculatedValues.add(rtcPrecalculatedValue);
					}

					int stdd = -5;
					List<Double> stds = new ArrayList<Double>();
					Double p = 94.45;
					for (int i = -4; i < 5; i++) {
						Double x = p * i;
						double roundoff = (double) Math.round(x * 100) / 100;
						stds.add(roundoff);
					}
					rtcQueryOutput.setValues(stds);
					ruleThresholdQuery.setQueryOutput(rtcQueryOutput);
					List<Double> stdvalues = ruleThresholdQuery.getQueryOutput().getValues();
					int stddx = -5;
					for (double stdval : stdvalues) {
						if (stdvalues.size() == 9) {
							stddx = stddx + 1;
						}
						RtcPrecalculatedValue rtcPrecalculatedValue = new RtcPrecalculatedValue();
						rtcPrecalculatedValue.setFuncName(String.valueOf(stddx) + "SD");
						rtcPrecalculatedValue.setValue(stdval);
						rtcPrecalculatedValue.setRuleid(ruleThresholdQuery.getRuleId());
						rtcPrecalculatedValue.setRtcid(ruleThresholdQuery.getRtcId());
						rtcPrecalculatedValue.setPopulation(rtcQueryOutput.getPopulation().toString());
						rtcPrecalculatedValue.setTexecid(ruleThresholdQuery.getExecutionId());
						rtcPrecalculatedValue.setType("standard deviation");
						rtcPrecalculatedValue.setSegmentname(rtcQueryOutput.getSegName());
						rtcPrecalculatedValue.setThresholdlabel(ruleThresholdQuery.getThresholdLabel());
						rtcPrecalculatedValues.add(rtcPrecalculatedValue);
					}
				}
				rtcCalculatedValue.setPrecalculatedValues(rtcPrecalculatedValues);
				ruleThresholdConfig.setStatus("Generated Data");
				ruleThresholdConfig.setLastUpdatedBy(userId);
				ruleThresholdConfig.setLastUpdatedDateTime(DateTimeUtils.getCurrentDateTime());
				ruleThresholdConfigRepository.save(ruleThresholdConfig);
				_logger.info("pre calculated ends=========");
			}
		}
	}
}

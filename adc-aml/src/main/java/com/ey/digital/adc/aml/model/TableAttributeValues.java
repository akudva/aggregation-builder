package com.ey.digital.adc.aml.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ey.digital.adc.core.model.IModel;

import lombok.Data;

@Entity
@Table(name = "tableattributevalues")
@Data
public class TableAttributeValues implements IModel {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	
	@Column(name = "tablename")
	private String tableName;
	
	@Column(name = "tableattribute")
	private String tableAttribute;
	
	@Column(name = "attributevalue")
	private String attributeValue;
}

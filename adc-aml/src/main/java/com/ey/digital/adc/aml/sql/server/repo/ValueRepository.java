package com.ey.digital.adc.aml.sql.server.repo;

import org.springframework.stereotype.Repository;

import com.ey.digital.adc.aml.model.Value;
import com.ey.digital.adc.core.sql.server.repo.ISQLRepository;

@Repository
public interface ValueRepository extends ISQLRepository<Value, Integer>{

}

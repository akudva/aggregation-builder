package com.ey.digital.adc.aml.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.ey.digital.adc.core.model.IModel;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.EqualsAndHashCode;


public class RtcQueryOutput implements IModel {
	
	private String  segName;
	private String  riskLevel;
	private Integer population;
	private String  funcName;
	private List<Double>  values;

	
	
		
	public String getSegName() {
		return segName;
	}




	public void setSegName(String segName) {
		this.segName = segName;
	}




	public String getRiskLevel() {
		return riskLevel;
	}




	public void setRiskLevel(String riskLevel) {
		this.riskLevel = riskLevel;
	}




	public Integer getPopulation() {
		return population;
	}




	public void setPopulation(Integer population) {
		this.population = population;
	}




	public String getFuncName() {
		return funcName;
	}




	public void setFuncName(String funcName) {
		this.funcName = funcName;
	}




	public List<Double> getValues() {
		return values;
	}




	public void setValues(List<Double> values) {
		this.values = values;
	}




	@Override
	public String toString() {
		return "RtcQueryOutput [segName=" + segName + ", riskLevel=" + riskLevel + " values=" + values + "]";
	}
	
	

	
	
}

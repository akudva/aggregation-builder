package com.ey.digital.adc.aml.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ey.digital.adc.core.model.IModel;

import lombok.Data;

@Data
public class ExecutionPayload implements IModel{
	
	private Integer id;
	private String actionType;
	private Integer sequenceId;
	private Integer ruleId;
	private boolean getResult;
	private List<String> sqlQueries = new ArrayList<>();
	private Map<String, List<String>> resultData = new HashMap<>();

  public ExecutionPayload() {

}

	public ExecutionPayload(ExecutionPayload executionPayload) {
	     this.id = executionPayload.id;
	     this.actionType = executionPayload.actionType;
	     this.sequenceId = executionPayload.sequenceId;
	     this.ruleId = executionPayload.ruleId;
	     this.getResult = executionPayload.getResult;
	     this.sqlQueries = executionPayload.sqlQueries;
	     this.resultData = executionPayload.resultData;

	  }

}

package com.ey.digital.adc.aml.sql.server.repo;


import org.springframework.stereotype.Repository;

import com.ey.digital.adc.aml.model.Filter;
import com.ey.digital.adc.core.sql.server.repo.ISQLRepository;

@Repository
public interface FilterRepository extends ISQLRepository<Filter, Integer>{

}

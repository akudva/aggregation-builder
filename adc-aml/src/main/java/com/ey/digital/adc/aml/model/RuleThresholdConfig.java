package com.ey.digital.adc.aml.model;


import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import com.ey.digital.adc.core.model.Audit;
import com.ey.digital.adc.core.model.IModel;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

//@EqualsAndHashCode(callSuper = false,exclude={"rtcSegment","rtcThresholdLabel","rtcRiskLevel","rtcRunDate"})
@Data
@Entity
@Table(name = "rulethresholdconfig")
public class RuleThresholdConfig extends Audit implements IModel {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "rtcid")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer rtcId;

    @NotNull(message = "Rule Id cannot be Null/Empty")
    @Column(name = "ruleid")
    private Integer ruleId;

    @NotNull(message = "Config name cannot be Null/Empty")
    @Column(name = "configname")
    private String configName;

    @Column(name = "status")
    private String status;

    @Transient
    private List<Integer> rtcIds;

    @Transient
    private String isApprove;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "rtcid", referencedColumnName = "rtcid", nullable = false)
    private List<RtcSegment> rtcSegments;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "rtcid", referencedColumnName = "rtcid", nullable = false)
    private List<RtcThresholdLabel> rtcThresholdLabels;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "rtcid", referencedColumnName = "rtcid", nullable = false)
    private List<RtcRiskLevel> rtcRiskLevels;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "rtcid", referencedColumnName = "rtcid", nullable = false)
    private List<RtcRunDate> rtcRunDates;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "rtcid", referencedColumnName = "rtcid", nullable = false)
    private List<RtcCalculatedValue> rtcCalculatedValue;

    @JsonProperty(value = "scheduledJobs")
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "rtcid", referencedColumnName = "rtcid", nullable = true)
    private List<RuleExecutionJob> ruleExecutionJobs;

    @JsonIgnore()
    @ManyToOne
    @JoinColumn(name = "ruleid", insertable = false, updatable = false)
    Rule rule;
    @Column(name = "approvalcomment")
    @JsonIgnore
    private String approvalComment;

    @Column(name = "rejectioncomment")
    @JsonIgnore
    private String rejectionComment;
    
    @Column(name="modelid")
    @JsonIgnore
    private Integer modelId;

    @Column(name = "approvedby")
    @JsonIgnore
    private Integer approvedBy;

    @Column(name = "approveddtm")
    @JsonIgnore
    private String approvedDtm;

    @Column(name = "rejectedby")
    @JsonIgnore
    private Integer rejectedBy;

    @Column(name = "rejecteddtm")
    @JsonIgnore
    private String rejectedDtm;

    @Override
    public String toString() {
        return "Threshold Config [configId=" + getRtcId() + ", configName=" + configName + ", status=" + status + "]";
    }

    public Integer getRtcId() {
        return rtcId;
    }

    public void setRtcId(Integer rtcId) {
        this.rtcId = rtcId;
    }
}

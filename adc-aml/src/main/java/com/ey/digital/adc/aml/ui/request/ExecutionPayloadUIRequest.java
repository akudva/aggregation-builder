package com.ey.digital.adc.aml.ui.request;

import com.ey.digital.adc.aml.model.ExecutionPayload;
import com.ey.digital.adc.core.ui.request.AbstractUIRequest;

public class ExecutionPayloadUIRequest extends AbstractUIRequest<ExecutionPayload>{

	private static final long serialVersionUID = 1L;
	
	
	@Override
	public String toString() {
		return "ExecutionPayloadUIRequest [message=" + getMessage() + ", toString()=" + super.toString() + "]";
	}

}

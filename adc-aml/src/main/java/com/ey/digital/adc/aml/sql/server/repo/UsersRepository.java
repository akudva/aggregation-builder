/**
 * 
 */
package com.ey.digital.adc.aml.sql.server.repo;



import org.springframework.stereotype.Component;

import com.ey.digital.adc.aml.model.Users;
import com.ey.digital.adc.core.sql.server.repo.ISQLRepository;



/**
 * @author mulaysh
 * Date Created: 2016-12-10
 */
@Component
public interface UsersRepository extends ISQLRepository<Users, Integer> {
	Users findByEmail(String email);
}

package com.ey.digital.adc.aml.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.ey.digital.adc.core.model.IModel;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = false, exclude = { "rulethresholdconfig" })
@Data
@Entity
@Table(name = "rtccalculatedvalue")
public class RtcCalculatedValue implements IModel {

	private static final long serialVersionUID = 1369129635458082496L;

	@Id
	@Column(name = "rtccalculatedvalueid")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer rtcCalculatedValueId;

	@Column(name = "rtcid", insertable = false, updatable = false)
	@JsonIgnore
	private Integer rtcId;
	//
	// @ManyToOne
	// @JoinColumn(name="rtcid")
	// private RuleThresholdConfig ruleThresholdConfig;

	// @Column(name = "segmentid")
	// private Integer segmentId;
	// changed from segid to seg name.
	@Column(name = "segmentname")
	private String segmentName;

	// @Column(name = "thresholdlabelid")
	// private Integer thresholdLabelId;
	// changed from thresholdid to threshold name
	@Column(name = "thresholdlabel")
	private String thresholdLabel;

	// @Column(name = "isselected")
	// private String isSelected;

	@Column(name = "calculatedtype")
	private String calculatedType;

	@Column(name = "calculatedfuncname")
	private String calculatedFuncName;

	@Column(name = "calculatedvalue")
	private Float calculatedValue;

	@Column(name = "population")
	private Integer population;

	@Column(name = "commitsql")
	private String commitsql;

	@Column(name = "comment")
	private String comment;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "rtccalculatedvalueid", referencedColumnName = "rtccalculatedvalueid", nullable = false) //, insertable = false, updatable = false)
	private List<RtcPrecalculatedValue> precalculatedValues;

	@Override
	public String toString() {
		return "RtcCalculatedValue [segmentname=" + segmentName + ",thresholdLabel=" + thresholdLabel
				+ ",calculatedType=" + calculatedType + ",calculatedFuncName=" + calculatedFuncName
				+ ",calculatedValue=" + calculatedValue + "]";
	}

}

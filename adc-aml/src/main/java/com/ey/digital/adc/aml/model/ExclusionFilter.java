package com.ey.digital.adc.aml.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.ey.digital.adc.core.model.Audit;
import com.ey.digital.adc.core.model.IModel;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.EqualsAndHashCode;


@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "exclusionfilter")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ExclusionFilter implements IModel{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "filterid")
	private Integer filterid;
	
	@Column(name = "type")
	private String type;
	
	//Bidirectional mapping with rule table
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "ruleid")
//	private Rule rule_id;
	
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "parent")    // Doesn't matter what you call it :)
//	@JsonIgnore
//	private ExclusionFilter parent;
	
	// Unidirectional mapping to value object
	@OneToOne(cascade=CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "exclusionfiltervalue")
	private FilterValue value;

	@Column(name = "resulttable")
	@JsonIgnore
	private String resultTable;
	
	@Column(name = "resultcolumn")
	@JsonIgnore
	private String resultcolumn;
	
	@Column(name = "resultdata")
	@JsonIgnore
	private String resultdata;

	@Column(name = "resultjoinkey")
	@JsonIgnore
	private String resultjoinkey;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY) 
	@JoinColumn(name = "parent")    // Unidirectional
	private List<ExclusionFilter> children;

	@Override
	public String toString() {
		return "ExclusionFilter [filterid=" + filterid + ", type=" + type + ", value=" + value
				+ ", resultTable=" + resultTable + ", resultcolumn=" + resultcolumn + ", resultdata=" + resultdata
				+ ", resultjoinkey=" + resultjoinkey + "]";
	}

}

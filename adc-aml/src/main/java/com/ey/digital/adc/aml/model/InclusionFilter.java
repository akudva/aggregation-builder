package com.ey.digital.adc.aml.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.ey.digital.adc.core.model.Audit;
import com.ey.digital.adc.core.model.IModel;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.EqualsAndHashCode;


@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "inclusionfilter")
public class InclusionFilter implements IModel{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "filterid")
	private Integer filterid;
	
	@Column(name = "type")
	private String type;
	
	// Unidirectional mapping to value object
	@OneToOne(cascade=CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "inclusionfiltervalue")
	private FilterValue value;

	@Column(name = "resulttable")
	@JsonIgnore
	private String resultTable;
	
	@Column(name = "resultcolumn")
	@JsonIgnore
	private String resultcolumn;
	
	@Column(name = "resultdata")
	@JsonIgnore
	private String resultdata;

	@Column(name = "resultjoinkey")
	@JsonIgnore
	private String resultjoinkey;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY) 
	@JoinColumn(name = "parent")    // Unidirectional
	private List<InclusionFilter> children;

	@Override
	public String toString() {
		return "InclusionFilter [filterid=" + filterid + ", type=" + type 
				+ ", resultTable=" + resultTable + ", resultcolumn=" + resultcolumn 
				+ ", resultdata=" + resultdata + ", resultjoinkey=" + resultjoinkey + "]";
	}

}

/**
 * 
 */
package com.ey.digital.adc.core.sql.server.utils;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author mulaysh
 * Date Created: 2016-12-30
 */
@Component
public class SqlServerUtils {

	private static final Logger _logger = LoggerFactory.getLogger(SqlServerUtils.class);
	
	@PersistenceContext
	private EntityManager entityManager;
	
	public int validateToken(String token) {
		int userId = -1;
		_logger.debug("running validate token stored procedure");
		StoredProcedureQuery storedProcedure = 
				getEntityManager().createStoredProcedureQuery("dbo.usp_ValidateToken");
		storedProcedure.registerStoredProcedureParameter("tokenvarchar", String.class, ParameterMode.IN);
		storedProcedure.registerStoredProcedureParameter("user_id", Integer.class, ParameterMode.OUT);
		
		storedProcedure.setParameter("tokenvarchar", token);
		
		storedProcedure.execute();
		userId = Integer.valueOf(storedProcedure.getOutputParameterValue("user_id").toString());
		_logger.info("returning user id [{}]", userId);
		return userId;
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
}

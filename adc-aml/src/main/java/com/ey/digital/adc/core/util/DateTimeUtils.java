/**
 * 
 */
package com.ey.digital.adc.core.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.ey.digital.adc.core.exceptions.InvalidDateException;



/**
 * @author mulaysh
 * Date Created: 2017-01-18
 */
public final class DateTimeUtils {
	public static String getCurrentDateTime(){
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss.SSS");
		LocalDateTime now = LocalDateTime.now();
		return dtf.format(now);
	}
	
	public static boolean validateDates(String startDate, String endDate) throws InvalidDateException{
		if (startDate == null || "".equals(startDate.trim())){
			throw new InvalidDateException("start date can't be null/empty");
		}
		if (endDate == null || "".equals(endDate.trim())){
			throw new InvalidDateException("end date can't be null/empty");
		}
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
		LocalDate localStartDate = LocalDate.from(formatter.parse(startDate));
		LocalDate localEndDate = LocalDate.from(formatter.parse(endDate));
		//DateTime startDateTime = formatter.parse(startDate);
		//DateTime endDateTime = formatter.parse(endDate);
		
		if (localEndDate.isBefore(LocalDate.now())){
			throw new InvalidDateException("end date is in the past");
		}
		if (localStartDate.isAfter(localEndDate)){
			throw new InvalidDateException("start date is after end date");
		}
		return true;
	}


}

package com.ey.digital.adc.aml.ui.response;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"name",
"description"
})
public class SegmentDetails {

@JsonProperty("name")
private String name;
@JsonProperty("description")
private String description;
@JsonProperty("focalentity")
private String focalentity;

/**
* 
* @param name
* @param description
*/
public SegmentDetails(String name, String description, String focalentity) {
super();
this.name = name;
this.description = description;
this.focalentity = focalentity;
}

}
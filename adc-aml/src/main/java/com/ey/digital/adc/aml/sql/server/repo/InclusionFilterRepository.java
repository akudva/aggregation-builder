package com.ey.digital.adc.aml.sql.server.repo;

import com.ey.digital.adc.aml.model.InclusionFilter;
import com.ey.digital.adc.core.sql.server.repo.ISQLRepository;

public interface InclusionFilterRepository extends ISQLRepository<InclusionFilter, Integer>{

}

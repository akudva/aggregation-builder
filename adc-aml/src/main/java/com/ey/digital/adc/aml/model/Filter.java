package com.ey.digital.adc.aml.model;



import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;


import com.ey.digital.adc.core.model.IModel;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "\"filter\"")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Filter implements IModel{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "filterid")
	private Integer filterId;
	
//	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
//	@JoinColumn(name = "parent")
//	@JsonIgnore
//	private Filter parent;
	
	@Column(name = "type")
	private String type;
	
	@Column(name = "ruleid")
	private Integer ruleId;
	
	@Column(name = "conditionid")
	private Integer conditionId;

	// Value object that contains the filters
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name = "valuedata")
//	@JsonIgnore
//	private Value filterval;
	
	// 
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "filterval")
	private FilterValue value;
	
	@Column(name = "resulttable")
	@JsonIgnore
	private String resultTable;
	
	@Column(name = "rescolumn")
	@JsonIgnore
	private String resultColumn;
	
	@Column(name = "resultdata")
	@JsonIgnore
	private String resultData;

	@Column(name = "resultjoinkey")
	@JsonIgnore
	private String resultjoinkey;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY) 
	@JoinColumn(name = "parent")    // Unidirectional
	private List<Filter> children;

	@Override
	public String toString() {
		return "Filter [filterId=" + filterId + ", type=" + type + ", ruleId=" + ruleId + ", conditionId=" + conditionId
				+ "]";
	}	

}

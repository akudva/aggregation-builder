package com.ey.digital.adc.aml.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data
public class RtcCalculatedThreshold {
	

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "rtccalculatedthresholdid")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer rtcCalculatedThresholdId;

	
    @Column(name = "thresholdlabelid")
    private Integer thresholdLabelId;

    
	@Column(name = "calculatedtype")
	private String calculatedType;
	
	@Column(name = "calculatedfuncname")
	private String calculatedFuncName;
	
	@Column(name = "calculatedvalue")
	private Float calculatedValue;
	
	
	@Override
	public String toString() {
		return "RtcCalculatedThreshold [thresholdLabelId=" +thresholdLabelId + "calculatedType=" + calculatedType + ", rtcId=" + "" + "]";
	}
	
	

	


}

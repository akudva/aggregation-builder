package com.ey.digital.adc.aml.sql.server.repo;



import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ey.digital.adc.aml.model.Rule;
import com.ey.digital.adc.aml.model.RuleThresholdConfig;
import com.ey.digital.adc.core.sql.server.repo.ISQLRepository;




public interface RuleThresholdConfigRepository extends ISQLRepository<RuleThresholdConfig, Integer> {
	
	@Query(value = "SELECT COALESCE(MAX(rtcid),0) FROM rulethresholdconfig ", nativeQuery=true)
	public Integer getMaxRtcId();

	public List<Integer> findDistinctRuleIdByStatus(String status);

	List<RuleThresholdConfig> findAllByRuleId(Integer ruleId);

	public RuleThresholdConfig findByRtcId(@Param("rtcid") Integer rtcId);

}
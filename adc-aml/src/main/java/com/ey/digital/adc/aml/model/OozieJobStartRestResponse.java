package com.ey.digital.adc.aml.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OozieJobStartRestResponse {
	
	@Override
	public String toString() {
		return "OozieJobStartRestResponse [id=" + id + "]";
	}

	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
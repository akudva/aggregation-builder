package com.ey.digital.adc.aml.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.ey.digital.adc.core.model.IModel;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "[value]")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Value implements IModel{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "valueid")
	private Integer valueId;
	
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "valuedata")
	private List<Filter> filters = new ArrayList<>();
	
	@Transient
	private Time time;
	
	@Transient
	private Offset offset;
	
	@Column(name = "aggregation")
	private String aggregation;
	
	@Column(name = "threshold_label")
	private String thresholdLabel;
	
	@Column(name = "data")
	private String data;
	
	@Column(name = "operator")
	private String operator;
	
	@Column(name = "logical")
	private String logical;
	
	@Column(name = "attribute")
	private String attribute;
	
	@Column(name = "value_table")
	private String table;
}

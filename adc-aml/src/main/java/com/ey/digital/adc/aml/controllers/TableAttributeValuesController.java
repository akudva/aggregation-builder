package com.ey.digital.adc.aml.controllers;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Valid;

import org.hibernate.validator.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ey.digital.adc.aml.model.TableAttributeValues;
import com.ey.digital.adc.aml.sql.server.repo.TableAttributeValueRepository;
import com.ey.digital.adc.aml.ui.request.TableAttributeValuesUIRequest;
import com.ey.digital.adc.core.controllers.AbstractController;

@RestController
@RequestMapping("/table-attribute-values")
public final class TableAttributeValuesController extends AbstractController<TableAttributeValuesUIRequest,TableAttributeValues>
{
	public TableAttributeValuesController(TableAttributeValueRepository repo) {
		super(repo);
		// TODO Auto-generated constructor stub
	}

	private final Logger _logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	TableAttributeValueRepository repo;

	@PersistenceContext
	protected EntityManager manager;

	@GetMapping(value = "/get-tables")
	public List<String> getTables(@Valid @NotEmpty(message = "token may not be Empty/Null") @RequestParam(value = "token") String token){
		
		return repo.getTableNames();
	}
	
	@GetMapping(value = "/get-attributes")
	public List<String> getAttributes(
			@Valid @NotEmpty(message = "token may not be Empty/Null") @RequestParam(value = "token") String token,
			@Valid @NotEmpty(message = "table name may not be Empty/Null") @RequestParam(value = "tablename") String tableName){
		return repo.getTableAttributesForTableName(tableName);
	}
	
	@GetMapping(value = "/get-values")
	public List<String> getValues(
			@Valid @NotEmpty(message = "token may not be Empty/Null") @RequestParam(value = "token") String token,
			@Valid @NotEmpty(message = "table name may not be Empty/Null") @RequestParam(value = "tablename") String tableName,
			@Valid @NotEmpty(message = "table attribute may not be Empty/Null") @RequestParam(value = "attribute") String tableAttribute){
		return repo.getAttributeValuesForTableNameAttribute(tableName, tableAttribute);
	}

	@Override
	public TableAttributeValues createInternal(int userId, TableAttributeValuesUIRequest request) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TableAttributeValues updateInternal(int userId, TableAttributeValuesUIRequest request) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TableAttributeValues> getListInternal(int listForId) {
		// TODO Auto-generated method stub
		return null;
	}

}

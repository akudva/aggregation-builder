/**
 * 
 */
package com.ey.digital.adc.aml.sql.server.repo;

import com.ey.digital.adc.aml.model.ActiveUser;
import com.ey.digital.adc.aml.model.RuleExecutionJob;
import com.ey.digital.adc.core.sql.server.repo.ISQLRepository;

import java.util.List;


/**
 * @author rudnevr
 * Date Created: 2017-09-04
 */
public interface RuleExecutionJobRepository extends ISQLRepository<RuleExecutionJob, Integer> {

    List<RuleExecutionJob> findAllByRtcidIn(List<Integer> rtcids);

    List<RuleExecutionJob> findAllByRtcidAndStatus(Integer ruleId, String status);
}

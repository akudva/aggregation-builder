package com.ey.digital.adc.aml.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.validation.Valid;
import javax.ws.rs.core.MediaType;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ey.digital.adc.aml.model.ExecutionPayload;
import com.ey.digital.adc.aml.model.RuleThresholdConfig;
import com.ey.digital.adc.aml.service.RuleThresholdConfigService;
import com.ey.digital.adc.aml.sql.server.repo.ConditionRepository;
import com.ey.digital.adc.aml.sql.server.repo.RuleThresholdConfigRepository;
import com.ey.digital.adc.aml.ui.request.ExecutionPayloadUIRequest;
import com.ey.digital.adc.aml.ui.request.RuleThresholdConfigUIRequest;
import com.ey.digital.adc.core.controllers.AbstractController;
import com.ey.digital.adc.core.exceptions.InvalidMessageException;
import com.ey.digital.adc.core.exceptions.InvalidTokenException;
import com.ey.digital.adc.core.ui.resp.utils.HttpResponseUtils;



@RestController
@RequestMapping("/generatedata")
public class GenerateDataController extends AbstractController<ExecutionPayloadUIRequest, ExecutionPayload> {
	
	private final static Logger _logger = Logger.getLogger(RuleController.class.getName());

	@Autowired
	private RuleThresholdConfigRepository ruleThresholdConfigRepository;
	
	@Autowired
	private RuleThresholdConfigService ruleThresholdConfigService;

	   public GenerateDataController(ConditionRepository repo) {
	        super(repo);
	        // TODO Auto-generated constructor stub
	    }

	@Override
	public ExecutionPayload createInternal(int userId, ExecutionPayloadUIRequest request) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ExecutionPayload updateInternal(int userId, ExecutionPayloadUIRequest request) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ExecutionPayload> getListInternal(int listForId) {
		// TODO Auto-generated method stub
		return null;
	}

	
	/**
	 * Get Threshold sqls
	 * 
	 * @param  request  ExecutionPayloadUIRequest
	 * @return 
	 */
	 @RequestMapping(value = "/getsqls",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON, params = {"token","ruleid", "rtcid","texecid"})
	    public ResponseEntity<List<ExecutionPayload>> getGenerateDataSqls(
	    		    @Valid @NotEmpty(message = "token may not be Empty/Null") @RequestParam(value = "token") String token,
	            @Valid @NotEmpty(message = "ruleid may not be Empty/Null") @RequestParam(value = "ruleid") Integer ruleid,
	            @Valid @NotEmpty(message = "rtcid may not be Empty/Null") @RequestParam(value = "rtcid") Integer rtcid,
	            @Valid @NotEmpty(message = "texecid may not be Empty/Null") @RequestParam(value = "texecid") String texecid
	            )
	            throws Exception {
		 int userId = 0;
	     //   List<Rule> rules = new ArrayList<Rule>();
		 _logger.log(Level.INFO, token + " ");

	        try {
	            userId = validateToken(token);
	        } catch (InvalidTokenException e) {
	        	_logger.log(Level.WARNING, e.getMessage());
	            return HttpResponseUtils.getInvalidTokenErrorResponse();
	        }
	        
		 List<ExecutionPayload> generateDataSqls  =  ruleThresholdConfigService.getThresholdSqls(ruleid,rtcid,texecid);
		 System.out.println("generate sqls:"+generateDataSqls);
		 return HttpResponseUtils.getResponse(generateDataSqls);
		 
	 }

	 /**
		 * Process Threshold query results
		 * 
		 * @param  request  ExecutionPayloadUIRequest
		 * @return 
		 */
	 	@PostMapping(value = "/thresholdqueryresults", consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
		@ResponseBody
		public void processQueryResults(@Valid @RequestBody ExecutionPayloadUIRequest request) throws Exception {
		 
			int userId = 0;
			userId = validateToken(request.getToken());

			List<ExecutionPayload> messages = request.getMessages();

			//RuleThresholdConfig returnThresholdConfig = null;
			

			if (messages == null)
				throw new InvalidMessageException("Null/empty message received from threshold query results");
			System.out.println("processQueryResults>>>>>>>>>>>>>>>>");
			
			ruleThresholdConfigService.processQueryResults(messages, userId);
		 
	 }
	 	
	 	/**
		 * Oozie job failed
		 * 
		 * @param  update the threshold generate data status to "Failed"
		 * @return 
		 */
	 	@PostMapping(value = "/failed", consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON, params = { "token", "id" ,"status"})
		public void processOozieFailed(
				@Valid @NotEmpty(message = "token may not be Empty/Null") @RequestParam(value = "token") String token,
				@Valid @NotEmpty(message = "id may not be Empty/Null") @RequestParam(value = "id") Integer id,
				@Valid @NotEmpty(message = "status may not be Empty/Null") @RequestParam(value = "status") String status)
				throws Exception {
	 		
	 		System.out.println("coming to failed>>>>>>>>>>>>>>");
	 		int userId = 0;
			userId = validateToken(token);
			ruleThresholdConfigService.updateRTCStatus(id,status, userId);
			
	 	}
	   
	  

	
}

package com.ey.digital.adc.aml.model;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper=false)
@MappedSuperclass
public abstract class AbstractModel {
		private static final long serialVersionUID = 1L;
		
		@Column(name = "createdby")
		protected int createdBy;

		@Transient
		protected String createdByName;

		@Column(name = "createddtm")
		protected String createdDateTime;

		@Column(name = "lastupdtuser")
		protected int lastUpdatedBy;

		@Transient
		protected String lastUpdatedByName;

		@Column(name = "lastupdtdtm")
		protected String lastUpdatedDateTime;

		@OneToOne
		@JoinColumn(name = "createdby", insertable=false, updatable = false)
		@JsonIgnore
		protected Users createdByUser;

		@OneToOne
		@JoinColumn(name = "lastupdtuser", insertable=false, updatable = false)
		@JsonIgnore
		protected Users lastUpdtByUser;

		@Override
		public String toString() {
			return "AbstractModel [createdBy=" + createdBy + ", createdByName=" + createdByName + ", createdDateTime="
					+ createdDateTime + ", lastUpdatedBy=" + lastUpdatedBy + ", lastUpdatedByName=" + lastUpdatedByName
					+ ", lastUpdatedDateTime=" + lastUpdatedDateTime + ", createdByUser=" + createdByUser
					+ ", lastUpdtByUser=" + lastUpdtByUser + "]";
		}

}

/**
 * 
 */
package com.ey.digital.adc.core.exceptions;

/**
 * @author mulaysh
 * Date Created: 2017-01-20
 */
public class InvalidCreateIdException extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * @param message
	 */
	public InvalidCreateIdException(String type) {
		super(String.format("%s ID is system generated, can't be populated in the POST request", type));
	}

}

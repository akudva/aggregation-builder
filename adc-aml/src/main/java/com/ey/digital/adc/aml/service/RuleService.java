package com.ey.digital.adc.aml.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RuleService {

	private final Logger _logger = LoggerFactory.getLogger(this.getClass());

	private final ConditionService conditionService;

	@Autowired
	public RuleService(ConditionService conditionService) {
		this.conditionService = conditionService;
		_logger.info("Created RuleService");
	}
}

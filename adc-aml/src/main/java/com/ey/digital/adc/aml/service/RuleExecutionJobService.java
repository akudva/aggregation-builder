package com.ey.digital.adc.aml.service;

import com.ey.digital.adc.aml.model.RuleExecutionJob;
import com.ey.digital.adc.aml.sql.server.repo.RuleExecutionJobRepository;
import com.ey.digital.adc.aml.sql.server.repo.RuleRepository;
import com.ey.digital.adc.aml.sql.server.repo.RuleThresholdConfigRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RuleExecutionJobService {

	private final Logger _logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	RuleExecutionJobRepository ruleExecutionJobRepository;
	@Autowired
	RuleRepository ruleRepository;
	@Autowired
	RuleThresholdConfigRepository ruleThresholdConfigRepository;

	@Autowired
	HiveExecutionService hiveExecutionService;

	public RuleExecutionJob createOrUpdate(RuleExecutionJob job) throws IOException, InterruptedException {
		RuleExecutionJob save = ruleExecutionJobRepository.save(job);
		hiveExecutionService.startOozieJob(job);
		return save;
	}

	public List<RuleExecutionJob> getJobsByRuleId(Integer ruleId) {
		List<Integer> rtcIds = ruleThresholdConfigRepository.findAllByRuleId(ruleId).stream()
				.map(ruleThresholdConfig -> ruleThresholdConfig.getRtcId()).collect(Collectors.toList());
		return ruleExecutionJobRepository.findAllByRtcidIn(rtcIds);
	}
}

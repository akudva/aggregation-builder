package com.ey.digital.adc.aml.ui.request;

import java.util.List;

import com.ey.digital.adc.aml.model.RiskLevel;
import com.ey.digital.adc.aml.model.Segment;

import lombok.Data;

import com.ey.digital.adc.aml.model.SegmentThresholdRequest;
import com.ey.digital.adc.aml.model.ThresholdMatrixSegment;
import com.ey.digital.adc.aml.model.ThresholdTuningInput;
import com.ey.digital.adc.core.model.IModel;
import com.ey.digital.adc.core.ui.request.AbstractUIRequest;

public class ThresholdTuningUIRequest extends AbstractUIRequest<ThresholdMatrixSegment> implements IModel {

}
package com.ey.digital.adc.aml.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.ey.digital.adc.core.model.IModel;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
@Table(name = "rtcprecalculatedvalue")
public class RtcPrecalculatedValue implements IModel{
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "rtcprecalculatedvalueid")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer RtcPrecalculatedValueId;
	
	@Column(name = "ruleid")
	private Integer ruleid;
	
	@Column(name = "rtcid")
	private Integer rtcid;
	
	@Column(name = "texecid")

	private String texecid;
	
	@Column(name = "segmentname")
	private String segmentname;
	
	@Column(name = "thresholdlabel")
	private String thresholdlabel;
	
	@Column(name = "population")
	private String population;
	
	@Column(name = "type")
	private String type;
	
	@Column(name = "name")
	private String funcName;
	
	@Column(name = "value")
	private Double value;

}
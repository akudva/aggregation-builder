/**
 * 
 */
package com.ey.digital.adc.aml.sql.server.repo;

import com.ey.digital.adc.aml.model.RuleExecutionJob;
import com.ey.digital.adc.aml.model.RuleExecutionResult;
import com.ey.digital.adc.core.sql.server.repo.ISQLRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;


/**
 * @author rudnevr
 * Date Created: 2017-09-04
 */
public interface RuleExecutionResultRepository extends ISQLRepository<RuleExecutionResult, Integer> {

    List<RuleExecutionResult> findAllByJobId(@Param("jobId") Integer jobId);
}

package com.ey.digital.adc.aml.oozie.rest.client;


import com.ey.digital.adc.aml.model.OozieJobStartRestResponse;
import com.ey.digital.adc.aml.model.RestResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties
@Data
public class OozieClient {

    private final Logger _logger = Logger.getLogger(OozieClient.class);

    HashMap<String, String> oozieConf;

    Properties prop = new Properties();

    InputStream input = null;
    private String oozieGdWorkflowFolder;

    public OozieClient() {
    }

    String oozieCmtExportWorkflowFolder;
    String oozieStartUrl;
    String oozieRtcCommitWorkflowFolder;
    String oozieRtcWorkflowFolder;
    String oozieOffMarketWorkflowFolder;
    String oozieHostname;
    String oozieProtocol;
    String ooziePort;
    String oozieContentType;
    String ooziePassword;
    String oozieUsername;

    @PostConstruct
    public void check() throws JsonProcessingException {
        System.out.println("oozieStartUrl = " + oozieStartUrl);
        System.out.println("props oozie = " + oozieConf);
    }


    private String serialize(Map<String, String> map) {
        StringBuilder payload = new StringBuilder("<configuration>");
        map.forEach((key, value) -> {
            payload.append("<property><name>");
            payload.append(key);
            payload.append("</name><value>");
            payload.append(value);
            payload.append("</value></property>");

        });
        String s = payload.append("</configuration>").toString();
        System.out.println("s = " + s);
        return s;
    }

    public String createPayloadForNoLabel(Integer ruleid, Integer rtcid, String texecid,
                                          String hiveSchema) {
        HashMap<String, String> map = new HashMap<>(this.oozieConf);
        if (hiveSchema != null)
            map.put("client", hiveSchema);
        map.put("oozie.wf.application.path", oozieOffMarketWorkflowFolder);
//        map.put("ruleid", ruleid.toString());
//        map.put("rtcid", rtcid.toString());
        map.put("texecid", texecid);
//        map.put("exec_id", texecid); //
        map.put("action", "RE");
        map.put("token", "82C36AF3-B220-459B-9AAD-88A81883E970");
        return serialize(map);
    }

    public String createPayloadForThresholdTuning(Integer ruleid, Integer rtcid, String texecid,
                                                  String hiveSchema) {
        HashMap<String, String> map = new HashMap<>(this.oozieConf);
        if (hiveSchema != null)
            map.put("client", hiveSchema);
        map.put("oozie.wf.application.path", oozieGdWorkflowFolder);
        map.put("ruleid", ruleid.toString());
        map.put("rtcid", rtcid.toString());
        map.put("texecid", texecid);
        map.put("exec_id", texecid);
        map.put("action", "RE");
        return serialize(map);
    }

    public String createPayloadForGenerateData(Integer ruleid, Integer rtcid, String texecid, String hiveSchema) {
        HashMap<String, String> map = new HashMap<>(this.oozieConf);
        if (hiveSchema != null)
            map.put("client", hiveSchema);
        map.put("oozie.wf.application.path", oozieGdWorkflowFolder);
        map.put("ruleid", ruleid.toString());
        map.put("rtcid", rtcid.toString());
        map.put("texecid", texecid);
        map.put("exec_id", texecid); //
        map.put("token", "82C36AF3-B220-459B-9AAD-88A81883E970");
        map.put("action", "GD");
        return serialize(map);
    }

    public String createCommitPayload(Integer ruleid, Integer rtcid, String texecid,
                                      String hiveSchema) {
        StringBuilder payload = new StringBuilder("<configuration>");

        Map<String, String> propertyMap = new LinkedHashMap<String, String>();
        if (hiveSchema != null)
            propertyMap.put("client", hiveSchema);
        propertyMap.put("ruleid", ruleid.toString());
        propertyMap.put("rtcid", rtcid.toString());
        propertyMap.put("texecid", texecid);
        propertyMap.put("exec_id", texecid); //
        propertyMap.put("action", "CM");
        propertyMap.put("modelid", "-1");
        propertyMap.put("user.name", prop.getProperty("oozie.username"));
        propertyMap.put("oozie.wf.application.path", oozieRtcCommitWorkflowFolder);
        propertyMap.put("oozie.use.system.libpath", "True");
        propertyMap.put("queueName", "default");
        propertyMap.put("nameNode", prop.getProperty("oozie.namenode"));
        propertyMap.put("jobTracker", prop.getProperty("oozie.jobtracker"));
        propertyMap.put("mapreduce.job.user.name", prop.getProperty("oozie.username"));
        propertyMap.put("dryrun", "false");
        propertyMap.put("hue-id-w", "137");
        propertyMap.put("security_enabled", "False");
        propertyMap.put("send_email", "False");
        final StringBuilder tempProperty = new StringBuilder();
        propertyMap.forEach((key, value) -> {
            tempProperty.append("<property><name>" + key + "</name><value>" + value + "</value></property>");
        });

        return payload.append(tempProperty.toString()).append("</configuration>").toString();
    }

    public String startCmtExport(String exportId, String jobId, String executionId, String rtcId, String ruleId, String hiveSchema, String ruleName) throws IOException {
    		ZonedDateTime utc = ZonedDateTime.now(ZoneOffset.UTC);
    		String currentTime = utc.format(DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss"));
       	HashMap<String, String> map = new HashMap<>(this.oozieConf);
        if (hiveSchema != null)
            map.put("client", hiveSchema);
        map.put("oozie.wf.application.path", oozieCmtExportWorkflowFolder);
        map.put("jobid", jobId);
        map.put("rtcid", rtcId);
        map.put("ruleid", ruleId);
        map.put("executionid", executionId);
        map.put("token", "82C36AF3-B220-459B-9AAD-88A81883E970");
        map.put("batchnumber", exportId);
        map.put("rulename", "'"+ruleName+"'");
        map.put("currenttime", "'"+currentTime+"'");
        
        return execute(RequestMethod.POST, serialize(map)).getRespBody();
    }

    public RestResponse oozieRestClientPost(String payload) throws IOException {
        return execute(RequestMethod.POST, payload);
    }


    public RestResponse oozieRestClientGet() throws IOException {
        return execute(RequestMethod.GET, null);
    }

    public RestResponse execute(RequestMethod requestMethod, String payload) throws IOException {
        RestResponse restResp = new RestResponse();
        HttpHost target = new HttpHost(oozieHostname,
                Integer.parseInt(ooziePort), oozieProtocol);
        CredentialsProvider credsProvider = new BasicCredentialsProvider();
        credsProvider.setCredentials(new AuthScope(target.getHostName(), target.getPort()),
                new UsernamePasswordCredentials(oozieUsername, ooziePassword));
        CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();
        AuthCache authCache = new BasicAuthCache();
        BasicScheme basicAuth = new BasicScheme();
        authCache.put(target, basicAuth);
        HttpClientContext localContext = HttpClientContext.create();
        localContext.setAuthCache(authCache);
        CloseableHttpResponse response = null;
        ObjectMapper mapper = new ObjectMapper();
        switch (requestMethod) {
            case GET:
                HttpGet getRequest = new HttpGet(oozieStartUrl);
                _logger.info("Executing request " + getRequest.getRequestLine() + " to target " + target);
                response = httpclient.execute(target, getRequest, localContext);
                break;
            case POST:
                HttpPost postRequest = new HttpPost(oozieStartUrl);
                StringEntity input = new StringEntity(payload);
                _logger.info("oozie payload " + payload);
                input.setContentType(oozieContentType);
                postRequest.setEntity(input);
                response = httpclient.execute(target, postRequest, localContext);
                String content = EntityUtils.toString(response.getEntity());
                OozieJobStartRestResponse job = mapper.readValue(content, OozieJobStartRestResponse.class);
                _logger.info("oozie id = " + job.getId());
                restResp.setRespBody(job.getId());
                break;
            case PUT:
                HttpPut put = new HttpPut(oozieStartUrl);
                StringEntity input1 = new StringEntity(payload);
                input1.setContentType(oozieContentType);
                put.setEntity(input1);
                _logger.info("Executing request " + put.getRequestLine() + " to target " + target);
                response = httpclient.execute(target, put, localContext);
        }
        httpclient.close();
        return restResp;
    }
}

package com.ey.digital.adc.aml.ui.request;

import com.ey.digital.adc.aml.model.RuleExecutionJob;
import com.ey.digital.adc.core.ui.request.AbstractUIRequest;


public class RuleExecutionJobUIRequest extends AbstractUIRequest<RuleExecutionJob> {
	private static final long serialVersionUID = 1L;
	
	
	@Override
	public String toString() {
		return "RuleUIRequest [message=" + getMessage() + ", toString()=" + super.toString() + "]";
	}

}

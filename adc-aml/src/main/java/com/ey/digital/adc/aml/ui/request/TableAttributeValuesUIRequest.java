package com.ey.digital.adc.aml.ui.request;

import com.ey.digital.adc.aml.model.TableAttributeValues;
import com.ey.digital.adc.core.ui.request.AbstractUIRequest;

public class TableAttributeValuesUIRequest extends AbstractUIRequest<TableAttributeValues> {

	private static final long serialVersionUID = 1L;
}

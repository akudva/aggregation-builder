package com.ey.digital.adc.aml.model;

import com.ey.digital.adc.core.model.IModel;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;

@Entity
@Table(name = "ruleexecutionjob")
@Data
public class RuleExecutionJob implements IModel {
    public JobStatus getStatus() {
        return status;
    }

    public void setStatus(JobStatus status) {
        this.status = status;
    }

    public enum JobStatus {pending, active, inactive}

    @Id
    @Column(name = "ruleexecutionjobid")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @Column(name = "rtcid")
    private Integer rtcid;
    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "rtcid", insertable = false, updatable = false)
    RuleThresholdConfig ruleThresholdConfig;
    private String frequency;
    @JsonFormat(pattern = "yyyy-MM-dd")
    @ElementCollection
    @CollectionTable(name = "customrundates", joinColumns = @JoinColumn(name = "ruleexecutionjobid"))
    @Column(name = "datevalue")
    private Set<Timestamp> customRunDates;
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "startrangerundate")
    private Timestamp startRangeRunDate;
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "endrangerundate")
    private Timestamp endRangeRunDate;
    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private JobStatus status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRtcid() {
        return rtcid;
    }

    public void setRtcid(Integer rtcid) {
        this.rtcid = rtcid;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public Set<Timestamp> getCustomRunDates() {
        return customRunDates;
    }

    public void setCustomRunDates(Set<Timestamp> customRunDates) {
        this.customRunDates = customRunDates;
    }

    public Timestamp getStartRangeRunDate() {
        return startRangeRunDate;
    }

    public void setStartRangeRunDate(Timestamp startRangeRunDate) {
        this.startRangeRunDate = startRangeRunDate;
    }

    public Timestamp getEndRangeRunDate() {
        return endRangeRunDate;
    }

    public void setEndRangeRunDate(Timestamp endRangeRunDate) {
        this.endRangeRunDate = endRangeRunDate;
    }

}

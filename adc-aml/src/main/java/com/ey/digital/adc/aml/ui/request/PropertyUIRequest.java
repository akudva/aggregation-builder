package com.ey.digital.adc.aml.ui.request;


import com.ey.digital.adc.aml.model.Property;
import com.ey.digital.adc.core.model.IModel;
import com.ey.digital.adc.core.ui.request.AbstractUIRequest;

public class PropertyUIRequest  extends AbstractUIRequest<Property> implements IModel {

}

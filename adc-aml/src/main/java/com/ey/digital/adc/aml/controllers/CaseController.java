package com.ey.digital.adc.aml.controllers;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.validator.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.ey.digital.adc.aml.model.Case;
import com.ey.digital.adc.aml.model.Page;
import com.ey.digital.adc.aml.sql.server.repo.CaseRepository;
import com.ey.digital.adc.aml.sql.server.repo.PaginationRepository;
import com.ey.digital.adc.core.exceptions.InvalidTokenException;
import com.ey.digital.adc.core.sql.server.utils.SqlServerUtils;
import com.ey.digital.adc.core.ui.resp.utils.HttpResponseUtils;
import com.ey.digital.adc.core.util.DateTimeUtils;

@RestController
@RequestMapping("/case")
public final class CaseController {

	private final Logger _logger = LoggerFactory.getLogger(this.getClass());
@Autowired SqlServerUtils sqlServerUtils;
	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	CaseRepository caseRepository;

	@PersistenceContext
	protected EntityManager manager;

	@RequestMapping(value = "/download")
	public void downloadBizArchTemplate(
			@Valid @NotEmpty(message = "token may not be Empty/Null") @RequestParam(value = "token") String token,
			@Valid @NotEmpty(message = "clientId may not be Empty/Null") @RequestParam(value = "clientId") Integer clientId,
			@Valid @NotEmpty(message = "pageNo may not be Empty/Null") @RequestParam(value = "pageNo") Integer pageNo,
			@Valid @NotEmpty(message = "pageSize may not be Empty/Null") @RequestParam(value = "pageSize") Integer pageSize,
			HttpServletResponse response) throws IOException, EncryptedDocumentException, InvalidFormatException {
		int userId = 0;
		try {
			userId = validateToken(token);
		} catch (InvalidTokenException e) {
			_logger.warn("Returning error response [{}]", e.getMessage());

		}
		long currentDateTime=new Date().getTime();
		String excelFile = "case_export_" + userId + "_" + currentDateTime + ".xlsx";
		Page<Case> casePage = caseRepository.getCasePageFromSQL(jdbcTemplate, clientId, pageNo, pageSize);
		List<Case> cases = casePage.getPageItems();
		Iterator<Case> it = cases.iterator();
		// Workbook wb = WorkbookFactory.create();
		Workbook wb = new XSSFWorkbook();
		Sheet sheet = wb.createSheet("Cases");
		int rowIndex = 0;
		Row row;
		row = sheet.createRow(rowIndex++);

		Cell cell0 = row.createCell(0);
		cell0.setCellValue("Case_id");

		Cell cell1 = row.createCell(1);
		cell1.setCellValue("Cust_id");

		Cell cell2 = row.createCell(2);
		cell2.setCellValue("Month_dt");

		Cell cell3 = row.createCell(3);
		cell3.setCellValue("Case_dt");

		Cell cell4 = row.createCell(4);
		cell4.setCellValue("Alert_ct");

		while (it.hasNext()) {
			Case _case = it.next();

			row = sheet.createRow(rowIndex++);

			cell0 = row.createCell(0);
			cell0.setCellValue(_case.getCase_id());

			cell1 = row.createCell(1);
			cell1.setCellValue(_case.getCust_id());

			cell2 = row.createCell(2);
			cell2.setCellValue(_case.getMonth_dt());

			cell3 = row.createCell(3);
			cell3.setCellValue(_case.getCase_dt());

			cell4 = row.createCell(4);
			cell4.setCellValue(_case.getAlert_ct());

		}
		// System.out.println("Path ="+ wb.getSheetAt(0).getRow(2));
		FileOutputStream out = new FileOutputStream(new File(excelFile));
		wb.write(out);
		out.close();
		File xlsxFile = new File(excelFile);
		response.setHeader("Content-disposition", "attachment;filename=" + excelFile);
		response.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
		response.setHeader("Access-Control-Allow-Origin", "*");
		InputStream inputStream = new BufferedInputStream(new FileInputStream(xlsxFile));
		FileCopyUtils.copy(inputStream, response.getOutputStream());
		xlsxFile.delete();
	}

	@RequestMapping(value = "/view")
	public Page<Case> getCases(
			@Valid @NotEmpty(message = "token may not be Empty/Null") @RequestParam(value = "token") String token,
			@Valid @NotEmpty(message = "clientId may not be Empty/Null") @RequestParam(value = "clientId") Integer clientId,
			@Valid @NotEmpty(message = "pageNo may not be Empty/Null") @RequestParam(value = "pageNo") Integer pageNo,
			@Valid @NotEmpty(message = "pageSize may not be Empty/Null") @RequestParam(value = "pageSize") Integer pageSize)
			throws SQLException {
		return caseRepository.getCasePageFromSQL(jdbcTemplate, clientId, pageNo, pageSize);

	}

	protected int validateToken(String token) throws InvalidTokenException {
		if (token == null || "".equals(token.trim())) {
			throw new InvalidTokenException();
		}
		final int userId = sqlServerUtils.validateToken(token);
		if (userId <= 0) {
			throw new InvalidTokenException();
		}
		return userId;
	}
}

package com.ey.digital.adc.aml.sql.server.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import com.ey.digital.adc.aml.model.TableAttributeValues;
import com.ey.digital.adc.core.sql.server.repo.ISQLRepository;

public interface TableAttributeValueRepository extends ISQLRepository<TableAttributeValues, Integer> {

	// Use POJO entity name instead of table name
	// Use POJO attribute names instead of table column names
	@Query("SELECT DISTINCT tableName FROM TableAttributeValues")
	List<String> getTableNames();

	@Query("SELECT DISTINCT tableAttribute FROM TableAttributeValues where tableName = ?1")	
	List<String> getTableAttributesForTableName(String tableName);

	@Query("SELECT DISTINCT attributeValue FROM TableAttributeValues WHERE tableName = ?1 AND tableAttribute = ?2")	
	List<String> getAttributeValuesForTableNameAttribute(String tableName, String tableAttribute);

}

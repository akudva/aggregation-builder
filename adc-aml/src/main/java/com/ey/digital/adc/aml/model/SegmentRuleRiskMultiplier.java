package com.ey.digital.adc.aml.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.ey.digital.adc.core.model.IModel;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Table(name = "segmentruleriskmult")
@Data
public class SegmentRuleRiskMultiplier implements IModel {
	@Id
	@Column(name = "segmentruleriskmultid")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer segmentRuleRiskMultId;
	
	@Column(name = "ruleconditionid")
	private String ruleConditionId;
	
	@Column(name = "ruleid")
	private Integer ruleid;
	
	@Column(name = "segmentid")
	private Integer segmentId;
	
	@Column(name = "risklevel")
	private String riskLevel;
	
	@Column(name = "threshold")
	private Integer threshold;
	
	@Column(name = "thresholdlabel")
	private String thresholdLabel;
	
	@NotNull
	@Column(name = "multiplier")
	private float multiplier;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "version")
	private Integer version;
	
	@ManyToOne()
	@JoinColumn(name = "ruleid", insertable=false, updatable = false)
	@JsonIgnore
	private Rule rule;
	
	@ManyToOne()
	@JoinColumn(name = "segmentid", insertable=false, updatable = false)
	@JsonIgnore
	private Segment segment;
	
	@Transient
	protected String segmentName;
	
	public void setSegmentName(String segmentName) {
		this.segmentName = segmentName;
	}

	/**
	 * @return the clientName
	 */
	public String getSegmentName() {
		if (segment != null){
			segmentName = segment.getName();
		}
		return segmentName;
	}
	
}
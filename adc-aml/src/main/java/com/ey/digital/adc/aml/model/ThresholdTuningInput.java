package com.ey.digital.adc.aml.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ey.digital.adc.core.model.Audit;
import com.ey.digital.adc.core.model.IModel;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"ruleId",
"thresholdSegmentRequest",
"isRiskTune",
"thresholdRiskRequest",
"runDates"
})
public class ThresholdTuningInput extends Audit implements IModel {

@JsonProperty("ruleId")
private Integer ruleId;
@JsonProperty("thresholdSegmentRequest")
private List<ThresholdSegmentsRequest> thresholdSegmentRequest = null;
@JsonProperty("isRiskTune")
private Boolean isRiskTune;
@JsonProperty("thresholdRiskRequest")
private List<ThresholdRiskRequest> thresholdRiskRequest = null;
@JsonProperty("runDates")
private List<String> runDates = null;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

/**
* No args constructor for use in serialization
* 
*/
public ThresholdTuningInput() {
}

/**
* 
* @param isRiskTune
* @param thresholdRiskRequest
* @param thresholdSegmentRequest
* @param ruleId
* @param runDates
*/
public ThresholdTuningInput(Integer ruleId, List<ThresholdSegmentsRequest> thresholdSegmentRequest, Boolean isRiskTune, List<ThresholdRiskRequest> thresholdRiskRequest, List<String> runDates) {
super();
this.ruleId = ruleId;
this.thresholdSegmentRequest = thresholdSegmentRequest;
this.isRiskTune = isRiskTune;
this.thresholdRiskRequest = thresholdRiskRequest;
this.runDates = runDates;
}

@JsonProperty("ruleId")
public Integer getRuleId() {
return ruleId;
}

@JsonProperty("ruleId")
public void setRuleId(Integer ruleId) {
this.ruleId = ruleId;
}

@JsonProperty("thresholdSegmentRequest")
public List<ThresholdSegmentsRequest> getThresholdSegmentsRequests() {
return thresholdSegmentRequest;
}

@JsonProperty("thresholdSegmentRequest")
public void setThresholdSegmentsRequests(List<ThresholdSegmentsRequest> thresholdSegmentRequest) {
this.thresholdSegmentRequest = thresholdSegmentRequest;
}

@JsonProperty("isRiskTune")
public Boolean getIsRiskTune() {
return isRiskTune;
}

@JsonProperty("isRiskTune")
public void setIsRiskTune(Boolean isRiskTune) {
this.isRiskTune = isRiskTune;
}

@JsonProperty("thresholdRiskRequest")
public List<ThresholdRiskRequest> getThresholdRiskRequests() {
return thresholdRiskRequest;
}

@JsonProperty("thresholdRiskRequest")
public void setThresholdRiskRequests(List<ThresholdRiskRequest> thresholdRiskRequest) {
this.thresholdRiskRequest = thresholdRiskRequest;
}

@JsonProperty("runDates")
public List<String> getRunDates() {
return runDates;
}

@JsonProperty("runDates")
public void setRunDates(List<String> runDates) {
this.runDates = runDates;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}
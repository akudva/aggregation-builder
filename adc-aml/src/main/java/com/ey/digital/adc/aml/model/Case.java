package com.ey.digital.adc.aml.model;

import com.ey.digital.adc.core.model.Audit;
import com.ey.digital.adc.core.model.IModel;

import lombok.Data;

@Data
public class Case implements IModel{

	
	private String case_id;
	private String cust_id;
	private String month_dt;
	private String case_dt;
	private Integer alert_ct;
	
	
	
	@Override
	public String toString() {
		return "Case [case_id=" + case_id + ", cust_id=" + cust_id + ", month_dt=" + month_dt + ", case_dt=" + case_dt
				+ ", alert_ct=" + alert_ct + "]";
	}
	public Case(String case_id, String cust_id, String month_dt, String case_dt, Integer alert_ct) {
		this.case_id = case_id;
		this.cust_id = cust_id;
		this.month_dt = month_dt;
		this.case_dt = case_dt;
		this.alert_ct = alert_ct;
	}

}

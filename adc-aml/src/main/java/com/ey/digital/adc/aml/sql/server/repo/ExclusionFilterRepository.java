package com.ey.digital.adc.aml.sql.server.repo;

import com.ey.digital.adc.aml.model.ExclusionFilter;
import com.ey.digital.adc.core.sql.server.repo.ISQLRepository;

public interface ExclusionFilterRepository extends ISQLRepository<ExclusionFilter, Integer>{

}

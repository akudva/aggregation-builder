package com.ey.digital.adc.aml.sql.server.repo;



import com.ey.digital.adc.aml.model.RtcSegment;
import com.ey.digital.adc.aml.model.RuleThresholdConfig;
import com.ey.digital.adc.core.sql.server.repo.ISQLRepository;




public interface RtcSegmentRepository extends ISQLRepository<RtcSegment, Integer> {
	

	
}
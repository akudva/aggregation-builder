package com.ey.digital.adc.aml.model;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.ey.digital.adc.core.model.Audit;
import com.ey.digital.adc.core.model.IModel;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "segment")
@Data
@EqualsAndHashCode(callSuper = false)
public class Segment extends Audit implements IModel{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "segmentid")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long segmentId;
	
	@Column(name = "name")
	private String Name;
	
	@Column(name = "focalentity")
	private String focalEntity;
	
	@Column(name = "clientid")
	private String clientId;
	
	@Column(name = "description")
	private String description;

	@Transient
	private Integer population;
	
}

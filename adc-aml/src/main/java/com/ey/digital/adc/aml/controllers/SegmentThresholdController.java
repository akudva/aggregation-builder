package com.ey.digital.adc.aml.controllers;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;


import javax.validation.Valid;
import javax.ws.rs.core.MediaType;

import org.hibernate.validator.constraints.NotEmpty;
import org.omg.CosNaming.Binding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ey.digital.adc.aml.model.RiskLevel;
import com.ey.digital.adc.aml.model.Rule;
//import com.ey.digital.adc.aml.model.RuleCondition;
import com.ey.digital.adc.aml.model.SegThresholdMetadata;
import com.ey.digital.adc.aml.model.Segment;
//import com.ey.digital.adc.aml.model.SegmentThreshold;
import com.ey.digital.adc.aml.model.SegmentThresholdRequest;
import com.ey.digital.adc.aml.model.ThresholdDefault;
import com.ey.digital.adc.aml.model.ThresholdMatrixSegment;
import com.ey.digital.adc.aml.model.ThresholdMatrixSegmentRisk;
import com.ey.digital.adc.aml.model.ThresholdMetadata;
import com.ey.digital.adc.aml.model.ThresholdMatrixSegment;
import com.ey.digital.adc.aml.model.ThresholdValue;
//import com.ey.digital.adc.aml.service.ThresholdSegmentService;
import com.ey.digital.adc.aml.model.SegmentThresholdRequest;
import com.ey.digital.adc.aml.sql.server.repo.ConditionRepository;
//import com.ey.digital.adc.aml.sql.server.repo.RuleConditionRepository;
import com.ey.digital.adc.aml.sql.server.repo.RuleRepository;
import com.ey.digital.adc.aml.sql.server.repo.SegmentRepository;
import com.ey.digital.adc.aml.sql.server.repo.SegmentThresholdRepository;
import com.ey.digital.adc.aml.sql.server.repo.ThresholdSegmentRiskRepository;
import com.ey.digital.adc.aml.ui.request.RuleUIRequest;
import com.ey.digital.adc.aml.ui.request.SegmentUIRequest;
import com.ey.digital.adc.aml.ui.request.ThresholdTuningUIRequest;
import com.ey.digital.adc.aml.ui.response.ResponseJSON;
import com.ey.digital.adc.aml.ui.response.ThresholdGnerateDataResponse;
import com.ey.digital.adc.core.controllers.AbstractController;
import com.ey.digital.adc.core.exceptions.InvalidCreateIdException;
import com.ey.digital.adc.core.exceptions.InvalidMessageException;
import com.ey.digital.adc.core.exceptions.InvalidTokenException;
import com.ey.digital.adc.core.model.ApiResponse;
import com.ey.digital.adc.core.ui.resp.utils.HttpResponseUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
//import groovy.lang.GroovyShell;


@RestController
@RequestMapping("/thresholdccccc")
public class SegmentThresholdController extends AbstractController<ThresholdTuningUIRequest, ThresholdMatrixSegment> {

	
	@Autowired
	private ThresholdSegmentRiskRepository thresholdSegRiskrepo;
	
	@Autowired
	private RuleRepository ruleRepo;
	
	@Autowired
	private ConditionRepository conditionRepo;

	//@Autowired
	//private RuleConditionRepository ruleCondRepo;
	
	private final Logger _logger = LoggerFactory.getLogger(this.getClass());
	
	public SegmentThresholdController(ThresholdSegmentRiskRepository thresholdSegRiskrepo, ConditionRepository conditionRepo ) {
		super(thresholdSegRiskrepo);
		this.thresholdSegRiskrepo=thresholdSegRiskrepo;
		this.conditionRepo=conditionRepo;
	}

	@Override
	public ThresholdMatrixSegment createInternal(int userId, ThresholdTuningUIRequest request) throws Exception {
		// TODO Auto-generated method stub
		return null;
		
		
	}

	@Override
	public ThresholdMatrixSegment updateInternal(int userId, ThresholdTuningUIRequest request) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ThresholdMatrixSegment> getListInternal(int listForId) {
		// TODO Auto-generated method stub
		return null;
	}
	

	@PostMapping(value = "/generateData", consumes = MediaType.APPLICATION_JSON,produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<ThresholdGnerateDataResponse> generateData(@Valid @RequestBody ThresholdTuningUIRequest request)
			throws Exception {
		
		int userId = 0;
		_logger.info("processing generateData for threshold tuning ", request.getToken(),
				request.getMessage());
		userId = validateToken(request.getToken());
		
		//List<String> errors = null;
		//ApiResponse apiResponse = null;
		ObjectMapper mapper = new ObjectMapper();
		ThresholdGnerateDataResponse thresholdGnerateDataResponse = new ThresholdGnerateDataResponse();

		try {					
			//RiskLevel riskLevel = null;
			ThresholdMatrixSegment message  = request.getMessage();
			Integer ruleId = message.getRuleId();
			System.out.println("Rule Id:" + ruleId);
			List<ThresholdMatrixSegmentRisk> ThresholdSegRiskList = new ArrayList<ThresholdMatrixSegmentRisk>();
			
			String jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(message);
			System.out.println(jsonInString);

			//get conditions(Threshold labels) 
			List<String> thresholdLabels = conditionRepo.findThersholds(ruleId); 
			
			System.out.println("threshoold label for rule id :" +  ruleId + " is " + thresholdLabels + " size " + thresholdLabels.size() );
			//save the record to ThresholdMatrixSegmentRisk
			
			message.getSegmentIds().forEach(s -> {
				   ThresholdMatrixSegmentRisk thSegOg = new ThresholdMatrixSegmentRisk();
			  	   // threshold original record
			    	   thSegOg.setRuleId(message.getRuleId());
			    	   thSegOg.setSegment(s.getName());
			    	   thSegOg.setIsSelected(s.getIsSelected());
			    	   thSegOg.setIsRiskTuned(message.getIsRiskTune());
			    	   thSegOg.setRiskLevel("OG");
			    	   thSegOg.setRiskValue("");
			    	   
			       message.getRiskLevels().forEach(r -> {
				    	   ThresholdMatrixSegmentRisk thSegRisk = new ThresholdMatrixSegmentRisk();
				    	   
				    	   thSegRisk.setRuleId(message.getRuleId());
				    	   
				    	   thSegRisk.setSegment(s.getName());
				    	   thSegRisk.setIsSelected(s.getIsSelected());
				    	   thSegRisk.setIsRiskTuned(message.getIsRiskTune());
				    	   thSegRisk.setRiskLevel(r.getRisk());
				    	   thSegRisk.setRiskValue(r.getValue());
				    	   
				    	   AtomicInteger atomicInteger = new AtomicInteger(1);
				    	   thresholdLabels.forEach(th -> {
				    		   Field declaredField =  null;
				    		   String thresholdFieldName = null;
				    		   String thresholdFieldValue = null;
				    		   String thresholdFieldType = null;
				    		   String thresholdFieldFunction = null;
				    		   boolean accessible;
				    		   if (th !=null) {
				    			   thresholdFieldName = "threshold"+atomicInteger.get()+"Name";
				    			   thresholdFieldType = "threshold"+atomicInteger.get()+"Type";
				    			   thresholdFieldValue = "threshold"+atomicInteger.get()+"Value";
				    			   thresholdFieldFunction = "threshold"+atomicInteger.get()+"Function";
				    			   atomicInteger.getAndIncrement();
				    			   try {
				    				   declaredField = ThresholdMatrixSegmentRisk.class.getDeclaredField(thresholdFieldName);
				    				   accessible = declaredField.isAccessible();
				    				   declaredField.setAccessible(true);
				    				   declaredField.set(thSegRisk,th);
				    				   declaredField.set(thSegOg,th);
				    				   declaredField.setAccessible(accessible);
				    				   
				    				   declaredField = ThresholdMatrixSegmentRisk.class.getDeclaredField(thresholdFieldType);
				    				   accessible = declaredField.isAccessible();
				    				   declaredField.setAccessible(true);
				    				   declaredField.set(thSegRisk,"");
				    				   declaredField.set(thSegOg,"");
				    				   declaredField.setAccessible(accessible);
				    				   
				    				   declaredField = ThresholdMatrixSegmentRisk.class.getDeclaredField(thresholdFieldValue);
				    				   accessible = declaredField.isAccessible();
				    				   declaredField.setAccessible(true);
				    				   declaredField.set(thSegRisk,"");
				    				   declaredField.set(thSegOg,"");
				    				   declaredField.setAccessible(accessible);
				    				   
				    				   declaredField = ThresholdMatrixSegmentRisk.class.getDeclaredField(thresholdFieldFunction);
				    				   accessible = declaredField.isAccessible();
				    				   declaredField.setAccessible(true);
				    				   declaredField.set(thSegRisk,"");
				    				   declaredField.set(thSegOg,"");
				    				   declaredField.setAccessible(accessible);
				    				   
				    				   
				    			   } catch (NoSuchFieldException
				    					   | IllegalAccessException e) {
				    				   _logger.error("", e);
				    			   }
				    			   //System.out.println(thresholdFieldName);
				    		   }
				    	   });
				    	   thSegRisk.setVersionId("");//need to determine in sprint 3
				    	   thSegRisk.setPopulation(0);// need to compute in sprint 3
//				    	   thSegOg.setVersionId("");
//				    	   thSegOg.setPopulation(0);
				    	   //thSegRisk
				    	   ThresholdSegRiskList.add(thSegRisk);
				    	  // ThresholdSegRiskList.add(thSegOg);
				    	   
			    	   });	
			       thSegOg.setVersionId("");
		    	       thSegOg.setPopulation(0);
			       ThresholdSegRiskList.add(thSegOg);
			});
			System.out.println("before adding ThresholdSegRiskList "+ThresholdSegRiskList);
            thresholdSegRiskrepo.save(ThresholdSegRiskList);  //uncomment to save the data into table
			
             
            Rule rule = ruleRepo.findOne(ruleId); /* need to check with Ashish if it is Int or String? */
            List<ThresholdDefault> thresholdDefaultList = new ArrayList<ThresholdDefault>();
	        thresholdGnerateDataResponse.setRule_id(ruleId);
	        thresholdGnerateDataResponse.setRule_name(rule.getDescription());
	        thresholdGnerateDataResponse.setRun_dates(null);
	        thresholdGnerateDataResponse.setVersion(null);
	        	        
	        thresholdLabels.forEach(label -> {
	        	    ThresholdDefault thresholdDefault = new ThresholdDefault();
	        	    thresholdDefault.setLabel(label);
	        	    thresholdDefault.setType("Amount");
	        	    thresholdDefault.setValue("1");
	        	    thresholdDefaultList.add(thresholdDefault);  
	         });
	        thresholdGnerateDataResponse.setThreshold_defaults(thresholdDefaultList);

	        List<SegThresholdMetadata> segThresholdMetadataList = new ArrayList<SegThresholdMetadata>();
	       // if (message.getIsRiskTune().equalsIgnoreCase("y")) {  // need to change the logic here
	        	   message.getSegmentIds().forEach(s -> {
	        	   SegThresholdMetadata segThMetadata  = new SegThresholdMetadata();
	        	   segThMetadata.setSegment(s.getName());
	        	   segThMetadata.setIsSelected(s.getIsSelected());
	        	   segThMetadata.setPopulation("");   	   
	        
	        	   List<ThresholdMetadata> thMetadataList = new ArrayList<ThresholdMetadata>();
		    	   thresholdLabels.forEach(label -> {
		    		   ThresholdMetadata thMetadata = new ThresholdMetadata();
		    		   thMetadata.setLabel(label);
		    		   thMetadata.setType("");// while generate data type should be empty. Next time there will be type of threshold in this attribute
		    		   ThresholdValue thresholdValue = new ThresholdValue();
		    		   thresholdValue.setFunction("");//// while generate data type should be empty. Next time there will be function name in this attribute
		    		   thresholdValue.setValue("");
		    		   thMetadata.setThresholdValue(thresholdValue);
		    		   thMetadataList.add(thMetadata);  		   
		    	   });
		    	   		segThMetadata.setThresholds(thMetadataList);
		    	   		segThresholdMetadataList.add(segThMetadata);
	        	    }) ;   
	       // }
	        thresholdGnerateDataResponse.setSegments(segThresholdMetadataList);
	
		} catch (Exception e) {
			_logger.error("", e);
			return HttpResponseUtils.getErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage());
		}
		
		return HttpResponseUtils.getResponse(HttpStatus.CREATED, "Success", thresholdGnerateDataResponse);
	}		
	
	
}

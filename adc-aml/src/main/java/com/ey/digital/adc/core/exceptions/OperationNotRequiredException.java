/**
 * 
 */
package com.ey.digital.adc.core.exceptions;

/**
 * @author mulaysh
 * Date Created: 2017-01-31
 */
public class OperationNotRequiredException extends Exception {
	private static final long serialVersionUID = 1L;

	/**
	 * @param message
	 */
	public OperationNotRequiredException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}
}

/**
 * 
 */
package com.ey.digital.adc.aml.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import com.ey.digital.adc.core.model.IModel;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author mulaysh
 * Date Created: 2016-12-30
 */
@Entity
@Table(name = "activeuser")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler", "status", "message" })
public class ActiveUser implements IModel{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;
	
	@Column(name = "loginid")
	private String loginId;
	
	@Column(name = "token", columnDefinition="uniqueidentifier")
	private String token;
	
	@Column(name = "logindttime")
	private String loginDtTime;
	
	@Column(name= "logoutdttime")
	private String logoutDtTime;
	
	@Column(name = "sessionvalidity")
	@JsonIgnore
	private String sessionValidity;

	@Transient 
	private String firstName;
	
	@Transient
	private String lastName;
	
	@Transient
	private String clientName;
	
	@Transient
	private Integer clientId;
	
	@Transient
	private int userId;
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the loginId
	 */
	public String getLoginId() {
		return loginId;
	}

	/**
	 * @param loginId the loginId to set
	 */
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * @return the loginDtTime
	 */
	public String getLoginDtTime() {
		return loginDtTime;
	}

	/**
	 * @param loginDtTime the loginDtTime to set
	 */
	public void setLoginDtTime(String loginDtTime) {
		this.loginDtTime = loginDtTime;
	}

	/**
	 * @return the logoutDtTime
	 */
	public String getLogoutDtTime() {
		return logoutDtTime;
	}

	/**
	 * @param logoutDtTime the logoutDtTime to set
	 */
	public void setLogoutDtTime(String logoutDtTime) {
		this.logoutDtTime = logoutDtTime;
	}

	/**
	 * @return the sessionValidity
	 */
	public String getSessionValidity() {
		return sessionValidity;
	}

	/**
	 * @param sessionValidity the sessionValidity to set
	 */
	public void setSessionValidity(String sessionValidity) {
		this.sessionValidity = sessionValidity;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
	/**
	 * @return the ptUserId
	 */
	//public Integer getPtUserId() {
		//if (activeUser != null){
			//ptUserId = activeUser.getPtUserId();
		//}
		//return ptUserId;
	//}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	public Integer getClientId() {
		return clientId;
	}

	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((loginDtTime == null) ? 0 : loginDtTime.hashCode());
		result = prime * result + ((loginId == null) ? 0 : loginId.hashCode());
		result = prime * result + ((logoutDtTime == null) ? 0 : logoutDtTime.hashCode());
		result = prime * result + ((sessionValidity == null) ? 0 : sessionValidity.hashCode());
		result = prime * result + ((token == null) ? 0 : token.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ActiveUser other = (ActiveUser) obj;
		if (id != other.id)
			return false;
		if (loginDtTime == null) {
			if (other.loginDtTime != null)
				return false;
		} else if (!loginDtTime.equals(other.loginDtTime))
			return false;
		if (loginId == null) {
			if (other.loginId != null)
				return false;
		} else if (!loginId.equals(other.loginId))
			return false;
		if (logoutDtTime == null) {
			if (other.logoutDtTime != null)
				return false;
		} else if (!logoutDtTime.equals(other.logoutDtTime))
			return false;
		if (sessionValidity == null) {
			if (other.sessionValidity != null)
				return false;
		} else if (!sessionValidity.equals(other.sessionValidity))
			return false;
		if (token == null) {
			if (other.token != null)
				return false;
		} else if (!token.equals(other.token))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ActiveUser [id=" + id + ", loginId=" + loginId + ", token=" + token + ", loginDtTime=" + loginDtTime
				+ ", logoutDtTime=" + logoutDtTime + ", sessionValidity=" + sessionValidity + "]";
	}
}

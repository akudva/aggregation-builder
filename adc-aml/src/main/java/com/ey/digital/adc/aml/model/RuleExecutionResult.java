package com.ey.digital.adc.aml.model;

import com.ey.digital.adc.core.model.IModel;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;

@Table(name = "ruleexecutionresult")
@Entity
@Data
public class RuleExecutionResult implements IModel {
    @Id
    @Column(name = "ruleexecutionresultid")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer executionId;
    @Column(name = "ruleexecutionjobid")
    private Integer jobId;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "ruleexecutionjobid", insertable = false, updatable = false)
    private RuleExecutionJob ruleExecutionJob;

    private String status;
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    @Column(name = "startexecutiontime")
    private Timestamp startExecutionTime;
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    @Column(name = "endexecutiontime")
    private Timestamp endExecutionTime;

    @Column(name = "oozieid")
    @JsonIgnore
    private String oozieId;

    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    @Column(name = "rundate")
    private Timestamp runDate;
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    @Column(name = "lookbackdate")
    private Timestamp lookbackDate;

    @Column(name = "alertnumber")
    Integer alertNumber;

    @JsonIgnore
    public String getExecutionString() {
        return executionId.toString() + "X";
    }
}

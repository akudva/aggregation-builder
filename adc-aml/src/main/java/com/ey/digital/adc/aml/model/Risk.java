package com.ey.digital.adc.aml.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ey.digital.adc.core.model.Audit;
import com.ey.digital.adc.core.model.IModel;

import lombok.Data;


@Entity
@Table(name = "risk")
@Data
public class Risk  implements IModel{

	@Id
	@Column(name = "riskid")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer riskId;
	
	@Column(name = "riskname")
	private String riskName;
	
	@Column(name = "riskdescription")
	private String riskDescription;
	

	public Integer getRiskId() {
		return riskId;
	}

	public void setRiskId(Integer riskId) {
		this.riskId = riskId;
	}

	public String getRiskName() {
		return riskName;
	}

	public void setRiskName(String riskName) {
		this.riskName = riskName;
	}

	public String getRiskDescription() {
		return riskDescription;
	}

	public void setRiskDescription(String riskDescription) {
		this.riskDescription = riskDescription;
	}

	
	
}


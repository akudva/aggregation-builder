package com.ey.digital.adc.aml.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ey.digital.adc.aml.model.Condition;
import com.ey.digital.adc.aml.model.Filter;
import com.ey.digital.adc.aml.model.Rule;

@Service
public class FilterService {

	private final Logger _logger = LoggerFactory.getLogger(this.getClass());

	private final TableJoinService tableJoinService;

	@Autowired
	public FilterService(TableJoinService tableJoinService) {

		this.tableJoinService = tableJoinService;
	}

	List<String> stringComparisons = new ArrayList<>(Arrays.asList("LIKE", "rLIKE"));

	public StringBuffer createFilterSQL(Filter filter, Rule rule, Condition condition, ArrayList<String> tableArr,
			ArrayList<Character> alias, StringBuffer joinCondition, StringBuffer fromClause,
			ArrayList<String> factTableArr, String hiveSchema) {

		StringBuffer filterSQL = new StringBuffer();
		String resultColumn;
		String resultTable = "";

		filter.setRuleId(rule.getRuleId());
		filter.setConditionId(condition.getConditionId());

		String focalEntity = rule.getFocalEntity();
		String focal_entity_column = "cust_id";
		if (focalEntity.equalsIgnoreCase("account"))
			focal_entity_column = "acct_id";

		String operator; // Always equal to 'operator'?
		String type = filter.getType();
		if (type != null)
			switch (type) {
			case "attribute":
				resultColumn = filter.getValue().getAttribute();
				resultTable = filter.getValue().getTable();

				filter.setResultColumn(resultColumn);
				filter.setResultTable(resultTable);

				if (tableArr.contains(resultTable)) {
					int index = 0;
					for (String t : tableArr) {
						if (t.equalsIgnoreCase(resultTable)) {
							break;
						} else {
							index++;
						}
					}
					filterSQL.append(" " + alias.get(index) + "." + resultColumn + " ");

				} else {
					tableArr.add(resultTable);

					alias.add(new Character((char) (alias.get(alias.size() - 1).charValue() + 1)));

					// append FROM clause
					filterSQL.append(" " + alias.get(alias.size() - 1) + "." + resultColumn + " ");
					fromClause.append(" JOIN " + hiveSchema + "." + resultTable + " " + alias.get(tableArr.size() - 1));

					if (tableArr.contains(condition.getValue().getTable())) {
						int joinindex = 0;
						for (String t : tableArr) {
							if (t.equalsIgnoreCase(resultTable)) {
								break;
							} else {
								joinindex++;
							}
						}
						_logger.info("Filter debug " + tableArr.get(joinindex) + " :: " + resultTable);
						joinCondition
								.append(tableJoinService.getJoinConditionForDataModelTables(tableArr.get(joinindex),
										resultTable, alias.get(joinindex), alias.get(alias.size() - 1)));
					}
				}

				break;
			case "data":
				String data = filter.getValue().getData();

				try {
					Double.valueOf(data);
					filterSQL.append(" CAST('" + data + "' as DOUBLE) ");
				} catch (NumberFormatException e) {
					if (!data.contains(",")) {
						filterSQL.append(" '" + data + "' ");
					} else if (data.contains(",")) {
						Set<String> dataArray = new HashSet<String>(
								Arrays.asList(filter.getValue().getData().split(Pattern.quote(","))));
						filterSQL.append("(");
						int index = 0;
						for (String a : dataArray) {
							index++;
							filterSQL.append("'" + a + "'");
							if (index != dataArray.size()) // TODO: test object equality. Need to validate
								filterSQL.append(",");
						}
						filterSQL.append(")");

						_logger.info("Inclusion Filter - Data SQL=" + filterSQL);
					}
				}

				break;
			case "thresholdLabel":
				String label = filter.getValue().getThreshold_label();

				// 1. join with threshold matrix and append to joinclause
				if (tableArr.contains("threshold_seg_risk_matrix")) {
					int index = 0;
					for (String t : tableArr) {
						if (tableArr.get(index).equalsIgnoreCase("threshold_seg_risk_matrix")) {
							break;
						} else {
							index++;
						}
					}
					filterSQL.append(" CAST('" + alias.get(index) + "." + label + "' as DOUBLE) ");

				} else {
					tableArr.add("threshold_seg_risk_matrix");

					alias.add(new Character((char) (alias.get(alias.size() - 1).charValue() + 1)));

					// append from clause

					filterSQL.append(" CAST('" + alias.get(alias.size() - 1) + "." + label + "' as DOUBLE)  ");
					fromClause.append(" JOIN " + "threshold_seg_risk_matrix " + alias.get(tableArr.size()));
					int joinIndex = 0;
					for (int x = 0; x < tableArr.size() - 1; x++) {
						if (factTableArr.contains(tableArr.get(joinIndex))) {
							break;
						} else {
							joinIndex++;
						}
					}
					joinCondition.append(" " + alias.get(joinIndex) + "." + focal_entity_column.substring(0, 5)
							+ "segment = " + alias.get(tableArr.size() - 1) + "." + focal_entity_column.substring(0, 5)
							+ "segment and " + alias.get(joinIndex) + "." + focal_entity_column.substring(0, 5)
							+ "risk = " + alias.get(tableArr.size() - 1) + "." + focal_entity_column.substring(0, 5)
							+ "risk,");
				}

				// 2. add from clause

				// 3. use the label as column in filterSQL.append

				break;
			case "operator":
				operator = filter.getValue().getOperator().toUpperCase();

				if (stringComparisons.contains(operator)) {
					filterSQL.append(" UPPER("
							+ createFilterSQL(filter.getChildren().get(0), rule, condition, tableArr, alias,
									joinCondition, fromClause, factTableArr, hiveSchema)
							+ ") " + operator + " " + " UPPER(" + createFilterSQL(filter.getChildren().get(1), rule,
									condition, tableArr, alias, joinCondition, fromClause, factTableArr, hiveSchema)
							+ ") ");
				} else {
					filterSQL.append(createFilterSQL(filter.getChildren().get(0), rule, condition, tableArr, alias,
							joinCondition, fromClause, factTableArr, hiveSchema) + " " + operator + " "
							+ createFilterSQL(filter.getChildren().get(1), rule, condition, tableArr, alias,
									joinCondition, fromClause, factTableArr, hiveSchema));
				}

				break;

			}

		_logger.info("Created Filter" + filterSQL);
		return filterSQL;

	}
}

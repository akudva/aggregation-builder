package com.ey.digital.adc.aml.sql.server.repo;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import com.ey.digital.adc.aml.model.Case;
import com.ey.digital.adc.aml.model.Page;

@Repository
public class CaseRepository {

	public Page<Case> getCasePageFromSQL(JdbcTemplate jdbcTemplate, Integer clientId, Integer pageNo, Integer pageSize) {

		String hiveSchema = getClientHiveSchema(jdbcTemplate,clientId);

		PaginationRepository<Case> ph = new PaginationRepository<Case>();

		String sqlCountRows = "SELECT count(*) FROM case_master_" + hiveSchema;
		String sqlFetchRows = "SELECT case_id, cust_id, month_dt, case_dt, alert_ct from case_master_" + hiveSchema;

		return ph.fetchPage(jdbcTemplate, sqlCountRows, sqlFetchRows, pageNo, pageSize, new RowMapper<Case>() {
			public Case mapRow(ResultSet rs, int i) throws SQLException {
				return new Case(rs.getString("case_id"), rs.getString("cust_id"), rs.getString("month_dt"),
						rs.getString("case_dt"), rs.getInt("alert_ct"));
			}
		});

	}

	private String getClientHiveSchema(JdbcTemplate jdbcTemplate,Integer clientId) {

		String getHiveSchemaSQL = "Select HiveSchema from client where clientId=?";

		return (String) jdbcTemplate.queryForObject(getHiveSchemaSQL, new Object[] { clientId }, String.class);

	}
}

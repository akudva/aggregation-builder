package com.ey.digital.adc.aml.ui.request;

import com.ey.digital.adc.aml.model.Rule;
import com.ey.digital.adc.core.ui.request.AbstractUIRequest;


public class RuleUIRequest extends AbstractUIRequest<Rule> {
	private static final long serialVersionUID = 1L;
	
	
	@Override
	public String toString() {
		return "RuleUIRequest [message=" + getMessage() + ", toString()=" + super.toString() + "]";
	}

}

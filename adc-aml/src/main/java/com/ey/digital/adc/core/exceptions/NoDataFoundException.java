/**
 * 
 */
package com.ey.digital.adc.core.exceptions;

/**
 * @author mulaysh
 * Date Created: 2017-01-21
 */
public class NoDataFoundException extends Exception {
	private static final long serialVersionUID = 1L;

	/**
	 * @param message
	 */
	public NoDataFoundException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}
}

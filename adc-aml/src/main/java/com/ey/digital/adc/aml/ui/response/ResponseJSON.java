package com.ey.digital.adc.aml.ui.response;

import java.util.HashMap;
import java.util.Map;

public class ResponseJSON {
	
	private Map<String, Object> responseMap = new HashMap<String, Object>();

	public Map<String, Object> getResponseMap() {
		return responseMap;
	}

	public void setResponseMap(Map<String, Object> responseMap) {
		this.responseMap = responseMap;
	}

}

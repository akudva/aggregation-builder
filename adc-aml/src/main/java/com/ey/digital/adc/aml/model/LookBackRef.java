package com.ey.digital.adc.aml.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ey.digital.adc.core.model.Audit;
import com.ey.digital.adc.core.model.IModel;

import lombok.Data;

@Data
@Entity
@Table(name = "lookbackref")
public class LookBackRef  implements IModel {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "lookbackrefid")
	private Integer lookbackrefid;
	
	
	@Column(name = "tablename",columnDefinition="uniqueidentifier")
	private String tablename;
	
	@Column(name = "lookbackunit")
	private LookBackUnit lookbackunit;
	
	public enum LookBackUnit {
		Daily, Monthly, Yearly
	}
	
	@Column(name = "datecolumn")
	private String datecolumn;
	
}
 
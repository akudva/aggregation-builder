package com.ey.digital.adc.aml.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.ey.digital.adc.core.model.Audit;
import com.ey.digital.adc.core.model.IModel;

import lombok.Data;

@Entity
@Table(name = "threshold_matrix_segment_risk")
@Data
public class ThresholdMatrixSegmentRisk  extends Audit implements IModel{

	@Id
	@Column(name = "threshold_segment_risk_sequence")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer threshold_segment_risk_sequence;
	
	@Column(name = "version")
	private String versionId;
	
	@Column(name = "ruleid")
	private Integer ruleId;
	
	@Column(name = "segment")
	private String segment;
	
	@Column(name = "isselected")
	private String isSelected;
	
	@Column(name = "isrisktuned")
	private String isRiskTuned;
	
	@Column(name = "risk")
	private String riskLevel;
	
	@Column(name = "riskvalue")
	private String riskValue;
	
	@Column(name = "population")
	private Integer population;
		
	  @Column(name = "threshold1name")
		private String threshold1Name;

		@Column(name = "threshold1type")
		private String threshold1Type;
		
		@Column(name = "threshold1value")
		private String threshold1Value;

		@Column(name = "threshold1function")
		private String threshold1Function;

	   @Column(name = "threshold2name")
		private String threshold2Name;

		@Column(name = "threshold2type")
		private String threshold2Type;
		
		@Column(name = "threshold2value")
		private String threshold2Value;

		@Column(name = "threshold2function")
		private String threshold2Function;

	  @Column(name = "threshold3name")
	  private String threshold3Name;

	  @Column(name = "threshold3type")
	  private String threshold3Type;

	  @Column(name = "threshold3value")
	  private String threshold3Value;

	  @Column(name = "threshold3function")
	  private String threshold3Function;

	  @Column(name = "threshold4name")
	  private String threshold4Name;

	  @Column(name = "threshold4type")
	  private String threshold4Type;

	  @Column(name = "threshold4value")
	  private String threshold4Value;

	  @Column(name = "threshold4function")
	  private String threshold4Function;

	  @Column(name = "threshold5name")
	  private String threshold5Name;

	  @Column(name = "threshold5type")
	  private String threshold5Type;

	  @Column(name = "threshold5value")
	  private String threshold5Value;

	  @Column(name = "threshold5function")
	  private String threshold5Function;

	  @Column(name = "threshold6name")
	  private String threshold6Name;

	  @Column(name = "threshold6type")
	  private String threshold6Type;

	  @Column(name = "threshold6value")
	  private String threshold6Value;

	  @Column(name = "threshold6function")
	  private String threshold6Function;

	  @Column(name = "threshold7name")
	  private String threshold7Name;

	  @Column(name = "threshold7type")
	  private String threshold7Type;

	  @Column(name = "threshold7value")
	  private String threshold7Value;

	  @Column(name = "threshold7function")
	  private String threshold7Function;

	  @Column(name = "threshold8name")
	  private String threshold8Name;

	  @Column(name = "threshold8type")
	  private String threshold8Type;

	  @Column(name = "threshold8value")
	  private String threshold8Value;

	  @Column(name = "threshold8function")
	  private String threshold8Function;

	  @Column(name = "threshold9name")
	  private String threshold9Name;

	  @Column(name = "threshold9type")
	  private String threshold9Type;

	  @Column(name = "threshold9value")
	  private String threshold9Value;

	  @Column(name = "threshold9function")
	  private String threshold9Function;

	  @Column(name = "threshold10name")
	  private String threshold10Name;

	  @Column(name = "threshold10type")
	  private String threshold10Type;

	  @Column(name = "threshold10value")
	  private String threshold10Value;

	  @Column(name = "threshold10function")
	  private String threshold10Function;

	  @Column(name = "threshold11name")
	  private String threshold11Name;

	  @Column(name = "threshold11type")
	  private String threshold11Type;

	  @Column(name = "threshold11value")
	  private String threshold11Value;

	  @Column(name = "threshold11function")
	  private String threshold11Function;

	  @Column(name = "threshold12name")
	  private String threshold12Name;

	  @Column(name = "threshold12type")
	  private String threshold12Type;

	  @Column(name = "threshold12value")
	  private String threshold12Value;

	  @Column(name = "threshold12function")
	  private String threshold12Function;

	  @Column(name = "threshold13name")
	  private String threshold13Name;

	  @Column(name = "threshold13type")
	  private String threshold13Type;

	  @Column(name = "threshold13value")
	  private String threshold13Value;

	  @Column(name = "threshold13function")
	  private String threshold13Function;

	  @Column(name = "threshold14name")
	  private String threshold14Name;

	  @Column(name = "threshold14type")
	  private String threshold14Type;

	  @Column(name = "threshold14value")
	  private String threshold14Value;

	  @Column(name = "threshold14function")
	  private String threshold14Function;

	  @Column(name = "threshold15name")
	  private String threshold15Name;

	  @Column(name = "threshold15type")
	  private String threshold15Type;

	  @Column(name = "threshold15value")
	  private String threshold15Value;

	  @Column(name = "threshold15function")
	  private String threshold15Function;

	  @Column(name = "threshold16name")
	  private String threshold16Name;

	  @Column(name = "threshold16type")
	  private String threshold16Type;

	  @Column(name = "threshold16value")
	  private String threshold16Value;

	  @Column(name = "threshold16function")
	  private String threshold16Function;

	  @Column(name = "threshold17name")
	  private String threshold17Name;

	  @Column(name = "threshold17type")
	  private String threshold17Type;

	  @Column(name = "threshold17value")
	  private String threshold17Value;

	  @Column(name = "threshold17function")
	  private String threshold17Function;

	  @Column(name = "threshold18name")
	  private String threshold18Name;

	  @Column(name = "threshold18type")
	  private String threshold18Type;

	  @Column(name = "threshold18value")
	  private String threshold18Value;

	  @Column(name = "threshold18function")
	  private String threshold18Function;

	  @Column(name = "threshold19name")
	  private String threshold19Name;

	  @Column(name = "threshold19type")
	  private String threshold19Type;

	  @Column(name = "threshold19value")
	  private String threshold19Value;

	  @Column(name = "threshold19function")
	  private String threshold19Function;

	  @Column(name = "threshold20name")
	  private String threshold20Name;

	  @Column(name = "threshold20type")
	  private String threshold20Type;

	  @Column(name = "threshold20value")
	  private String threshold20Value;

	  @Column(name = "threshold20function")
	  private String threshold20Function;
		
	@Transient
	private List<Segment> segmentIds;
	
	@Transient
	private List<RiskLevel> riskLevels;
	
//	@Transient
//	private String isRiskTune;
//	
}

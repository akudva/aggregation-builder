package com.ey.digital.adc.aml;

import com.ey.digital.adc.core.exceptions.InvalidTokenException;
import com.ey.digital.adc.core.sql.server.utils.SqlServerUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class AuthInterceptor implements HandlerInterceptor {
    @Autowired
    SqlServerUtils sqlServerUtils;

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {
        String auth = httpServletRequest.getHeader("X-Authorization");
        System.out.println("auth = " + auth);
//        validateToken(auth);
        return true;
    }

    protected int validateToken(String token) throws InvalidTokenException {
        if (token == null || "".equals(token.trim())) {
            throw new InvalidTokenException();
        }
        final int userId = sqlServerUtils.validateToken(token);
        if (userId <= 0) {
            throw new InvalidTokenException();
        }
        return userId;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}

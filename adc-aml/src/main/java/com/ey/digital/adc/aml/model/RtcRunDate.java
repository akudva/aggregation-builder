package com.ey.digital.adc.aml.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.ey.digital.adc.core.model.Audit;
import com.ey.digital.adc.core.model.IModel;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper=false)
@Data
@Entity
@Table(name = "rtcrundate")
public class RtcRunDate extends Audit implements IModel {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "rtcrundateid")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer rtcRunDateId;

	
	@Column(name = "rundate")
	private String runDate;

////	@Transient
//	@Column(name = "rtcid")
//	private Integer rtcId;
	
	//@ManyToOne
	//@JsonIgnore
	//@JoinColumn(name="configid", nullable=false,insertable =  false, updatable = false)
	//private RuleThresholdConfig ruleThresholdConfig;
	
	@Override
	public String toString() {
		return "RtcRunDate [rtcId=" + "" +  ", rtcId=" + "" + "]";
	}
	
	

	
	
}

/**
 * 
 */
package com.ey.digital.adc.core.util;

import com.ey.digital.adc.core.exceptions.InvalidRequiredFieldException;

/**
 * @author mulaysh
 * Date Created: 2017-01-23
 */
public final class FieldValidationUtil {

	private final static String REQD_FIELD_ERROR = "Required field [%s] can't be null/empty";
	public static boolean validateRequiredField(String fieldName, String fieldValue) 
			throws InvalidRequiredFieldException{
		if (fieldValue == null || "".equals(fieldValue.trim())){
			throw new InvalidRequiredFieldException(String.format(REQD_FIELD_ERROR
					, fieldName));
		}
		return true;
	}
	
	public static <V> boolean validateRequiredField(String fieldName, V value) 
			throws InvalidRequiredFieldException {
		if (value == null){
			throw new InvalidRequiredFieldException(String.format(REQD_FIELD_ERROR
					, fieldName));
		}
		if (value instanceof Integer){
			Integer intValue = (Integer)value;
			if (intValue <= 0){
				throw new InvalidRequiredFieldException(String.format(REQD_FIELD_ERROR
						, fieldName));
			}
		}
		return true;
	}
}

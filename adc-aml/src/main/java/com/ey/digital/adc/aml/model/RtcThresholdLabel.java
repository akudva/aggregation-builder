package com.ey.digital.adc.aml.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.ey.digital.adc.core.model.Audit;
import com.ey.digital.adc.core.model.IModel;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper=false ,exclude={"rulethresholdconfig"})
@Data
@Entity
@Table(name = "rtcthresholdlabel")
public class RtcThresholdLabel extends Audit implements IModel {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "rtcthresholdlabelid")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer rtcThresholdLabelId;

	
	@Column(name = "thresholdlabelname")
	private String thresholdLabelName;
	
	//@Column(name = "rtcid")
	//private Integer rtcId;
		
	//@ManyToOne
	//@JsonIgnore
	//@JoinColumn(name="configid", nullable=false,insertable =  false, updatable = false)
	//private RuleThresholdConfig ruleThresholdConfig;
	
	@Override
	public String toString() {
		return "RtcThresholdLabel [thresholdLabelName=" + thresholdLabelName + ", rtcId=" +  "]";
	}
	
	

	
	
}

package com.ey.digital.adc.aml.model;

import java.io.Serializable;

import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.IdentifierGeneratorHelper.BigDecimalHolder;
import org.hibernate.id.IntegralDataTypeHolder;
import org.hibernate.id.SequenceGenerator;

public class SequenceFetcher extends SequenceGenerator {
    @Override
    public Serializable generate(SessionImplementor session, Object obj) {
        return "TEXEC_1234" ;//+ super.generate( session, obj ).toString();
    }

    protected IntegralDataTypeHolder buildHolder() {
        return new BigDecimalHolder();
    }
}
package com.ey.digital.adc.aml.model;

public class RestResponse {
	
	@Override
	public String toString() {
		return "RestResponse [statusCd=" + statusCd + ", respBody=" + respBody + "]";
	}
	private String statusCd;
	private String respBody;
	public String getStatusCd() {
		return statusCd;
	}
	public void setStatusCd(String statusCd) {
		this.statusCd = statusCd;
	}
	public String getRespBody() {
		return respBody;
	}
	public void setRespBody(String respBody) {
		this.respBody = respBody;
	}

}
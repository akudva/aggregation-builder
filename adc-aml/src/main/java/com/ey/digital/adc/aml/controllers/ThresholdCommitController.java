package com.ey.digital.adc.aml.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.validation.Valid;
import javax.ws.rs.core.MediaType;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ey.digital.adc.aml.model.ExecutionPayload;
import com.ey.digital.adc.aml.model.RtcCalculatedValue;
import com.ey.digital.adc.aml.model.RtcRiskLevel;
import com.ey.digital.adc.aml.model.RtcThresholdSegmentRisk;
import com.ey.digital.adc.aml.model.RuleThresholdConfig;
import com.ey.digital.adc.aml.sql.server.repo.RtcCalculatedValueRepository;
import com.ey.digital.adc.aml.sql.server.repo.RtcThresholdSegmentRiskRepo;
import com.ey.digital.adc.aml.sql.server.repo.RuleThresholdConfigRepository;
import com.ey.digital.adc.aml.ui.request.ExecutionPayloadUIRequest;
import com.ey.digital.adc.core.exceptions.InvalidMessageException;
import com.ey.digital.adc.core.model.ApiResponse;
import com.ey.digital.adc.core.ui.resp.utils.HttpResponseUtils;

@RestController
@RequestMapping("/threshold-commit")
public class ThresholdCommitController {

	private final static Logger LOGGER = Logger.getLogger(ThresholdCommitController.class.getName());

	@Autowired
	private RtcCalculatedValueRepository calculatedvalueRepository;

	@Autowired
	private RtcThresholdSegmentRiskRepo rtcThresholdSegRiskRepo;

	@Autowired
	private RuleThresholdConfigRepository ruleThresholdConfigRepository;

	@GetMapping(produces = MediaType.APPLICATION_JSON, params = { "rtcid" })
	public List<ExecutionPayload> getPayLoad(
			@Valid @NotEmpty(message = "rtcid may not be Empty/Null") @RequestParam(value = "rtcid") Integer rtcid)
			throws Exception {
		List<ExecutionPayload> payload = new ArrayList<>();
		List<RtcCalculatedValue> calculatedValues = calculatedvalueRepository.findEmptyCalculatedValuesByRtcid(rtcid);

		calculatedValues.forEach(cv -> {
			ExecutionPayload pl = new ExecutionPayload();
			List<String> sqls = new ArrayList<>();
			sqls.add(cv.getCommitsql());
			pl.setGetResult(true);
			pl.setId(cv.getRtcCalculatedValueId());
			pl.setSqlQueries(sqls);
			payload.add(pl);
		});
		return payload;
	}

	@PostMapping(consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<ApiResponse> getResults(@Valid @RequestBody List<ExecutionPayload> messages)
			throws InvalidMessageException {
		// List<ExecutionPayload> messages = request.getMessages();
		if (messages == null)
			throw new InvalidMessageException("Null/empty message received for approve/reject rules");
		ApiResponse apiResponse = null;
		// int userId = 0;
		// LOGGER.info("processing approve/reject rules for token [{}] message [{}]" +
		// request.getToken()
		// + request.getMessage());
		RuleThresholdConfig rtc = null;
		try {
			for (ExecutionPayload payload : messages) {
				RtcCalculatedValue cv = calculatedvalueRepository.findOne(payload.getId());
				Map<String, List<String>> results = payload.getResultData();
				String result = results.get(cv.getRtcCalculatedValueId().toString()).get(0);
				if (result != null && !result.trim().equalsIgnoreCase(""))
					cv.setCalculatedValue(Float.valueOf(result)); // TODO
				cv = calculatedvalueRepository.save(cv);

				if (rtc == null)
					rtc = ruleThresholdConfigRepository.findOne(cv.getRtcId());

				for (RtcRiskLevel rl : rtc.getRtcRiskLevels()) {
					RtcThresholdSegmentRisk _rtc = rtcThresholdSegRiskRepo.findByRtcCalculatedValueIdAndScalar(
							cv.getRtcCalculatedValueId(), Double.valueOf(rl.getRiskScalarValue()));

					if (_rtc == null)
						_rtc = new RtcThresholdSegmentRisk();

					_rtc.setCalculatedFuncName(cv.getCalculatedFuncName());
					_rtc.setCalculatedType(cv.getCalculatedType());
					_rtc.setCalculatedValue(cv.getCalculatedValue());
					_rtc.setRtcCalculatedValueId(cv.getRtcCalculatedValueId());
					_rtc.setSegmentName(cv.getSegmentName());
					_rtc.setThresholdLabel(cv.getThresholdLabel());
					_rtc.setRisk(rl.getRiskLevelName());
					_rtc.setRtcid(rtc.getRtcId());

					Double scalar = null;
					if (rl.getRiskScalarValue() != null) {
						scalar = Double.valueOf(rl.getRiskScalarValue());
						_rtc.setScalar(scalar);
					}

					Double val = null;
					if (_rtc.getCalculatedValue() != null) {
						val = Double.valueOf(_rtc.getCalculatedValue());
						_rtc.setFinalvalue(scalar * val);
					}
					rtcThresholdSegRiskRepo.save(_rtc);
				}
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, e.getMessage());
			return HttpResponseUtils.getErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage());
		}
		return HttpResponseUtils.getResponse(HttpStatus.CREATED, "Success", apiResponse);
	}
}

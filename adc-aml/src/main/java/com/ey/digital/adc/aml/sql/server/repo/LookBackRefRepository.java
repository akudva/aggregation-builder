package com.ey.digital.adc.aml.sql.server.repo;

import com.ey.digital.adc.aml.model.LookBackRef;
import com.ey.digital.adc.core.sql.server.repo.ISQLRepository;

public interface LookBackRefRepository extends ISQLRepository<LookBackRef, Integer>{
	LookBackRef findBytablename(String tablename);
}

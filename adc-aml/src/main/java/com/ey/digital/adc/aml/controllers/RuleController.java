package com.ey.digital.adc.aml.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.hibernate.validator.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ey.digital.adc.aml.model.Risk;
import com.ey.digital.adc.aml.model.Rule;
import com.ey.digital.adc.aml.model.RuleThresholdConfig;
import com.ey.digital.adc.aml.model.Segment;
import com.ey.digital.adc.aml.model.Condition;
import com.ey.digital.adc.aml.model.ExclusionFilter;
import com.ey.digital.adc.aml.model.InclusionFilter;
import com.ey.digital.adc.aml.service.ConditionService;
import com.ey.digital.adc.aml.sql.server.repo.ConditionRepository;
import com.ey.digital.adc.aml.sql.server.repo.RiskLevelsRepository;
import com.ey.digital.adc.aml.sql.server.repo.RuleRepository;
import com.ey.digital.adc.aml.sql.server.repo.RuleThresholdConfigRepository;
import com.ey.digital.adc.aml.sql.server.repo.SegmentRepository;
import com.ey.digital.adc.aml.ui.request.RuleUIRequest;
import com.ey.digital.adc.core.controllers.AbstractController;
import com.ey.digital.adc.core.exceptions.InvalidCreateIdException;
import com.ey.digital.adc.core.exceptions.InvalidMessageException;
import com.ey.digital.adc.core.util.DateTimeUtils;
import com.ey.digital.adc.core.exceptions.InvalidTokenException;
import com.ey.digital.adc.core.model.ApiResponse;
import com.ey.digital.adc.core.ui.resp.utils.HttpResponseUtils;

import javax.validation.Valid;
import javax.ws.rs.core.MediaType;

@RestController
@RequestMapping("/rule")
public final class RuleController extends AbstractController<RuleUIRequest, Rule> {

	private final Logger _logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ConditionService conditionService;

	@Autowired
	private RuleRepository ruleRepository;

	@Autowired
	private SegmentRepository segmentRepo;

	@Autowired
	RuleThresholdConfigRepository ruleThresholdConfigRepository;

	@Autowired
	private RiskLevelsRepository riskRepo;

	@Autowired
	private ConditionRepository conditionRepo;

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	RuleThresholdConfigRepository rtcRepo;

	private Rule savedRule;

	List<InclusionFilter> inclusionFilters = new ArrayList<>();
	List<ExclusionFilter> exclusionFilters = new ArrayList<>();
	private final String controllerName = this.getClass().getSimpleName().replaceAll("Controller", "");

	public RuleController(RuleRepository ruleRepository) {
		super(ruleRepository);
		this.ruleRepository = ruleRepository;
	}

	@Override
	@PostMapping(value = "/save", consumes = MediaType.APPLICATION_JSON)
	public ResponseEntity<Rule> create(@Valid @RequestBody RuleUIRequest request) {
		_logger.info("processing create request for: [{}]", request.toString());
		final String token = request.getToken();
		int userId = 0;
		try {
			userId = validateToken(token);
		} catch (InvalidTokenException e) {
			_logger.info("Returning error response [{}]", e.getMessage());
			return HttpResponseUtils.getInvalidTokenErrorResponse();
		}

		Rule inserted = null;
		try {
			inserted = createInternal(userId, request);
		} catch (Exception e) {
			_logger.error("Exception ", e);
			return HttpResponseUtils.getErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage());
		}

		String message = String.format("%s created successfully", controllerName);
		return HttpResponseUtils.getResponse(HttpStatus.CREATED, message, inserted);
	}

	public Rule createInternal(int userId, RuleUIRequest request) throws Exception {
		Rule rule = request.getMessage();
		if (rule == null)
			throw new InvalidMessageException("Null/empty message received for create");
		if (rule.getRuleId() != null) {
			throw new InvalidCreateIdException(getControllerName());
		}

		String currentDateTime = DateTimeUtils.getCurrentDateTime();
		String ruleUUID = UUID.randomUUID().toString();

		rule.setCreatedBy(userId);
		rule.setLastUpdatedBy(userId);

		rule.setCreatedDateTime(currentDateTime);
		rule.setLastUpdatedDateTime(currentDateTime);
		rule.setAlertDetailCreateQuery("");
		rule.setAlertDetailInsertQuery("");
		rule.setAlertTransactionQuery("");
		rule.setStatus("Draft");

		String hiveSchema = jdbcTemplate.queryForObject(
				"select hiveschema from client where clientId='" + rule.getClientId() + "'", String.class);
		rule.setHiveSchema(hiveSchema);

		// Get parentCondition from rule
		Condition parentCondition = rule.getCondition();

		conditionService.createCondition(parentCondition, rule, ruleUUID);

		// Save rule before creating conditions to generate ruleId
		savedRule = ruleRepository.save(rule);
		conditionService.setConditionRuleIDs(savedRule, ruleUUID);
		_logger.info(savedRule.toString());

		return savedRule;
	}

	@Override
	public Rule updateInternal(int userId, RuleUIRequest request) throws Exception {

		Rule rule = request.getMessage();
		if (rule == null)
			throw new InvalidMessageException("Null/empty message received for edit");
		if (rule.getRuleId() == null) {
			throw new InvalidMessageException("Null rule id received for edit");
		} else {
			if (rule.getRuleId().toString().isEmpty()) {
				throw new InvalidMessageException("Empty rule id received for edit");
			}
		}

		String currentDateTime = DateTimeUtils.getCurrentDateTime();
		String ruleId = rule.getRuleId().toString();
		rule.setLastUpdatedBy(userId);
		rule.setLastUpdatedDateTime(currentDateTime);
		rule.setAlertDetailCreateQuery("");
		rule.setAlertDetailInsertQuery("");
		rule.setAlertTransactionQuery("");
		rule.setStatus("Draft");

		String hiveSchema = jdbcTemplate.queryForObject(
				"select hiveschema from client where clientId='" + rule.getClientId() + "'", String.class);
		rule.setHiveSchema(hiveSchema);

		// Delete the existing conditions for that rule
		Boolean delStatus = conditionService.deleteCondition(ruleId);
		// conditionService.deleteCondition(ruleId);
		if (delStatus) {
			// Get parentCondition from rule
			Condition parentCondition = rule.getCondition();

			// Add audit data to each condition
			conditionService.createCondition(parentCondition, rule, ruleId);
			savedRule = ruleRepository.save(rule);
			conditionService.setConditionRuleIDs(savedRule, ruleId);
		}
		_logger.info(savedRule.toString());

		return savedRule;
	}

	@Override
	public List<Rule> getListInternal(int listForId) {
		throw new UnsupportedOperationException("This operation is not supported");

	}

	/*
	 * (non-Javadoc)
	 *
	 * get all approved rules
	 */
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON, params = { "token", "status" })
	public ResponseEntity<List<Rule>> getRules(
			@Valid @NotEmpty(message = "token may not be Empty/Null") @RequestParam(value = "token") String token,
			@Valid @NotEmpty(message = "status may not be Empty/Null") @RequestParam(value = "status") String status)
			throws Exception {

		int userId = 0;
		List<Rule> rules = new ArrayList<Rule>();
		_logger.info(token + " " + status);

		try {
			userId = validateToken(token);
		} catch (InvalidTokenException e) {
			_logger.warn(e.getMessage());
			return HttpResponseUtils.getInvalidTokenErrorResponse();
		}
		try {
			List<Rule> rulesList = new ArrayList<Rule>();
			rulesList = ruleRepository.findByRuleStatus(status);
			List<Segment> ruleSegmentsList = null;

			if (!rulesList.isEmpty()) {
				for (Rule rule : rulesList) {
					ruleSegmentsList = segmentRepo.findByFocalEntity(rule.getFocalEntity());
					rule.setRuleSegments(ruleSegmentsList);
					List<Risk> risks = riskRepo.findAll();
					rule.setRuleRisks(risks);
					List<String> thresholdLabels = conditionRepo.findThersholds(rule.getRuleId());
					rule.setRuleThresholdLabels(thresholdLabels);
				}
				return HttpResponseUtils.getResponse(rulesList);
			} else {
				return HttpResponseUtils.getResponse(HttpStatus.OK, "No " + status + " rules", rulesList);
			}

		} catch (Exception e) {

			_logger.warn(e.getMessage());
			return HttpResponseUtils.getErrorResponse(HttpStatus.NOT_FOUND, e.getMessage());

		}

	}

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON, params = { "token",
			"thresholdconfigstatus" })
	public ResponseEntity<List<Rule>> getRulesByRtcConfig(
			@Valid @NotEmpty(message = "token may not be Empty/Null") @RequestParam(value = "token") String token,
			@Valid @NotEmpty(message = "status may not be Empty/Null") @RequestParam(value = "thresholdconfigstatus") String status)
			throws Exception {

		int userId = 0;
		List<Rule> rules = new ArrayList<Rule>();
		_logger.info(token + " " + status);

		try {
			userId = validateToken(token);
		} catch (InvalidTokenException e) {
			_logger.warn(e.getMessage());
			return HttpResponseUtils.getInvalidTokenErrorResponse();
		}
		try {
			List<Rule> rulesList = ruleRepository.findDistinctByRuleThresholdConfigStatus(status);
			List<Segment> ruleSegmentsList = null;
			for (int i = 0; i < rulesList.size(); i++) {
				Rule rule = rulesList.get(i);
				ruleSegmentsList = segmentRepo.findByFocalEntity(rule.getFocalEntity());
				rule.setRuleSegments(ruleSegmentsList);
				List<Risk> risks = riskRepo.findAll();
				rule.setRuleRisks(risks);
				List<String> thresholdLabels = conditionRepo.findThersholds(rule.getRuleId());
				rule.setRuleThresholdLabels(thresholdLabels);
			}

			return HttpResponseUtils.getResponse(rulesList);

		} catch (Exception e) {

			_logger.warn(e.getMessage());
			return HttpResponseUtils.getErrorResponse(HttpStatus.NOT_FOUND, e.getMessage());

		}
	}

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON, params = { "token",
			"thresholdconfigstatus", "status" })
	public ResponseEntity<List<Rule>> getRulesByRtcConfigAndRuleConfig(
			@Valid @NotEmpty(message = "token may not be Empty/Null") @RequestParam(value = "token") String token,
			@Valid @NotEmpty(message = "status may not be Empty/Null") @RequestParam(value = "thresholdconfigstatus") String rtcStatus,
			@Valid @NotEmpty(message = "status may not be Empty/Null") @RequestParam(value = "status") String status)
			throws Exception {

		int userId = 0;
		List<Rule> rules = new ArrayList<Rule>();
		_logger.info(token + " " + status);

		try {
			userId = validateToken(token);
		} catch (InvalidTokenException e) {
			_logger.warn(e.getMessage());
			return HttpResponseUtils.getInvalidTokenErrorResponse();
		}
		try {
			List<Rule> rulesList = ruleRepository.findDistinctByRuleThresholdConfigStatusAndStatus(rtcStatus, status);
			List<Segment> ruleSegmentsList = null;
			for (int i = 0; i < rulesList.size(); i++) {
				Rule rule = rulesList.get(i);
				ruleSegmentsList = segmentRepo.findByFocalEntity(rule.getFocalEntity());
				rule.setRuleSegments(ruleSegmentsList);
				List<Risk> risks = riskRepo.findAll();
				rule.setRuleRisks(risks);
				List<String> thresholdLabels = conditionRepo.findThersholds(rule.getRuleId());
				rule.setRuleThresholdLabels(thresholdLabels);
			}

			return HttpResponseUtils.getResponse(rulesList);

		} catch (Exception e) {

			_logger.warn(e.getMessage());
			return HttpResponseUtils.getErrorResponse(HttpStatus.NOT_FOUND, e.getMessage());

		}
	}

	/**
	 * Approves or Rejects rules based upon ruleIds
	 * 
	 * @param request
	 *            RuleUIRequest
	 * @return ResponseEntity for Rules
	 */
	@PostMapping(value = "/approve-reject-rules", consumes = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<ApiResponse> approveorRejectRules(@Valid @RequestBody RuleUIRequest request)
			throws Exception {
		Rule message = request.getMessage();
		if (message == null)
			throw new InvalidMessageException("Null/empty message received for approve/reject rules");

		if (message.getRuleIds() == null || message.getRuleIds().isEmpty())
			throw new InvalidMessageException("Null/Empty rule ids list received for approve/reject rules");
		List<String> errors = null;
		ApiResponse apiResponse = null;
		int userId = 0;
		_logger.info("processing approve/reject rules for token [{}] message [{}]" + request.getToken()
				+ request.getMessage());
		try {
			userId = validateToken(request.getToken());
		} catch (InvalidTokenException e) {
			_logger.warn("Returning error response [{}]" + e.getMessage());
			return HttpResponseUtils.getInvalidTokenErrorResponse();
		}
		try {
			for (Integer ruleId : message.getRuleIds()) {
				String currentDateTime = DateTimeUtils.getCurrentDateTime();
				Rule current = ruleRepository.findOne(ruleId);
				// if (!current.getStatus().equalsIgnoreCase("pending approval")) {
				// throw new InvalidMessageException(
				// "Rule Id" + current.getRuleId() + " is not in pending approval status");
				// }
				current.setLastUpdatedBy(userId);
				current.setLastUpdatedDateTime(currentDateTime);
				if (Boolean.parseBoolean(message.getIsApprove())) {
					current.setApprovedBy(userId);
					current.setApprovedDtm(currentDateTime);
					current.setApprovalComment(message.getApprovalComment());
					current.setStatus("Approved");

				} else {
					current.setRejectedBy(userId);
					current.setRejectedDtm(currentDateTime);
					current.setStatus("Rejected");
					current.setRejectionComment(message.getRejectionComment());
				}
				ruleRepository.save(current);
			}
		} catch (Exception e) {
			_logger.error(e.getMessage());
			return HttpResponseUtils.getErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage());

		}
		apiResponse = new ApiResponse(HttpStatus.CREATED, "Success", errors);
		return HttpResponseUtils.getResponse(HttpStatus.CREATED, "Success", apiResponse);
	}

	/**
	 * Delete rule based upon ruleId
	 * 
	 * @param request
	 *            RuleUIRequest
	 * @return ResponseEntity for Rules i.e. success or failure api response
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.DELETE)
	public ResponseEntity<ApiResponse> deleteRules(@Valid @RequestBody RuleUIRequest request) throws Exception {
		List<String> errors = null;
		ApiResponse apiResponse = null;
		Rule message = request.getMessage();
		if (message == null)
			throw new InvalidMessageException("Null/empty message received for deleting rule");

		if (message.getRuleIds() == null || message.getRuleIds().isEmpty())
			throw new InvalidMessageException("Null/Empty rule ids list received for deleting rule");
		try {
			for (Integer ruleId : message.getRuleIds()) {
				Rule current = ruleRepository.findOne(ruleId);
				// delete only if rule is not approved
				if (!current.getStatus().equalsIgnoreCase("approved")) {
					ruleRepository.delete(current);
				}
			}
		} catch (Exception e) {
			_logger.error(e.getMessage());
			return HttpResponseUtils.getErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage());
		}

		apiResponse = new ApiResponse(HttpStatus.OK, "Success", errors);
		return HttpResponseUtils.getResponse(HttpStatus.OK, "Success", apiResponse);
	}
	
	 /**
     * Deletes rule/s irrespective of their status
     *
     * @param request -> contains list of rules ids for which rules needs to be deleted
     * 
     * returns Success as response if rule/s are successfully deleted otherwise bad request
     */
	@RequestMapping(value = "/delete-hard", method = RequestMethod.DELETE)
	public ResponseEntity<ApiResponse> hardDeleteRules(@Valid @RequestBody RuleUIRequest request) throws Exception {
		List<String> errors = null;
		ApiResponse apiResponse = null;
		Rule message = request.getMessage();
		if (message == null)
			throw new InvalidMessageException("Null/empty message received for deleting rule");

		if (message.getRuleIds() == null || message.getRuleIds().isEmpty())
			throw new InvalidMessageException("Null/Empty rule ids list received for deleting rule");
		try {
			for (Integer ruleId : message.getRuleIds()) {
				Rule current = ruleRepository.findOne(ruleId);
				if (current != null) {
					List<RuleThresholdConfig> rtcs = rtcRepo.findAllByRuleId(current.getRuleId());
					rtcs.forEach(rtc -> {
						rtcRepo.delete(rtc.getRtcId());
					});
					List<Condition> conditions = conditionRepo.findByRuleId(ruleId);
					conditions.forEach(c -> {
						conditionRepo.delete(c.getPrimaryconditionid());
					});
					current.setRuleThresholdConfig(null);
					current.setCondition(null);
					ruleRepository.save(current);
					ruleRepository.delete(current);
				}
			}
		} catch (Exception e) {
			_logger.error(e.getMessage());
			return HttpResponseUtils.getErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage());
		}

		apiResponse = new ApiResponse(HttpStatus.OK, "Success", errors);
		return HttpResponseUtils.getResponse(HttpStatus.OK, "Success", apiResponse);
	}

	/**
	 * Submit rule in a draft mode
	 * 
	 * @param request
	 *            RuleUIRequest
	 * @return ResponseEntity for Rules i.e. success or Bad Request
	 */
	@PostMapping(value = "/submit", consumes = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<Rule> suhmitRules(@Valid @RequestBody RuleUIRequest request) throws Exception {
		Rule message = request.getMessage();
		if (message == null)
			throw new InvalidMessageException("Null/empty message received when submitting rule");
		int userId = 0;
		Rule savedRule = null;
		_logger.info("processing submit rule for token [{}] message [{}]" + request.getToken() + request.getMessage());
		try {
			userId = validateToken(request.getToken());
		} catch (InvalidTokenException e) {
			_logger.warn("Returning error response [{}]" + e.getMessage());
			return HttpResponseUtils.getInvalidTokenErrorResponse();
		}

		try {
			savedRule = message;
			if (savedRule.getRuleId() == null)
				savedRule = createInternal(userId, request);

			savedRule.setStatus("Pending Approval");
			ruleRepository.save(savedRule);
			conditionService.createQueries(message);
		} catch (Exception e) {
			_logger.error(e.getMessage());
			return HttpResponseUtils.getErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage());
		}
		return HttpResponseUtils.getResponse(HttpStatus.CREATED, "Success", savedRule);
	}

	/**
	 * Submit rule in a edit draft mode
	 * 
	 * @param request
	 *            RuleUIRequest
	 * @return ResponseEntity for Rules i.e. success or Bad Request
	 */
	@PostMapping(value = "/edit/submit", consumes = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<Rule> editSubmitRules(@Valid @RequestBody RuleUIRequest request) throws Exception {
		Rule message = request.getMessage();
		if (message == null)
			throw new InvalidMessageException("Null/empty message received when editting rule");
		int userId = 0;
		Rule savedRule = null;
		_logger.info("processing edit rule for token [{}] message [{}]" + request.getToken() + request.getMessage());
		try {
			userId = validateToken(request.getToken());
		} catch (InvalidTokenException e) {
			_logger.warn("Returning error response [{}]" + e.getMessage());
			return HttpResponseUtils.getInvalidTokenErrorResponse();
		}

		try {
			savedRule = message;
			if (savedRule.getRuleId() != null) {
				savedRule = updateInternal(userId, request);
				savedRule.setStatus("Pending Approval");
				ruleRepository.save(savedRule);
				conditionService.createQueries(message);
			} else {
				throw new InvalidMessageException("Null rule id received for edit");
			}
		} catch (Exception e) {
			_logger.error(e.getMessage());
			return HttpResponseUtils.getErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage());
		}
		return HttpResponseUtils.getResponse(HttpStatus.CREATED, "Success", savedRule);
	}

	/**
	 * Submit rule in a edit draft mode
	 * 
	 * @param request
	 *            RuleUIRequest
	 * @return ResponseEntity for Rules i.e. success or Bad Request
	 */
	@PostMapping(value = "/edit/save", consumes = MediaType.APPLICATION_JSON)
	@ResponseBody
	public ResponseEntity<Rule> editSaveRules(@Valid @RequestBody RuleUIRequest request) throws Exception {
		Rule message = request.getMessage();
		if (message == null)
			throw new InvalidMessageException("Null/empty message received when editting rule");
		int userId = 0;
		Rule savedRule = null;
		_logger.info("processing edit rule for token [{}] message [{}]" + request.getToken() + request.getMessage());
		try {
			userId = validateToken(request.getToken());
		} catch (InvalidTokenException e) {
			_logger.warn("Returning error response [{}]" + e.getMessage());
			return HttpResponseUtils.getInvalidTokenErrorResponse();
		}

		try {
			savedRule = message;
			if (savedRule.getRuleId() != null) {
				savedRule = updateInternal(userId, request);
				ruleRepository.save(savedRule);
			} else {
				throw new InvalidMessageException("Null rule id received for edit");
			}
		} catch (Exception e) {
			_logger.error(e.getMessage());
			return HttpResponseUtils.getErrorResponse(HttpStatus.BAD_REQUEST, e.getMessage());
		}
		return HttpResponseUtils.getResponse(HttpStatus.CREATED, "Success", savedRule);
	}
}

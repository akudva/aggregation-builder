package com.ey.digital.adc.aml.sql.server.repo;

import com.ey.digital.adc.aml.model.RtcThresholdSegmentRisk;
import com.ey.digital.adc.core.sql.server.repo.ISQLRepository;

public interface RtcThresholdSegmentRiskRepo extends ISQLRepository<RtcThresholdSegmentRisk, Integer> {
	
  // RtcThresholdSegmentRisk
	public RtcThresholdSegmentRisk findByRtcCalculatedValueIdAndScalar(Integer rtcCalculatedValueId, Double scalar);
}

package com.ey.digital.adc.aml.sql.server.repo;



import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ey.digital.adc.aml.model.RtcCalculatedValue;
import com.ey.digital.adc.aml.model.RtcPrecalculatedValue;
import com.ey.digital.adc.aml.model.Rule;
import com.ey.digital.adc.aml.model.RuleThresholdConfig;
import com.ey.digital.adc.aml.model.Segment;
import com.ey.digital.adc.core.sql.server.repo.ISQLRepository;




public interface RtcPrecalculatedRepository extends ISQLRepository<RtcPrecalculatedValue, Integer> {
	

	
	
	
}
package com.ey.digital.adc.aml.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ey.digital.adc.aml.model.Condition;
import com.ey.digital.adc.aml.model.ExclusionFilter;
import com.ey.digital.adc.aml.model.Filter;
import com.ey.digital.adc.aml.model.InclusionFilter;
import com.ey.digital.adc.aml.model.LookBackRef;
import com.ey.digital.adc.aml.model.Rule;
import com.ey.digital.adc.aml.sql.server.repo.LookBackRefRepository;
import com.ey.digital.adc.aml.util.FocalEntityUtil;

@Service
public class AggregationWithOneOperandService {

	private final Logger _logger = LoggerFactory.getLogger(this.getClass());

	private final FilterService filterService;

	private final InclusionFilterService inclusionFilterService;

	private final ExclusionFilterService exclusionFilterService;
	
	@Autowired
	private LookBackRefRepository lookBackRepo;

	@Autowired
	public AggregationWithOneOperandService(FilterService filterService, InclusionFilterService inclusionFilterService,
			ExclusionFilterService exclusionFilterService) {

		this.filterService = filterService;
		this.inclusionFilterService = inclusionFilterService;
		this.exclusionFilterService = exclusionFilterService;
	}

	public String getHiveSqlForAggregationWithOneOperand(Condition condition, String aggregation, String hiveFunction,
			Rule rule, String focalEntity, String focalEntityColumn, String segment, String risk, String hiveSchema,
			String segmentMappingTable) {

		Integer ruleId = rule.getRuleId();
		Integer sequenceId = condition.getSequenceid();

		StringBuffer hiveAlertInsertSQL = new StringBuffer();
		StringBuffer hiveAlertCreateSQL = new StringBuffer();
		StringBuffer hiveAlertTxnInsertSQL = new StringBuffer();
		StringBuffer filterclause = new StringBuffer();
		StringBuffer fromClause = new StringBuffer();
		StringBuffer joinCondition = new StringBuffer();
		StringBuffer inclusionClause = new StringBuffer();
		StringBuffer exclusionClause = new StringBuffer();
		StringBuffer txnCreateOrInsertClause = new StringBuffer();

		ArrayList<Character> alias = new ArrayList<Character>();
		alias.add('a');

		ArrayList<String> tableArr = new ArrayList<String>();
		ArrayList<String> factTableArr = new ArrayList<String>(
				Arrays.asList("Settlement", "Trade", "Transaction", "settlement_view"));

		Condition child1 = condition.getChildren().get(0); // Only has one operand
		String sourceColumnParam1 = child1.getResultColumn();
		String sourceTableParam1 = child1.getResultTable();
		String sourceDetailTable = child1.getTxnDetailTable();

		if (sourceDetailTable == null) {
			sourceDetailTable = hiveSchema + "." + sourceTableParam1;
		}

		StringBuffer alertResultSelectColumn = new StringBuffer();

		String joinkeyLHS = child1.getResultjoinkey();
		condition.setResultjoinkey(joinkeyLHS);

		tableArr.add(sourceTableParam1);

		String alertTable = hiveSchema + ".alert_" + ruleId + "_${exec_id}_" + aggregation + "_condition_" + sequenceId;
		String alertTxnTable = alertTable + "_transaction";
		Character tableAlias = alias.get(0);

		String[] alertResultSelectColumnArray = null;
		
		String focalEntityConcatClause = FocalEntityUtil.getFocalEntityConcatClause(focalEntityColumn, tableAlias) ;
		String focalEntityClause = FocalEntityUtil.getFocalEntityColumn(focalEntity,focalEntityColumn, tableAlias);
		hiveAlertInsertSQL.append("CREATE TABLE IF NOT EXISTS " + alertTable
				+ " AS SELECT to_date(from_unixtime(unix_timestamp())) as exec_date, '${run_date}' as alert_date, "
				+ " CAST(concat(" + tableAlias + ".party_key, concat(" + focalEntityConcatClause
				+ ", SUBSTRING('${exec_id}',0,length('${exec_id}')-1) )) AS BIGINT) as alert_id, " + "'alert_" + ruleId + "_${exec_id}' as alert_exec_id, "
				+ hiveFunction + "(" + tableAlias + "." + sourceColumnParam1 + ") as " + aggregation + "_" + sequenceId
				+ ", " + "'" + focalEntity + "' as focal_Entity, "
				+ FocalEntityUtil.getFocalEntityColumn(focalEntityColumn, tableAlias));

		// =========================================================================================================================
		if (condition.isRootCondition())
			txnCreateOrInsertClause.append("INSERT INTO " + hiveSchema + ".final_alert_table ").append(
					"(exec_date,alert_date,alert_id,ruleid,tran_key,focal_entity,focus_key_1,focus_key_2,executionid,party_key) "
					+ "SELECT to_date(from_unixtime(unix_timestamp())) as exec_date, '${run_date}' as alert_date, "
							+ "b.alert_id as alert_id, " + ruleId + " as ruleid, " + tableAlias + ".tran_key as tran_key, " + "'"
							+ focalEntity + "' as focal_entity, "
						    + FocalEntityUtil.getAlertFocalEntityColumn(focalEntityColumn, tableAlias) +","
							+ "CAST(SUBSTRING('${exec_id}',0,length('${exec_id}')-1) AS INT) as executionid "); // Adds
																											// focus_key
																											// aliases
		else
			txnCreateOrInsertClause.append(" CREATE TABLE IF NOT EXISTS " + alertTxnTable + " AS ").append(
					"SELECT to_date(from_unixtime(unix_timestamp())) as exec_date, '${run_date}' as alert_date, "
							+ "b.alert_id, " + ruleId + " as ruleid, " + tableAlias + ".tran_key as tran_key, " + "'"
							+ focalEntity + "' as focal_entity, "
							+ "CAST(SUBSTRING('${exec_id}',0,length('${exec_id}')-1) AS INT) as executionid, "
							+ focalEntityClause);

		hiveAlertTxnInsertSQL.append(txnCreateOrInsertClause);
		// =========================================================================================================================

		alertResultSelectColumn.append("alert_exec_id,focal_Entity," + focalEntityColumn);

		if (!focalEntity.equalsIgnoreCase("customer")) {
			hiveAlertInsertSQL.append(", " + tableAlias + "." + "party_key ");
			hiveAlertTxnInsertSQL.append(", " + tableAlias + ".party_key ");
			alertResultSelectColumn.append(", party_key");
		} else if (condition.isRootCondition())
			hiveAlertTxnInsertSQL.append(", " + tableAlias + ".party_key ");

		fromClause.append(" FROM " + hiveSchema + "." + sourceTableParam1 + " " + tableAlias);

		hiveAlertTxnInsertSQL.append(" FROM " + sourceDetailTable + " " + tableAlias + " join " + alertTable + " b"
				+ " ON " + FocalEntityUtil.getJoinConditionFocalEntityColumn(focalEntityColumn, 'a', 'b'));

		// Filters
		List<Filter> filters = child1.getValue().getFilters();
		if (filters != null && !filters.isEmpty())
			filters.forEach(f -> {
				filterclause.append(filterService.createFilterSQL(f, rule, condition, tableArr, alias, joinCondition,
						fromClause, factTableArr, hiveSchema));
				if (!f.equals(filters.get(filters.size() - 1))) // TODO: test object equality. Need to validate
					filterclause.append(" AND ");
			});

		// Call inclusion filter service
		List<InclusionFilter> inclusionFilters = rule.getInclusionlist();
		if (inclusionFilters != null && !inclusionFilters.isEmpty())
			inclusionFilters.forEach(f -> {
				_logger.info("inclusionfilters - check if type is an  attribute ="
						+ f.getChildren().get(0).getType().equalsIgnoreCase("attribute"));
				if (f.getChildren().get(0).getType().equalsIgnoreCase("attribute")
						&& tableArr.contains(f.getChildren().get(0).getValue().getTable())) {
					inclusionClause.append(inclusionFilterService.createInclusionFilterSQL(f, rule, condition, tableArr,
							alias, joinCondition, fromClause, factTableArr)).append(" and ");
				}
			});
		if (inclusionClause != null && inclusionClause.length() > 0)
			inclusionClause.setLength(inclusionClause.length() - 5);

		// Call exclusion filter service
		List<ExclusionFilter> exclusionFilters = rule.getExclusionlist();

		if (exclusionFilters != null && !exclusionFilters.isEmpty())
			exclusionFilters.forEach(f -> {
				_logger.info("exclusionfilters - check if type is an  attribute ="
						+ f.getChildren().get(0).getType().equalsIgnoreCase("attribute"));
				if (f.getChildren().get(0).getType().equalsIgnoreCase("attribute")
						&& tableArr.contains(f.getChildren().get(0).getValue().getTable())) {
					exclusionClause.append(exclusionFilterService.createExclusionFilterSQL(f, rule, condition, tableArr,
							alias, joinCondition, fromClause, factTableArr)).append(" and ");
				}
			});
		if (exclusionClause != null && exclusionClause.length() > 0)
			exclusionClause.setLength(exclusionClause.length() - 5);

		if (joinCondition.length() > 0) {
			hiveAlertInsertSQL.append(fromClause + " ON " + joinCondition);
		} else {
			hiveAlertInsertSQL.append(fromClause);
		}

		
		 LookBackRef lookBackRef = lookBackRepo.findBytablename(sourceTableParam1);
         if(lookBackRef != null) {
	         LookBackRef.LookBackUnit lookBackUnit = lookBackRef.getLookbackunit();
	         StringBuffer lookbackFilterClause = new StringBuffer();
	         switch(lookBackUnit.toString()) {
	 	        	case "Daily":
	 	        		lookbackFilterClause.append(" WHERE " + tableAlias + "."+ lookBackRef.getDatecolumn()+" >= " + "'${lookback_date}' AND " + tableAlias + "."+ lookBackRef.getDatecolumn()+" <= " + "'${run_date}' ");
	 	        		break;
		        	case "Monthly":
		        		lookbackFilterClause.append(" WHERE " + tableAlias + "."+lookBackRef.getDatecolumn()+" >= " + "substr(" + "'${lookback_date}'"+",1,7) " +"AND "+tableAlias + "."+lookBackRef.getDatecolumn()+" <= " + "substr(" + "'${run_date}'"+",1,7) " );
	 	        		break;
	 	        	case "Yearly":
	 	        		lookbackFilterClause.append(" WHERE " + tableAlias + "."+lookBackRef.getDatecolumn()+" >= " + "YEAR("+"'${lookback_date}'"+") AND " + tableAlias + "."+lookBackRef.getDatecolumn()+" <= " + "YEAR("+ "'${run_date}'"+") ");
	 	        		break;
	         }
	         hiveAlertInsertSQL.append(lookbackFilterClause);
	         hiveAlertTxnInsertSQL.append(lookbackFilterClause);
	    }
		if (filterclause.length() > 0) {
			hiveAlertInsertSQL.append(" AND " + filterclause);
			hiveAlertTxnInsertSQL.append(" AND " + filterclause);
		}

		// append inclusion filter clause if it is not empty
		if (inclusionClause.length() > 0) {
			hiveAlertInsertSQL.append(" AND " + inclusionClause);
			hiveAlertTxnInsertSQL.append(" AND " + inclusionClause);
		}

		// append exclusion filter clause if it is not empty
		if (exclusionClause.length() > 0) {
			hiveAlertInsertSQL.append(" AND " + exclusionClause);
			hiveAlertTxnInsertSQL.append(" AND " + exclusionClause);
		}

		if (!aggregation.equalsIgnoreCase("value")) {
			// No 'GROUP BY' clause for value
			if (focalEntityColumn.indexOf("party") >= 0)
				hiveAlertInsertSQL
						.append(" GROUP BY " + FocalEntityUtil.getFocalEntityColumn(focalEntityColumn, tableAlias));
			else
				hiveAlertInsertSQL.append(" GROUP BY " + tableAlias + ".party_key, "
						+ FocalEntityUtil.getFocalEntityColumn(focalEntityColumn, tableAlias));
		}

		condition.setResultColumn(aggregation + "_" + sequenceId);
		condition.setAlertResultSelectColumn(alertResultSelectColumn.toString());
		condition.setAlertInsertSql(hiveAlertInsertSQL.toString());
		condition.setAlertTranInsertSql(hiveAlertTxnInsertSQL.toString());
		condition.setTxnDetailTable(alertTxnTable);

		_logger.info(hiveAlertInsertSQL.toString());
		_logger.info(hiveAlertTxnInsertSQL.toString());

		return hiveAlertInsertSQL.toString();
	}
}

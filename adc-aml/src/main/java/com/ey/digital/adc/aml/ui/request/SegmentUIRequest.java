package com.ey.digital.adc.aml.ui.request;

import com.ey.digital.adc.aml.model.Segment;
import com.ey.digital.adc.core.model.IModel;
import com.ey.digital.adc.core.ui.request.AbstractUIRequest;

public class SegmentUIRequest extends AbstractUIRequest<Segment> {

	private static final long serialVersionUID = 1L;
}

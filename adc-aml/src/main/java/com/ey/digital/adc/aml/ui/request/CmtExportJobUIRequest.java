package com.ey.digital.adc.aml.ui.request;

import com.ey.digital.adc.aml.model.CmtExportResult;
import com.ey.digital.adc.core.model.IModel;
import com.ey.digital.adc.core.ui.request.AbstractUIRequest;

import java.util.List;


public class CmtExportJobUIRequest extends AbstractUIRequest<CmtExportResult> implements IModel {
    private static final long serialVersionUID = 1L;


}

package com.ey.digital.adc.aml.sql.server.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ey.digital.adc.aml.model.Condition;
import com.ey.digital.adc.core.sql.server.repo.ISQLRepository;

@Repository
public interface ConditionRepository extends ISQLRepository<Condition, Integer> {

	public List<Condition> findByRuleIdOrderBySequenceidDesc(Integer ruleid);

	public Condition findByConditionIdAndRuleId(Integer conditionId, Integer ruleId);

	public List<Condition> findByRuleId(@Param("ruleId") Integer ruleId);

	public Condition findByRuleIdAndThresholdLabel(Integer ruleId, String thresholdLabel);

	public List<Condition> findByRuleuuid(@Param("ruleuuid") String ruleuuid);

	public List<Condition> findByRuleIdAndIsThresholdTrue(@Param("ruleid") Integer ruleId);

	public List<Condition> findByRuleIdAndIsThreshold(Integer ruleId, boolean bool);

	public Integer deleteByRuleId(Integer ruleId);
	
	@Query(value = "SELECT NEXT VALUE FOR conditionSequence", nativeQuery = true)
	public Integer getNextConditionSequenceValue();

	@Query(value = "SELECT threshold_label FROM Condition where ruleid=:ruleid and type='thresholdLabel' and isThreshold = '1' order by condition_id", nativeQuery = true)
	public List<String> findThersholds(@Param("ruleid") Integer ruleid);

	@Query(value = "SELECT parent FROM Condition where primaryconditionid=:primaryconditionid ", nativeQuery = true)
	public Integer findParentByPrimaryconditionid(@Param("primaryconditionid") Integer primaryConditionId);

	@Query(value = "SELECT * FROM Condition where parent=:parent order by sequenceid ", nativeQuery = true)
	public List<Condition> findByParentId(@Param("parent") Integer parent);

	@Query(value = "SELECT primaryconditionid FROM Condition where ruleid=:ruleid and parent is null", nativeQuery = true)
	public Integer getRootConditionIdByRuleId(@Param("ruleid") Integer ruleid);

}

package com.ey.digital.adc.aml.controllers;

import com.ey.digital.adc.aml.model.ExecutionPayload;
import com.ey.digital.adc.aml.service.HiveExecutionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.core.MediaType;

@RestController
@RequestMapping("/execute-rule")
public class RuleExecutionController {
    @Autowired
    HiveExecutionService hiveExecutionService;

    @GetMapping(params = {"executionId"})
    public ExecutionPayload retrieveSQL(@RequestParam(name = "executionId") Integer executionId) {
    		return hiveExecutionService.generateExecutionPayload(executionId);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    public @ResponseBody
    ExecutionPayload testExec(@RequestBody ExecutionPayload executionPayload) {
        System.out.println("executionPayload = [" + executionPayload + "]");
        hiveExecutionService.updateExecution(executionPayload.getSequenceId(), executionPayload.getResultData().get("result").get(0));
        return executionPayload;
    }
//
//    @GetMapping(path = "/kick", params = {"jobId"})
//    public Integer kick(@RequestParam(name = "jobId") Integer jobId) throws IOException, InterruptedException, DuplicateElementException {
//        return hiveExecutionService.startOozieJob(jobId);
//    }
}

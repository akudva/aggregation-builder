package com.ey.digital.adc.aml.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.ey.digital.adc.core.model.Audit;
import com.ey.digital.adc.core.model.IModel;

import lombok.Data;

@Data
public class SegThresholdMetadata {
	
	private String segment;
	private String population;
	private String isSelected;
	private List<ThresholdMetadata> thresholds;
	
	public String getSegment() {
		return segment;
	}



	public String getIsSelected() {
		return isSelected;
	}



	public void setIsSelected(String isSelected) {
		this.isSelected = isSelected;
	}



	public void setSegment(String segment) {
		this.segment = segment;
	}

	public String getPopulation() {
		return population;
	}

	public void setPopulation(String population) {
		this.population = population;
	}

	public List<ThresholdMetadata> getThresholds() {
		return thresholds;
	}

	public void setThresholds(List<ThresholdMetadata> thresholds) {
		this.thresholds = thresholds;
	}

	
	
	

}
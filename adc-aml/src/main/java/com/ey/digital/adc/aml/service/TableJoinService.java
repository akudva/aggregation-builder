package com.ey.digital.adc.aml.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ey.digital.adc.aml.model.Condition;
import com.ey.digital.adc.aml.model.Rule;
import com.ey.digital.adc.aml.model.TableJoin;
import com.ey.digital.adc.aml.sql.server.repo.TableJoinRepository;
import com.ey.digital.adc.aml.util.FocalEntityUtil;

@Service
public class TableJoinService {

	private final TableJoinRepository tableJoinRepo;

	private final Logger _logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	public TableJoinService(TableJoinRepository tableJoinRepo) {

		this.tableJoinRepo = tableJoinRepo;
	}

	public StringBuffer getJoinConditionForIntermediateTables(String joinkeyLHS, String joinkeyRHS, Character alias1,
			Character alias2, String focalEntityColumn) { // Add focal entity as param,
															// pass the alias
		StringBuffer joinCondition = new StringBuffer();
		if (joinkeyLHS == null) {
			joinCondition
					.append(" " + FocalEntityUtil.getJoinConditionFocalEntityColumn(focalEntityColumn, alias1, alias2));
		} else if (joinkeyLHS.contains(",")) {

			String[] joinkeyLHSArray = joinkeyLHS.split(Pattern.quote(","));
			String[] joinkeyRHSArray = joinkeyRHS.split(Pattern.quote(","));

			for (int i = 0; i < joinkeyLHSArray.length; i++) {
				joinCondition.append(
						" " + alias1 + "." + joinkeyLHSArray[i] + " = " + alias2 + "." + joinkeyRHSArray[i] + " and ");
			}
			joinCondition.setLength(joinCondition.length() - 4);
		} else {
			joinCondition.append(" " + alias1 + "." + joinkeyLHS + " = " + alias2 + "." + joinkeyRHS);
		}

		return joinCondition;
	}

	public StringBuffer getJoinConditionForDataModelTables(String sourceTableParam1, String sourceTableParam2,
			Character alias1, Character alias2) {

		List<TableJoin> tableJoin;
		StringBuffer joinCondition = new StringBuffer();

		tableJoin = tableJoinRepo.getJoinKeys(sourceTableParam1, sourceTableParam2);
		String joinkeyLHS = tableJoin.get(0).getJoinkey_lhs();
		String joinkeyRHS = tableJoin.get(0).getJoinkey_rhs();

		if (joinkeyLHS.contains(",")) {

			String[] joinkeyLHSArray = joinkeyLHS.split(Pattern.quote(","));
			String[] joinkeyRHSArray = joinkeyRHS.split(Pattern.quote(","));

			for (int i = 0; i < joinkeyLHSArray.length; i++) {
				joinCondition.append(
						" " + alias1 + "." + joinkeyLHSArray[i] + " = " + alias2 + "." + joinkeyRHSArray[i] + " and");
			}
			joinCondition.setLength(joinCondition.length() - 4);
		} else {
			joinCondition.append(" " + alias1 + "." + joinkeyLHS + " = " + alias2 + "." + joinkeyRHS);
		}

		return joinCondition;

	}

	public String getResultJoinKey(String sourceTableParam1, String sourceTableParam2) {

		List<TableJoin> tableJoin;

		tableJoin = tableJoinRepo.getJoinKeys(sourceTableParam1, sourceTableParam2);

		return tableJoin.get(0).getJoinkey_lhs();

	}

	public StringBuffer getSelectColumnsWithoutDuplicates(Condition condition, Character alias1, Character alias2) {

		StringBuffer selectStatementWithoutDups = new StringBuffer();

		_logger.info("condition " + condition.getConditionId() + " LHS Select columns= "
				+ condition.getChildren().get(0).getAlertResultSelectColumn());
		_logger.info("condition " + condition.getConditionId() + " RHS Select columns= "
				+ condition.getChildren().get(1).getAlertResultSelectColumn());

		String[] lhsSourceSelectColumn = null;
		if (condition.getChildren().get(0).getAlertResultSelectColumn() != null) {
			lhsSourceSelectColumn = condition.getChildren().get(0).getAlertResultSelectColumn().trim()
					.split(Pattern.quote(","));
		}

		List<String> rhsSourceSelectColumnList = null;
		if (condition.getChildren().get(1) != null && condition.getChildren().get(1).getAlertResultSelectColumn() != null
				&& !condition.getChildren().get(1).getType().equalsIgnoreCase("thresholdlabel")) {
			String[] rhsSourceSelectColumn = condition.getChildren().get(1).getAlertResultSelectColumn().trim()
					.split(Pattern.quote(","));

			if (rhsSourceSelectColumn != null)
				rhsSourceSelectColumnList = new ArrayList<String>(
						Arrays.asList(rhsSourceSelectColumn).stream().map(s -> s.trim()).collect(Collectors.toList()));
		}

		if (lhsSourceSelectColumn.length == 1 && lhsSourceSelectColumn[0].length() == 0)
			return selectStatementWithoutDups;

		Set<String> unionNoDups = new HashSet<String>(
				Arrays.asList(lhsSourceSelectColumn).stream().map(s -> s.trim()).collect(Collectors.toList()));

		int index = 0;
		if (!unionNoDups.isEmpty()) {
			for (String column : unionNoDups) {
				index++;
				if (column.length() > 0 && index == unionNoDups.size()) {
					selectStatementWithoutDups.append(alias1 + "." + column);
				} else if (column.length() > 0) {
					selectStatementWithoutDups.append(alias1 + "." + column + ", ");
				}
			}
		}
		if (rhsSourceSelectColumnList != null) {
			for (String column : rhsSourceSelectColumnList) {
				if (!unionNoDups.contains(column)) {
					unionNoDups.add(column);
					selectStatementWithoutDups.append("," + alias2 + "." + column);
				}
			}
		}
		condition.setAlertResultSelectColumn(StringUtils.join(unionNoDups, ","));
		_logger.info("condition " + condition.getConditionId() + " selectStatementWithoutDups= "
				+ selectStatementWithoutDups);
		_logger.info(
				"condition " + condition.getConditionId() + " StringUtilsJoin= " + StringUtils.join(unionNoDups, ", "));
		return selectStatementWithoutDups;
	}

	public void getJoinConditionforSegment(Rule rule, Condition condition, ArrayList<String> tableArr,
			ArrayList<Character> alias, StringBuffer joinCondition, StringBuffer fromClause, StringBuffer filterclause,
			ArrayList<String> factTableArr, String focalEntity, String focalEntityColumn, String segment, String risk,
			String segmentMappingTable) {

		tableArr.add(segmentMappingTable);
		alias.add(new Character((char) (alias.get(alias.size() - 1).charValue() + 1)));

		fromClause.append(" JOIN " + segmentMappingTable + " " + alias.get(tableArr.size() - 1));

		int joinIndex = 0;
		for (int x = 0; x < tableArr.size() - 1; x++) {
			if (factTableArr.contains(tableArr.get(joinIndex))) {
				break;
			} else {
				joinIndex++;
			}
		}

		joinCondition.append(" " + alias.get(joinIndex) + "." + focalEntityColumn + "=" + alias.get(tableArr.size() - 1)
				+ "." + focalEntityColumn + " ");
		filterclause.append(alias.get(tableArr.size() - 1) + "." + "segmodel = ${segModel}");

	}
}

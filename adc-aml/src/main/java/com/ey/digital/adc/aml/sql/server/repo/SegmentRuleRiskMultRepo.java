package com.ey.digital.adc.aml.sql.server.repo;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ey.digital.adc.aml.model.SegmentRuleRiskMultiplier;
import com.ey.digital.adc.core.sql.server.repo.ISQLRepository;

public interface SegmentRuleRiskMultRepo extends ISQLRepository<SegmentRuleRiskMultiplier, Integer>{
	
		
		@Modifying
		@Transactional
		@Query(value = "UPDATE segmentruleriskmult SET status='InActive' where ruleId= :ruleId and status='Active'" , nativeQuery = true)
		void updateStatus( @Param("ruleId") Integer ruleId);


}

package com.ey.digital.adc.aml.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import com.ey.digital.adc.core.model.Audit;
import com.ey.digital.adc.core.model.IModel;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "[condition]")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Condition extends Audit implements IModel{
	
	private static final long serialVersionUID = 1L;

	@Column(name = "condition_id")
	private Integer conditionId;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "primaryconditionid")
	private Integer primaryconditionid;
	
	@Column(name = "sequenceid")
	private Integer sequenceid;
	
	@Column(name = "type")
	private String type;

	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "valuedata")
	private Value value;

	@Column(name = "thresholdLabel")
	@JsonIgnore
	private String thresholdLabel;
	
	@Column(name = "ruleid")
	private Integer ruleId; 
	
	@Column(name = "ruleuuid")
	@JsonIgnore
	private String ruleuuid;

	@Column(name = "isthreshold", columnDefinition = "true_false")
	@JsonIgnore
	private boolean isThreshold;

	@Transient
	@JsonIgnore
	private String txnDetailTable;
	
	@Transient
	@JsonIgnore
	private String platformTable;
	
	@JsonIgnore
	@Column(name = "parentConditionId")
	private Integer parentConditionId;
	
	@Column(name = "resulttable")
	@JsonIgnore
	private String resultTable;
	
	@Column(name = "resultcolumn")
	@JsonIgnore
	private String resultColumn;
	
	@Column(name = "alertresultselectcolumn")
	@JsonIgnore
	private String alertResultSelectColumn;
	
	@Column(name = "resultdata")
	@JsonIgnore
	private String resultData;

	@Column(name = "resultjoinkey")
	@JsonIgnore
	private String resultjoinkey;

	@Column(name = "thresholdquery")
	@JsonIgnore
	private String thresholdQuery;
	
	@Column(name = "alertcreatesql")
	@JsonIgnore
	private String alertCreateSql;
	
	@Column(name = "alertinsertsql")
	@JsonIgnore
	private String alertInsertSql;
	
	@Column(name = "alerttrancreatesql")
	@JsonIgnore
	private String alertTranCreateSql;
	
	@Column(name = "alerttraninsertsql")
	@JsonIgnore
	private String alertTranInsertSql;
	
	@Column(name = "alertmetricsql")
	@JsonIgnore
	private String alertMetricSql;
	
	@Transient
	@JsonIgnore
	private boolean isRootCondition;
	
	public void setIsRootCondition(boolean isRootCondition) {
		this.isRootCondition = isRootCondition;
	}
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY) 
	@JoinColumn(name = "parent")    // Unidirectional
	private List<Condition> children = new ArrayList<Condition>();

	@Override
	public String toString() {
		return "Condition [conditionId=" + conditionId + ", primaryconditionid=" + primaryconditionid + ", sequenceid="
				+ getSequenceid() + ", type=" + type + ", value=" + value + ", thresholdLabel=" + thresholdLabel
				+ ", ruleId=" + ruleId + ", isThreshold=" + isThreshold + "]";
	}
}

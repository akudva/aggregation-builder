package com.ey.digital.adc.aml.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ey.digital.adc.aml.model.Condition;
import com.ey.digital.adc.aml.model.InclusionFilter;
import com.ey.digital.adc.aml.model.Rule;

@Service
public class InclusionFilterService {

	private final Logger _logger = LoggerFactory.getLogger(this.getClass());

	private Set<String> operatorsThatCompareMultipleValues = new HashSet<String>(Arrays.asList("in", "not in"));

	@Autowired
	public InclusionFilterService() {
	}

	public StringBuffer createInclusionFilterSQL(InclusionFilter filter, Rule rule, Condition condition,
			ArrayList<String> tableArr, ArrayList<Character> alias, StringBuffer joinCondition, StringBuffer fromClause,
			ArrayList<String> factTableArr) {

		StringBuffer filterSQL = new StringBuffer();
		String resultColumn;
		String resultTable = "";

		String operator; // Always equal to 'operator'?
		String type = filter.getType();
		if (type != null)
			switch (type) {
			case "attribute":
				_logger.info("Inclusion Filter - attribute=" + type);
				resultColumn = filter.getValue().getAttribute();
				resultTable = filter.getValue().getTable();

				filter.setResultcolumn(resultColumn);
				filter.setResultTable(resultTable);

				if (tableArr.contains(resultTable)) {
					int index = 0;
					for (String t : tableArr) {
						if (tableArr.get(index).equalsIgnoreCase(resultTable)) {
							break;
						} else {
							index++;
						}
					}
					filterSQL.append(" " + alias.get(index) + "." + resultColumn + " ");
				}
				/*
				 * Only apply Inclusion Filter if the condition table is same as the inclusion
				 * filter table
				 */

				_logger.info("Inclusion Filter - Attribute SQL=" + filterSQL);
				break;
			case "data":
				_logger.info("Inclusion Filter - Data=" + type);
				String data = filter.getValue().getData();

				try {
					Double.valueOf(data);
					filterSQL.append(" CAST('" + data + "' as DOUBLE) ");
					_logger.info("Inclusion Filter - Data SQL=" + filterSQL);
				} catch (NumberFormatException e) {
					if (!data.contains(",")) {
						filterSQL.append(" '" + data + "' ");
					} else if (data.contains(",")) {
						Set<String> dataArray = new HashSet<String>(
								Arrays.asList(filter.getValue().getData().split(Pattern.quote(","))));
						int index = 0;
						for (String a : dataArray) {
							index++;
							filterSQL.append("'" + a + "'");
							if (index != dataArray.size()) // TODO: test object
															// equality. Need to
															// validate
								filterSQL.append(",");
						}

						_logger.info("Inclusion Filter - Data SQL=" + filterSQL);
					}
				}

				break;

			case "operator":
				_logger.info("Inclusion Filter - Operator=" + type);
				operator = filter.getValue().getOperator().toLowerCase();

				filterSQL.append(createInclusionFilterSQL(filter.getChildren().get(0), rule, condition, tableArr, alias,
						joinCondition, fromClause, factTableArr) + " " + operator);

				if (operatorsThatCompareMultipleValues.contains(operator)) {
					filterSQL.append(" (" + createInclusionFilterSQL(filter.getChildren().get(1), rule, condition,
							tableArr, alias, joinCondition, fromClause, factTableArr) + ")");
				} else {
					filterSQL.append(" " + createInclusionFilterSQL(filter.getChildren().get(1), rule, condition,
							tableArr, alias, joinCondition, fromClause, factTableArr));
				}

				_logger.info("Inclusion Filter - Operator SQL=" + filterSQL);
				break;

			}

		_logger.info("Created Inclusion Filter" + filterSQL);
		return filterSQL;

	}
}
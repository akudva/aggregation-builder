#####################################################

View Cases
Rest End Point: http://localhost:8080/case/view?token=82C36AF3-B220-459B-9AAD-88A81883E970&clientId=10&pageNo=1&pageSize=5

#####################################################

Export Cases as Excel
Rest End Point: http://localhost:8080/case/download?token=82C36AF3-B220-459B-9AAD-88A81883E970&clientId=10&pageNo=1&pageSize=2

#####################################################

Approve or Reject Rules
Rest End Point: http://localhost:8080/rule/approve-reject-rules

Payload:
{
	    "token":"82C36AF3-B220-459B-9AAD-88A81883E970",
    "message" : { 
        "ruleIds":[1158],
        "isApprove":true
    }

}

#####################################################

Create Rule -1
Rest End Point: http://localhost:8080/rule

Payload: Excessive Funds Transfers / Large Transactions (contains acct_open_dt)
{
   "token":"82C36AF3-B220-459B-9AAD-88A81883E970",
   "message":{
      "ruleName":"AML_TL_RUL_1157",
      "ruleText":"max|acct_open_dt|all|560|>=^AND^sum|tran_amt|all|560|>=^OR^count|tran_amt|all|560|>=",
      "focalEntity":"Customer",
      "clientId":"10",
   	  "description":"rule description",
   	  "comment":"rule comment",
      "segRuleRiskMultList":[
         {
            "ruleConditionId":"condition-1",
            "segmentId":"1002",
            "riskLevel":"RR",
            "threshold":"5000",
            "thresholdLabel":"max account open date",
            "multiplier":"1"
         },
         {
            "ruleConditionId":"condition-1",
            "segmentId":"1002",
            "riskLevel":"HR",
            "threshold":"5000",
            "thresholdLabel":"max account open date",
            "multiplier":"1"
         },
         {
            "ruleConditionId":"condition-1",
            "segmentId":"1002",
            "riskLevel":"MR",
            "threshold":"5000",
            "thresholdLabel":"max account open date",
            "multiplier":"1"
         },
         {
            "ruleConditionId":"condition-1",
            "segmentId":"1002",
            "riskLevel":"LR",
            "threshold":"5000",
            "thresholdLabel":"max account open date",
            "multiplier":"1"
         },
          {
            "ruleConditionId":"condition-2",
            "segmentId":"1002",
            "riskLevel":"RR",
            "threshold":"10",
            "thresholdLabel":"sum amount",
            "multiplier":"1"
         },
         {
            "ruleConditionId":"condition-2",
            "segmentId":"1002",
            "riskLevel":"HR",
            "threshold":"10",
            "thresholdLabel":"sum amount",
            "multiplier":"1"
         },
         {
            "ruleConditionId":"condition-2",
            "segmentId":"1002",
            "riskLevel":"MR",
            "threshold":"10",
            "thresholdLabel":"sum amount",
            "multiplier":"1"
         },
         {
            "ruleConditionId":"condition-2",
            "segmentId":"1002",
            "riskLevel":"LR",
            "threshold":"10",
            "thresholdLabel":"sum amount",
            "multiplier":"1"
         },
          {
            "ruleConditionId":"condition-3",
            "segmentId":"1002",
            "riskLevel":"RR",
            "threshold":"1",
            "thresholdLabel":"count amount",
            "multiplier":"1"
         },
         {
            "ruleConditionId":"condition-3",
            "segmentId":"1002",
            "riskLevel":"HR",
            "threshold":"1",
            "thresholdLabel":"count amount",
            "multiplier":"1"
         },
         {
            "ruleConditionId":"condition-3",
            "segmentId":"1002",
            "riskLevel":"MR",
            "threshold":"1",
            "thresholdLabel":"count amount",
            "multiplier":"1"
         },
         {
            "ruleConditionId":"condition-3",
            "segmentId":"1002",
            "riskLevel":"LR",
            "threshold":"1",
            "thresholdLabel":"count amount",
            "multiplier":"1"
         }
      
      ]
   }
}

#####################################################
Create Rule-2

Payload: High Risk Transactions: Focal High Risk Entity (contains cust_risk
{
   "token":"82C36AF3-B220-459B-9AAD-88A81883E970",
   "message":{
      "ruleName":"AML_TL_RUL_1158",
      "ruleText":"max|cust_risk|all|560|>=^AND^sum|tran_amt|all|560|>=^OR^count|tran_amt|all|560|>=",
      "focalEntity":"Customer",
      "clientId":"10",
   	  "description":"rule description",
   	  "comment":"rule comment",
      "segRuleRiskMultList":[
         {
            "ruleConditionId":"condition-1",
            "segmentId":"1002",
            "riskLevel":"RR",
            "threshold":"1",
            "thresholdLabel":"max customer effective risk",
            "multiplier":"1"
         },
         {
            "ruleConditionId":"condition-1",
            "segmentId":"1002",
            "riskLevel":"HR",
            "threshold":"1",
            "thresholdLabel":"max customer effective risk",
            "multiplier":"1"
         },
         {
            "ruleConditionId":"condition-1",
            "segmentId":"1002",
            "riskLevel":"MR",
            "threshold":"1",
            "thresholdLabel":"max customer effective risk",
            "multiplier":"1"
         },
         {
            "ruleConditionId":"condition-1",
            "segmentId":"1002",
            "riskLevel":"LR",
            "threshold":"1",
            "thresholdLabel":"max customer effective risk",
            "multiplier":"1"
         },
          {
            "ruleConditionId":"condition-2",
            "segmentId":"1002",
            "riskLevel":"RR",
            "threshold":"10",
            "thresholdLabel":"sum amount",
            "multiplier":"1"
         },
         {
            "ruleConditionId":"condition-2",
            "segmentId":"1002",
            "riskLevel":"HR",
            "threshold":"10",
            "thresholdLabel":"sum amount",
            "multiplier":"1"
         },
         {
            "ruleConditionId":"condition-2",
            "segmentId":"1002",
            "riskLevel":"MR",
            "threshold":"10",
            "thresholdLabel":"sum amount",
            "multiplier":"1"
         },
         {
            "ruleConditionId":"condition-2",
            "segmentId":"1002",
            "riskLevel":"LR",
            "threshold":"10",
            "thresholdLabel":"sum amount",
            "multiplier":"1"
         },
          {
            "ruleConditionId":"condition-3",
            "segmentId":"1002",
            "riskLevel":"RR",
            "threshold":"1",
            "thresholdLabel":"count amount",
            "multiplier":"1"
         },
         {
            "ruleConditionId":"condition-3",
            "segmentId":"1002",
            "riskLevel":"HR",
            "threshold":"1",
            "thresholdLabel":"count amount",
            "multiplier":"1"
         },
         {
            "ruleConditionId":"condition-3",
            "segmentId":"1002",
            "riskLevel":"MR",
            "threshold":"1",
            "thresholdLabel":"count amount",
            "multiplier":"1"
         },
         {
            "ruleConditionId":"condition-3",
            "segmentId":"1002",
            "riskLevel":"LR",
            "threshold":"1",
            "thresholdLabel":"count amount",
            "multiplier":"1"
         }
      
      ]
   }
}

#####################################################
Create Rule-3

Payload:
{
   "token":"82C36AF3-B220-459B-9AAD-88A81883E970",
   "message":{
      "ruleName":"AML_TL_RUL_019",
      "ruleText":"count|tran_amt|all_inc|300|>=|1~sum|tran_amt|ce_inc|365|>=|10^AND^max|tran_amt|ce_all|560|>=|10^OR^sum|tran_amt|all|500|>=|10",
      "focalEntity":"Account",
      "clientId":"10",
   	  "description":"rule description",
   	  "comment":"rule comment",
      "segRuleRiskMultList":[
         {
            "ruleConditionId":"condition-1",
            "segmentId":"1000",
            "riskLevel":"RR",
            "threshold":"100",
            "thresholdLabel":"minimum amount",
            "multiplier":"1.4"
         },
         {
            "ruleConditionId":"condition-1",
            "segmentId":"1000",
            "riskLevel":"HR",
            "threshold":"100",
            "thresholdLabel":"minimum amount",
            "multiplier":"1.2"
         },
         {
            "ruleConditionId":"condition-1",
            "segmentId":"1000",
            "riskLevel":"MR",
            "threshold":"100",
            "thresholdLabel":"minimum amount",
            "multiplier":"1.0"
         },
         {
            "ruleConditionId":"condition-1",
            "segmentId":"1000",
            "riskLevel":"LR",
            "threshold":"100",
            "thresholdLabel":"minimum amount",
            "multiplier":"0.8"
         },
         {
            "ruleConditionId":"condition-2",
            "segmentId":"1000",
            "riskLevel":"RR",
            "threshold":"10",
            "thresholdLabel":"minimum amount",
            "multiplier":"1.4"
         },
         {
            "ruleConditionId":"condition-2",
            "segmentId":"1000",
            "riskLevel":"HR",
            "threshold":"10",
             "thresholdLabel":"minimum amount",
            "multiplier":"1.2"
         },
         {
            "ruleConditionId":"condition-2",
            "segmentId":"1000",
            "riskLevel":"MR",
            "threshold":"10",
            "thresholdLabel":"minimum amount",
            "multiplier":"1.0"
         },
         {
            "ruleConditionId":"condition-2",
            "segmentId":"1000",
            "riskLevel":"LR",
            "threshold":"10",
            "thresholdLabel":"minimum amount",
            "multiplier":"0.8"
         },
         {
            "ruleConditionId":"condition-3",
            "segmentId":"1000",
            "riskLevel":"RR",
            "threshold":"25", 
            "thresholdLabel":"minimum amount",
            "multiplier":"1.4"
         },
         {
            "ruleConditionId":"condition-3",
            "segmentId":"1000",
            "riskLevel":"HR",
            "threshold":"25",
            "thresholdLabel":"minimum amount",
            "multiplier":"1.2"
         },
         {
            "ruleConditionId":"condition-3",
            "segmentId":"1000",
            "riskLevel":"MR",
            "threshold":"25",
            "thresholdLabel":"minimum amount",
            "multiplier":"1.0"
         },
         {
            "ruleConditionId":"condition-3",
            "segmentId":"1000",
            "riskLevel":"LR",
            "threshold":"25",
            "thresholdLabel":"minimum amount",
            "multiplier":"0.8"
         },
         		{
            "ruleConditionId":"condition-1",
            "segmentId":"1001",
            "riskLevel":"RR",
            "threshold":"100",
            "thresholdLabel":"minimum amount",
            "multiplier":"1.4"
         },
         {
            "ruleConditionId":"condition-1",
            "segmentId":"1001",
            "riskLevel":"HR",
            "threshold":"100",
            "multiplier":"1.2"
         },
         {
            "ruleConditionId":"condition-1",
            "segmentId":"1001",
            "riskLevel":"MR",
            "threshold":"100",
            "thresholdLabel":"minimum amount",
            "multiplier":"1.0"
         },
         {
            "ruleConditionId":"condition-1",
            "segmentId":"1001",
            "riskLevel":"LR",
            "threshold":"100",
            "thresholdLabel":"minimum amount",
            "multiplier":"0.8"
         },
         {
            "ruleConditionId":"condition-2",
            "segmentId":"1001",
            "riskLevel":"RR",
            "threshold":"10",
            "thresholdLabel":"minimum amount",
            "multiplier":"1.4"
         },
         {
            "ruleConditionId":"condition-2",
            "segmentId":"1001",
            "riskLevel":"HR",
            "threshold":"10",
            "thresholdLabel":"minimum amount",
            "multiplier":"1.2"
         },
         {
            "ruleConditionId":"condition-2",
            "segmentId":"1001",
            "riskLevel":"MR",
            "threshold":"10",
            "thresholdLabel":"minimum amount",
            "multiplier":"1.0"
         },
         {
            "ruleConditionId":"condition-2",
            "segmentId":"1001",
            "riskLevel":"LR",
            "threshold":"10",
            "thresholdLabel":"minimum amount",
            "multiplier":"0.8"
         },
         {
            "ruleConditionId":"condition-3",
            "segmentId":"1001",
            "riskLevel":"RR",
            "threshold":"25",
            "thresholdLabel":"minimum amount",
            "multiplier":"1.4"
         },
         {
            "ruleConditionId":"condition-3",
            "segmentId":"1001",
            "riskLevel":"HR",
            "threshold":"25",
            "thresholdLabel":"minimum amount",
            "multiplier":"1.2"
         },
         {
            "ruleConditionId":"condition-3",
            "segmentId":"1001",
            "riskLevel":"MR",
            "threshold":"25",
            "thresholdLabel":"minimum amount",
            "multiplier":"1.0"
         },
         {
            "ruleConditionId":"condition-3",
            "segmentId":"1001",
            "riskLevel":"LR",
            "threshold":"25",
            "thresholdLabel":"minimum amount",
            "multiplier":"0.8"
         }
      
      ]
   }
}

#####################################################
Create Rule with only one condition

payload sample 2
{
   "token":"82C36AF3-B220-459B-9AAD-88A81883E970",
   "message":{
      "ruleName":"AML_TL_RUL_019",
      "ruleText":"sum|tran_amt|all|360|>=",
      "focalEntity":"Account",
      "clientId":"10",
      "runDate":"2016-12-30",
      "segRuleRiskMultList":[
         {
            "ruleConditionId":"condition-1",
            "segmentId":"1000",
            "riskLevel":"VR",
            "threshold":"100",
            "multiplier":"1.4"
         },
         {
            "ruleConditionId":"condition-1",
            "segmentId":"1000",
            "riskLevel":"HR",
            "threshold":"100",
            "multiplier":"1.2"
         },
         {
            "ruleConditionId":"condition-1",
            "segmentId":"1000",
            "riskLevel":"MR",
            "threshold":"100",
            "multiplier":"1.0"
         },
         {
            "ruleConditionId":"condition-1",
            "segmentId":"1000",
            "riskLevel":"LR",
            "threshold":"100",
            "multiplier":"0.8"
         }
        
      ]
   }
}


Customer Payload 

{
   "token":"82C36AF3-B220-459B-9AAD-88A81883E970",
   "message":{
      "ruleName":"AML_TL_RUL_019",
      "ruleText":"count|tran_amt|all_inc|300|>=|~sum|tran_amt|ce_inc|365|>=|^AND^max|tran_amt|ce_all|560|>=|^OR^sum|tran_amt|all|500|>=",
      "focalEntity":"Customer",
      "clientId":"10",
   	  "description":"rule description",
   	  "comment":"rule comment",
      "segRuleRiskMultList":[
         {
            "ruleConditionId":"condition-1",
            "segmentId":"1002",
            "riskLevel":"RR",
            "threshold":"100",
            "thresholdLabel":"minimum amount",
            "multiplier":"1.4"
         },
         {
            "ruleConditionId":"condition-1",
            "segmentId":"1002",
            "riskLevel":"HR",
            "threshold":"100",
            "thresholdLabel":"minimum amount",
            "multiplier":"1.2"
         },
         {
            "ruleConditionId":"condition-1",
            "segmentId":"1002",
            "riskLevel":"MR",
            "threshold":"100",
            "thresholdLabel":"minimum amount",
            "multiplier":"1.0"
         },
         {
            "ruleConditionId":"condition-1",
            "segmentId":"1002",
            "riskLevel":"LR",
            "threshold":"100",
            "thresholdLabel":"minimum amount",
            "multiplier":"0.8"
         },
         {
            "ruleConditionId":"condition-2",
            "segmentId":"1002",
            "riskLevel":"RR",
            "threshold":"10",
            "thresholdLabel":"minimum amount",
            "multiplier":"1.4"
         },
         {
            "ruleConditionId":"condition-2",
            "segmentId":"1002",
            "riskLevel":"HR",
            "threshold":"10",
             "thresholdLabel":"minimum amount",
            "multiplier":"1.2"
         },
         {
            "ruleConditionId":"condition-2",
            "segmentId":"1002",
            "riskLevel":"MR",
            "threshold":"10",
            "thresholdLabel":"minimum amount",
            "multiplier":"1.0"
         },
         {
            "ruleConditionId":"condition-2",
            "segmentId":"1002",
            "riskLevel":"LR",
            "threshold":"10",
            "thresholdLabel":"minimum amount",
            "multiplier":"0.8"
         },
         {
            "ruleConditionId":"condition-3",
            "segmentId":"1002",
            "riskLevel":"RR",
            "threshold":"25", 
            "thresholdLabel":"minimum amount",
            "multiplier":"1.4"
         },
         {
            "ruleConditionId":"condition-3",
            "segmentId":"1002",
            "riskLevel":"HR",
            "threshold":"25",
            "thresholdLabel":"minimum amount",
            "multiplier":"1.2"
         },
         {
            "ruleConditionId":"condition-3",
            "segmentId":"1002",
            "riskLevel":"MR",
            "threshold":"25",
            "thresholdLabel":"minimum amount",
            "multiplier":"1.0"
         },
         {
            "ruleConditionId":"condition-3",
            "segmentId":"1002",
            "riskLevel":"LR",
            "threshold":"25",
            "thresholdLabel":"minimum amount",
            "multiplier":"0.8"
         }
         	      ]
   }
}




Rest End point for approve/reject rules

End Point: 
http://localhost:8080/rule/approve-reject-rules
Payload:
{
	"token":"82C36AF3-B220-459B-9AAD-88A81883E970",
	"message" : { 
		"ruleIds":[1011],
		"isApprove":true
	}
}


==================================================================


update thresholds payload sample payload
http://localhost:8080/rule?token=82C36AF3-B220-459B-9AAD-88A81883E970
method =put and pass ruleid and rulecondition id integer value in payload

{
   "token":"82C36AF3-B220-459B-9AAD-88A81883E970",
   "message":{
      "ruleId":"1072",
     "segRuleRiskMultList":[
         {
            "ruleConditionId":"1181",
            "segmentId":"1000",
            "riskLevel":"RR",
            "threshold":"100",
            "thresholdLabel":"minimum amount",
            "multiplier":"1.4"
         },
         {
            "ruleConditionId":"1181",
            "segmentId":"1000",
            "riskLevel":"HR",
            "threshold":"100",
            "thresholdLabel":"minimum amount",
            "multiplier":"1.2"
         },
         {
            "ruleConditionId":"1181",
            "segmentId":"1000",
            "riskLevel":"MR",
            "threshold":"100",
            "thresholdLabel":"minimum amount",
            "multiplier":"1.0"
         },
         {
            "ruleConditionId":"1181",
            "segmentId":"1000",
            "riskLevel":"LR",
            "threshold":"100",
            "thresholdLabel":"minimum amount",
            "multiplier":"0.8"
         },
         {
            "ruleConditionId":"1182",
            "segmentId":"1000",
            "riskLevel":"RR",
            "threshold":"10",
            "thresholdLabel":"minimum amount",
            "multiplier":"1.4"
         },
         {
            "ruleConditionId":"1182",
            "segmentId":"1000",
            "riskLevel":"HR",
            "threshold":"10",
             "thresholdLabel":"minimum amount",
            "multiplier":"1.2"
         },
         {
            "ruleConditionId":"1182",
            "segmentId":"1000",
            "riskLevel":"MR",
            "threshold":"10",
            "thresholdLabel":"minimum amount",
            "multiplier":"1.0"
         },
         {
            "ruleConditionId":"1182",
            "segmentId":"1000",
            "riskLevel":"LR",
            "threshold":"10",
            "thresholdLabel":"minimum amount",
            "multiplier":"0.8"
         },
         {
            "ruleConditionId":"1183",
            "segmentId":"1000",
            "riskLevel":"RR",
            "threshold":"25", 
            "thresholdLabel":"minimum amount",
            "multiplier":"1.4"
         },
         {
            "ruleConditionId":"1183",
            "segmentId":"1000",
            "riskLevel":"HR",
            "threshold":"25",
            "thresholdLabel":"minimum amount",
            "multiplier":"1.2"
         },
         {
            "ruleConditionId":"1183",
            "segmentId":"1000",
            "riskLevel":"MR",
            "threshold":"25",
            "thresholdLabel":"minimum amount",
            "multiplier":"1.0"
         },
         {
            "ruleConditionId":"1183",
            "segmentId":"1000",
            "riskLevel":"LR",
            "threshold":"25",
            "thresholdLabel":"minimum amount",
            "multiplier":"0.8"
         },
         		{
            "ruleConditionId":"1181",
            "segmentId":"1001",
            "riskLevel":"RR",
            "threshold":"100",
            "thresholdLabel":"minimum amount",
            "multiplier":"1.4"
         },
         {
            "ruleConditionId":"1181",
            "segmentId":"1001",
            "riskLevel":"HR",
            "threshold":"100",
            "multiplier":"1.2"
         },
         {
            "ruleConditionId":"1181",
            "segmentId":"1001",
            "riskLevel":"MR",
            "threshold":"100",
            "thresholdLabel":"minimum amount",
            "multiplier":"1.0"
         },
         {
            "ruleConditionId":"1181",
            "segmentId":"1001",
            "riskLevel":"LR",
            "threshold":"100",
            "thresholdLabel":"minimum amount",
            "multiplier":"0.8"
         },
         {
            "ruleConditionId":"1182",
            "segmentId":"1001",
            "riskLevel":"RR",
            "threshold":"10",
            "thresholdLabel":"minimum amount",
            "multiplier":"1.4"
         },
         {
            "ruleConditionId":"1182",
            "segmentId":"1001",
            "riskLevel":"HR",
            "threshold":"10",
            "thresholdLabel":"minimum amount",
            "multiplier":"1.2"
         },
         {
            "ruleConditionId":"1182",
            "segmentId":"1001",
            "riskLevel":"MR",
            "threshold":"10",
            "thresholdLabel":"minimum amount",
            "multiplier":"1.0"
         },
         {
            "ruleConditionId":"1182",
            "segmentId":"1001",
            "riskLevel":"LR",
            "threshold":"10",
            "thresholdLabel":"minimum amount",
            "multiplier":"0.8"
         },
         {
            "ruleConditionId":"1183",
            "segmentId":"1001",
            "riskLevel":"RR",
            "threshold":"25",
            "thresholdLabel":"minimum amount",
            "multiplier":"1.4"
         },
         {
            "ruleConditionId":"1183",
            "segmentId":"1001",
            "riskLevel":"HR",
            "threshold":"25",
            "thresholdLabel":"minimum amount",
            "multiplier":"1.2"
         },
         {
            "ruleConditionId":"1183",
            "segmentId":"1001",
            "riskLevel":"MR",
            "threshold":"25",
            "thresholdLabel":"minimum amount",
            "multiplier":"1.0"
         },
         {
            "ruleConditionId":"1183",
            "segmentId":"1001",
            "riskLevel":"LR",
            "threshold":"25",
            "thresholdLabel":"minimum amount",
            "multiplier":"0.8"
         }
      
      ]
   }
}


=======================================================

==================================================================


Register a User to client
End Point: http://localhost:8080/register
Payload:
 
{
"firstName": "satya",
"lastName": "agarwal",
"email": "satyanarayana.agarwal@ey.com",
"password":"password",
"clientId": "10"
}

Login
End Point: http://localhost:8080/
payload
{
"email": "satyanarayana.agarwal@ey.com",
"password":"password"
}

=======================================================

To get all picklist values

http://localhost:8080/picklist?token=82C36AF3-B220-459B-9AAD-88A81883E970

================================

To get all segment values accross focal entities

http://localhost:8080/segment?token=82C36AF3-B220-459B-9AAD-88A81883E970

===========================================

To get all properties and post

http://localhost:8080/property?token=82C36AF3-B220-459B-9AAD-88A81883E970

{
   "token":"82C36AF3-B220-459B-9AAD-88A81883E970",
   "message":{
   	"pageName": "Landing Page",
   	"label": "title",
   	"value": "AML"
   }
}